#160910#########################################################
Returning after a while... where are we?
----------------------------------------------------------------

CMake 3.3.0 on /Users/brice/MacDev/ws/git/BankReader/BankReader/build-XCode

When building, I get a weird error:
make[1]: *** No rule to make target `/Users/brice/MacDev/Tools/CMake/cmake-3.3.0-Darwin-universal/CMake.app/Contents/share/cmake-3.3/Modules/CMakeCInformation.cmake', needed by `/Users/brice/MacDev/ws/git/BankReader/BankReader/build-XCode/CMakeFiles/cmake.check_cache'.  Stop.

-> get new CMake 3.6.2
Try again... weird error gone
----------------------------------------------------------------

Get boost 1.61.0

----------------------------------------------------------------

I have a main CMake in src with a recurse down.
It hits tools/ each cmd-line tool 

#160414#########################################################
I'm thinking that I should move away from the OSX nib approach.
I thought the nib stuff might not build on XCode 7.3
It does open the nob files OK.
#160303#########################################################
Cross platform icon generation can be handled by ImageMagik's convert

sample CMake code under windows

		set(ico_output ${CMAKE_CURRENT_BINARY_DIR}/${Target}.ico)
		set(ico_input ${CMAKE_CURRENT_SOURCE_DIR}/${Target}.png)
		add_custom_command(
			OUTPUT ${ico_output}
		  	COMMAND ${DevBin}/convert ${ico_input} -define icon:auto-resize=256,64,48,32,16 ${ico_output}
			DEPENDS ${ico_input}
		  	COMMENT "Generating ${Target}.ico file."
		)

-160303---------------------------------------------------------
Looked at the imakeDataObj approach:
The idea is to store resources (like small images) in a platform independent way.
imakeDataObj's approach is to:
-generate a .c file with a static array filled with a magic pattern
-compile it into a .o file
-patch that .o file with the actual data.

It seems like a simpler approach could be to have the data directly into the c file.
However that requires encoding that I'm not too happy to deal with.

I made a new, tighter version of imakeDataObj (in tools/)
I built it using Eclipse CDT

works like this (see main.cpp of BankIconProcessor):
	imakeDataObj -c imakeDataObj gnu >> t.c
	gcc -arch x86_64 -c -x c++ -O0 t.c -o t.o
	imakeDataObj -p imakeDataObj t.o

-160303---------------------------------------------------------
Created a git repo at BankeReader/src instead of BankReader
BankReader-src is here to grab stuff from - it is not to be build

The tool needed to build are:
imakeDataObj

----------------------------------------------------------------
BankReader is complex, build-wise. I could not convert it yet.
The only buildable version is still under p4 at
	/Users/brice/MacDev/p4ws/BankReader
----------------------------------------------------------------
BankReader-src is just a source to work on creating a CMake version in src.
It is incomplete 
----------------------------------------------------------------
Too much work to move to CMake
I'll move it to git first as it is easier and more important
----------------------------------------------------------------
I am moving BankReader to CMake/git

The move to CMake is started but it takes a lot of work because the build is complex:
-Make the tools
-Use the tools to process the pictures and icons
-Make the plugins
-Make the bundle