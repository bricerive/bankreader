How to release a new version:

-Update version string in Bankreader's plist
in info.plist: udpate CFBundleVersion

-Build release mode
-When there is a dylib version of the boost lib, it seems to take over the static one even if the static one is explicitly linked.

-Check dependencies:
otool -L BankReader.app/Contents/MacOS/BankReader

-Create realease file:
tar cvfz BankReader_1.0.7a.tgz BankReader.app
 (was: -Create dmg)

-Update bankreader.xml on web site so the updates work

-Update archive version number in index.html so we download the right one
