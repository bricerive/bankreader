
1 - Description du projet

● origine du projet
Experience personnelle de l'utilisation en ligne des banques.
Le côté impratique du browser comme interface
La perte de temps due à l'utilisation d'une application Web par banque pour les utilisateurs multi-banque
Le fait qu'une grande partie de l'utilisation est vouée à la consultation
Les applications de banque en ligne sont mono-banque et mono-client
Les application bancaires sont en Pull pas en Push.

● description du produit, service ou procédé
Une application qui automatise et unifie l'accès à l'état des comptes dans plusieurs banques et pour plusieurs clients
Basée sur le principe de Web Scripting où un programme simule les actions que l'utilisateur effectue avec son browser

● caractère innovant de la technologie
Utilisateur Web virtuel dédié aux site bancaires

● liberté d’exploitation, éventuels risques de contrefaçon

● aspects réglementaires
-Les banques elles-mêmes peuvent-elles s'y opposer?
-Ont-elles interet à le faire vis à vis de la pression concurrentielle

● risques

-concurrentiels:
--concurrence des banques: elles sont bloquées par le côté mono-banque
--paralleles: être le premier sur le marché pour bénéficier de l'inertie de changement d'outil

-légaux

-technologiques

-Sécurité


2 - Marché visé

● déscription du marché

-Haut débit -> connection permanente
-utilisateurs par tranche de richesse
-Mac/PC
-France/reste du monde
-utilisateurs individuels/institutionnels
-utilisateurs mono/multi banque

-Marché initial test: Marché Français des utilisateurs Mac sur le dix premières banques
La France: en retard mais avec une bonne courbe de progression
accès personnel aux banques françaises
Mac: utilisateurs plus proches du monde payant (PC~chine)
-Marché réel
-Marché potentiel


● avantage concurrentiel
Marché vierge sur le secteur visé.

● informations sur la concurrence
Pas de concurrence connue.


3 - Projet d’entreprise

● équipe : fonctions et rôles respectifs du candidat et de ses partenaires dans la société
0-Seul maître à bord
1-Une équipe de surveillance et de mise à jour des sites bancaires cible (web scripting, C++)
1bis-une équipe en plus pour/dans chaque pays cible
2-Une équipe marketing pour innover sur les business-models 
3-Une équipe de développement de la technologie (traitement d'image, intelligence artificielle)

● expérience professionnelle du candidat
25 ans d'experience en developpement de technologies innovantes dans plusieurs start-ups en France et aux US.
Expertise technologique en génie logiciel, traitement d'image, intelligence artificielle, architectures objet, internet.

● moyens techniques : besoins en locaux, en matériel...
besoins de locaux informatisés et eventuellement de web hosting pour le serveur de mise à jour.

● moyens financiers : besoins financiers et financements envisagés (apport personnel, emprunts, fonds de capital d’amorçage, aides publiques, etc.)
Apport personnel pour toutes les dépenses de démarrage et la R&D initiale.
??? pour la constitution de l'équipe

● statut envisagé (SARL, SA, SAS…)
à définir


4 - Moyens nécessaires à la maturation du projet

● études à réaliser (technologique, d’organisation commerciale, financière, juridique…)
Etude de marché sur le marché test par diffusion gratuite du logiciel

● formation personnelle à apporter au candidat
Formations de gestion d'une entreprise Française
Information sur les meilleurs moyens financiers et fiscaux

● partenariats
Possibilité de business model incluant un partenariat avec les banques

● planning des dépenses prévisionnelles

