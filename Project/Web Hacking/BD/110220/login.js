function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

function delv6cook() {
	eraseCookie('vgnvisitor');
	eraseCookie('ssuid');
	eraseCookie('__utma');
	eraseCookie('__utmb');
	eraseCookie('__utmc');
	eraseCookie('__utmz');
}

function resizelogin() {
	$("input#login").attr("size", 7);
	$("input#codepasse").attr("size", 5);
}
 
jQuery(document).ready(function($){
    delv6cook();
	resizelogin();
});
