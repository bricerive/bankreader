/*function insertion_bloc_bas(){

    var bloc_bas = document.getElementById("menu_dynamique_bloc_bas_traduit");
    var menu = document.getElementById("menu_dynamique_left_navigation");
    
    if(bloc_bas && menu){
      menu.innerHTML = bloc_bas.innerHTML;
    }
}*/

var JMenu = function(menu,options)
{
	// Default Values
	var _position = 'bottom' ;
	var _effect = '' ;
	var _vitesse = '' ;
	var _className = 'onglet';
	var _btns = null ;
	var _queueLimit = 2 ;
	var _currentElement = null ;

	// Variable de chargement
	var _id = '#'+menu ;
	var _menu = $(_id);
	var _options = options ;

	// Si le bloc menu n'existe pas on stop 
	if(_menu == undefined){return null ;}
		
	// Methode de mon objet
	var _init = function()
	{	
		_loadOptions();
		_loadBtns();
	};
	
	var _isDefined = function(elt)
	{
		return !(elt == null || elt == undefined || elt == '') ;
	};
	
	var _loadOptions = function()
	{		
		if(_isDefined(_options))
		{
			if(_isDefined(options.position)){_position = _options.position;}
			if(_isDefined(options.effect)){_effect = _options.effect;}			
			if(_isDefined(options.vitesse)){_vitesse = _options.vitesse;}			
			if(_isDefined(options.className)){_className = _options.className;}			
			if(_isDefined(options.queueLimit)){_queueLimit = _options.queueLimit;}			
		}
	};
	
	var _loadBtns = function(){
		
		_btns = $(_id + ' li.'+_className) ;		
		
		_btns.each(function(){
			$(this).css('position','relative');		
		});
	
		_btns.mouseenter(function(){
			var parent = $(this) ;
			var elt = parent.children('div:first');
				_makePosition(elt,parent);
				_show(elt);	
				_currentElement = elt ;				



		}).mouseleave(function(e){
			var elt = $(this).children('div:first');
				_hide(elt);		
			
		});	
	};
	
	var _show = function(elt)
	{	
			if(_currentElement != null)
			{
				_currentElement.hide();
			}
		if(_executeFX(elt))
		{		
			setTimeout(function(){				
				switch(_effect)
				{
					case 'slide' : elt.slideDown(_vitesse); break;		
					default : elt.show(_vitesse); break;			
				}
			},400);
		}			
	};
	
	var _hide = function(elt)
	{
		if(_executeFX(elt))
		{
			setTimeout(function()
			{				
				switch(_effect)		
				{
					case 'slide' : elt.slideUp(_vitesse); break;		
					default : elt.hide(_vitesse); break;			
				}		
			},400);
		}
	};
	
	var _executeFX = function(elt)
	{
		return (elt.queue("fx").length < _queueLimit) ? true : false ;	
	};	
	
	var _makePosition = function(elt,parent)
	{
		if(!elt.hasClass('positioned'))
		{
			elt.css('position','absolute');
			elt.css('z-index','5000');	
			elt.addClass('positioned');
			
			switch(_position)
			{
				case 'right' :
					elt.css('left',parent.outerWidth()) ;
					elt.css('top','0px');
				break;
				
				default :
					elt.css('left','0px') ;
					elt.css('top',parent.outerHeight());	
				break; 		
			}
		}
	};
	
	_init();	
};

function insertion_bloc_bas(){
	
	$("#menu_dynamique_left_navigation").css("display", "block");
	var menu_content = $("#menu_dynamique_bloc_bas_traduit").html();
	$("#menu_dynamique_left_navigation").html(menu_content);

	var menu = new JMenu('menu_list_bloc',{'position':'right','queueLimit':'1'});
	var sous_menu = new JMenu('services_liste',{'className':'services_onglet'});	
	
}



/**
 * --------------------------------------------------------------------
 * jQuery-Plugin "pngFix"
 * Version: 1.2, 09.03.2009
 * by Andreas Eberhard, andreas.eberhard@gmail.com
 *                      http://jquery.andreaseberhard.de/
 *
 * Copyright (c) 2007 Andreas Eberhard
 * Licensed under GPL (http://www.opensource.org/licenses/gpl-license.php)
 *
 * Changelog:
 *    09.03.2009 Version 1.2
 *    - Update for jQuery 1.3.x, removed @ from selectors
 *    11.09.2007 Version 1.1
 *    - removed noConflict
 *    - added png-support for input type=image
 *    - 01.08.2007 CSS background-image support extension added by Scott Jehl, scott@filamentgroup.com, http://www.filamentgroup.com
 *    31.05.2007 initial Version 1.0
 * --------------------------------------------------------------------
 * @example $(function(){$(document).pngFix();});
 * @desc Fixes all PNG's in the document on document.ready
 *
 * jQuery(function(){jQuery(document).pngFix();});
 * @desc Fixes all PNG's in the document on document.ready when using noConflict
 *
 * @example $(function(){$('div.examples').pngFix();});
 * @desc Fixes all PNG's within div with class examples
 *
 * @example $(function(){$('div.examples').pngFix( { blankgif:'ext.gif' } );});
 * @desc Fixes all PNG's within div with class examples, provides blank gif for input with png
 * --------------------------------------------------------------------
 */

(function($) {

jQuery.fn.pngFix = function(settings) {

	// Settings
	settings = jQuery.extend({
		blankgif: 'blank.gif'
	}, settings);

	var ie55 = (navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion) == 4 && navigator.appVersion.indexOf("MSIE 5.5") != -1);
	var ie6 = (navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion) == 4 && navigator.appVersion.indexOf("MSIE 6.0") != -1);

	if (jQuery.browser.msie && (ie55 || ie6)) {

		//fix images with png-source
		jQuery(this).find("img[src$=.png]").each(function() {

			jQuery(this).attr('width',jQuery(this).width());
			jQuery(this).attr('height',jQuery(this).height());

			var prevStyle = '';
			var strNewHTML = '';
			var imgId = (jQuery(this).attr('id')) ? 'id="' + jQuery(this).attr('id') + '" ' : '';
			var imgClass = (jQuery(this).attr('class')) ? 'class="' + jQuery(this).attr('class') + '" ' : '';
			var imgTitle = (jQuery(this).attr('title')) ? 'title="' + jQuery(this).attr('title') + '" ' : '';
			var imgAlt = (jQuery(this).attr('alt')) ? 'alt="' + jQuery(this).attr('alt') + '" ' : '';
			var imgAlign = (jQuery(this).attr('align')) ? 'float:' + jQuery(this).attr('align') + ';' : '';
			var imgHand = (jQuery(this).parent().attr('href')) ? 'cursor:hand;' : '';
			if (this.style.border) {
				prevStyle += 'border:'+this.style.border+';';
				this.style.border = '';
			}
			if (this.style.padding) {
				prevStyle += 'padding:'+this.style.padding+';';
				this.style.padding = '';
			}
			if (this.style.margin) {
				prevStyle += 'margin:'+this.style.margin+';';
				this.style.margin = '';
			}
			var imgStyle = (this.style.cssText);

			strNewHTML += '<span '+imgId+imgClass+imgTitle+imgAlt;
			strNewHTML += 'style="position:relative;white-space:pre-line;display:inline-block;background:transparent;'+imgAlign+imgHand;
			strNewHTML += 'width:' + jQuery(this).width() + 'px;' + 'height:' + jQuery(this).height() + 'px;';
			strNewHTML += 'filter:progid:DXImageTransform.Microsoft.AlphaImageLoader' + '(src=\'' + jQuery(this).attr('src') + '\', sizingMethod=\'scale\');';
			strNewHTML += imgStyle+'"></span>';
			if (prevStyle != ''){
				strNewHTML = '<span style="position:relative;display:inline-block;'+prevStyle+imgHand+'width:' + jQuery(this).width() + 'px;' + 'height:' + jQuery(this).height() + 'px;'+'">' + strNewHTML + '</span>';
			}

			jQuery(this).hide();
			jQuery(this).after(strNewHTML);

		});

		// fix css background pngs
		jQuery(this).find("*").each(function(){
			var bgIMG = jQuery(this).css('background-image');
			if(bgIMG.indexOf(".png")!=-1){
				var iebg = bgIMG.split('url("')[1].split('")')[0];
				jQuery(this).css('background-image', 'none');
				jQuery(this).get(0).runtimeStyle.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + iebg + "',sizingMethod='scale')";
			}
		});
		
		//fix input with png-source
		jQuery(this).find("input[src$=.png]").each(function() {
			var bgIMG = jQuery(this).attr('src');
			jQuery(this).get(0).runtimeStyle.filter = 'progid:DXImageTransform.Microsoft.AlphaImageLoader' + '(src=\'' + bgIMG + '\', sizingMethod=\'scale\');';
   		jQuery(this).attr('src', settings.blankgif)
		});
	
	}
	
	return jQuery;

};

})(jQuery);
