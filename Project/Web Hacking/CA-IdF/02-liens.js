
			/* Definition des attributs et des methodes de l objet StringDelimiter */

			function StringDelimiter(chaine, delimiteur) {
				this.tokens = chaine.split(delimiteur);
				this.cursor = -1;
				this.nextToken = nextToken;
				this.hasMoreElements = hasMoreElements;
			}

			function nextToken() {
				var nt = "";
				if (this.hasMoreElements()) {
					this.cursor = this.cursor + 1;
					nt = this.tokens[this.cursor];
				}
				return nt;
			}

			function hasMoreElements() {
				return (this.cursor + 1 < this.tokens.length);
			}

			/* Definition de la fonction de recuperation de l id groupe et du boot strap node */

			function getGroupId() {
				var sd = new StringDelimiter(document.URL, "/");
				while ((sd.hasMoreElements()) && (sd.nextToken() != window.location.hostname)) {
				}
				return sd.nextToken();
			}

			/* Definition de la fonction de recuperation de la source de l image pour tracking de la page */

			function getTrackingTag(trackingCode) {
				document.writeln("<img src=\"https://" + window.location.hostname + "/" + getGroupId() + "/ssl/vitrine/tracking.act?libelle=" + trackingCode + "\">");
				return;
			}
			
			function getAbsUrl(str)
			{	
				var sUrl = document.URL;
				var secur = "ssl"
				if(sUrl.substr(0,8).indexOf("https://")<0) {	
					secur = "nonssl";
				}
				return "https://"+ location.hostname + "/" + getGroupId() + "/" + secur + "/vitrine/" + str;
			}
			
			function getParamjs()
			{	
				return "https://" + location.hostname + "/" + getGroupId() + "/" + "ssl/vitrine/javascript/msgparm.js";
			}
