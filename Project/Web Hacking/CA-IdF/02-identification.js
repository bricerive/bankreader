// gestion des navigateurs
var userAgent=navigator.appName + " " + navigator.appVersion;
var agentInfo=userAgent.substring(0, 12);
var typeNS = (agentInfo >= "Netscape 4.0");
var typeIE = (agentInfo.substring(0,9)  == "Microsoft");
// boolean pour la gestion des fenetres sous netscape
// lorsqu'un alerte est ouvert, il y a des pb avec la touche enter
var afficheAlert=false;
var typeIdent;

// si le navigateur est netscape
function evenementNE(evenement) { 
  var retval = true;
  if (evenement.which == 13) {
  if (!afficheAlert) 
     if (verif()) document.form_ident.submit(); 
	 afficheAlert=!afficheAlert
  } else {
    if (evenement.which < 30) {		   
	 retval = document.form_ident.handleEvent(evenement);
   	 } else retval = routeEvent(evenement);
   return (retval);
  }
}
 
if (typeNS) {
    // declaration de la gestion des evenements sous NS
    document.captureEvents(Event.KEYUP);
    document.onkeyup = evenementNE;
}




// fonction de validation du form
function verif() {

  var nameForm = document.form_ident;

  (nameForm.caseCookie.checked)? nameForm.cookie.value="on":nameForm.cookie.value="off";
  if (isNaN(nameForm.numero.value) || String(nameForm.numero.value).length != 11) {
	alert("Vous devez saisir votre num�ro de compte ou de contrat compos� de 11 chiffres.");
	nameForm.numero.focus();
	return false;
  } else 
  if (isNaN(nameForm.code.value) || String(nameForm.code.value).length != 6) {
	alert("Votre code personnel est compos� de 6 chiffres. Si vous ne le connaissez pas, veuillez contacter votre agence.");
	if (typeIdent == 'clavier')
		nameForm.code_clavier.focus();
	return false;
  } else return true;
}

/**
 * Permet de lancer les traitements avant de valider le formulaire
 */
function validForm() {
	// Recopie du code
	if (typeIdent == "clavier") {
		document.form_ident.code.value = document.form_ident.code_clavier.value;
	}
	else if (typeIdent == "souris") {
		document.form_ident.code.value = document.form_ident.code_souris.value;
	}
	return verif();
}

// gestion des liens 	vers vitrine
function ecrireLien(strLien) {
  var serveur = window.location.hostname;
  document.write("<a href='http://"+serveur+strLien+"' target='_top'>");
}

// gestion des liens vers l'identification
function ecrireLienIdentification(strLien) {
  var serveur = window.location.hostname;
  document.write("<a href='https://"+serveur+strLien+"' target='_top'>");
}

// recuperation du cookie
function getCookie(name) {
	var value = getValueCookie(name);
	return (value.length < 11)? "":value;
}

// recuperation d'un cookie
function getValueCookie(name) {
   var search = name + "="
   if (document.cookie.length > 0) {
      var offset = document.cookie.indexOf(search)
	  if (offset != -1) {
	    offset += search.length
		end = document.cookie.indexOf(";", offset)
		if (end == -1)
		  end = document.cookie.length
		var value = unescape(document.cookie.substring(offset, end));
		return value;	  }
   }
   return "";
}

// function d'init de champs par rappport au cookie
function init() {

  var nameForm = document.form_ident;
  
  nameForm.numero.value = getCookie("numero_contrat");
  nameForm.code.value = "";

  if (nameForm.numero.value.length>0) { 
	nameForm.caseCookie.checked="true";
	if (typeIdent == 'clavier')
		nameForm.code_clavier.focus();
  }
  else nameForm.numero.focus();
}


/**
 * Affiche ou masque un �l�ment
 * @param id L'�l�ment � traiter
 * @param affiche Sp�cifie si on doit afficher ou masquer la zone. true si afficher et false si masquer
 */
function toggle(id, affiche) {

    var element = document.getElementById(id);
    with (element.style) {
        if (affiche) {
            display = "";
        } else {
            display = "none";
        }
    }
}



/* Retourne un nombre entier all�atoire entre 0 et max
 * @param max La borne maximale (incluse) pour les nombres renvoy�s 
 * @return Un entier g�n�r� all�atoirement
 */
function random(max) { return Math.floor(Math.random()*(max+1)); }


/* Permet de g�n�rer le tableau avec les chiffres de 0 � 9 plac�s dans un ordre all�atoire
 * (bas� sur des permutations)
 */
function genereTableau()
	{
	var tab = new Array(0,1,2,3,4,5,6,7,8,9);
	var indice1, indice2 , val;
	for (i=0 ; i<50 ; ++i) // On effectue des permutations all�atoires sur le tableau
		{
		indice1 = random(9);
		indice2 = random(9);
		val = tab[indice1];
		tab[indice1] = tab[indice2];
		tab[indice2] = val;
		}
	return tab;
	}

/**
 * Affichage du tableau de saisie du code � la souris
 */
function afficheTableau()
	{
	var tab = genereTableau();
	document.write('<table id="tabcode"><tr>');
	for (i=0 ; i<10 ; i=i+2)
		{ document.write('<td><input type="button" class="form_buttoncode" value="'+tab[i]+'" title="Le chiffre ' + tab[i] + ' dans votre code secret" onclick="javascript:saisieChiffre('+tab[i]+')"></td>'); }
	document.write("</tr><tr>");
	for (i=1 ; i<10 ; i=i+2)
		{ document.write('<td><input type="button" class="form_buttoncode" value="'+tab[i]+'" title="Le chiffre ' + tab[i] + ' dans votre code secret" onclick="javascript:saisieChiffre('+tab[i]+')"></td>'); }
	document.write('</tr><tr>');
	document.write('<td colspan="3"><input type="password" readonly="readonly" size="6" maxlength="6" id="code_souris" name="code_souris" class="code" tabindex="-1" ></td>');
	document.write('<td colspan="2"><input type="button" id="form_buttoneffacer" class="form_buttoncode" value="effacer" onclick="javascript:effacer();" title="Effacer le code" ></td>');
	document.write('</tr></table>');
}

/**
 * Saisie d'un chiffre du code � la souris
 * @param val La valeur saisie � la souris
 */
function saisieChiffre(val) {
	if (document.form_ident.code_souris.value.length == 6) {
		return;
	}
	document.form_ident.code_souris.value = document.form_ident.code_souris.value + val;
}

/**
 * Permet d'effacer la zonne de saisie du code � la souris
 */
function effacer() {
	document.form_ident.code_souris.value = "";
}
