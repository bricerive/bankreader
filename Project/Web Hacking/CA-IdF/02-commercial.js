// ******************************************
// FONCTIONS "NOUVELLE FENETRE DE NAVIGATEUR"
// ******************************************

// old 1
function openw(urlpage,lienssl,largeur,hauteur,interne)
{
 if (interne)
   if (lienssl)
     urlfinal='https://'+window.location.hostname+'/'+getGroupId()+urlpage
   else urlfinal='http://'+window.location.hostname+'/'+getGroupId()+urlpage
 else urlfinal=urlpage;
 window.open(urlfinal,'helpme',"toolbar=yes,location=no,directories=no,status=no,resizable=no,width="+largeur+",height="+hauteur+",noresize");
}

// old 2
function zzopenWinPubOld(lien,titre,parametres, largeur, hauteur) {
	var proprietes = parametres + ",width=" + largeur + ",height=" + hauteur;
	if(zzwinPub != null && !zzwinPub.closed) {
		zzwinPub.focus();
	} else {
		zzwinPub = window.open(lien,titre,proprietes)
	}
}

// new good one
var zzwinPub = null;
function zzopenWinPub(lien,titre,parametres) {
	if (parametres == "")
		{ parametres = "toolbar=yes,width=600,height=400" }
	if(zzwinPub != null && !zzwinPub.closed) { zzwinPub.location=lien;zzwinPub.focus(); }
	else { zzwinPub = window.open(lien,titre,parametres) }
}
function zzcloseWinPub() { if (zzwinPub != null) { if(!zzwinPub.closed) { zzwinPub.close(); } } }

/* ******************************************************************************************
*********************************** Messages barre d'�tat ******************************** */

function zzMsg(msg) { top.status = msg; }
function zzMsgOff() { top.status = " "; }
function zzGoToVitrine() { alert("Vous allez �tre redirig� vers une autre zone du site. La consultation de vos comptes reste active et vous pouvez y revenir � tout moment. Lorsque vous d�sirerez quitter d�finitivement le service, nous vous conseillons de cliquer sur le choix \"Fin de consultation\" qui se trouve en permanence sur la partie gauche de l'�cran."); }


/* ******************************************************************************************
*********************************** menu telecommande ************************************ */

var zzuriImgPucePlus_1  = "../caimg/puce_menu_plus_blanc.gif";
var zzuriImgPucePlus_n  = "../caimg/puce_menu_plus_noir.gif";
var zzuriImgPuceMoins_1 = "../caimg/puce_menu_moins_blanc.gif";
var zzuriImgPuceMoins_n = "../caimg/puce_menu_moins_noir.gif";

function developperReduireToutCommercial(reduire){
	var tab;
	var str;
	var dlElement = document.getElementById("menuSynergie");
	if (dlElement) {
		tab = dlElement.childNodes;
		for (i=0; i<tab.length; i++)
			if (tab.item(i).nodeName.toLowerCase() == "div" ) {
				str = tab.item(i).id;
				k = str.indexOf("_",0);
				if (str.substring(0,k) == "divCommercial") {
					var img = "imgCommercial_" + str.substring(k+1,str.length);
					var imgElement = document.getElementById(img);
					if (reduire) {
						tab.item(i).style.display="none";
						imgElement.src = zzuriImgPucePlus_1;
					} else {
						tab.item(i).style.display="block";
						imgElement.src = zzuriImgPuceMoins_1;
					}
				}
			}	
	}
}

function zzdevelopperNiveau(elementId) {
	var div = "divCommercial_" + elementId;
	var img = "imgCommercial_" + elementId;
	var divElement = document.getElementById(div);
	var imgElement = document.getElementById(img);

	if (divElement) {
		var etat = divElement.style.display;
		if (etat == "")
			etat="none";
		developperReduireToutTelecommande(true);
		if (etat == "none") {
			divElement.style.display="block";
			imgElement.src = zzuriImgPuceMoins_1;
		} else {
			divElement.style.display="none";
			imgElement.src = zzuriImgPucePlus_1;
		}
	}
	return;
}

// ****************************************************************************************
// calculatrice euro
// ****************************************************************************************
var calculette = null;
function zzopencalc()
{
calculette=window.open('../commercial/calc_euro/calc.htm','calculatrice_convertisseur',"toolbar=no,location=no,directories=no,status=no,resizable=no,width=294,height=400,noresize,noscrolling");
var x = screen.width - 310;
calculette.moveTo(x,0);
}

// ****************************************************************************************
// popup de l'aide
// ****************************************************************************************
var aide = null;
function zzopenAide(lien,largeur,hauteur) {
	var param = "scrollbars=yes,resizable=yes,width=" + largeur + ",height=" + hauteur;
	if(aide != null && !aide.closed) {
		aide.focus();
	} else {
		aide = open(lien,'aide',param)
	}
	var x = screen.width - (largeur + 10);
	aide.moveTo(x,0);
}