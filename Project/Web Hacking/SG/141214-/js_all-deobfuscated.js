    // 	document.getElementById("print0").innerHTML = gda_docroot_version;

function gda_css_include(e) {
    if (typeof gda_css_includes != "undefined" && gda_css_includes[e] == 1) return;
    if (document.createStyleSheet) document.createStyleSheet(e);
    else {
        var t = $jGda(document.createElement("link")).attr("rel", "stylesheet").attr("type", "text/css").attr("media", "screen").attr("href", e);
        $jGda($jGda("head")[0]).append(t)
    }
    typeof gda_css_includes == "undefined" && (gda_css_includes = new Array), gda_css_includes[e] = 1
}

function gda_auth(formName) {
    formName = typeof formName == "object" ? formName.id : formName;
 	gdavk.auth(formName)
}

function gda_auth_close() {
    gdavk.close()
}

function showFAQ(e, t) {
    window.open(e, t, "width=780,height=540,top=0,left=0,toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,alwaysRaised=yes,resizable=yes")
}

// this, null, null, 0, "codsec", options
function VirtualKeyboard(options) {
	document.getElementById("print1").innerHTML = document.getElementById("print1").innerHTML + " VirtualKeyboard";

    if (!$jGda("#codsec")[0]) {
        alert("Erreur: le champ de code secret d'identifiant 'codsec' n'existe pas dans la page!");
        return
    }

 	this.inSession = 0,
	this.codeSz = 6,
	this.opacity = .56,
	this.switch_button_left = 102,
	this.switch_button_top = 0,
	this.urlBase = "",
	this.urlStaticBase = "",
	this.offx = 0,
	this.offy = 0,
	this.incompleteCodeMsg = "Le Code Secret saisi est incorrect.\r\nMerci de bien vouloir ressaisir votre Code Secret " + unescape("compos%E9") + " de " + this.codeSz + " chiffres.",
	this.showHelpButton = !0,
	this.faqURI = "javascript:showFAQ('" + this.urlBase + "/codeSecret.html', 'CodeSecret')",
	this.faqNVURI = "javascript:showFAQ('" + this.urlBase + "/html/faq/faq43.html', 'CodeSecret')",
	this.overwriteImages = {},
	this.useDisableLayer = !1,
	this.disableLayerOpacity = .05;

	options != undefined && $jGda.extend(this, options)

 	this.waitingForJSONP = !1,
 	this.numKeys = 0,
 	this.state = "",
 	this.currentKeys = "",
 	this.urlError = this.urlBase + "/pageErreur.html",
 	this.urlKeyboard = this.urlBase + "/sec/vkm/gen_crypto?estSession=",
 	this.urlImage = this.urlBase + "/sec/vkm/gen_ui?modeClavier=",
 	this.urlCSS = this.urlStaticBase + "/static/styles/vk/vk.css",
 	this.urlParamCrypto = "&cryptogramme=",
 	this.urlParamError = "cvcsCodeErreur",
 	dragObj.elNode = $jGda("#tc_divdeplace"),
 	$jGda("#img_tc_haut").bind("mousedown", function(e) {
        dragStart.call(e.currentTarget, e)
    }),
 	$jGda("#img_tc_haut").bind("mouseup", function(e) {
        dragStop.call(e.currentTarget, e)
    }),
 	this.images = {},
 	this.images.img_tc_haut = "/static/img/vk/tc_haut_sans.gif",
 	this.images.tc_fond_img = "/static/img/vk/tc_fond.gif",
 	this.images.legendeInput = "/static/img/vk/tc_votre_code.gif",
 	this.images.tc_aideimg = "/static/img/vk/bouton_question.gif",
 	this.images.tc_corriger = "/static/img/vk/tc_corriger.gif",
 	this.images.tc_valider = "/static/img/vk/tc_valider.gif",
 	this.images.surlignage = "/static/img/vk/tc_touche_cache_hover.gif",
 	this.images.trans = "/static/img/vk/trans.gif",
 	update_object(this.images, this.overwriteImages);
    var o = this.urlBase,
        u = this.urlStaticBase,
        a = this.images;
    $jGda("#tc_cvcs").find("img").each(function(e, t) {
        a[$jGda(t).attr("id")] && $jGda(t).attr("src", u + a[$jGda(t).attr("id")])
    }),
 	$jGda("#tc_aidelien").attr("href", this.faqURI);
    this.showHelpButton && $jGda("#tc_aidelien").toggle(), $jGda("#tc_fond_img").css({
        opacity: this.opacity
    }),
 	$jGda("#vk_layer").css({
        opacity: this.disableLayerOpacity
    }),
 	this.getWidth = function() {
        return $jGda("#tc_fond_img").width()
    }

 	this.getHeight = function() {
        return $jGda("#img_tc_haut").height() + $jGda("#tc_fond_img").height()
    }

 	this.onKeyClicked = function(e) {
		$jGda(e.currentTarget).attr("src", this.urlStaticBase + "/static/img/vk/tc_touche_cache_click_1s2.gif")
		this.clickKeyX = numCurX, this.clickKeyY = numCurY
    }

 	this.onKeyUnclicked = function(e) {
		$jGda(e.currentTarget).attr("src", this.urlStaticBase + "/static/img/vk/tc_touche_cache_hover.gif")
	 	this.clickKeyX == numCurX && this.clickKeyY == numCurY && this.updateValue(parseInt((numCurY - 1) * this.nbCols + parseInt(numCurX)) - 1)
    }

 	this.show = function() {
        $jGda("#shapeHautFermer").unbind("click").bind("click", { thisObj: this }, function(e) { e.data.thisObj.close() }),
 		$jGda("#tc_corriger").unbind("click").bind("click", { thisObj: this }, function(e) { e.data.thisObj.EraseKeys() }),
 		$jGda("#tc_valider").unbind("click").bind("click", { thisObj: this }, function(e) { e.data.thisObj.validate(e) }),
 		$jGda("#surlignage").unbind("mousedown").bind("mousedown", { thisObj: this }, function(e) { e.data.thisObj.onKeyClicked(e) }),
 		$jGda("#surlignage").unbind("mouseup").bind("mouseup", { thisObj: this }, function(e) { e.data.thisObj.onKeyUnclicked(e) }),
 		$jGda("#surlignage").unbind("mouseout").bind("mouseout", { thisObj: this }, function(e) { desactiverSurlignage.call(e.data.thisObj, e) }),
 		this.enableVisual()
    }

 	this.enableVisual = function() {
        if (this.crypto != undefined) {
            this.showKeyboard();
            return
        }
        this.generateKeyboardCoords();
        if (this.state == "") {
            var e = this;
            this.SendJSONPRequest(function() {
                e.state = "i",
 				e.generateKeyboard(),
 				e.inSession || (e.timer = (new Date).getTime()),
 				e.showKeyboard()
            })
        }
    }

 	this.showKeyboard = function() {
		this.useDisableLayer && this.vk_layer.fadeIn("300");
		var e = $jGda("#tc_divdeplace");
		typeof N2GHideFlash == "function" && N2GHideFlash(), e.css("left", $jGda(window).scrollLeft() + this.posx + "px"), e.css("top", $jGda(window).scrollTop() + this.posy + "px"), this.state = "v"
    }

 	this.EraseAll = function() {
        this.EraseKeys(), this.state = ""
    }

 	this.EraseKeys = function() {
		$jGda("#tc_visu_saisie").val("")
		$jGda("#cvcs_visu_cache").val("")
		this.numKeys = 0,
		this.currentKeys = ""
    }

 	this.jsonpCallback = function(e, t) {
        if (t.code != 0) return window.location = this.urlError + "?" + this.urlParamError + "=104", !1;
        this.crypto = t.crypto;
 		$jGda("#cryptocvcs").val(this.crypto);
 		this.nbRows = t.nbrows;
 		this.nbCols = t.nbcols;
 		this.adaptKeyboard();

        if (this.crypto == "" || this.nbRows <= 0 || this.nbCols <= 0) return window.location = this.urlError + "?" + this.urlParamError + "=106", !1;
        var n = this.nbRows * this.nbCols,
            r = this.codeSz;
        t.grid = Decoder.decode(t.grid);
 		t.grid = t.grid.match(/([0-9]{3})/g), r += 1;
        if (t.grid.length != r * n) return window.location = this.urlError + "?" + this.urlParamError + "=105", !1;
        this.gridRow0 = t.grid.slice(0, n),
 		t.grid = t.grid.slice(n),
 		this.gridRows = t.grid;
        var s = this.nbRows * this.nbCols,
            o = ["180", "149", "244", "125", "115", "058", "017", "071", "075", "119", "167", "040", "066", "083", "254", "151"
				, "212", "245", "193", "224", "006", "068", "139", "054", "089", "083", "111", "208", "105", "235", "109", "030"
				, "130", "226", "155", "245", "157", "044", "061", "233", "036", "101", "145", "103", "185", "017", "126", "142"
				, "007", "192", "239", "140", "133", "250", "194", "222", "079", "178", "048", "184", "158", "158", "086", "160"
				, "001", "114", "022", "158", "030", "210", "008", "067", "056", "026", "042", "113", "043", "169", "128", "051"
				, "107", "112", "063", "240", "108", "003", "079", "059", "053", "127", "116", "084", "157", "203", "244", "031"
				, "062", "012", "062", "093"],
            u = this.crypto.match(/./g);
        for (j = 0; j < s; j++) u[j] = u[j].toString().charCodeAt().toString(), u[j] = String("000").substr(1, 3 - u[j].length) + u[j];
        for (i = 5; i > 0; i--)
            for (j = 0; j < s; j++) {
	 			this.gridRows[i * s + j] = this.gridRows[i * s + j] ^ this.gridRows[(i - 1) * s + j],
	 			this.gridRows[i * s + j] = this.gridRows[i * s + j].toString(),
	 			this.gridRows[i * s + j] = String("000").substr(1, 3 - this.gridRows[i * s + j].length) + this.gridRows[i * s + j];
			}
        for (j = 0; j < s; j++) this.gridRows[j] = this.gridRows[j] ^ o[j] ^ this.gridRow0[j], this.gridRows[j] = this.gridRows[j].toString(), this.gridRows[j] = String("000").substr(1, 3 - this.gridRows[j].length) + this.gridRows[j];
        for (j = 0; j < s; j++) this.gridRow0[j] = u[j] ^ this.gridRow0[j];
        t.grid = this.gridRows;
		this.gridRows = t.grid;

 		e();
 		this.waitingForJSONP = !1;
    }

 	this.SendJSONPRequest = function(e) {
        if (this.waitingForJSONP) return;
        this.JsonpHandler && this.JsonpHandler.remove(), this.waitingForJSONP = !0;
        var t = this.urlKeyboard;
        this.inSession ? t += "1" : t += "0";
 		_vkCallback = function(e, t, n) {
            return function() {
                e.call(t, n, arguments[0])
            }
        }(this.jsonpCallback, this, e);
        var n = document.createElement("script");
        typeof n != "undefined" && (n.setAttribute("type", "text/javascript"), n.setAttribute("src", t), document.getElementsByTagName("head")[0].appendChild(n), this.JsonpHandler = $jGda(n))
    }

 	this.hide = function() {
        if (this.state != "" || $jGda("#tc_cvcs").is(":visible")) {
            this.useDisableLayer && this.vk_layer.hide();
            var e = $jGda("#tc_divdeplace");
            e.css("left", "-600px"), e.css("top", "-600px"), this.state = ""
        }
        typeof N2GShowFlash == "function" && N2GShowFlash()
    }

 	this.generateKeyboard = function() {
        var e = this.nbRows * this.nbCols,
            t = 0,
            n = $jGda("#tc_tclavier");
        this.kbdmapIdUnique != undefined && (n = $jGda("#" + this.kbdmapIdUnique)),
 		n.find("area").remove(),
 		this.kbdmapIdUnique = "tc_tclavier" + (new Date).getTime(),
 		n.attr("id", this.kbdmapIdUnique).attr("name", this.kbdmapIdUnique),
 		n = $jGda("#" + this.kbdmapIdUnique);
        for (i = 1; i <= this.nbRows; i++)
            for (j = 1; j <= this.nbCols; j++) {
                var r = "touche" + i + j,
                    s = 24;
                var o = (j - 1) * s + "," + (i - 1) * s + "," + j * s + "," + i * s,
                    u = $jGda(document.createElement("area")).attr("id", r).attr("coords", o).attr("style", "cursor: pointer;display:block;");
                n.append(u),
 				this.gridRows[t] != 0 ? (
					u.addClass("touche"),
					u.bind("mouseout", { thisObj: this }, function(e) { desactiverSurlignage.call(e.data.thisObj, e) }),
					u.bind("mouseover", { thisObj: this, jArg: j, iArg: i}, function(e) { activerSurlignage.call(e.data.thisObj, e, e.data.jArg, e.data.iArg) })
				) : (
					u.addClass("toucheVide"),
					 u.bind("mouseover", { thisObj: this }, function(e) { desactiverSurlignage.call(e.data.thisObj, e) })
				),
				t++
            }
        $jGda("#img_clavier").attr("useMap", "#" + this.kbdmapIdUnique),
 		$jGda("#img_clavier").attr("src", this.urlImage + "0" + this.urlParamCrypto + this.crypto)
    }

 	this.close = function() {
        this.hide(), this.EraseAll(), this.crypto = null, this.closefn && this.closefn()
    }

 	this.updateKeyboard = function(e) {
 		$jGda("#tc_fond_img").css({
            opacity: this.opacity
        });
        var t = $jGda(e).find("*"),
            n = t.length;
        for (var r = 0; r < n; ++r) {
            var i = t[r];
            if (i.nodeName.toLowerCase() == "img" && $jGda(i).attr("src")) {
                var s = i.src;
                s != "" && (i.src = s.replace(/_mv(\.\w+)$/, "$1"))
            }
            i.className && i.className != "" && (i.className = i.className.replace(/_mv$/, ""))
        }
    }

 	this.adaptKeyboard = function() {
        var e = "";
        if ($jGda.browser.opera) {
            var t = navigator.appVersion.charAt(0);
            t < 9 && (e = "_1s2")
        }
        var n = this.urlStaticBase + "/static/img/vk/tc_haut_sans" + (this.nbCols > 4 ? "_" + this.nbCols : "") + ".gif",
            r = this.urlStaticBase + "/static/img/vk/tc_fond.gif";
        this.nbCols == 4 && this.nbRows == 4 && (r = this.urlStaticBase + "/static/img/vk/tc_fond" + e + ".gif");
        var i = this.urlStaticBase + "/static/img/vk/tc_haut_sans" + (this.nbCols > 4 ? "_" + this.nbCols : "") + "_mv.gif",
            s = this.urlStaticBase + "/static/img/vk/tc_fond_mv.gif";
        this.nbCols > 4 ?
 			this.nbRows > 4 ?
 				(r = this.urlStaticBase + "/static/img/vk/tc_fond" + e + "_5x5.gif", s = this.urlStaticBase + "/static/img/vk/tc_fond_5x5_mv.gif")
 				: (r = this.urlStaticBase + "/static/img/vk/tc_fond" + e + "_5x4.gif", s = this.urlStaticBase + "/static/img/vk/tc_fond_5x4_mv.gif")
			: this.nbRows > 4 && (r = this.urlStaticBase + "/static/img/vk/tc_fond" + e + "_4x5.gif", s = this.urlStaticBase + "/static/img/vk/tc_fond_4x5_mv.gif"),
		 	(
				$jGda("#img_tc_haut").attr("src", n),
				 $jGda("#tc_fond_img").attr("src", r),
				 $jGda("#shapeHautFermer").attr("coords", this.nbCols > 4 ? "192,6,241,16" : "167,3,230,16"),
				 $jGda("#tc_boutons").css("left", 139 + (this.nbCols - 4) * 24 + "px"),
				 $jGda("#tc_boutons").css("top", 80 + (this.nbRows - 4) * 24 + "px"),
				 $jGda("#tc_haut_switch").size() > 0 && (
					$jGda("#tc_haut_switch").css("left", this.switch_button_left + (this.nbCols - 4) * 25 + "px"),
					 $jGda("#tc_haut_switch").css("top", this.switch_button_top + "px"),
					 $jGda("#tc_haut_switch").attr("alt", "AGRANDIR"),
					 $jGda("#tc_haut_switch").attr("title", "AGRANDIR")
				)
			)
        var o = $jGda("#img_tc_haut").attr("usemap"),
            u = "maphaut" + (new Date).getTime();
        $jGda("#img_tc_haut").attr("usemap", "#" + u), $jGda(o).attr("id", u).attr("name", u)
    }

 	this.generateKeyboardCoords = function() {
        if (this.zones == undefined) {
 			this.posx = genererCoordAleatoire(this.offx, this.getWidth(), $jGda(window).width());
 			this.posy = genererCoordAleatoire(this.offy, this.getHeight(), $jGda(window).height());
        } else {
            var e = Math.floor(Math.random() * this.zones.length),
                t = this.zones[e];
            if (t.width == undefined || t.width > $jGda(window).width()) t.width = $jGda(window).width();
            if (t.height == undefined || t.height > $jGda(window).height()) t.height = $jGda(window).height();
            this.posx = genererCoordAleatoire(t.x, this.getWidth(), t.width), this.posy = genererCoordAleatoire(t.y, this.getHeight(), t.height)
        }
    }

 	this.updateValue = function(e) {
        var t = $jGda("#tc_visu_saisie"),
            n = $jGda("#cvcs_visu_cache"),
            r = this.nbRows * this.nbCols;
        this.numKeys < this.codeSz && (
			t.val(t.val() + "*"),
 			n.val(t.val()),
 			this.currentKeys += this.gridRows[this.numKeys * r + e],
 			this.numKeys < this.codeSz - 1 && (this.currentKeys += ","),
 			this.numKeys++
		)
    }

 	this.validate = function(e) {
        $jGda("#codsec").val(this.currentKeys);
        if (!this.inSession && this.timer != null && ((new Date).getTime() - this.timer) / 1e3 > timeout) {
            alert("Attention, vous avez " + unescape("d%E9pass%E9") + " la " + unescape("dur%E9e") + " maximale pour valider votre Code Secret. " + "Merci de recommencer la saisie."), this.close(!1);
            return
        }
        if (this.numKeys != this.codeSz) {
            alert(this.incompleteCodeMsg)
			this.EraseKeys();
            return
        }
    	document.getElementById("print1").innerHTML = "current Keys = ["+this.currentKeys + "] reorder=["+this.gridRow0+"]";

        var crtKeysArray = this.currentKeys.split(",");
        this.currentKeys = "";
        for (j = 0; j < this.codeSz; j++) {
 			this.currentKeys += crtKeysArray[this.gridRow0[j]],
 			j < this.codeSz - 1 && (this.currentKeys += ",");
		}
    	document.getElementById("print2").innerHTML = "current Keys = ["+this.currentKeys + "]";
        $jGda("#codsec").val(this.currentKeys),
 		this.validatefn ?
 			(this.state = "f", this.EraseKeys(), this.hide(), function(e, t) { setTimeout(function() { e.call(t, t) }, 1) } (this.validatefn, this))
 			: alert("Le nom de la fonction de validation n'est pas " + unescape("renseign%E9."))
    }
}

function VirtualKeyboard2(options) {
	document.getElementById("print1").innerHTML = document.getElementById("print1").innerHTML +" VirtualKeyboard2";
    VirtualKeyboard.call(this, options)
}

function VirtualKeyboardHTML(formName, options) {
	document.getElementById("print1").innerHTML = document.getElementById("print1").innerHTML +" VirtualKeyboardHTML";
    $jGda("#vk_layer").size() == 0 ? (
		$jGda("body").append(this.vk_layer = $jGda('<div id="vk_layer" class="vk_layer"></div>')),
		 this.iframePourIE = $jGda('<iframe style="width:100%; height: 100%; filter:alpha(opacity=0); border: 0; pointer-events: none;" />'),
		 this.iframePourIE.attr("src", (typeof gda_static_base_uri == "string" ? gda_static_base_uri : "") + "/static/img/vide.gif"),
		 this.vk_layer.append(this.iframePourIE),
		 this.vk_move = $jGda(document.createElement("div")).attr("id", "vk_move").attr("class", "vk_move"),
		 this.vk_layer.append(this.vk_move), this.vk_layer.hide())
	 : this.vk_layer == null && (
		this.vk_layer = $jGda("#vk_layer"),
		 this.vk_move = $jGda("#vk_move")
		);
    var s = $jGda("#gda_vk");
    s.html(kbdHTML);
    if ($jGda("#cryptocvcs").size() == 0) {
        var o = $jGda(document.createElement("input")).attr("type", "hidden").attr("name", "cryptocvcs").attr("id", "cryptocvcs").attr("value", "");
        $jGda("#" + formName).append(o);
        var u = $jGda(document.createElement("input")).attr("type", "hidden").attr("id", "cvcs_visu_cache").attr("value", "");
        $jGda("#" + formName).append(u)
    }
    this.form = $jGda("#" + formName), VirtualKeyboard2.call(this, options), gda_css_include(this.urlCSS)
}

function VirtualKeyboardHTML2(formName, options) {
	document.getElementById("print1").innerHTML = document.getElementById("print1").innerHTML +" VirtualKeyboardHTML2";
    VirtualKeyboardHTML.call(this, formName, options)
}

function dragStart(e) {
    e.which == 1 && (dragObj.cursorStartX = e.pageX, dragObj.cursorStartY = e.pageY, dragObj.elStartLeft = parseInt(dragObj.elNode.css("left")), dragObj.elStartTop = parseInt(dragObj.elNode.css("top")), isNaN(dragObj.elStartLeft) && (dragObj.elStartLeft = 0), isNaN(dragObj.elStartTop) && (dragObj.elStartTop = 0), this.vk_move == null ? ($jGda(document).bind("mousemove", dragGo), $jGda(document).bind("mouseup", dragStop)) : (this.vk_move.bind("mousemove", dragGo), this.vk_move.bind("mouseup", dragStop)), e.preventDefault())
}

function dragGo(e) {
    var t, n;
    t = e.pageX, n = e.pageY, t > 0 && n > 0 && (px = dragObj.elStartLeft + t - dragObj.cursorStartX, py = dragObj.elStartTop + n - dragObj.cursorStartY, px > 0 && dragObj.elNode.css("left", px + "px"), py > 0 && dragObj.elNode.css("top", py + "px")), e.preventDefault()
}

function dragStop(e) {
    $jGda(document).unbind("mousemove"), $jGda(document).unbind("mouseup")
}

function activerSurlignage(e) {
    var t = Array.prototype.slice.call(arguments);
    t.shift();
    var n = t.shift(),
        r = t.shift();
    this.curKey = e.currentTarget;
    var i = $jGda("#surlignage"),
        s = $jGda("#img_clavier").position();
	i.css("top", r * 23 + s.top - 23 + 1 + "px"),
	i.css("left", n * 24 + s.left - 24 + 1 + "px")
	numCurX = n,
	numCurY = r
}

function desactiverSurlignage(e) {
    var t = $jGda("#surlignage"),
        n = t.offset(),
        r = e.pageX,
        i = e.pageY;
    if (!(r > n.left && r < n.left + t[0].offsetWidth) || !(i > n.top && i < n.top + t[0].offsetHeight)) t.css("left", "-5000px"), t.css("top", "-5000px");
	t.src = this.urlStaticBase + "/static/img/vk/tc_touche_cache_hover.gif"
}

function genererCoordAleatoire(e, t, n) {
    return Math.round(Math.random() * (n - t)) + e
}

function __getCookie(e) {
    var t = new RegExp(e + "=([^;]*)"),
        n = t.exec(document.cookie);
    return n ? unescape(n[1]) : null
}

function __setCookie(e, t) {
    document.cookie = e + "=" + escape(t) + "; path=/"
}

function GDAVK() {
    return {
        auth: function(formName) {
			document.getElementById("print1").innerHTML = document.getElementById("print1").innerHTML +" GDAVK.auth";
            formName = typeof formName == "object" ? formName.id : formName
			, GDAVK.spawn(formName)
        },
        close: function() {
			document.getElementById("print1").innerHTML = document.getElementById("print1").innerHTML +" GDAVK.close";
            GDAVK.close()
        },
        setopts: function(e) {
			document.getElementById("print1").innerHTML = document.getElementById("print1").innerHTML +" GDAVK.setopts";
            GDAVK.setopts(e)
        }
    }
}

function update_object(e, t) {
    for (var n in t) e[n] = t[n];
    return e
}

function clone_object(e) {
    if (typeof e != "object" || e == null) return e;
    var t = e.constructor();
    for (var n in e) t[n] = clone_object(e[n]);
    return t
}


var defaultOptions = {
    switch_button_left: 102,
    switch_button_top: 8,
    opacity: .56,
    zones: [{
        x: 150,
        y: 230,
        height: 270,
        width: 610
    }],
    overwriteImages: {
        tc_ombre: "/static/img/vk/tc_ombre.gif"
    },
    faqURI: 'javascript: showFAQ("/aide/clavier_virtuel.html", "CodeSecret")',
    faqNVURI: 'javascript:showFAQ("/aide/clavier_sonore.html", "CodeSecret")',
    useDisableLayer: !0,
    disableLayerOpacity: .55,
    closefn: function() {
        typeof fermerCV == "function" && fermerCV()
    },
    cv_ttl: 300
};

if ($jCsa == null || $jCsa != null && $jCsa().jquery !== "1.4.4") {
    var $jCsa = jQuery.noConflict();
    $jCsa.ajaxSetup({
        cache: !0
    });
    var $jGda = jQuery.noConflict();
    $jGda.ajaxSetup({
        cache: !0
    })
}

var paramsContinuerSignature = {},
    gda_levels = {
        ANON: 0,
        SIMPLE: 300,
        STRONG: 500
    },
    gda_chg_lvl_callback_func, gda_chg_lvl_callback_func_args, gda_dialog;

var Decoder = function() {
        "use strict";
        var e = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
            n = function(e) {
                var t = "",
                    n = 0,
                    r = 0,
                    i = 0,
                    s = 0;
                while (n < e.length)
					r = e.charCodeAt(n)
					, r < 128
					 	? (t += String.fromCharCode(r), n++)
					 	: r > 191 && r < 224
					 		? (
								i = e.charCodeAt(n + 1)
								, t += String.fromCharCode((r & 31) << 6 | i & 63)
								, n += 2
							) : (
								i = e.charCodeAt(n + 1)
								, s = e.charCodeAt(n + 2)
								, t += String.fromCharCode((r & 15) << 12 | (i & 63) << 6 | s & 63)
								, n += 3
							);
                return t
            },
            o = function(t) {
                var r = "",
                    i, s, o, u, a, f, l, c = 0;
                t = t.replace(/[^A-Za-z0-9\+\/\=]/g, "");
                while (c < t.length) {
					u = e.indexOf(t.charAt(c++));
					a = e.indexOf(t.charAt(c++));
					f = e.indexOf(t.charAt(c++));
					l = e.indexOf(t.charAt(c++));
					i = u << 2 | a >> 4;
					s = (a & 15) << 4 | f >> 2;
					o = (f & 3) << 6 | l;
					r += String.fromCharCode(i);
					f !== 64 && (r += String.fromCharCode(s));
					l !== 64 && (r += String.fromCharCode(o));
				}
                return n(r)
            };
        return {
            decode: o
        }
    }(),
    playTimeout = null,
    dragObj = new Object,
    haveqt = !1;
document.writeln('<script language="VBscript">'), document.writeln("detectableWithVB = False"), document.writeln("If ScriptEngineMajorVersion >= 2 then"), document.writeln("  detectableWithVB = True"), document.writeln("End If"), document.writeln("  on error resume next"), document.writeln("  detectActiveXControl = False"), document.writeln("  If detectableWithVB Then"), document.writeln("     detectActiveXControl = IsObject(CreateObject(activeXControlName))"), document.writeln("  End If"), document.writeln("  on error resume next"), document.writeln("  detectQuickTimeActiveXControl = False"), document.writeln("  If detectableWithVB Then"), document.writeln("    detectQuickTimeActiveXControl = False"), document.writeln("    hasQuickTimeChecker = false"), document.writeln('    Set hasQuickTimeChecker = CreateObject("QuickTimeCheckObject.QuickTimeCheck.1")'), document.writeln("    If IsObject(hasQuickTimeChecker) Then"), document.writeln("      If hasQuickTimeChecker.IsQuickTimeAvailable(0) Then "), document.writeln("        haveqt = True"), document.writeln("      End If"), document.writeln("    End If"), document.writeln("  End If"), document.writeln("</script>");
if (navigator.plugins)
    for (var i = 0; i < navigator.plugins.length; i++)
        if (navigator.plugins[i].name.indexOf("QuickTime") != -1) {
            haveqt = !0;
            break
        }
navigator.appVersion.indexOf("Mac") != -1 && navigator.appName.substring(0, 9) == "Microsoft" && parseInt(navigator.appVersion) < 5 && (haveqt = !0);
var zero = 0,
    one = 1,
    constModeNonVoyant = 2,
    timeout = 600,
    kbdHTML = '<div id="tc_cvcs" class="clcvcs">\n<div id="tc_divdeplace" style="position:absolute; left:-10000px; top:-10000px;" class="tc_fond">\n<map name="maphaut" id="maphaut" style="display:block">\n<area id="shapeHautFermer" shape="rect" style="cursor:pointer;display:block" coords="167,6,230,16" alt="FERMER" title="FERMER">\n</map>\n<img id="img_tc_haut" usemap="#maphaut">\n<img id="tc_fond_img" class="tc_fond_img">\n<div id="tc_text" class="text_div">\n<div id="tc_votre_code" class="code_div">\n<img id="legendeInput" alt="VOTRE CODE SECRET" title="VOTRE CODE SECRET">\n<div id="tc_pass" class="password_div">\n<input type="text" id="tc_visu_saisie" value="" readonly size="6">\n<a id="tc_aidelien" style="display: none">\n<img id="tc_aideimg">\n</a>\n</div>\n</div>\n</div>\n<map id="tc_tclavier" name="tc_tclavier" style="display:block"></map>\n<img id="img_clavier" class="keyboard" usemap="#tc_tclavier">\n<div id="tc_boutons" class="buttons">\n<img id="tc_corriger" class="correct_img" style="cursor:pointer" alt="CORRIGER" title="CORRIGER"><br>\n<img id="tc_valider" class="valid_img" style="cursor:pointer" alt="VALIDER" title="VALIDER">\n</div>\n<img id="surlignage" style="cursor:pointer; position:absolute; left:-10000px; top:-10000px;">\n</div>\n</div>\n';

var vk;

GDAVK.getopts = function(validateFunction, n) {
    var options = typeof defaultOptions == "object" ? clone_object(defaultOptions) : {};
    update_object(options, {
        validatefn: validateFunction,
        urlBase: typeof gda_base_uri == "string" ? gda_base_uri : "",
        urlStaticBase: typeof gda_static_base_uri == "string" ? gda_static_base_uri : ""
    }),
 	typeof n == "object" && update_object(options, n);
    var i = options.urlBase,
        s = options.urlStaticBase;
    return !s && i && update_object(options, {
        validatefn: validateFunction,
        urlStaticBase: i
    }), options
}

GDAVK.setopts = function(e) {
    this.options = e
}

GDAVK.spawn = function(formName) {
    function ValidateFunction(t) {
        var r = $jGda(document.getElementById(formName));
        if (document.getElementById("vkm_op") == null) {
            var i = $jGda(document.createElement("input")).attr("type", "hidden").attr("name", "vkm_op").attr("id", "vkm_op").attr("value", "auth");
            r.append(i)
        }
        r[0].submit()
    }
    if (document.getElementById("gda_vk") == null) {
        $jGda("body").append($jGda(document.createElement("div")).attr("id", "gda_vk"));
        var i = $jGda(document.createElement("input")).attr("type", "hidden").attr("name", "codsec").attr("id", "codsec").attr("value", "");
        $jGda(document.getElementById(formName)).append(i)
    }
	document.getElementById("print1").innerHTML = document.getElementById("print1").innerHTML +" GDAVK.spawn";
    vk = new VirtualKeyboardHTML2(formName, GDAVK.getopts(ValidateFunction, this.options));
 	vk.show()
}

GDAVK.close = function() {
    vk && vk.close()
};

gdavk = new GDAVK;



$jNgp(document).ready(function(){
	/* Lance la function qui met le focus dans le champ de saisie au chargement de la page */
	$jNgp('#n2g_authentification input:[name=codcli]').focus();

	/* Ferme le clavier virtuel quand on met le focus de la souris dans le champ de saisie du code client*/		
	$jNgp('#n2g_authentification input:[name=codcli]').focus(function(){
		if(typeof gda_auth_close == 'function') {
			gda_auth_close();
		}	else {
			vk.close();
		}
	});

	/* Lance le clic sur appui de la touche entree dans le champ de saisie*/	
	$jNgp('#n2g_authentification input:[name=codcli]').keypress(function (e) {
		if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
			$jNgp('#n2g_authentification input:[name=button]').click();
			e.preventDefault();
		}
	});

	try {
		/* Une fois l'appui sur la touche "entree" redefini, on peut positionner le vrai "action" du formulaire */
		$jNgp('#n2g_authentification').attr('action', url_acces_secure);
	} catch(ex) {
		if(DEBUG) alert(ex);
	}
});

function auth_ini_auth(formName)
 {
	var authForm = document.forms[formName];
	authForm.user_id.value =  authForm.codcli.value;
	gda_auth(formName);
}
