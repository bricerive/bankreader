var gda_js_includes = new Array;

function gda_insert_version(e) {
    if (typeof gda_docroot_version == "string" && typeof e == "string") {
        if (e.indexOf("/static/js/") != -1 && e.indexOf("/static/js/v" + gda_docroot_version + "/") == -1) return e.replace("/static/js/", "/static/js/v" + gda_docroot_version + "/");
        if (e.indexOf("/static/styles/") != -1 && e.indexOf("/static/styles/v" + gda_docroot_version + "/") == -1) return e.replace("/static/styles/", "/static/styles/v" + gda_docroot_version + "/")
    }
    return e
}

function gda_js_include(e) {
    if (typeof gda_js_includes != "undefined" && gda_js_includes[e] == 1) return;
    typeof gda_base_uri == "string" && (e = gda_base_uri + e), e = gda_insert_version(e), document.write('<script type="text/javascript" src="' + e + '"></script>'), typeof gda_js_includes == "undefined" && (gda_js_includes = new Array), gda_js_includes[e] = 1
}

function __getCookie(e) {
    var t = new RegExp(e + "=([^;]*)"),
        n = t.exec(document.cookie);
    return n ? unescape(n[1]) : null
}

function __setCookie(e, t) {
    document.cookie = e + "=" + escape(t) + "; path=/"
}

function prepare_gda_auth(e) {
    e = typeof e == "object" ? e.id : e, __getCookie("vk_mode") == constModeNonVoyant && __setCookie("vk_mode", O00000O0)
}

function GDAVK() {
    return {
        auth: function(e) {
            e = typeof e == "object" ? e.id : e, GDAVK.spawn(e, OO000O0O, "auth", !0)
        },
        auth_json: function(e) {
            GDAVK.spawn_json(OO000O0O, "auth", e, !0)
        },
        sign: function(e) {
            e = typeof e == "object" ? e.id : e, GDAVK.spawn(e, OO000O0O, "sign", !1)
        },
        sign_json: function(e) {
            GDAVK.spawn_json(OO000O0O, "sign", e, !1)
        },
        close: function() {
            GDAVK.close()
        },
        setopts: function(e) {
            GDAVK.setopts(e)
        }
    }
}

var gda_modules = {},
    gda_auth_module = "vk",
    gda_sign_module = "vk",
    gda_origin_lgn = "LGN",
    gda_origin_sas = "SAS",
    gda_action_levels = {
        NORMAL: 0,
        SENSIBLE: 100,
        TRES_SENSIBLE: 200
    };

gda_modules.vk = new GDAVK, gda_js_include("/static/js/csa/csat_dialog.js");

function gda_auth(e) {
    e = typeof e == "object" ? e.id : e, gda_modules[gda_auth_module].auth(e)
}

function auth_ini_auth_submit(formName)
{
	
	var authForm = document.forms[formName];
	if (typeof authForm.codcli == 'object')
	{
		var userId = authForm.codcli.value;
		if (authForm.user_id) {
			authForm.user_id.value =  userId;
		}
	}
	
	gda_auth(formName);
}

function auth_ini_auth(formName) {
	prepare_gda_auth();
	return auth_ini_auth_submit(formName);
}
