var msgWindow;
var estMac = (navigator.userAgent.toLowerCase().indexOf("mac")) != -1 ? 1 : 0;
var estOpera = (navigator.userAgent.toLowerCase().indexOf("opera")) != -1 ? 1 : 0;


var URLFrmAssistance = "/html/assistance/frm_assistance.html"

function OpenWindow(url,nom,options)
{
  msgWindow = window.open(url,nom,options);
}

function OpenWindowFocus(url,nom,options)
{
  if ((msgWindow) && (!estMac) && (msgWindow.close)){
    		msgWindow.close();
  }
  
  if (estOpera) {
  	var commande = 'OpenWindow("' + url + '","' + nom + '","' + options + '")';
  	setTimeout(commande,1);
  }else{
  	OpenWindow(url,nom,options);
  }
  	
}

function printwindow()
{
  if (window.print)
    window.print();
  else
    alert("Pour lancer l'impression, veuillez utiliser le menu \"fichier/imprimer\" de votre navigateur");
}

function open_contact()
{
  window.open(URLFrmAssistance + "?urlReferrer="+ escape(location.pathname),'MAILTO','width=518,height=540,top=0,left=0,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,alwaysRaised=yes,resizable=yes');
}

function OpenWindow_RadioBox(tabRadio)
{
	for(var i=0; i<tabRadio.length; i++ )
	if(tabRadio[i].checked){
	 if(tabRadio[i].value.indexOf("https://")>=0)OpenWindowFocus(tabRadio[i].value,'newWin','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=541,height=600');
	 else window.location.href=tabRadio[i].value; 
	 break;
	} 
}