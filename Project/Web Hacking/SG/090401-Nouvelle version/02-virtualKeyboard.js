//**********************************************************
// Constantes
//**********************************************************

var playTimeout = null;

/* For dragStart, dragStop, etc. */
var dragObj = new Object();
var haveqt = false;

/* Insert VBScript code detecting whether the plugin we need for blind people. */
document.writeln('<script language="VBscript">');
document.writeln('detectableWithVB = False');
document.writeln('If ScriptEngineMajorVersion >= 2 then');
document.writeln('  detectableWithVB = True');
document.writeln('End If');
document.writeln('  on error resume next');
document.writeln('  detectActiveXControl = False');
document.writeln('  If detectableWithVB Then');
document.writeln('     detectActiveXControl = IsObject(CreateObject(activeXControlName))');
document.writeln('  End If');
document.writeln('  on error resume next');
document.writeln('  detectQuickTimeActiveXControl = False');
document.writeln('  If detectableWithVB Then');
document.writeln('    detectQuickTimeActiveXControl = False');
document.writeln('    hasQuickTimeChecker = false');
document.writeln('    Set hasQuickTimeChecker = CreateObject("QuickTimeCheckObject.QuickTimeCheck.1")');
document.writeln('    If IsObject(hasQuickTimeChecker) Then');
document.writeln('      If hasQuickTimeChecker.IsQuickTimeAvailable(0) Then ');
document.writeln('        haveqt = True');
document.writeln('      End If');
document.writeln('    End If');
document.writeln('  End If');
document.writeln('</script>');

if (navigator.plugins) {
	for (var i = 0; i < navigator.plugins.length; i++) {
		if (navigator.plugins[i].name.include('QuickTime')) {
			haveqt = true;
			break;
		}
	}
}

if (navigator.appVersion.include("Mac") && navigator.appName.substring(0,9) == "Microsoft" && parseInt(navigator.appVersion) < 5)
	haveqt = true;

/*
 * Nouveau clavier virtuel utilisant l'API cross-browser Prototype.
 *
 * Documentation de l'API:
 *	http://www.prototypejs.org/api/
 */

/* A few constants. */
var constModeDefaut = 0;
var constModeMalVoyant = 1;
var constModeNonVoyant = 2;
var constSessionDuration = 600;

function showFAQ(url,name) {
  window.open(url, name,
    'width=780,height=540,top=0,left=0,toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,alwaysRaised=yes,resizable=yes');
}

var VirtualKeyboard = Class.create({
	initialize: function(width, height, inSession, codeFieldId, options) {
		if (!$(codeFieldId)) {
			alert("Erreur: le champ de code secret d'identifiant '" + codeFieldId + "' n'existe pas dans la page!");
			return;
		}

		this.fieldId = codeFieldId;
		this.inSession = inSession;

		this.initParams(options);

		this.mode = constModeDefaut;
		this.loadingCrypto = false;
		this.numKeys = 0;
		this.state = '';
		this.codsec = '';
		this.urlError = this.urlBase + '/pageErreur.html';
		this.urlKeyboard = this.urlBase + '/cvcsgenclavier?mode=json&estSession=';
		this.urlImage = this.urlBase + '/cvcsgenimage?modeClavier='; 
		this.urlCSS = this.urlBase + '/styles/virtualKeyboard.css';
		this.urlParamCrypto = '&cryptogramme=';
		this.urlParamError = 'cvcsCodeErreur';

		/* Drag'n'drop handling. */
		dragObj.zIndex = 0;
		dragObj.elNode = $('tc_divdeplace');
		$('img_tc_haut').observe('mousedown', dragStart);
		$('img_tc_haut').observe('mouseup', dragStop);

		/* Path to images. */
		this.images = new Array();
		this.images["img_tc_haut"] = "/img/cvcs/tc_haut_sans.gif";
		this.images["tc_fond_img"] = "/img/cvcs/tc_fond.gif";
		this.images["legendeInput"] = "/img/cvcs/tc_votre_code.gif";
		this.images["tc_aideimg"] = "/img/cvcs/bouton_question.gif";
		this.images["tc_corriger"] = "/img/cvcs/tc_corriger.gif";
		this.images["tc_valider"] = "/img/cvcs/tc_valider.gif";
		this.images["surlignage"] = "/img/cvcs/tc_touche_cache_hover.gif";

		/* Make images in the HTML code point to the correct location. */
		var urlBase = this.urlBase;
		var images = this.images;
		$('tc_cvcs').select('img').each(function(img) {
			if (images[img.id])
				img.writeAttribute('src', urlBase + images[img.id]);
		});
		/* Create the appropriate link for the help button. */
		$('tc_aidelien').writeAttribute('href',
		    'javascript:showFAQ(\'' + urlBase + '/codeSecret.html\', \'CodeSecret\')');

		if (this.allowSwitch) {
			var img_mv = new Element('img', {
				'id': 'tc_haut_switch',
				'class': 'tc_haut_switch',
				'src': this.urlBase + '/img/cvcs/tc_agrandir.gif',
				'style': 'cursor: pointer',
				'alt': 'AGRANDIR'
			});
			img_mv.removeAttribute('height');
			img_mv.removeAttribute('width');
			$('img_tc_haut').insert({ after: img_mv });
			img_mv.observe('click', this.toggleMode.bindAsEventListener(this));
		}

		if (this.showHelpButton)
			$('tc_aidelien').toggle();

		/* Attach events to anything clickable. */
		$('shapeHautFermer').observe('click', this.close.bindAsEventListener(this));
		$('tc_corriger').observe('click', this.emptyCode.bindAsEventListener(this));
		$('tc_valider').observe('click', this.validate.bindAsEventListener(this));

		$('surlignage').observe('mousedown', this.onKeyClicked.bindAsEventListener(this));
		$('surlignage').observe('mouseup', this.onKeyUnclicked.bindAsEventListener(this));

		/* This event is for unhilighting the keys. */
		$('surlignage').observe('mouseout', desactiverSurlignage.bindAsEventListener(this));

		$('tc_fond_img').setOpacity(this.opacity);
	},

	initParams: function(options) {
		/* Default values. */
		this.pwdlen = 6;	/* Secret code length */
		this.allowSwitch = true;
		this.wantMode = constModeDefaut;
		this.opacity = 0.56;
		this.urlBase = '';
		this.offx = 0;
		this.offy = 0;
		this.emptyCodeMsg = "La saisie de votre Code Secret est obligatoire.";
		this.incompleteCodeMsg = "Le Code Secret saisi est incorrect.\r\n" +
		    "Merci de bien vouloir ressaisir votre Code Secret " + unescape("compos%E9") + " de " +
		    this.pwdlen + " chiffres.";
		this.showHelpButton = true;

		/* Override with supplied options. */
		if (options != undefined)
			Object.extend(this, options);
	},

	getWidth: function() {
		return $('tc_fond_img').getWidth();
	},

	getHeight: function() {
		return $('img_tc_haut').getHeight() + $('tc_fond_img').getHeight();
	},

	onKeyClicked: function(event) {
		if (this.mode == constModeDefaut)
			event.element().src = this.urlBase + '/img/cvcs/tc_touche_cache_click_1s2.gif';
		else
			event.element().src = this.urlBase + '/img/cvcs/tc_touche_cache_click_1s2_mv.gif';
		this.clickKeyX = numCurX;
		this.clickKeyY = numCurY;
	},

	onKeyUnclicked: function(event) {
		if (this.mode == constModeDefaut)
			event.element().src = this.urlBase + '/img/cvcs/tc_touche_cache_hover.gif';
		else
			event.element().src = this.urlBase + '/img/cvcs/tc_touche_cache_hover_mv.gif';
		if (this.clickKeyX == numCurX && this.clickKeyY == numCurY)
			this.updateValue(parseInt((numCurY - 1) * this.nbc + parseInt(numCurX)) - 1);
	},

	show: function() {
		if (this.crypto != undefined) {
			this.showKeyboard();
			return;
		}
		this.generateKeyboardCoords();
		if (this.state.empty()) {
			if (this.wantMode != this.mode) {
				this.mode = this.wantMode;
				this.updateKeyboard($('tc_cvcs'));
			}
			var vk = this;
			this.getCryptogram(function() {
				vk.state = 'i';
				vk.generateKeyboard();
				if (!vk.inSession) {
					vk.timeout = false;
					vk.timer = vk.timedOut.bind(vk).delay(constSessionDuration);
				}
				vk.showKeyboard();
			});
		}
	},

	timedOut: function() {
		if (this.state == 'v')
			this.timeout = true;
	},

	showKeyboard: function() {
		if (this.mode != constModeNonVoyant) {
			var div = $('tc_divdeplace');
			var off = document.viewport.getScrollOffsets();
			if (typeof(N2GHideFlash) == 'function')
				N2GHideFlash(); // masks flash objects
			div.style.left = off.left + this.posx + 'px';
			div.style.top  = off.top + this.posy + 'px';

			this.state = 'v';
		}
	},

	/* XXX - looks like this one should go now. */
	emptyFields: function() {
		this.emptyCode();
		this.state = ''; // XXX - Was in vider_champs() but looks misplaced.
	},

	emptyCode: function() {
		if (this.mode != constModeNonVoyant) {
			$('tc_visu_saisie').value = '';
			$('cvcs_visu_cache').value = '';
		} else {
			$('debutClavier').focus();
		}
		this.numKeys = 0;
		this.codsec = '';
	},

	/* Callback for handling the JSON data returned by the server. */
	JSONHandler: function(onSuccess, vkdata) {
		if (vkdata.code != 0) {
			window.location = this.urlError + '?' + this.urlParamError + '=104'; // + codeR;
			return false;
		}
		this.crypto = vkdata.crypto;
		$('cryptocvcs').value = this.crypto;
		this.nbl = vkdata.nbrows;
		this.nbc = vkdata.nbcols;
		this.adaptKeyboard();

		if (this.crypto.empty() || this.nbl <= 0 || this.nbc <= 0) {
			window.location = this.urlError + '?' + this.urlParamError + '=106'; // + codeERR_GEN;
			return false;
		}
		var ndata = this.nbl * this.nbc;
		if (vkdata.grid.length != this.pwdlen * ndata) {
			window.location = this.urlError + '?' + this.urlParamError + '=105'; // + codeERR_GEN;
			return false;
		}
		if (this.mode == constModeNonVoyant)
			this.keyCodes = vkdata.grid.without(0);
		else
			this.keyCodes = vkdata.grid;
		onSuccess();
		this.loadingCrypto = false;
	},

	/*
	 * Call server to get cryptogramme and size of the virtual keyboard.
	 * This is needed later on to construct an URL to the server-generated
	 * image, and to send the secret code in encoded form.
	 */
	getCryptogram: function(onSuccess) {
		if (this.loadingCrypto)
			return;

		if (this.scriptTag)
			this.scriptTag.remove();

		this.loadingCrypto = true;
		var url = this.urlKeyboard;
		if (this.inSession)
			url += '1';
		else
			url += '0';

		_vkCallback = this.JSONHandler.bind(this, onSuccess);
		var scriptTag = new Element('script', { 'type': 'text/javascript', 'src': url });
		$$('head').first().insert(scriptTag);
		this.scriptTag = scriptTag;
	},

	hide: function() {
		if (this.mode != constModeNonVoyant) {
			if (!this.state.empty() || $('tc_cvcs').visible()) {
				var div = $('tc_divdeplace');
				div.style.left = '-600px';
				div.style.top = '-600px';
				this.state = '';
			}
		}
		window.clearTimeout(this.timer);
		this.timeout = false;
		if (typeof(N2GShowFlash) == 'function')
			N2GShowFlash(); // shows Flash objects
	},

	toggleMode: function(event) {
		this.hide();
		if (this.mode == constModeDefaut)
			this.mode = constModeMalVoyant;
		else
			this.mode = constModeDefaut;
		this.generateKeyboard();

		this.updateKeyboard($('tc_cvcs'));
		this.adaptKeyboard();
		/* The delay helps for displaying only when the browser is done rendering. */
		this.showKeyboard.bind(this).delay(0.3);

		this.state = 'i';
		event.stop();
	},

	getKeyboardMode: function() {
		return this.mode;
	},

	generateKeyboard: function() {
		var ndt = this.nbl * this.nbc;
		var k = 0;

		var kbdmap = $('tc_tclavier');
		/* First, remove any <area> element contained within the <map>. */
		kbdmap.select('area').invoke('remove');

		/* Then, add back all the <area> elements matching this keyboard. */
		for (i = 1; i <= this.nbl; i++) {
			for (j = 1; j <= this.nbc; j++) {
				var keyId = 'touche' + i + j;
				var factor = 24;
				if (this.mode == constModeMalVoyant)
					factor *= 2;
				var coords = (j - 1) * factor + "," + (i - 1) * factor + "," + j * factor + "," + i * factor;
				var area = new Element('area', { 'id': keyId, 'coords': coords, 'style': 'cursor: pointer' });
				/* Insert element in the DOM before registering events. */
				kbdmap.insert(area);
				if (this.keyCodes[k] != 0) {
					area.className = 'touche';
					area.observe('mouseout', desactiverSurlignage.bindAsEventListener(this));
					area.observe('mouseover', activerSurlignage.bindAsEventListener(this, j, i));
				} else {
					area.className = 'toucheVide';
					area.observe('mouseover', desactiverSurlignage.bindAsEventListener(this));
				}
				k++;
			}
		}

		$('img_clavier').src = this.urlImage + this.mode + this.urlParamCrypto + this.crypto;
	},

	close: function() {
		this.hide();
		this.emptyFields();
		this.crypto = null;
		if (this.closefn)
			this.closefn();
	},

	/* Update keyboard tags to point to the correct images and update CSS classes. */
	updateKeyboard: function(elem) {
		if (this.mode == constModeMalVoyant)
			$('tc_fond_img').setOpacity(1);
		else
			$('tc_fond_img').setOpacity(this.opacity);
		var children = elem.descendants();
		var numchilds = children.length;
		for (var i = 0; i < numchilds; ++i) {
			var child = children[i];
			if (child.nodeName.toLowerCase() == 'img' && child.hasAttribute('src')) {
				var src = child.getAttribute('src');
				if (!src.empty()) {
					if (this.mode == constModeMalVoyant)
						child.setAttribute('src', src.replace(/(\.\w+)$/, '_mv$1'));
					else
						child.setAttribute('src', src.replace(/_mv(\.\w+)$/, '$1'));
				}
			}
			if (child.className && !child.className.empty()) {
				if (this.mode == constModeMalVoyant)
					child.className = child.className.replace(/$/, '_mv');
				else
					child.className = child.className.replace(/_mv$/, '');
			}
		}
	},

	adaptKeyboard: function() {
		if (this.mode == constModeNonVoyant)
			return;
		var extOpera = '';
		if (Prototype.Browser.Opera) {
			/* Get the major version number. */
			var version = navigator.appVersion.charAt(0);
			if (version < 9)
				extOpera = '_1s2';
		}

		var img_tc_haut = this.urlBase + '/img/cvcs/tc_haut_sans' + (this.nbc > 4 ? '_' + this.nbc : '') + '.gif';
		var img_tc_fond = this.urlBase + '/img/cvcs/tc_fond.gif';
		if (this.nbc == 4 && this.nbl == 4)
			img_tc_fond = this.urlBase + '/img/cvcs/tc_fond' + extOpera + '.gif';
		var img_tc_haut_mv = this.urlBase + '/img/cvcs/tc_haut_sans' + (this.nbc > 4 ? '_' + this.nbc : '') + '_mv.gif';
		var img_tc_fond_mv = this.urlBase + '/img/cvcs/tc_fond_mv.gif';
		if (this.nbc > 4) {
			if (this.nbl > 4){
				img_tc_fond = this.urlBase + '/img/cvcs/tc_fond' + extOpera + '_5x5.gif';
				img_tc_fond_mv = this.urlBase + '/img/cvcs/tc_fond_5x5_mv.gif';
			} else {
				img_tc_fond = this.urlBase + '/img/cvcs/tc_fond' + extOpera + '_5x4.gif';
				img_tc_fond_mv = this.urlBase + '/img/cvcs/tc_fond_5x4_mv.gif';
			}
		} else if (this.nbl > 4) {
			img_tc_fond = this.urlBase + '/img/cvcs/tc_fond' + extOpera + '_4x5.gif';
			img_tc_fond_mv = this.urlBase + '/img/cvcs/tc_fond_4x5_mv.gif';
		}
		if (this.mode == constModeDefaut) {
			$('img_tc_haut').src = img_tc_haut;
			$('tc_fond_img').src = img_tc_fond;
			$('shapeHautFermer').coords = this.nbc > 4 ? '192,6,241,16' : '167,3,217,16';
			$('tc_boutons').style.left = 139 + (this.nbc - 4) * 24 + 'px';
			$('tc_boutons').style.top = 80 + (this.nbl - 4) * 24 + 'px';
			if ($('tc_haut_switch'))
				$('tc_haut_switch').style.left = 102 + (this.nbc - 4) * 25 + 'px';
		} else {
			$('img_tc_haut').src = img_tc_haut_mv;
			$('tc_fond_img').src = img_tc_fond_mv;
			/* The next two lines are needed for Safari. */
			$('tc_fond_img').removeAttribute('height');
			$('tc_fond_img').removeAttribute('width');
			$('shapeHautFermer').coords = this.nbc > 4 ? '325,4,498,40' : '275,4,448,40';
			$('tc_boutons').style.left = 245 + (this.nbc - 4) * 48 + 'px';
			$('tc_boutons').style.top = 115 + (this.nbl - 4) * 48 + 'px';
			if ($('tc_haut_switch'))
				$('tc_haut_switch').style.left = 80 + (this.nbc - 4) * 30 + 'px';
		}
	},

	generateKeyboardCoords: function() {
		if (this.zones == undefined) {
			var dim = document.viewport.getDimensions();
			this.posx = genererCoordAleatoire(this.offx, this.getWidth(), dim.width);
			this.posy = genererCoordAleatoire(this.offy, this.getHeight(), dim.height);
		} else {
			var n = Math.floor(Math.random() * this.zones.length);
			var zone = this.zones[n];
			var dim = document.viewport.getDimensions();
			/* If the width or height aren't defined, or if we are given numbers that
                           exceed the size of the viewport, restrict the zone to the viewport. */
			if (zone.width == undefined || zone.width > dim.width)
				zone.width = dim.width;
			if (zone.height == undefined || zone.height > dim.height)
				zone.height = dim.height;
			this.posx = genererCoordAleatoire(zone.x, this.getWidth(), zone.width);
			this.posy = genererCoordAleatoire(zone.y, this.getHeight(), zone.height);
		}
	},

	updateValue: function(index) {
		var vi = $('tc_visu_saisie');
		var vc = $('cvcs_visu_cache');
		var ndata = this.nbl * this.nbc;
		if (this.numKeys < this.pwdlen) {
			vi.value += '*';
			vc.value = vi.value;
			this.codsec += this.keyCodes[this.numKeys * ndata + index] ;
			if (this.numKeys < this.pwdlen - 1)
				this.codsec += ',';
			this.numKeys++;
		}
	},

	validate: function(event) {
		$(this.fieldId).value = this.codsec;

		if (!this.inSession && this.timeout) {
			alert("Attention, vous avez " + unescape("d%E9pass%E9") + " la " +
			    unescape("dur%E9e") + " maximale pour valider votre Code Secret. " +
			    "Merci de recommencer la saisie.");
			this.close(false);
			return;
		}

		if (this.numKeys == 0) {
			alert(this.emptyCodeMsg);
			return;
		} else if (this.numKeys != this.pwdlen) {
			alert(this.incompleteCodeMsg);
			this.emptyCode();
			return;
		}

		if (this.validatefn) {
			this.state = 'f';
			this.emptyCode();
			this.hide();
			/* If the validation function tries to submit a form, it won't work with IE6 unless
			 * we defer this function for some reason. */
			this.validatefn.defer(this);
		} else
			alert("Le nom de la fonction de validation n'est pas " + unescape("renseign%E9."));
	},

	enableSound: function(clientCodeFocus) {
		if (!Prototype.Browser.IE)
			return;
		if (this.mode == constModeNonVoyant)
			return;

		var res = confirm("Vous avez " + unescape("activ%E9") + " le clavier virtuel sonore.");
		if (!res)
			return;

		if (!haveqt) {
			res = confirm("Le plugin Quicktime n'est pas " + unescape("install%E9") +
			    " sur votre navigateur. Pour le " + unescape("t%E9l%E9charger") +
			    ", choisissez OK (ouverture d'une nouvelle " + unescape("fen%EAtre") + ").");
			if (res)
				window.open("http://www.apple.com/quicktime/download", "_blank", "");
			else
				window.history.go(0);
			return;
		}

		this.mode = constModeNonVoyant;
		this.getCryptogram(this.buildSoundKeyboard.bind(this));
	},

	buildSoundKeyboard: function() {
		var pluginHTML = '<object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" width="1" height="1" id="sonCode"><param name="src" value="';
		pluginHTML += this.urlImage + this.mode + this.urlParamCrypto + this.crypto;
		pluginHTML += '"><param name="name" value="sonCode"><param name="id" value="sonCode"><param name="autostart" value="false"><embed width="1" height="1" src="';
		pluginHTML += this.urlImage + this.mode + this.urlParamCrypto + this.crypto;
		pluginHTML += '" name="sonCode" id="sonCode" autostart="false" enablejavascript="true"></embed></object>';
		var p = $('pluginQt');
		if (!p) {
			p = new Element('p', { 'id': 'pluginQt', 'class': 'noMarge' });
			$('tc_cvcs').insert({ 'after': p });
		}
		p.innerHTML = pluginHTML;

		var help = new Element('a', { 'href': 'javascript:showFAQ(\'' + this.urlBase + '/html/faq/faq43.html\', \'FAQ\')' });
		var help_img = new Element('img', {
			'src': this.urlBase + '/img/commun/trans.gif',
			'class': 'noMarge',
			'alt': 'Aide ' + unescape('%E0') + ' la solution accessible nonvoyant nouvelle ' + unescape('fen%EAtre'),
			'id': 'aideNonVoyantClav'
		});
		help.insert(help_img);
		p.insert(help);

		var a0 = new Element('a', {
			'href': 'javascript:void(0)',
			'name': 'Ensemble',
			'id': 'Ensemble'
		});
		var img0 = new Element('img', {
			'src': this.urlBase + '/img/commun/trans.gif',
			'class': 'noMarge',
			'alt': 'Ecoutez lensemble de la suite de chiffres',
			'id': 'ListenAll'
		});
		a0.insert(img0);
		p.insert(a0);
		a0.observe('click', playAllSelection);

		for (var i = 0; i <= 9; i++) {
			/* Used to construct the alt attribute. */
			var digitsAlt = ['premier', 'second', 'troisi%E8me', 'quatri%E8me',
				'cinqui%E8me', 'sixi%E8me', 'septi%E8me', 'huiti%E8me',
				'neuvi%E8me', 'dernier'];

			var a = new Element('a', { 'href': 'javascript:void(0)' });
			if (i == 0)
				a.writeAttribute({ 'id': 'debutClavier', 'name': 'debutClavier' });
			var img = new Element('img', {
				'class': 'noMarge',
				'src': this.urlBase + '/img/commun/trans.gif',
				'alt': unescape(digitsAlt[i]) + ' chiffre du clavier virtuel'
			});
			a.insert(img);
			p.insert(a);
			a.observe('focus', armPlayNV.curry(i));
			a.observe('blur', disarmPlayNV);
			a.observe('click', this.keyPressedNV.bindAsEventListener(this, i));
		}

		/* Add the 'corriger' link. */
		var correct = new Element('a', {
			'name': 'corriger',
			'id':   'corriger',
			'href': 'javascript:void(0)'
		});
		var cimg = new Element('img', {
			'src': this.urlBase + '/img/commun/trans.gif',
			'class': 'noMarge',
			'alt': 'Corriger le code secret'
		});
		correct.insert(cimg);
		p.insert(correct);
		correct.observe('click', this.emptyCode.bindAsEventListener(this));

		/* Add the 'valider' link. */
		var submit = new Element('a', { 'href': 'javascript:void(42)' });
		var simg = new Element('img', {
			'src': this.urlBase + '/img/commun/trans.gif',
			'class': 'noMarge',
			'name': 'validation',
			'id': 'validation',
			'alt': 'Valider le code secret'
		});
		submit.insert(simg);
		p.insert(submit);
		submit.observe('click', this.validate.bindAsEventListener(this));

		if ($('aideNonVoyantClav'))
			$('aideNonVoyantClav').focus();
		else
			$('Ensemble').focus();
	},

	keyPressedNV: function(event, codeInterrupt) {
		if (this.mode != constModeNonVoyant || this.numKeys >= this.pwdlen)
			return;

		this.codsec += this.keyCodes[this.numKeys * 10 + codeInterrupt] ;
		if (this.numKeys < this.pwdlen - 1)
			this.codsec += ',';
		this.numKeys++;
		if (this.numKeys == this.pwdlen && $('corriger'))
			$('corriger').focus();
		newPlayNV(codeInterrupt);
	}
});

var VirtualKeyboard2 = Class.create(VirtualKeyboard, {
	initialize: function($super, inSession, codeFieldId, options) {
		$super(null, null, inSession, codeFieldId, options);
	}
});

var kbdHTML = '<div id="tc_cvcs" class="clcvcs">\n' +
  '<div id="tc_divdeplace" style="position:absolute; left:-10000px; top:-10000px;" class="tc_fond">\n' +
    '<map name="maphaut" id="maphaut">\n' +
      '<area id="shapeHautFermer" shape="rect" style="cursor:pointer" coords="167,6,217,16" alt="FERMER">\n' +
    '</map>\n' +
    '<img id="img_tc_haut" usemap="#maphaut">\n' +
    '<img id="tc_fond_img" class="tc_fond_img">\n' +
    '<div id="tc_text" class="text_div">\n' +
      '<div id="tc_votre_code" class="code_div">\n' +
        '<img id="legendeInput" alt="VOTRE CODE SECRET">\n' +
        '<div id="tc_pass" class="password_div">\n' +
          '<input type="text" id="tc_visu_saisie" value="" readonly size="6">\n' +
          '<a id="tc_aidelien" style="display: none">\n' +
            '<img id="tc_aideimg">\n' +
          '</a>\n' +
        '</div>\n' +
      '</div>\n' +
    '</div>\n' +
    '<map id="tc_tclavier" name="tc_tclavier"></map>\n' +
    '<img id="img_clavier" class="keyboard" usemap="#tc_tclavier">\n' +
    '<div id="tc_boutons" class="buttons">\n' +
      '<img id="tc_corriger" class="correct_img" style="cursor:pointer" alt="CORRIGER"><br>\n' +
      '<img id="tc_valider" class="valid_img" style="cursor:pointer" alt="VALIDER">\n' +
    '</div>\n' +
    '<img id="surlignage" style="cursor:pointer; position:absolute; left:-10000px; top:-10000px;">\n' +
  '</div>\n' +
'</div>\n' +
'<input id="cryptocvcs" type="hidden" name="cryptocvcs" value="">\n' +
'<input id="cvcs_visu_cache" type="hidden" value="">';

var VirtualKeyboardHTML = Class.create(VirtualKeyboard2, {
	initialize: function($super, elemId, inSession, codeFieldId, options) {
		var elem = $(elemId);
		elem.innerHTML = kbdHTML;
		$super(inSession, codeFieldId, options);
	}
});

var VirtualKeyboardHTML2 = Class.create(VirtualKeyboardHTML, {
	initialize: function($super, elemId, inSession, codeFieldId, options) {
		$super(elemId, inSession, codeFieldId, options);
		var linkTag = new Element('link', { 'rel': 'stylesheet', 'type': 'text/css',
			'media': 'screen', 'href': this.urlCSS });
		$$('head').first().insert(linkTag);
	}
});

function dragStart(event) {
	if (event.isLeftClick()) {
		/* Get cursor position with respect to the page. */
		dragObj.cursorStartX = event.pointerX();
		dragObj.cursorStartY = event.pointerY();

		/* Save starting positions of element. */
		dragObj.elStartLeft  = parseInt(dragObj.elNode.style.left);
		dragObj.elStartTop   = parseInt(dragObj.elNode.style.top);

		if (isNaN(dragObj.elStartLeft)) dragObj.elStartLeft = 0;
		if (isNaN(dragObj.elStartTop))  dragObj.elStartTop  = 0;

		/* Update element's z-index. */
		dragObj.elNode.style.zIndex = ++dragObj.zIndex;

		/* Capture mousemove and mouseup events on the page. */
		document.observe('mousemove', dragGo);
		document.observe('mouseup', dragStop);
		event.stop();
	}
}

function dragGo(event) {
	var x, y;

	/* Get cursor position with respect to the page. */
	x = event.pointerX();
	y = event.pointerY();

	/* Move drag element by the same amount the cursor has moved. */
	if (x > 0 && y > 0) {
		px = dragObj.elStartLeft + x - dragObj.cursorStartX;
		py = dragObj.elStartTop  + y - dragObj.cursorStartY;

		if (px > 0) dragObj.elNode.style.left = px + 'px';
		if (py > 0) dragObj.elNode.style.top  = py + 'px';
	}

	event.stop();
}

function dragStop(event) {
	/* Stop capturing mousemove and mouseup events. */
	document.stopObserving('mousemove', dragGo);
	document.stopObserving('mouseup', dragStop);
}

function activerSurlignage(event) {
	var data = $A(arguments);
	data.shift();
	var x = data.shift();
	var y = data.shift();

	this.curKey = event.element();

	var surlignage = $('surlignage');
	if (this.mode == constModeDefaut) {
		surlignage.style.top = ((y * 23) + 53) + 'px';
		surlignage.style.left = ((x * 24) - 3) + 'px';
	} else {
		surlignage.style.top = ((y * 46) + 122) + 'px';
		surlignage.style.left = ((x * 48) - 24) + 'px';
	}
	numCurX = x;
	numCurY = y;
}

function desactiverSurlignage(event) {
	var surlignage = $('surlignage');
	var off = surlignage.cumulativeOffset();
	var posx = event.pointerX();
	var posy = event.pointerY();
	if (!(posx > off.left && posx < off.left + surlignage.offsetWidth)
	    || !(posy > off.top && posy < off.top + surlignage.offsetHeight)) {
		surlignage.style.left = '-5000px';
		surlignage.style.top = '-5000px';
	}
	/* XXX duplicated with this.onKeyUnclicked() */
	if (this.mode == constModeDefaut)
		surlignage.src = this.urlBase + '/img/cvcs/tc_touche_cache_hover.gif';
	else
		surlignage.src = this.urlBase + '/img/cvcs/tc_touche_cache_hover_mv.gif';
}

function genererCoordAleatoire(coordOrigine, tailleClavier, tailleTotale)
{
	return Math.round(Math.random() * (tailleTotale - tailleClavier - 2 * coordOrigine)) + coordOrigine;
}

function playAllSelection() {
	var sonCode = $('pluginQt').down('object');
	if (sonCode) {
		sonCode.Rewind();
		sonCode.SetStartTime(0);
		sonCode.SetEndTime(10 * 666 - 50);
		sonCode.Play();
	}
}

function playSelection(digitIndex) {
	var sonCode = $('pluginQt').down('object');
	if (sonCode) {
		sonCode.Rewind();
		sonCode.SetStartTime(0);
		sonCode.SetEndTime((digitIndex + 1) * 666 - 50);
		sonCode.SetStartTime(digitIndex * 666);
		sonCode.Play();
	}
}

function armPlayNV(index) {
	disarmPlayNV();
	playTimeout = newPlayNV.delay(2, index);
}

function newPlayNV(index) {
	playSelection(index);
	playTimeout = null;
}

function disarmPlayNV() {
	if (playTimeout) {
		window.clearTimeout(playTimeout);
		playTimeout = null;
	}
}
