function n2g_estFormatString( maString, monFormat, op)
{
  var expREG = new RegExp(monFormat, op?op:"");
  if(expREG.test(maString)) return maString;
  else return false;
}

function n2g_estNumString( numString, min, max)
{
  return n2g_estFormatString(numString, "^[0-9]"+((min)?((max)?("{"+min+","+max+"}"):("{"+min+"}")):"+")+"$");
}

function n2g_ctrlSaisie(champ, msgSiVide, msgSiFormatErr, valSiErr, majSiOk, funCtrl, a1,a2,a3,a4,a5,a6,a7,a8,a9)
{
  var ret = true;
  if( champ.value.length > 0)
  {
    if(funCtrl)
    {
      ret = funCtrl(champ.value,a1,a2,a3,a4,a5,a6,a7,a8,a9);
      if(!ret && msgSiFormatErr) alert(msgSiFormatErr);
      if(ret && majSiOk) champ.value = ret;
    }
  }
  else
  {
    if(msgSiVide) {ret=false; alert(msgSiVide);}
  }
  if(!ret && valSiErr) champ.value = valSiErr;
  if(!ret && (msgSiVide || msgSiFormatErr) && champ.type != 'hidden') champ.focus();
  
  return ret;
}

/* Lance la function qui met le focus dans le champ de saisie (du moteur de recherche) au chargement de la page (necessite prototype.js) */
Event.observe(window, "load", function(){document.getElementById("codcli").focus();});
/* Ferme le clavier virtuel quand on met le focus de la souris dans le champ de saisie du code client*/
/* Lance le click  sur le codequand on met le focus de la souris dans le champ de saisie du code client*/
Event.observe(window, 'load', function() {
  Event.observe("codcli", "focus", function(){vk.hide();});
  Event.observe("codcli", "keypress", function(event){ if(event.keyCode == 13) {document.getElementById("button").click(); } });
});
