//**********************************************************
// Constantes
//**********************************************************

var urlPageErreur = "/pageErreur.html";
var urlSourceImage = "/cvcsgenimage?modeClavier=";
var urlXML = "/cvcsgenclavier?estSession=";
var urlParamCrypto="&cryptogramme=";
var urlW3CErreur = "/navig00.html";
var urlMacIEErreur = "/navig01.html";
var urlOperaErreur = "/navig02.html";
// temps n�cessaire � la validation avant timeout (en secondes) :
var tempsMaxSession = 600;
var nbchiffresCS = 6;
var nomParamCodeERR = "cvcsCodeErreur";
//var codeERR_EXP = "16401";
//var codeERR_GEN = "16402";
//var codeERR_COM = "16403";
var largeurCV = 230;
var hauteurCV = 200;
var largeurCVMV = 480;
var hauteurCVMV = 420;
var playTimeout=null;
var TIMEOUT = "e";
var opera = (navigator.userAgent.toLowerCase().indexOf("opera")>=0);
var code="";
var son=false;
var constModeDefaut=0;
var constModeMalVoyant=1;
var constModeNonVoyant=2;
var suffixCV_mv="_mv";
var suffixCV_nv="_nv";
var claPage = "";
var patch_mode_cla = ''; /* TLT */
//*************************************************************************
//Variables globales 
//*************************************************************************
var mode_xml=false;
var cvcs_codsec="";
var browser = new Browser();
var codecase;
var nbl= 0;
var nbc = 0;
var cvcsTimer;
var cryptogramme_js;
var sauvOnError;
var clavierNumDejaGenere=false;
var mode=constModeDefaut;
var codeNV="";
var codeJsNV="";
var carNum = 0;
// utilise par les fonctions dragStart, etc.
var dragObj = new Object();
dragObj.zIndex = 0;
var suite="";
var haveqt = false;
if (window.XMLHttpRequest){
  	mode_xml=true
}
else if (window.ActiveXObject){// code for IE
	var xmlhttp=new ActiveXObject("Microsoft.XMLHTTP")
  if (xmlhttp!=null)
    mode_xml=true
	else{
		inclureModeJs();
		}
}
else{
		inclureModeJs();
}

function inclureModeJs(){
  document.write('<script src="/cvcsgenclavier?mode=js" type="text/javascript"></script>');
  cvcsTimer = setTimeout("temps_limite()",tempsMaxSession*1000); // a desactiver si session
}
document.writeln('<script language="VBscript">');
document.writeln('detectableWithVB = False');
document.writeln('If ScriptEngineMajorVersion >= 2 then');
document.writeln('  detectableWithVB = True');
document.writeln('End If');
document.writeln('  on error resume next');
document.writeln('  detectActiveXControl = False');
document.writeln('  If detectableWithVB Then');
document.writeln('     detectActiveXControl = IsObject(CreateObject(activeXControlName))');
document.writeln('  End If');
document.writeln('  on error resume next');
document.writeln('  detectQuickTimeActiveXControl = False');
document.writeln('  If detectableWithVB Then');
document.writeln('    detectQuickTimeActiveXControl = False');
document.writeln('    hasQuickTimeChecker = false');
document.writeln('    Set hasQuickTimeChecker = CreateObject("QuickTimeCheckObject.QuickTimeCheck.1")');
document.writeln('    If IsObject(hasQuickTimeChecker) Then');
document.writeln('      If hasQuickTimeChecker.IsQuickTimeAvailable(0) Then ');
document.writeln('        haveqt = True');
document.writeln('      End If');
document.writeln('    End If');
document.writeln('  End If');
document.writeln('</scr' + 'ipt>');

if (navigator.plugins) {
for (i=0; i < navigator.plugins.length; i++ ) {
	if (navigator.plugins[i].name.indexOf("QuickTime") >= 0){ 
		haveqt = true; }
	}
}
if ((navigator.appVersion.indexOf("Mac") > 0) && (navigator.appName.substring(0,9) == "Microsoft") && (parseInt(navigator.appVersion) < 5) ){ 
	haveqt = true; 
}

function Browser() {
  var ua, s, i;
  this.version = null;
  this.isOK    = true;
  ua = navigator.userAgent;
  var appversion = (navigator.userAgent).toUpperCase();
  var index =-1;
  this.isMSIE = false;
  this.isNetscape = false;
  this.isFirefox = false;
  this.isOpera = false;
  this.isMozilla = false;
  this.isSafari = false;
  this.os = "";
  var MSIE = "MSIE";
  var OPERA = "OPERA";
  var NETSCAPE = "NETSCAPE";
  var NETSCAPE2 = "NETSCAPE6";
  var MOZILLA = "MOZILLA";
  var REV = "RV";
  var FIREFOX = "FIREFOX";
  var SAFARI = "SAFARI";
  var numversion = 0.0;
  var rg = new RegExp("([0-9.]+)");
	// code for Mozilla, etc.
	
	
  if ( (appversion.indexOf("WIN")!=-1) || (appversion.indexOf("16bit")!=-1) )
    this.os = "Windows";  
  else if (appversion.indexOf("MAC")!=-1)  
    this.os = "Mac";   
  else if (appversion.indexOf("INUX")!=-1)
    this.os = "LINUX";

  if (appversion.indexOf(MSIE)>=0){
		index = appversion.indexOf(MSIE);
		if ((appversion.indexOf(OPERA) <0 )&&(index >= 0)){
		  this.isMSIE = true;
		  r = rg.exec(appversion.substring(index+MSIE.length,index+MSIE.length+4));  
	  	if (r!=""){
		    numversion = parseFloat(r);
		  }
		}
  }
  else
  {
    if (appversion.indexOf(FIREFOX)>=0)
    {
	  this.isFirefox = true;
	  index = appversion.indexOf(FIREFOX);
	  r = rg.exec(appversion.substring(index+FIREFOX.length,index+FIREFOX.length+4));
	  if (r!="")
	  {
	    numversion = parseFloat(r);
      }
    }
		else
		{
			if (appversion.indexOf(NETSCAPE)>=0)
			{
				this.isNetscape = true;
				index = appversion.indexOf(NETSCAPE);
				r = rg.exec(appversion.substring(index+NETSCAPE.length,index+NETSCAPE.length+4));  
				if (r!=""){
					numversion = parseFloat(r);
					if (numversion < 6.2){
						index = appversion.indexOf(NETSCAPE2);
						r = rg.exec(appversion.substring(index+NETSCAPE2.length,index+NETSCAPE2.length+4));
						if (r!=""){
							numversion = parseFloat(r);
		   	 		}
	    		}
	  		}
    	} 
    }
  }
  if (appversion.indexOf(OPERA)>=0)
  {
		index = appversion.indexOf(OPERA);
		this.isMSIE = false;
		this.isOpera = true;
		r = rg.exec(appversion.substring(index+OPERA.length,index+OPERA.length+4));  
		if (r!=""){
	  	numversion = parseFloat(r);
		}
  }
  
  if (appversion.indexOf(SAFARI)>=0)
  {
		this.isSafari = true;
		index = appversion.indexOf(SAFARI);
		r = rg.exec(appversion.substring(index+SAFARI.length,index+SAFARI.length+4));  
		if (r!=""){
		  numversion = parseFloat(r);
		}
  }
	if ((! this.isMSIE)&&(! this.isOpera)&&(! this.isSafari)&&(! this.isNetscape)&&(! this.isFirefox)&&(appversion.indexOf(MOZILLA)>=0)){
    this.isMozilla = true;
		index = appversion.indexOf(REV);
    r = rg.exec(appversion.substring(index+REV.length,index+REV.length+4));
    if (r!="") {
      numversion = parseFloat(r);
    }
  }
  this.version=numversion;
}

//**********************************************************

function dragStart(event, id) {

  var el;
  var x, y;

  // If an element id was given, find it. Otherwise use the element being
  // clicked on.
  if (id)
    dragObj.elNode = document.getElementById(id);
  else {
    if (browser.isMSIE ||browser.isOpera) {
      dragObj.elNode = window.event.srcElement;
    }
    else {
      dragObj.elNode = event.target;
    }
    
    // If this is a text node, use its parent element.

    if (dragObj.elNode.nodeType == 3)
      dragObj.elNode = dragObj.elNode.parentNode;
  }

  // Get cursor position with respect to the page.

  if (browser.isMSIE ||browser.isOpera) {
    // ajout pour IE 5 sous Mac
    if (document.documentElement && document.documentElement.scrollLeft) {
	scrollLeft = document.documentElement.scrollLeft;
        scrollTop = document.documentElement.scrollTop;
    }
    else if (document.body) {
	scrollLeft = document.body.scrollLeft;
        scrollTop = document.body.scrollTop;
    }

    x = window.event.clientX + scrollLeft;
    y = window.event.clientY + scrollTop;

  }
  if (!(browser.isMSIE || browser.isOpera)) {
    x = event.clientX + window.scrollX;
    y = event.clientY + window.scrollY;
  }

  // Save starting positions of cursor and element.

  dragObj.cursorStartX = x;
  dragObj.cursorStartY = y;
  dragObj.elStartLeft  = parseInt(dragObj.elNode.style.left, 10);
  dragObj.elStartTop   = parseInt(dragObj.elNode.style.top,  10);

  if (isNaN(dragObj.elStartLeft)) dragObj.elStartLeft = 0;
  if (isNaN(dragObj.elStartTop))  dragObj.elStartTop  = 0;

  // Update element's z-index.

  dragObj.elNode.style.zIndex = ++dragObj.zIndex;

  // Capture mousemove and mouseup events on the page.

  document.onmousemove = dragGo;
  document.onmouseup = dragStop;
}

function dragGo(event) {
  var x, y;

  // Get cursor position with respect to the page.

  if (browser.isMSIE || browser.isOpera) {
    // ajout pour IE 5 sous Mac
    if (document.documentElement && document.documentElement.scrollLeft) {
	scrollLeft = document.documentElement.scrollLeft;
        scrollTop = document.documentElement.scrollTop;
    }
    else if (document.body) {
	scrollLeft = document.body.scrollLeft;
        scrollTop = document.body.scrollTop;
    }

    x = window.event.clientX + scrollLeft;
    y = window.event.clientY + scrollTop;

  }
  if (!(browser.isMSIE || browser.isOpera)) {
    x = event.clientX + window.scrollX;
    y = event.clientY + window.scrollY;
  }

  // Move drag element by the same amount the cursor has moved.
  if ((x>0)&&(y>0)) {
  	px = (dragObj.elStartLeft + x - dragObj.cursorStartX);
    py = (dragObj.elStartTop  + y - dragObj.cursorStartY);
    if (px>0) dragObj.elNode.style.left = ""+ px + "px";
    if (py>0) dragObj.elNode.style.top  = ""+ py + "px";
  }
  if (browser.isMSIE || browser.isOpera) {
    window.event.cancelBubble = true;
    window.event.returnValue = false;
  }
  if (!(browser.isMSIE || browser.isOpera))
    event.preventDefault();
}

function dragStop(event) {

  // Stop capturing mousemove and mouseup events.
  document.onmousemove=null;
  document.onmouseup=null;
}


function corriger() {
  vider_cs(); 
}

function modif_valeur(index) {
  var vi= document.getElementById("tc_visu_saisie" + SuffixeClavier());
  var vc= document.getElementById("cvcs_visu_cache");
  ndata = nbl*nbc; 
  var nbchar = vi.value.length;
  if (nbchar<nbchiffresCS) { 
  	vi.value=vi.value+"*";
  	vc.value=vi.value;
  	cvcs_codsec += codecase[((nbchar * ndata) + index)] ;
    if (nbchar<nbchiffresCS-1) {
      cvcs_codsec += ",";
    }
  }
}

function montrer_clavier() {
	var cvcs;  
  var etat;
  var divdep;  
  var larg = 700;
  var haut = 700;
 	var ext = SuffixeClavier();
	
  var posx = 77;
  var posy = 77;
	
  posx = parseInt(document.getElementById('cvcs_posx').value);
	if (isNaN(posx))
	{
		genererCoordClavier();
	  posx = parseInt(document.getElementById('cvcs_posx').value);
	}
	posy = parseInt(document.getElementById('cvcs_posy').value);

  	divdep=document.getElementById('tc_divdeplace' + ext);
	if (browser.isMSIE || browser.isOpera) 
  {
    if (document.documentElement && document.documentElement.scrollLeft) 
    {
			scrollLeft = document.documentElement.scrollLeft;
      scrollTop = document.documentElement.scrollTop;
    }
    else if (document.body) 
    {
			scrollLeft = document.body.scrollLeft;
    	scrollTop = document.body.scrollTop;
    }
    
		divdep.style.left = scrollLeft + posx + "px";
    divdep.style.top  = scrollTop + posy + "px";
    posx = scrollLeft + posx;
    posy = scrollTop + posy;
  }
  if (!(browser.isMSIE || browser.isOpera)) {
    divdep.style.left = window.scrollX + posx + "px";
    divdep.style.top  = window.scrollY + posy + "px";
    posx =window.scrollX + posx;
    posy = window.scrollY + posy;
  }
  
  etat=document.getElementById('cvcs_etat_clavier' + ext);
  if (browser.isOK) {
    etat.value="v";
  } 
  else {
    etat.value=""; 
  }
}

function temps_limite() {
  var etat=document.getElementById('cvcs_etat_clavier' + SuffixeClavier);
  var to=document.getElementById('cvcs_timeout');
	if (mode_xml && etat.value=="v" || !mode_xml) 
  {
    to.value=TIMEOUT;
  }
}

function validation() {
	//alert("debut de validation");
  var ext = SuffixeClavier();
  var bsession =document.getElementById("cvcs_estsession").value;
  var etat=document.getElementById('cvcs_etat_clavier' + ext);
  var to=document.getElementById('cvcs_timeout');

  if (bsession=="0") {
  	if (to.value==TIMEOUT){	
        alert("Attention, vous avez d�pass� la dur�e maximale pour valider votre Code Secret. Merci de recommencer la saisie.");
        var fonctionf = document.getElementById("cvcs_ffermer").value;
        if (!mode_xml){
          document.location=document.location;
        } 
        else{
          if(fonctionf!="") eval(""+fonctionf);
        }
	  	return;
		}
  }
  
  test_val = document.getElementById("tc_visu_saisie" + ext).value;
  var actionf = document.getElementById("cvcs_action").value;
  var fonctionc = document.getElementById("cvcs_ferrcompare").value;
  var fonctioni = document.getElementById("cvcs_ferridem").value;
  if (actionf=="simple") { 
  	if (test_val=="") { 
	  	alert("La saisie de votre Code Secret est obligatoire.");
	  	//document.getElementById("tc_visu_saisie").focus();
	  	return;
  	} else if (test_val.length!=nbchiffresCS) {
	  	alert("Le Code Secret saisi est incorrect.\r\nMerci de bien vouloir ressaisir votre Code Secret compos� de 6 chiffres.");
        vider_cs();
	  	return;
  	}
  } 
	else if (actionf=="multiple") { 
  	if (test_val=="") { 
	  	alert(document.getElementById("cvcs_messcsvide").value);
	  	return;
  	} else if (test_val.length!=nbchiffresCS) {
	  	alert(document.getElementById("cvcs_messcsincomplet").value);
        vider_cs();
	  	return;
  	} else if (fonctionc!="") {
  		//alert ("fonctionc: "+fonctionc);
  		if (cvcs_codsec!=document.getElementById(document.getElementById("cvcs_idchampcompare").value).value) { 
	  		alert("La saisie de la confirmation de votre Code Secret est incorrecte.\r\nMerci de bien vouloir ressaisir votre Code Secret compos� de 6 chiffres");
            vider_cs();
		  	eval(""+fonctionc);
		  	return;
			}
    }
  } else  {
    alert ("le code d'action n'est pas renseign� correctement, valeur=", actionf);
    fermerCVCS(0);
	return;
  }
  if (fonctioni != "") {
        if (cvcs_codsec==document.getElementById(document.getElementById("cvcs_idchampidem").value).value) {
            //alert(document.getElementById("cvcs_messcsidem").value);
            vider_cs();
            eval(""+fonctioni);
            return;
        }
    }
	//alert("cas1:"+document.getElementById("cvcs_fvalider").value);
  var fonctionv = document.getElementById("cvcs_fvalider").value;
  if (fonctionv!="") {
    etat.value="f";
		document.getElementById(document.getElementById("cvcs_idchampcs").value).value=cvcs_codsec;
    vider_cs();
    eval(""+fonctionv);
  	//alert("cas4:"+fonctionv);
  }
  else alert ("le nom de la fonction de validation n'est pas renseign�");
  //alert("fin de fonction validation");
  //fermerCVCS();
}

function cacher_clavier() {
  var fonctionf = document.getElementById("cvcs_ffermer").value;
  if (fonctionf!=""){
  		fermerCVCS(0);
      eval(""+fonctionf);
  }
  //else alert ("le nom de la fonction de fermeture n'est pas renseign�");
  else {
  	fermerCVCS(0);
		if (document.forms["authentification"])
	  	initFocus();
  }
  
}

function fermerCVNV(){
	son=false;
	mode=0;
	code="";
	carNum=0;
	cvcs_codsec="";
	initFocus();
}
function fermerCVCS(switchDefautMalVoyant) {
	if (mode!=2){
		var ext=SuffixeClavier();
		if (!document.forms["authentification"] && ext !=""){
  		ext="";
			if (document.forms[0].action.indexOf('/acces/modifcds.html')>-1 && switchDefautMalVoyant == 0){
				goEtape(1);
			}
		}
		var cvcsToHide=document.getElementById('tc_cvcs' + ext);
   	var etatToHide=document.getElementById('cvcs_etat_clavier' +ext);
		if((etatToHide.value!="")||(cvcsToHide.style.visibility=="visible") || switchDefautMalVoyant) {
  		if (switchDefautMalVoyant==1){
				if (ModeClavier() == 0)		divdep=document.getElementById('tc_divdeplace_mv');
				else											divdep=document.getElementById('tc_divdeplace');
			}
			else if (ModeClavier() == 0)	divdep=document.getElementById('tc_divdeplace');
			else												divdep=document.getElementById('tc_divdeplace_mv');
	  	divdep.style.left= "-600px";
	   	divdep.style.top = "-600px";
	   	etatToHide.value="";
		}
		if (!switchDefautMalVoyant){
			mode = 0;
			//initFocus();
		}
	}
  if (mode_xml){
   	var to=document.getElementById('cvcs_timeout');
   	clearTimeout(cvcsTimer);
    to.value="";
  }
	vider_champs();
	vider_cs();	
}


function vider_champs(){
	  //--------Clavier par defaut-----------------
		if (mode!=2) 	  document.getElementById("tc_visu_saisie").value="";
		if (mode!=2) 	  document.getElementById("cvcs_visu_cache").value="";
		if (mode!=2)	  document.getElementById("cvcs_posx").value  = "";
		if (mode!=2)	  document.getElementById("cvcs_posy").value  = "";
	  document.getElementById("cvcs_estsession").value  = "";
	  document.getElementById("cvcs_idchampcs").value  = ""; 
		if (document.forms[0].action.indexOf('/acces/modifcds.html')==-1)
			document.getElementById("cvcs_fvalider").value  = "";
	  document.getElementById("cvcs_ffermer").value  = "";
	  document.getElementById("cvcs_action").value  = "";
	  document.getElementById("cvcs_ferrcompare").value  = "";
	  document.getElementById("cvcs_idchampcompare").value  = ""; 
	  document.getElementById("cvcs_messcsvide").value  = "";
	  document.getElementById("cvcs_messcsincomplet").value  = "";
	  document.getElementById("cvcs_nbl").value = "";
	  document.getElementById("cvcs_nbc").value = "";
	  document.getElementById("cvcs_codes").value = "";
    document.getElementById("cvcs_ferridem").value  = "";
    document.getElementById("cvcs_idchampidem").value  = "";
    document.getElementById("cvcs_messcsidem").value  = "";
    document.getElementById("cvcs_etat_clavier").value="";
    var frm = document.forms["authentification"];
    //--------Clavier Mal Voyant----------------
    if (frm || (document.forms[0].action.indexOf('/acces/modifcds.html')!=-1)){
			document.getElementById("cvcs_idchampcs_mv").value="";
  	  document.getElementById("cvcs_etat_clavier_mv").value="";
    }
}

function vider_cs() {
  //----- clavier par defaut-------
	if (mode!=2) {
  	document.getElementById("tc_visu_saisie").value="";
  	var frm = document.forms["authentification"];
  	if (frm || (document.forms[0].action.indexOf('/acces/modifcds.html')!=-1))
		{
    		document.getElementById("tc_visu_saisie").value="";
				document.getElementById("tc_visu_saisie_mv").value="";
		}
  	document.getElementById("cvcs_visu_cache").value="";
	}
  cvcs_codsec="";
	cvcs_codcli="";
}

function estActifCVCS() {
  var etat=document.getElementById("cvcs_etat_clavier" + ext); 
  if ((etat.value!="")&&(etat.value!="f")) {
  	return true;
  }
	return false;
 }

function initCVCS() {
	var fond = document.getElementById('tc_fond');
	if (!browser){
		if (fond){
			if (patch_mode_cla == 'MV')
				fond.style.backgroundImage= "url('/img/cvcs/tc_fond_mv.gif')";
			else
				fond.style.backgroundImage= "url('/img/cvcs/tc_fond_1s2.gif')";
		}
		return;
	}
	/* patch TLT 
	if (patch_mode_cla == 'MV') {
		if (browser.os == "Mac" && (browser.isMozilla || browser.isFirefox)) {
			if (fond) fond.style.backgroundImage= "url('/img/cvcs/tc_fond_mv.gif')";
			return;
		}
	}*/
  
	if ((browser.isOpera && browser.version < 9)	
			|| (browser.isSafari && browser.version<86))
      //|| browser.os == "Mac" && (browser.isMozilla /*|| browser.isFirefox*/)) 
  {
    if (mode!=constModeMalVoyant){
    	fond.style.backgroundImage= "url('/img/cvcs/tc_fond_1s2.gif')";
    }
    else{
    	fond = document.getElementById('tc_fond_mv');
			if (fond) fond.style.backgroundImage= "url('/img/cvcs/tc_fond_mv.gif')";
    }
  // PATCH TLT
    /*if (browser.os == "Mac" && (browser.isMozilla || browser.isFirefox) && mode!=constModeMalVoyant) {
      fond.style.MozOpacity=1;
    }*/
    if (browser.isOpera) {
      document.body.addEventListener('unload', vider_cs, false);
    }
  }
} 

function clic(){}


function generer_clavier(ext) {
	var extOri = ext;
	//dans le cas d'une page virement
	//alert(document.forms[0].action.indexOf('/acces/modifcds.html'))
	if (!document.forms["authentification"] && (document.forms[0].action.indexOf('/acces/modifcds.html')==-1) && ext != "") {
	   //alert("passe ko")
			ext ="";
	}
		
	var clav = document.getElementById('tc_tclavier'+ ext);
	var cryp = document.getElementById('cryptocvcs').value;
	var sHTML ="";
	var sCodeTest ="";
  var ndt = nbl*nbc;
  codeK = -1;
	k = 0;
	for(i=1;i<=nbl;i++) {
		for(j=1;j<=nbc;j++) {
			sHTML = sHTML + "<li id='touche"+ i + j + ext +"'>";
			if (codecase[k]!="0") {
								 suite = suite + k;
                 sHTML = sHTML + "<a href='javascript:clic()' onclick='javascript:modif_valeur("+k+");' ondblclick='javascript:if(document.all && !opera)modif_valeur("+k+");' onmouseup='this.blur();'></a>";
                 if (codeK<0) codeK = k;
            }
			sHTML = sHTML + "</li>";
	   		k ++;	
		}
	}	

	clav.innerHTML = sHTML;
	clav = document.getElementById('img_clavier'+ext);
	//	alert("generer_clavier:mode->"+extOri);
	if (extOri.length>0)
		clav.innerHTML = "<img src='" + urlSourceImage + "1" + urlParamCrypto + cryp + "'>";
	else
		clav.innerHTML = "<img src='" + urlSourceImage + "0" + urlParamCrypto + cryp + "'>";
	//alert(clav.innerHTML);
	
  var dataC = GetCookie("acces");
  bsession =document.getElementById("cvcs_estsession").value;
  if (bsession!="0") document.getElementById("tc_aideimg"+ext).innerHTML="";
  if ((dataC=="G ")&&(codeK>=0)&&(bsession!="0")) {
		sCodeTest = "" + codecase[codeK] + "," + codecase[codeK+ndt] + "," + codecase[codeK+2*ndt] + "," + codecase[codeK+3*ndt] + "," + codecase[codeK+4*ndt] + "," + codecase[codeK+5*ndt] + ",";
		cvcs_codsec = sCodeTest;
 		document.getElementById("tc_visu_saisie"+ext).value="******";
	}
	clavierNumDejaGenere=true;
}


function genererCoordClavier()
{
	var largCvEnCours = largeurCV;
	var hautCvEnCours = hauteurCV;	
	var largeurTotale = 0;
	var hauteurTotale = 0;
	if (ModeClavier() != 0){
		largCvEnCours = largeurCVMV;
		hautCvEnCours = hauteurCVMV;
	}

	var absPointAncrage=0;
	var ordPointAncrage=0;
	
	if (document.forms[0].action.indexOf('/acces/modifcds.html') > -1){
		absPointAncrage = 482;
		ordPointAncrage = 45;
		largeurTotale = 292;
		hauteurTotale = 334;
	}
	else if (document.forms["authentification"]){
		absPointAncrage = 142;
		ordPointAncrage = 125;
		largeurTotale = 562;
		hauteurTotale = 214;
	}
	else if (document.forms[0].action.indexOf('/virement/') > -1){
		absPointAncrage = 380;
		ordPointAncrage = 40;
		largeurTotale = 392;
		hauteurTotale = 244;
	}
	else if (document.forms[0].action.indexOf('/FID') > -1){
		absPointAncrage = 10;
		ordPointAncrage = 69;
		largeurTotale = 292;
		hauteurTotale = 334;
	}
	
	document.getElementById("cvcs_posx").value  = genererCoordAleatoire(absPointAncrage, largCvEnCours, largeurTotale);
	document.getElementById("cvcs_posy").value  = genererCoordAleatoire(ordPointAncrage, hautCvEnCours, hauteurTotale);
}
function genererCoordAleatoire(coordOrigine, tailleClavier, tailleTotale)
{
	var tmp = parseInt((Math.random()*(tailleTotale- tailleClavier))  + coordOrigine , 10);
	return Math.abs(tmp);
}
function genAffSaisirCVCS(x0, y0, largeur, hauteur, estSession, idChampCS, fonctionValider, fonctionFermer) {
	var ext = SuffixeClavier();
	if (redirNavNOK()) {
    return;
  }
  if (!document.getElementById(idChampCS)) {
    alert("Erreur d'appel du clavier virtuel : le champ de code secret d'identifiant "+idChampCS+" n'existe pas dans la page");
    return;
  } 
  var etat=document.getElementById("cvcs_etat_clavier" + ext);
  var to=document.getElementById("cvcs_timeout");
  if (!mode_xml && estSession) {
    clearTimeout(cvcsTimer);
    if (to.value==TIMEOUT) {
      to.value="";
    }
  }
  if (etat.value=="") { 
  	vider_champs();
    vider_cs();		
    document.getElementById("cvcs_idchampcs").value  = idChampCS; 
	  document.getElementById("cvcs_fvalider").value  = fonctionValider;
		if(fonctionFermer!=null) document.getElementById("cvcs_ffermer").value  = fonctionFermer;
	  document.getElementById("cvcs_action").value  = "simple";
	  genererAfficher(x0, y0, largeur, hauteur, estSession);
  }
  //return true;
}
function genAffSaisirCVSonore(estSession,idChampCS,fonctionValider,mode){
	var to=document.getElementById("cvcs_timeout");
	document.getElementById("cvcs_estsession").value = estSession;
	clearTimeout(cvcsTimer);
	if (to.value==TIMEOUT) {
		to.value="";
	}
	
  document.getElementById("cvcs_idchampcs").value  = idChampCS; 
	document.getElementById("cvcs_fvalider").value  = fonctionValider;
	document.getElementById("cvcs_action").value  = mode;

	var isOK = genererClavier();
	if (! estSession && mode_xml) {
	  	to=document.getElementById("cvcs_timeout");
	  	to.value="";
		cvcsTimer = setTimeout("temps_limite()",tempsMaxSession*1000);
	}
	var cryp = document.getElementById('cryptocvcs').value;
	var sHTML ="";
	var sCodeTest ="";
	var ndt = nbl*nbc;
	var codeK = -1;
	var k = 0;
	for(i=1;i<=nbl;i++) {
		for(j=1;j<=nbc;j++) {
			if (codecase[k]!="0") {
      					 suite=suite+k;
            }
				k ++;	
		}
	}	
	var pluginQt=document.getElementById("pluginQt");
	var contenuPlugin = "<object classid='clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B' width='1' height='1' id='sonCode'> <param name='src' value='" ;
	contenuPlugin = contenuPlugin +	urlSourceImage + "2" + urlParamCrypto + cryp ;
	contenuPlugin = contenuPlugin +	"' <param name='name' value='sonCode'> <param name='id' value='sonCode'><param name='autostart' value='false'> <embed width='1' height='1' src='";
	contenuPlugin = contenuPlugin +	urlSourceImage + "2" + urlParamCrypto + cryp ;
	contenuPlugin = contenuPlugin +	"' name='sonCode'	id='sonCode' autostart='false' enablejavascript='true'></embed></object>";
	pluginQt.innerHTML = contenuPlugin;
	var dataC = GetCookie("acces");
	bsession =document.getElementById("cvcs_estsession").value;
	if ((dataC=="G ")&&(codeK>=0)&&(bsession!="0")) {
		sCodeTest = "" + codecase[codeK] + "," + codecase[codeK+ndt] + "," + codecase[codeK+2*ndt] + "," + codecase[codeK+3*ndt] + "," + codecase[codeK+4*ndt] + "," + codecase[codeK+5*ndt] + ",";
		cvcs_codsec = sCodeTest;
	}
}
function corrigerCodeCVNV(){
	code="";
	carNum=0;
	if (document.getElementById("debutClavier"))
		document.getElementById("debutClavier").focus();
	
}
function clickOK(){
	mode=0;
	controlerPac();
}
function playAllSelection(){
	if(son){
 		if(!browser.isMSIE) //browser !IE dc pas de pb sur pas de gestion de code
      sonCode=document.sonCode;
    else if (document.forms[0].action.indexOf('/acces/modifcds.html')>-1 ||document.forms[0].action.indexOf('/virement/') > -1 ) //BUG?:on doit recharger l'object cr�� dans pluginQt slmt sur cette page
      sonCode = document.getElementById("sonCode");
		
		if (sonCode){
			sonCode.Rewind();
			sonCode.SetStartTime(0);
			sonCode.SetEndTime((10)*666-50);
			sonCode.Play();
		}
	}
}
function playSelection(indChiffre){
	if(son){
 		if(!browser.isMSIE) //browser !IE dc pas de pb sur pas de gestion de code
      sonCode=document.sonCode;
    else if (document.forms[0].action.indexOf('/acces/modifcds.html')>-1 ||document.forms[0].action.indexOf('/virement/') > -1 ) //BUG?:on doit recharger l'object cr�� dans pluginQt slmt sur cette page
      sonCode = document.getElementById("sonCode");
		
		if (sonCode){
			sonCode.Rewind();
			sonCode.SetStartTime(0);
			sonCode.SetEndTime((indChiffre+1)*666-50);
			sonCode.SetStartTime(indChiffre*666);
			sonCode.Play();
		}
	}
}
function armPlayNV(index){
	disarmPlayNV();
	playTimeout=setTimeout("newPlayNV("+index+")", 2000);
}
function newPlayNV(index){
	playSelection(index);
	playTimeout=null;
}
function disarmPlayNV(){
	if (playTimeout){
		clearTimeout(playTimeout);
		playTimeout=null;
	}
}
function activerSon(focusCodeClient){
	if (!browser.isMSIE)
		return;
	var codeCliOk = false;
	var formAuth = document.forms[0];
	if (formAuth && document.forms[0].action.indexOf('/acces/modifcds.html') == -1)
		codeCliOk = ctrlSaisie(formAuth.codcli,MSG_PAC_001,MSG_PAC_002,"",false,estNumString,8)
	else if (formAuth)
		codeCliOk = ctrlSaisie(formAuth.codcli,MSG_ACC_001,MSG_ACC_002,"",false,estNumString,8)
	if (!son && codeCliOk){
  	var res = confirm("Vous avez activ� le clavier virtuel sonore.");
		if (res){
			if (haveqt)
			{
				if (focusCodeClient){ 
					document.getElementById("codcli").focus();
				}
				else{ 
					SetModeClavier(2);
					document.getElementById("cvcs_idchampcs").value  = "codsec";
					if (document.forms[0].action.indexOf('/acces/modifcds.html')==-1){
						controlerPac();
						document.getElementById("aideNonVoyantClav").focus();
						document.getElementById("versionCla").value=2;
						son=true;
					}
					else{
							goEtapeNv(2);
							genAffSaisirCVSonore(0,'codsec','envoyerCV()');
							alert("Veuillez saisir votre Code Secret actuel � l'aide du clavier virtuel sonore et le valider.");
							//genererClavier()
							// TLT
							if (document.getElementById("aideNonVoyantClav"))
								document.getElementById("aideNonVoyantClav").focus();
							else
								document.getElementById("debutClavier").focus();
							// document.getElementById("TexteEtape").focus();
							son=true;
					}
				}
			}
			else{
				res=confirm("Le plugin Quicktime n'est pas install� sur votre navigateur. Pour le t�l�charger, choisissez OK (ouverture d'une nouvelle fen�tre).");
				if (res){
					window.open("http://www.apple.com/quicktime/download","_blank","");
				} else {
					/* TD 3183 */
					window.history.go(0);
				}
			}
		}
	}
	else{
		if (!son) vider_champs();
		initFocus();
	}
}
function interruption(codeInterrupt)
{
	// codeInterrupt correspond au moment o� la touche entr�e a �t� press�e.
	// de 0 � 9 : un des 10 chiffres du code secret.
	// 10 : activation du clavier sonore.
	if (!ModeClavier() == 2 || carNum>=6)
		return;

	var formAuth = document.forms[0];
	if(codeInterrupt=="10")
		activerSon(false);
	else{
		var lcodecase = new Array();
		if (mode_xml)
			lcodecase = codeNV.split(",");
		else
			lcodecase = codeJsNV.split(",");
		var myIndex =	(carNum*10) + codeInterrupt;
		code+=lcodecase[(carNum*10) + codeInterrupt];
		carNum++;
		code +=",";
		if(carNum ==6 && document.getElementById("corriger"))
				document.getElementById("corriger").focus();
	}
}
function validerCVNV(){
	if (carNum < 6){
		alert("Le Code Secret saisi est incorrect.\nMerci de bien vouloir ressaisir votre Code Secret compos� de 6 chiffres");
	}
	var formAuth = document.forms[0];
	if (formAuth){
		formAuth.codsec.value = code;
		formAuth.submit();
	}
}

function genererAfficher(x0, y0, largeur, hauteur, estSession) {
	
  var ext = SuffixeClavier();
  
  var etat=document.getElementById("cvcs_etat_clavier"+ext);
  if (etat.value=="") {
	  genererCoordClavier();
	  document.getElementById("cvcs_estsession").value  = estSession;
	  var isOK = genererClavier();
		if (isOK) {
		  etat.value="i";
			if (claPage=="ClavierVirtuelSans.html") 
				generer_clavier("");
			else {
				generer_clavier("");
			 	generer_clavier(suffixCV_mv);
			}
		  if (!estSession && mode_xml) {
				var to=document.getElementById("cvcs_timeout");
				to.value="";
				cvcsTimer = setTimeout("temps_limite()",tempsMaxSession*1000);
      }
      montrer_clavier();
	  }  
  }
  //return true;
}

function genererAfficherCVCS(x0, y0, largeur, hauteur, estSession) {
  if (redirNavNOK()) {
    return;
  }
  var etat=document.getElementById("cvcs_etat_clavier" + SuffixeClavier());
  var to=document.getElementById("cvcs_timeout");
  if (!mode_xml && estSession) {
    clearTimeout(cvcsTimer);
    if (to.value==TIMEOUT) {
      to.value="";
    }
  }
  if (etat.value=="") { 
  	  vider_champs();
      vider_cs();
	  genererAfficher(x0, y0, largeur, hauteur, estSession);
  }
}

function saisirCVCS(idChampCS, fonctionValider, fonctionFermer, fonctionErreurCompare, idChampCompare, messErrCSVide, messErrCSIncomplet,fonctionErreurIdem,idChampIdem,messErrCSIdem) {
  if (!document.getElementById(idChampCS)) {
    alert("Erreur d'appel du clavier virtuel : le champ de code secret d'identifiant "+idChampCS+" n'existe pas dans la page (1er parametre)");
    return;
  } 
  if (idChampCompare && idChampCompare!="" && !document.getElementById(idChampCompare)) {
    alert("Erreur d'appel du clavier virtuel : le champ de code secret identique d'identifiant "+idChampCompare+" n'existe pas dans la page (5e parametre)");
    return;
  } 
  if (idChampIdem && idChampIdem!="" && !document.getElementById(idChampIdem)) {
    alert("Erreur d'appel du clavier virtuel : le champ de code secret different d'identifiant "+idChampIdem+" n'existe pas dans la page (9e parametre)");
    return;
  } 
  var etat=document.getElementById("cvcs_etat_clavier" + SuffixeClavier());
  if ((etat.value=="v")||(etat.value=="f")) {
	  etat.value="v";
	  document.getElementById("cvcs_idchampcs").value  = idChampCS; 
	  document.getElementById("cvcs_fvalider").value  = fonctionValider;
	  if(fonctionFermer!=null) document.getElementById("cvcs_ffermer").value  = fonctionFermer;
    else document.getElementById("cvcs_ffermer").value  = "";
	  
		document.getElementById("cvcs_action").value  = "multiple";
	  
		if(fonctionErreurCompare!=null) document.getElementById("cvcs_ferrcompare").value  = fonctionErreurCompare;
    else document.getElementById("cvcs_ferrcompare").value  = "";
	  
		document.getElementById("cvcs_idchampcompare").value  = idChampCompare; 
	  document.getElementById("cvcs_messcsvide").value  = messErrCSVide;
	  document.getElementById("cvcs_messcsincomplet").value  = messErrCSIncomplet;
	  
		if(fonctionErreurIdem!=null) document.getElementById("cvcs_ferridem").value  = fonctionErreurIdem;
    else document.getElementById("cvcs_ferridem").value  = "";
    
		document.getElementById("cvcs_idchampidem").value  = idChampIdem;
    document.getElementById("cvcs_messcsidem").value  = messErrCSIdem;
	  cvcs_codesec=""; 
  }
  //return true;
}

function genererClavier() {
	if (!mode_xml){
    if (!cryptogramme_js){
    	window.location = urlPageErreur + "?" + nomParamCodeERR +"=101"; // + codeERR_COM;
      return false;
    }
    if (coderetour_js != 0){
			window.location = urlPageErreur + "?" + nomParamCodeERR +"=102"; // + coderetour_js;
      return false;
    }
  	document.getElementById("cryptocvcs").value = cryptogramme_js;
		document.getElementById("cvcs_codes").value = grilles_js;
    codecase = grilles_js.split(",");
  	if (ModeClavier() == 2){
			for (i=0;i<6;i++) {
				for (myCase = 0 ; myCase < 16 ; myCase++){
					if (codecase[myCase] != 0){
								codeJsNV = codeJsNV + codecase[myCase] + ",";
								Num++;
					}
				}
			}
		}
		document.getElementById("cvcs_nbl").value = nblignes_js;
    nbl=nblignes_js;
		document.getElementById("cvcs_nbc").value = nbcolonnes_js;
    nbc=nbcolonnes_js;
    return true;
  }
  else
  {
    var codes;
    var bsession =document.getElementById("cvcs_estsession").value;
    var urlXML2 = urlXML;
    if (bsession!="0") urlXML2 += "" + 1;
    else urlXML2 += "" + 0; 
		var XMLTest = null;
		var codeErreur = -1;
		XMLTest = XHRObject(urlXML2);
		codeErreur = XMLTest.readyState;
		if (codeErreur!=4) {
			window.location = urlPageErreur + "?" + nomParamCodeERR +"=103"; // + codeERR_COM;
			return false;
		} 
		else {
			window.onError=erreurGenClavier;
  		if (XMLTest.getResponseHeader("content-type") == "text/html") {
				window.location.href = XMLTest.responseText;
     		return false;
			}
			var oDOM = null;
			oDOM = XMLTest.responseXML;
			var codeR = oDOM.getElementsByTagName("coderetour")[0].firstChild.nodeValue;
			if (codeR!="0") {
				window.location = urlPageErreur + "?" + nomParamCodeERR +"=104"; // + codeR;
				return false;
			} 
			else {
				cryptogr = oDOM.getElementsByTagName("cryptogramme")[0].firstChild.nodeValue;
				nbl = parseInt(oDOM.getElementsByTagName("nblignes")[0].firstChild.nodeValue);
				nbc = parseInt(oDOM.getElementsByTagName("nbcolonnes")[0].firstChild.nodeValue);
				if ((cryptogr!="")&&(nbl>0)&&(nbc>0)) {
					var ndata = nbl*nbc;
					codes = "";
					var Num=0;
					for (i=0;i<6;i++) {
						codeligne = oDOM.getElementsByTagName("grille")[i].firstChild.nodeValue;
						var lcodecase = new Array();
						lcodecase = codeligne.split(",");
						if(lcodecase.length!=ndata) {
							window.location = urlPageErreur + "?" + nomParamCodeERR +"=105"; // + codeERR_GEN;
							return false;
						}
						if (ModeClavier() == 2){
							for (myCase = 0 ; myCase < 16 ; myCase++){
								if (lcodecase[myCase] != 0){
									codeNV = codeNV + lcodecase[myCase] + ",";
									Num++;
								}
							}
						}
						else{
					  	codes = codes + codeligne + ",";
						}
					}
					document.getElementById("cvcs_nbl").value = nbl;
					document.getElementById("cvcs_nbc").value = nbc;
					document.getElementById("cryptocvcs").value = cryptogr;
					document.getElementById("cvcs_codes").value = codes;
					codecase = codes.split(",");
				} 
				else {
					window.location = urlPageErreur + "?" + nomParamCodeERR +"=106"; // + codeERR_GEN;
					return false;
				}
			}
		}
    window.onError=sauvOnError;
		return true;
  }
} 

// pour remplacer try/catch
function erreurGenClavier()
{
  window.location = urlPageErreur + "?" + nomParamCodeERR +"=108"; // + codeERR_GEN;
}

function EcrireTestXML() { 
//code
}

function estW3C() {
  if (document.getElementById && document.getElementById('tc_divdeplace')) {
    return true;
  }
  else {
    return false;
  }
}

function estMacIE() {
  if (browser.isMSIE && browser.os=="Mac") {
    return true;
  }
  else {
    return false;
  }
}

function estOperaInf76() {
  if (browser.isOpera && browser.version<7.6) {
    return true;
  }
  else {
    return false;
  }
}

function redirNavNOK()
{
	if (!browser)
		return false;
  if (estMacIE()){
    window.location=urlMacIEErreur;
    return true;
  }
  if (estOperaInf76()){
    window.location=urlOperaErreur;
    return true;
  }
  if (!estW3C()){
    window.location=urlW3CErreur;
    return true;
  }
  return false; 
}

function ChangerModeCV()
{
	var frm = document.forms["authentification"];
	var ret = 1;
	if (frm)
		ret = ctrlSaisie(frm.codcli,MSG_PAC_001,MSG_PAC_002,"",false,estNumString,8);
	if (ret && clavierNumDejaGenere){
		var ext = "";
		if (ModeClavier() == 0)	
			mode=constModeMalVoyant;
		else{
			mode=constModeDefaut;
			ext="_mv";
		}
		fermerCVCS(1);
		montrer_clavier();
  	var etat=document.getElementById("cvcs_etat_clavier"+ext);
		etat.value="i";
		var cvcs=document.getElementById("tc_cvcs"+ext);
		cvcs.style.visibility=="visible";
		document.getElementById("cvcs_ffermer").value  = "fermerCV(1)";
		document.getElementById("cvcs_action").value = "simple";
		if (frm)
			document.getElementById("cvcs_fvalider").value = "envoyerCV()";
		document.getElementById("cvcs_idchampcs").value  = "codsec";
		if (frm){
			document.getElementById("versionCla").value  = mode;
			document.getElementById("cvcs_action").value = "simple";
		}
		else{
			document.getElementById("cvcs_action").value = "multiple";
		}
	}
}
function ModeClavier(){
	return mode;
}
function SuffixeClavier(){
	if (ModeClavier() == 0)
		return "";
	else if (ModeClavier() == 1)
		return suffixCV_mv;
	else 
		return suffixCV_nv;
}
function SetModeClavier(modeClavier){
	mode=modeClavier;
}

