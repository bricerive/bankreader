var MSG_PAC_001="Vous devez saisir votre code client.";
var MSG_PAC_002="La saisie de votre code client est incorrecte.\nPour en savoir plus, cliquez sur le ? � c�t� du code client,\nou reportez-vous au paragraphe 'Code Client, Code Secret' de l'aide en ligne.";
var MSG_PAC_003="Vous devez saisir votre code secret.";
var MSG_PAC_004="La saisie de votre code secret est incorrecte.\nPour en savoir plus, cliquez sur le ? � c�t� du code secret,\nou reportez-vous au paragraphe 'Code Client Code Secret' de l'aide en ligne.";
var lien_secu = "http://par.societegenerale.fr/EIP/resources/production/actu_logitelnet/";

var acces = GetCookie("acces");
if (acces == null || acces == "NN")
{
SetCookie("acces","NN",null,"/",null,true);
SetCookie("type","NN",null,"/",null,true);
DeleteCookie("idUser",null,null,"/",null,true);
DeleteCookie("Crypto",null,null,"/",null,true);
}

var onverif = false;
var ccSaisi = false;

function controlerPac()
{
  var ret = false;
  var categNav= "";
  var frm = document.forms["authentification"];
  if(frm && !onverif)
  {
    onverif = true;
    ret = ctrlSaisie(frm.codcli,MSG_PAC_001,MSG_PAC_002,"",false,estNumString,8);
    //if(ret) ret = ctrlSaisie(frm.codsec,MSG_PAC_003,MSG_PAC_004,"",false,estNumString,6);
    if(ret){
      detectClient();
      var agent = navigator.userAgent.toLowerCase();
      var isNS4 = (agent.indexOf("mozilla/4.") != -1 && agent.indexOf("mozilla/4.0") == -1) ? 1 : 0;
      if (isNS4){
        var rg = new RegExp("mozilla/([0-9.]+) ");
        var r = rg.exec(agent);
        versionNav = r? r[1]: "";
        categNav= "NS4";
        typeNav = "NS4";
      }
      else if (document.getElementById){ 
				categNav="W3C";
			}
      else{ 
				categNav= "NonW3C";
			}
      frm.x.value=screen.width;
      frm.y.value=screen.height;
      frm.os.value=os;
      frm.categNav.value=categNav;
      frm.typeNav.value=typeNav;
      frm.versionNav.value=versionNav;
			if (isNS4){
				window.document.location.href = "/navig00.html";
			}
			else if(!ccSaisi){   
				var i=agent.indexOf("netscape");
	  		if(i>0 && agent.substr(agent.indexOf("/",i+8)+1,1) <= "6"){
					window.document.location.href = "/navig03.html"; 
					return;
	  		}
	  		ccSaisi=true; validerCV();
			}
    }
    onverif = false;
  }
}
function controlerCV(){ 
	var ret = false;
  if(frm && !onverif){
		onverif = true;
	if(document.getElementById)
  {
		if(ctrlSaisie(frm.codcli,MSG_PAC_001,MSG_PAC_002,'',false,estNumString,8)){ 
			if(!ccSaisi){
				ccSaisi=true; 
				validerCV();
			}
		}
   }
	 else{
	 	window.document.location.href = '/navig00.html';
	 }
   onverif = false;
  }
}
function envoyerCV()
{
	//alert("debut de fonction envoyerCV");
  var frm = document.forms["authentification"];
  fermerCVCS();
  if(frm){
		frm.submit();
		disableAllAction();
		/*alert("\ncodcli:"+frm.codcli.value+" - codsec:" + frm.codsec.value + " - categNav:"  +frm.categNav.value  + 
		"\nos:" + frm.os.value + " - typeNav:" + frm.typeNav.value+ " - versionNav:" + frm.versionNav.value);*/
	}
	//alert("fin de fonction envoyerCV")
}
function fermerCV(vider)
{
	fermerCVCS();
  ccSaisi=false;
  if(vider)initFocus();
}
function activTexte(i,on)
{
	var etape = document.getElementById("etape" + i);
  var nombre = document.getElementById("nombre" + i);

  if (etape && nombre){
  	if(on){ 
			etape.className = "titrenoir";
			nombre.className = "nombrerouge";
    }
    else{
			etape.className = "textegris";
			nombre.className = "textegris";
    } 
  }
}
