var dayJour;
var modeDegrade = false;
var msg = "";

function GoLGN(page)
{
  var hostLGN;
  if (window.location.host.search("logitelnet.socgen.com") != -1)
    hostLGN = "https://" + window.location.host.replace("www.","");
  else
	hostLGN = "https://logitelnet.socgen.com";
    //hostLGN = "https://" + window.location.host;
  window.location.href = hostLGN + page;
}

function valider(page)
{
  if (document.getElementById)
    GoLGN(page);
  else
    window.location.href = "/navig00.html";
}

function HostNameBackEi()
{
  var protocol = document.location.protocol;
  var hostname = document.location.hostname;
  var hostnamebackei;
  if (hostname.search("acs") != -1)
    hostnamebackei = "";
  else if (document.location.port != "")
  {
    if (protocol == "https:") {
			if (document.location.port.substr(0,2) == "40")
      	hostnamebackei = "https://eifv6.neo.socgen.com:2443";
			else
				hostnamebackei = "https://eifv6.cypher.secure.socgen.com:4443";
		}			
    if (protocol == "http:") {
			if (document.location.port.substr(0,2) == "40")
      	hostnamebackei = "http://eifv6.cypher.socgen.com:4080";
			else
				hostnamebackei = "http://eifv6.neo.socgen.com:2080";
		}
  }
  else if (hostname.search("homo") != -1)
    hostnamebackei = protocol + "//www.homooffre2.societegenerale.fr";
  else
    hostnamebackei = protocol + "//www.offre2.societegenerale.fr";
  return hostnamebackei;
}

function EcrireAppelJSBack(urlJS)
{
  var lien = HostNameBackEi();
  var urlJSBack = lien + urlJS;
  document.write('<script language="JavaScript" src="' + urlJSBack + '"></script>');
}

function TestModeDegrade()
{
  if (TableauActus.length==0)
    modeDegrade = true;
}

var TableauActus = new Array();
var j = 0;
function Actu(id,titre,texte,url,image)
{
  this.id = id;
  this.titre = titre;
  this.texte = texte;
  this.url = url;
  this.image = image;
}

var bandeau_10=null;
var bandeau_20=null;
var bandeau_40=null;
var bandeau_60=null;

function Bandeau(swf,gif,url,title,target_win,target_features)
{
  this.swf = swf;
  this.gif = gif;
  this.url = url;
  this.title = title;
  this.target_win = target_win;
  this.target_features = target_features;
}

function EcrireBandeau(bandeau,pref_code,suff_code,width,height,top_force)
{
  // - width & height obligatoires
  // - attention au param�tre top_force : si on �crit un bandeau flash, width et height sont forc�s de toute fa�on,
  //   en revanche si on �crit un bandeau image, on ne force width et height que si top_force diff�rent de 0 
  var isflash=false;
  var iscomplex=false;
  var swf_url="";
  var gif_url="";
  var title_bandeau="";
  var Code="";
  var lien=HostNameBackEi();
  if ((bandeau != null) && ((bandeau.swf != "") || (bandeau.gif != "")))
  {
    if (bandeau.swf != "") isflash=true;
    if (bandeau.target_win) iscomplex=true;
    if (bandeau.title) title_bandeau=bandeau.title;
    if (modeDegrade)
    {
      if (isflash) swf_url = bandeau.swf;
      else gif_url = bandeau.gif;
    }
    else
    { 	        
      if (isflash) swf_url = lien + bandeau.swf;
      else gif_url = lien + bandeau.gif;
    }     
    Code += pref_code;
    if (isflash) Code += EcrireBandeauFlash(swf_url,width,height);
    else if (iscomplex) Code += EcrireBandeauImgComplex(gif_url,bandeau.url,title_bandeau,width,height,top_force,bandeau.target_win,bandeau.target_features);      
    else Code += EcrireBandeauImg(gif_url,bandeau.url,title_bandeau,width,height,top_force);               
    Code += suff_code;
    document.write(Code) ;
  }  
}

function EcrireBandeauFlash(swf_url,width,height)
{
  var Code="";
  var W="";
  var H="";
  if (width != "") W = ' width=' + width ;
  if (height != "") H = ' height=' + height ;    
  
  Code += '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="https://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"';
  Code += W + H;
  Code += '>\n' ;
  Code += '<param name=movie value="' + swf_url + '">\n' ;
  Code += '<param name=quality value=high>\n';
  Code += '<param name="wmode" value="opaque">\n';
  Code += '<embed src="' + swf_url + '" quality=high wmode="opaque" pluginspage="https://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash"';
  Code += W + H;
  Code += '>\n';
  Code += '</embed>\n';
  Code += '</object>\n';  
  return Code;
}

function EcrireBandeauImg(img_url,target_url,title,width,height,top_force)
{
  var Code="";
  var W="";
  var H="";
  var top_link;
  if ((top_force != 0) && (width != "")) W = ' width=' + width ;
  if ((top_force != 0) && (height != "")) H = ' height=' + height ;
  top_link = (target_url != "");  
  Code += ((top_link)?('<a href="' + target_url + '">'):'') + '<img border=0' +W + H + ' src="' + img_url +'" title="' + title + '"/>' + ((top_link)?'</a>':'');
  return Code;
}

function EcrireBandeauImgComplex(img_url,target_url,title,width,height,top_force,target_win,target_features)
{
  var Code="";
  var W="";
  var H="";
  var top_link;
  if ((top_force != 0) && (width != "")) W = ' width=' + width ;
  if ((top_force != 0) && (height != "")) H = ' height=' + height ;
  top_link = (target_url != "");
  if (top_link) {  
    Code += '<a href="javascript:ouvrirFenetre(\'' + target_url + '\',\'' + target_win + '\'';
    if (target_features) Code += ',\'' + target_features + '\'';
    Code += ')">';
  }
  Code += '<img border=0' + W + H + ' src="' + img_url +'" title="' + title + '"/>'; 
  if (top_link) Code += '</a>';
  return Code;
}

function EcrireBandeau10()
{
  if (modeDegrade) 
  {
    bandeau_10 = new Bandeau("", "/img/pac/bandeau_promo_d.jpg", "http://par.societegenerale.fr/EIP/resources/production/Home/prets_conso/expresso_prets_conso_vehicule/", "Pr&ecirc;t Expresso");        
  }
  EcrireBandeau(bandeau_10,'','\n',709,190,0);  
}

function EcrireBandeau20()
{
  if (modeDegrade) 
  {
    bandeau_20 = new Bandeau("", "/img/pac/menu_bg_mobile_d.gif", "https://rechargement.societegenerale.fr/", "Recharger votre mobile");
  } 
  EcrireBandeau(bandeau_20,"<div class='bandeaugauche' style='margin-top:5px;'>",'</div>\n',129,35,1);  
}

function EcrireBandeau40()
{
  if (modeDegrade) 
  {
    bandeau_40 = new Bandeau("", "/img/pac/formulaires_indisp.gif", "", "Veuillez nous excuser, nos formulaires sont actuellement indisponibles");
  } 
	EcrireBandeau(bandeau_40,"<tr><td colspan=1><div style='padding-bottom:10px;'>",'</div></td></tr>\n',129,63,1);  
}

function EcrireBandeau60()
{
  //pas de bandeau en mode d�grad�
	EcrireBandeau(bandeau_60,"<div class='bandeaugauche' style='margin-top:5px;'>",'</div>\n',129,0,0);  
}

function RotationActus()
{
  var dateJour = new Date();
  dayJour = dateJour.getDate();
  var test = Math.ceil(dayJour/3);
  if (test>5)
  {
    if (test==11)
      test=5;
    else
      test=test-5;
  }
  test=test-1;
  var TableauTampon = new Array();
  for (var cpt=0; cpt<5; cpt++)
  {
    TableauTampon[cpt] = new Actu(cpt, TableauActus[cpt].titre, TableauActus[cpt].texte, TableauActus[cpt].url, TableauActus[cpt].image);
  }
  for (var cpt=0; cpt<5; cpt++)
  {
    var indice = cpt-test;
    if (indice<0)
      indice = indice + 5;  
    TableauActus[cpt] = TableauTampon[indice];    
  }
}
function PreparerActus()
{
  if (modeDegrade)
  {  
    j=0;
    TableauActus[j] = new Actu(j++, "Indisponibilit&eacute; des formulaires", "Nos formulaires de contact et de souscription sont actuellement indisponibles. Veuillez nous excuser pour la g&ecirc;ne occasionn&eacute;e.", "","/img/pac/formulaires.jpg");
    TableauActus[j] = new Actu(j++, "Pr&eacute;parez votre projet immobilier", "Calculez votre capacit&eacute; d'acquisition, effectuez des simulations et transmettez en ligne votre demande de financement.", "http://par.societegenerale.fr/EIP/resources/production/Home/prets_immo/","/img/pac/immobilier.gif");
    TableauActus[j] = new Actu(j++, "3933 - Toute votre banque par t&eacute;l&eacute;phone.", "C'est pratique, rapide et des conseillers vous r�pondent jusqu'� 22h !","http://par.societegenerale.fr/EIP/resources/production/Home/quotidien/suivre_comptes_quotidien/3933/","/img/pac/3933.gif");
    TableauActus[j] = new Actu(j++, "Assurance auto", "D&eacute;couvrez les nouvelles garanties - Pr&ecirc;t d'un v&eacute;hicule jusqu'&agrave; 30 jours, d&eacute;pannage en cas de crevaison...", "http://par.societegenerale.fr/EIP/resources/production/Home/assurances/proteger_biens/assurance_auto_assurances/","/img/pac/assurance_auto_nov05.gif");
  }
  if (!modeDegrade)
  {     
    RotationActus();
  } 
}

function EcrireActu(num_actu)
{
  var Code = "";
  var lien=HostNameBackEi(); 
	var urlPresente = (TableauActus[num_actu].url != "");
	//titre
	Code += "<div class='titactu'>";
  if (urlPresente) Code +=  "<a href='"+ TableauActus[num_actu].url + "'>";
  Code += TableauActus[num_actu].titre;
  if (urlPresente) Code += '</a>';
  Code += "</div>\n";
  Code += "<div class='fl1'>\n"
	//image
  if (urlPresente) Code += "<a href='"+ TableauActus[num_actu].url + "'>";
  Code += "<img border=0  src='";
  if (!modeDegrade) Code += lien;
  Code += TableauActus[num_actu].image + "' alt=''/>";
  if (urlPresente) Code += "</a>";
  Code += "</div>\n";
  Code += "<div class='fl2'>\n";
	//texte, lien
  Code += TableauActus[num_actu].texte;
  if (urlPresente) Code += "<br><a class='lien' href='" + TableauActus[num_actu].url + "'>&gt;&gt;&gt;</a>";
  Code += "</div>\n";
  document.write(Code); 
}
                        
var TableauFocus = new Array();
var i = 0;
function Focus(id,titre,url,title)
{
  this.id = id;
  this.titre = titre;
  this.url = url;
  this.title = title;
}
function EcrireFocus()
{
  if (modeDegrade)
  {
    i=0;
    TableauFocus[i] = new Focus(i++, "Moins de 25 ans", "http://jeunes.societegenerale.fr/home/","Site jeunes Soci�t� G�n�rale");
    TableauFocus[i] = new Focus(i++, "Client�les Expatri�es", "http://par.societegenerale.fr/EIP/resources/production/THEME/clienteles_expatriees/","Client�les expatri�es");
    TableauFocus[i] = new Focus(i++, "Le Club Soci�t� G�n�rale American Express", "http://par.societegenerale.fr/EIP/resources/production/club/","Club Soci�t� G�n�rale American Express");
    TableauFocus[i] = new Focus(i++, "Gestion de patrimoine", "http://par.societegenerale.fr/EIP/resources/production/THEME/espace_patrimoine/","Gestion de Patrimoine");
    TableauFocus[i] = new Focus(i++, "Soci�t� G�n�rale Gestion Priv�e", "http://par.societegenerale.fr/EIP/resources/production/THEME/gestion_privee/","Soci�t� G�n�rale Gestion Priv�e");
  }
  if (TableauFocus.length!=0)
  {
    var Code = "<div id='menudroitebas'><table>\n";
    var title = "";              
    for (var i in TableauFocus)
    {
      if (!TableauFocus[i].title)
        title="";
      else
        title=TableauFocus[i].title;
      Code += '<tr ><td colspan=2 ></td></tr><tr><td valign=top align=center class="focuscell"><a class="focus" href="' + TableauFocus[i].url + '" title="' + title + '">'+ '&gt;</a></td>\n'; 
      Code += '<td class="focuscell2"><a class="focus" href="' + TableauFocus[i].url + '" title="' + title + '">'+ TableauFocus[i].titre + '</a></td>\n</tr>'; 
    }
    Code += "</table></div>"; 
    document.write(Code);
  }  
}

function webo_zpi(_WEBOZONE,_WEBOPAGE,_WEBOID)
{
  var result = "";
  var wbs_da = new Date();
  wbs_da = parseInt(wbs_da.getTime()/1000 - 60*wbs_da.getTimezoneOffset());
  var wbs_ref = '' + escape(document.referrer);
  var wbs_ta = '0x0';
  var wbs_co = 0;
  var wbs_nav = navigator.appName;
  if (parseInt(navigator.appVersion)>=4)
  {
    wbs_ta = screen.width + "x" + screen.height;
    wbs_co = (wbs_nav!="Netscape") ? screen.colorDepth : screen.pixelDepth;
  }
  var wbs_arg = ".weborama.fr/fcgi-bin/comptage.fcgi?ID=" + _WEBOID;
  if (location.protocol == 'https:')
    wbs_arg = "https://ssl" + wbs_arg; 
  else 
    wbs_arg =  "http://pro" + wbs_arg;
  wbs_arg += "&ZONE=" + _WEBOZONE + "&PAGE=" + _WEBOPAGE;
  wbs_arg += "&ver=2&da2=" + wbs_da + "&ta=" + wbs_ta + "&co=" + wbs_co + "&ref=" + wbs_ref;
  if (parseInt(navigator.appVersion)>=3)
  {
    webo_compteur = new Image(1,1);
    webo_compteur.src = wbs_arg;
  }else{
    result = '<img src=' + wbs_arg + ' border="0" height="1" width="1" alt="">';
  }
  return result;
}

function EcrireStat()
{
  var stat = webo_zpi(1,1,83083);
  document.write(stat);
}


