var delaiMSIE = 10000;
var delaiUP = false;

function XMLObject_testStateChange()
{
  if (this.xmlDoc.readyState == 4)
  {
    var commande = this.ecrireXMLtoHTML + "()";
    eval(commande);
    return;
  } else if (delaiUP) {                    	
    return;
  }
  setTimeout("this.testStateChange()",10);
}

function XMLObject_constructor()
{
  if (this.xmlDoc == null)
  {
    if (document.implementation && document.implementation.createDocument)
    {
      this.xmlDoc = document.implementation.createDocument("", "", null);
      this.xmlDoc.onload = eval(this.ecrireXMLtoHTML);
    }
    else if (window.ActiveXObject)
    {
      this.xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
      this.xmlDoc.async = false;
    }    
  }
}

function XMLObject_loadXML()
{
  this.xmlDoc.load(this.urlXML);
  if (window.ActiveXObject)
  {
    setTimeout("delaiUP=true",delaiMSIE);
    this.testStateChange();
  }
}

function XMLObject_parseErrorXML()
{
  if (!this.xmlDoc.documentElement)
  {    
    return -1;
  }
  else if ((this.xmlDoc.documentElement.tagName == "html") || (this.xmlDoc.documentElement.tagName == "parsererror"))
  {    
    return -1;
  } 
  else 
  {    
    return 0;
  }
}

function XMLObject(ecrireXMLtoHTML, urlXML)
{

  this.ecrireXMLtoHTML = ecrireXMLtoHTML;
  this.xmlDoc = null;
  this.urlXML = urlXML;

  this.testStateChange = XMLObject_testStateChange;
  this.importXML = XMLObject_loadXML;
  this.parseErrorXML = XMLObject_parseErrorXML;

  // Constructor
  this.constructor = XMLObject_constructor;
  this.constructor();
}

function XHRObject(urlXML)
{
  var obj = null;	
  if(window.XMLHttpRequest) // Firefox
    obj = new XMLHttpRequest();
  else if(window.ActiveXObject) // Internet Explorer
    obj = new ActiveXObject("Microsoft.XMLHTTP");
  else { // XMLHttpRequest non support� par le navigateur
    return;
  }
  obj.open("GET", urlXML, false);
  obj.send(null);
  return obj;
}
