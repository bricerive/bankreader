This is the form:

<form class="n2g_mire_connexion_form"  id="n2g_authentification" method="POST" name="n2g_authentification" action="https://particuliers.secure.societegenerale.fr//acces/authlgn.html">
	<input id="codcli" name="codcli" class="n2g_mire_connexion_champs_actif" type="text" title="saisissez votre code client" value="" autocomplete="off" maxlength="8" size="20" onfocus="n2g_codcliOnFocus(this);" onBlur="n2g_codcliOnBlur(this);" />
	<input id="button" name="button" class="n2g_mire_connexion_btn" type="button" title="Activation du clavier virtuel graphique" value="OK" onclick='if (n2g_ctrlSaisie(document.forms["n2g_authentification"].codcli, "Vous devez saisir votre code client.","La saisie de votre code client est incorrecte.\n Pour en savoir plus, cliquez sur \&#34;Aide\&#34; à côté du code client.","",false,n2g_estNumString,8)) { auth_ini_auth("n2g_authentification"); }'/>
	<input name="user_id" id="user_id" type="hidden"/>
	<input name="versionCla" type="hidden" value="0"/>
	<input name="categNav" type="hidden" value="W3C"/>
</form>


The button::onclick validates the content of codcli (must be 8 digits) and then does

	auth_ini_auth("n2g_authentification");
	
Which does (n2g_util_login.js)

function auth_ini_auth(formName) {
	prepare_gda_auth();
	return auth_ini_auth_submit(formName);
}

In gda_lgn.js:

function prepare_gda_auth(form_id) {
	if ((__getCookie('vk_mode')!= constModeDefaut) && (__getCookie('vk_mode')!= constModeMalVoyant)) __setCookie('vk_mode', constModeDefaut);
}

In n2g_util_login.js:

function auth_ini_auth_submit(formName)
{

	var authForm = document.forms[formName];
	if (typeof authForm.codcli == 'object')
	{
		var userId = authForm.codcli.value;
		if (authForm.user_id) {
			authForm.user_id.value =  userId;
		}
	}

	gda_auth(formName);
}

In gda_lgn.js:

var gda_auth_module = 'vk';

function gda_auth(form_id) {
	gda_modules[gda_auth_module].auth(form_id);
}


In gda_vk.js:

/* Register with SAS */
gda_modules['vk'] = new GDAVK();

var gda_vk_div = 'gda_vk';

function GDAVK() {
	return {
		/* Authentification */
		auth: function(form_id) {
			GDAVK.spawn(form_id, gda_vk_div, 'auth', true);
		},
		
		/* Authentification JSON */
		auth_json: function(params) {
			GDAVK.spawn_json(gda_vk_div, 'auth', params, true);
		},

		/* Operation signing */
		sign: function(form_id) {
			GDAVK.spawn(form_id, gda_vk_div, 'sign', false);
		},
		
		/* Operation signing JSON */
		sign_json: function(params) {
			GDAVK.spawn_json(gda_vk_div, 'sign', params, false);
		},
		
		close: function() {
			GDAVK.close();
		},
		
		setopts: function(options) {
			GDAVK.setopts(options);
		}
	}
};


GDAVK.spawn = function(form_id, vk_id, vk_op, allow_rebase)
{
	/* Insert keyboard <div> and hidden <input> */
	if (document.getElementById(vk_id) == null) {
		$jGda('body').append($jGda(document.createElement('div')).attr('id', vk_id));
		var input = $jGda(document.createElement('input')).attr('type', 'hidden').attr('name', 'codsec').attr('id', 'codsec').attr('value', '');
		$jGda(document.getElementById(form_id)).append(input);
	}
		
	/* Define validation function */
	function gda_vk_validate(kbd) {
		var form = $jGda(document.getElementById(form_id));
		if (document.getElementById('vk_op') == null) {
			var input = $jGda(document.createElement('input')).attr('type', 'hidden').attr('name', 'vk_op').attr('id', 'vk_op').attr('value', vk_op);
			form.append(input);
		}
		form[0].submit();
	}
	
	/* Create virtual keyboard */
	vk = new VirtualKeyboardHTML2(vk_id, form_id, false, 'codsec', GDAVK.getopts(gda_vk_validate, allow_rebase, this.options));

	/* Show the virtual keyboard */
	vk.show();
}


function VirtualKeyboardHTML2(elemId, formId, inSession, codeFieldId, options) {
	VirtualKeyboardHTML.call(this, elemId, formId, inSession, codeFieldId, options);
};


function VirtualKeyboardHTML(elemId, formId, inSession, codeFieldId, options) {
	if ($jGda('#vk_layer').size() == 0) {
		$jGda('body').append(this.vk_layer = $jGda('<div id="vk_layer" class="vk_layer"></div>'));
		this.iframePourIE = $jGda('<iframe style="width:100%; height: 100%; filter:alpha(opacity=0); border: 0; pointer-events: none;" />');
		this.iframePourIE.attr("src",(typeof(gda_static_base_uri) == "string" ? gda_static_base_uri : '')+"/static/img/vide.gif");
		this.vk_layer.append(this.iframePourIE);
		this.vk_move = $jGda(document.createElement('div')).attr('id', 'vk_move').attr('class', 'vk_move');
		this.vk_layer.append(this.vk_move);
		this.vk_layer.hide();
	} else if (this.vk_layer == null) {
		this.vk_layer = $jGda('#vk_layer');
		this.vk_move = $jGda('#vk_move');

	}
	var elem = $jGda('#'+elemId);
	elem.html(kbdHTML);
	if ($jGda('#cryptocvcs').size() == 0) {
		var inputCVCS = $jGda(document.createElement('input')).attr('type', 'hidden').attr('name', 'cryptocvcs').attr('id', 'cryptocvcs').attr('value', '');
		$jGda('#'+formId).append(inputCVCS);
		var inputCVCSCache = $jGda(document.createElement('input')).attr('type', 'hidden').attr('id', 'cvcs_visu_cache').attr('value', '');
		$jGda('#'+formId).append(inputCVCSCache);
	}
	this.form = $jGda('#'+formId);
	VirtualKeyboard2.call(this, inSession, codeFieldId, options);
	
	gda_css_include(this.urlCSS);
};
