function mobile_detect()
{
if (navigator.userAgent.toLowerCase().indexOf("android")>-1)
	if (confirm("Souhaitez-vous consulter la version mobile du site Société Générale ?"))
		window.location.href = "https://societegenerale.mobi";

if ( (navigator.userAgent.toLowerCase().indexOf("iphone")>-1) || (navigator.userAgent.toLowerCase().indexOf("ipod")>-1) )
	if (confirm("Souhaitez-vous consulter la version iPhone du site Société Générale ?"))
		window.location.href = "https://societegenerale.mobi";
}

mobile_detect();