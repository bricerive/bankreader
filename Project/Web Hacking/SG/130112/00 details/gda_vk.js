/*
 * gda_vk.js
 *
 * VK implementation of gda_ functions
 */

gda_js_include('/static/js/vk/virtualKeyboard.js');
gda_js_include('/static/js/vk/config.js');

var vk;
var gda_vk_div = 'gda_vk';
function GDAVK() {
	return {
		/* Authentification */
		auth: function(form_id) {
			GDAVK.spawn(form_id, gda_vk_div, 'auth', true);
		},
		
		/* Authentification JSON */
		auth_json: function(params) {
			GDAVK.spawn_json(gda_vk_div, 'auth', params, true);
		},

		/* Operation signing */
		sign: function(form_id) {
			GDAVK.spawn(form_id, gda_vk_div, 'sign', false);
		},
		
		/* Operation signing JSON */
		sign_json: function(params) {
			GDAVK.spawn_json(gda_vk_div, 'sign', params, false);
		},
		
		close: function() {
			GDAVK.close();
		},
		
		setopts: function(options) {
			GDAVK.setopts(options);
		}
	}
};

GDAVK.getopts = function(validate_fn, allow_rebase, custom_opts)
{
	var opts = typeof(gda_vk_config) == "object" ? clone_object(gda_vk_config) : {};
	update_object(opts, {validatefn: validate_fn, 
						urlBase: allow_rebase && (typeof(gda_base_uri) == "string") ? gda_base_uri : '',
						urlStaticBase: typeof(gda_static_base_uri) == "string" ? gda_static_base_uri : '' });

	if (typeof(custom_opts) == "object")
		update_object(opts, custom_opts);

	var localUrlBase = opts.urlBase;
	var localurlStaticBase = opts.urlStaticBase;
	if (!localurlStaticBase && localUrlBase) {
		update_object(opts, {validatefn: validate_fn, urlStaticBase: localUrlBase });
	}

	return opts;
}

GDAVK.setopts = function(options)
{
	this.options = options;
}

GDAVK.spawn = function(form_id, vk_id, vk_op, allow_rebase)
{
	/* Insert keyboard <div> and hidden <input> */
	if (document.getElementById(vk_id) == null) {
		$jGda('body').append($jGda(document.createElement('div')).attr('id', vk_id));
		var input = $jGda(document.createElement('input')).attr('type', 'hidden').attr('name', 'codsec').attr('id', 'codsec').attr('value', '');
		$jGda(document.getElementById(form_id)).append(input);
	}
		
	/* Define validation function */
	function gda_vk_validate(kbd) {
		var form = $jGda(document.getElementById(form_id));
		if (document.getElementById('vk_op') == null) {
			var input = $jGda(document.createElement('input')).attr('type', 'hidden').attr('name', 'vk_op').attr('id', 'vk_op').attr('value', vk_op);
			form.append(input);
		}
		form[0].submit();
	}
	
	/* Create virtual keyboard */
	vk = new VirtualKeyboardHTML2(vk_id, form_id, false, 'codsec', GDAVK.getopts(gda_vk_validate, allow_rebase, this.options));

	/* Show the virtual keyboard */
	vk.show();
}

GDAVK.spawn_json = function(vk_id, vk_op, params, allow_rebase)
{
	/* Insert keyboard <div> and hidden <input> */
	if ($jGda('#' + vk_id) == null || $jGda('#' + vk_id).length == 0) {
		var divVk = $jGda('<div />').attr('id', vk_id);
		$jGda('body').append(divVk);
		divVk.append($jGda('<input />').attr('type', 'hidden').attr('name', 'codsec').attr('id', 'codsec'));
		var divVkInner = $jGda('<div />').attr('id', vk_id + '_inner');  
		divVk.append(divVkInner);
		var formVk = $jGda('<form />').attr('id', vk_id + '_formVK');
		divVk.append(formVk);
	}
		
	/* Define validation function */
	function gda_vk_validate() {
		var secureParams = {
				'cryptocvcs': vkJSON.crypto,
				'codsec' : $jGda("#codsec").val(),
				'vk_op' : vk_op
		};
		
		if (vk_op = 'auth') {
			secureParams['user_id'] = params['idUser'];
		}
		
		params.success(secureParams);
	}
	
   /* Create virtual keyboard */
	vkJSON = new VirtualKeyboardHTML(
				vk_id + '_inner', 
				vk_id + '_formVK',
				false, 
				'codsec', 
				GDAVK.getopts(gda_vk_validate, allow_rebase));

   /* Show the virtual keyboard */
   vkJSON.show();
}

GDAVK.close = function()
{
	if (vk) vk.close();
}

/* 
 * Code change
 */

var gda_vk_chcode_errors = {
    ERROR_STEP2_CODE_SHOULD_BE_NEW: 1,
    ERROR_STEP3_CODE_CONFIRMATION_FAILED: 2
}


/* Internal stuff */
var gda_vk_chcode_step;
var gda_vk_chcode_step_handler;
var gda_vk_chcode_error_handler;
var gda_vk_chcode_form;
		
function gda_vk_chcode_validate(vk)
{
	switch (gda_vk_chcode_step) {
		case 1:
			$jGda('#codsec_old').val($jGda('#codsec').val());
			break;

		case 2:
			$jGda('#codsec_new').val($jGda('#codsec').val());
			if ($jGda('#codsec_new').val() == $jGda('#codsec_old').val() && typeof(gda_vk_chcode_error_handler) == 'function')  {
                        	gda_vk_chcode_error_handler(gda_vk_chcode_errors.ERROR_STEP2_CODE_SHOULD_BE_NEW);
				vk.emptyFields()
				vk.show();
				return;
			}
			break;

		case 3:
			if ($jGda('#codsec').val() != $jGda('#codsec_new').val()) {
				if (typeof(gda_vk_chcode_error_handler) == 'function')
					gda_vk_chcode_error_handler(gda_vk_chcode_errors.ERROR_STEP3_CODE_CONFIRMATION_FAILED);
					//gda_vk_chcode_error_handler();
				else
					alert('VK new code confirm mismatch!');
				vk.emptyFields()
				vk.show();
				//vk.hide();
				return;
			}
			break;

		default:
			alert('Internal error: unknown step ' + gda_vk_chcode_step);
			return;
	}

	/* Clear VK input buffer */
	$jGda('#codsec').val('');
	/* Call step handler, if any */
	if (typeof(gda_vk_chcode_step_handler) == 'function')
		gda_vk_chcode_step_handler();

	/* Step 1 or 2 => advance 1 step, reset VK */
	if (gda_vk_chcode_step == 1 || gda_vk_chcode_step == 2) {
		gda_vk_chcode_step++;
		vk.emptyFields()
		vk.show();
	}

	/* Step 3 => submit */
	else if (gda_vk_chcode_step == 3)
		document.forms[gda_vk_chcode_form].submit();	
}


/* API */
function gda_vk_chcode_init(form_id)
{
	/* Insert keyboard and hidden tags if needed */
	if ($jGda('#gda_chcode_vk').size() == 0) {
		$jGda('body').append($jGda(document.createElement('div')).attr('id', 'gda_chcode_vk'));
		var form = $jGda(document.getElementById(form_id));
		var inputCodsec = $jGda(document.createElement('input')).attr('type', 'hidden').attr('name', 'codsec').attr('id', 'codsec').attr('value', '');
		var inputCodsecOld = $jGda(document.createElement('input')).attr('type', 'hidden').attr('name', 'codsec_old').attr('id', 'codsec_old').attr('value', '');
		var inputCodsecNew = $jGda(document.createElement('input')).attr('type', 'hidden').attr('name', 'codsec_new').attr('id', 'codsec_new').attr('value', '');
		var inputVKOP = $jGda(document.createElement('input')).attr('type', 'hidden').attr('name', 'vk_op').attr('id', 'vk_op').attr('value', 'chcode');
		form.append(inputCodsec);
		form.append(inputCodsecOld);
		form.append(inputCodsecNew);
		form.append(inputVKOP);
		
		gda_vk_chcode_form = form_id;
		gda_vk_chcode_step_handler = null;
		gda_vk_chcode_error_handler = null;

		vk = new VirtualKeyboardHTML2('gda_chcode_vk', form_id, false, 'codsec', GDAVK.getopts(gda_vk_chcode_validate, false, this.options));
	}
	else {
		$jGda('#codsec').val('');
		$jGda('#codsec_old').val('');
		$jGda('#codsec_new').val('');
		vk.reset('codsec');
	}

	gda_vk_chcode_step = 0;
}

function gda_vk_chcode_start()
{
	gda_vk_chcode_step = 1;
	vk.show();
}


function gda_vk_chcode_set_step_handler(func)
{
	gda_vk_chcode_step_handler = func;
}

function gda_vk_chcode_set_error_handler(func)
{
	gda_vk_chcode_error_handler = func;
}


function gda_vk_suspend(form_id)
{
	GDAVK.spawn(form_id, gda_vk_div, 'susp', false);
}


function update_object(objDest, objSrc)
{
	for (var property in objSrc) {
		objDest[property] = objSrc[property];
	}
	return objDest;
}


function clone_object(obj)
{
	if(typeof(obj) != 'object' || obj == null) {
		return obj;
	}
	
	var newObj = obj.constructor();
	for (var property in obj) {
		newObj[property] = clone_object(obj[property]);
	}
	
	return newObj;
}

/* Register with SAS */
gda_modules['vk'] = new GDAVK();

