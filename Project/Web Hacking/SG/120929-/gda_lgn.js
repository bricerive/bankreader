var gda_js_includes = new Array();
gda_js_includes['/static/js/jquery-1.4.4.min.js'] = 1;
gda_js_includes['/static/js/jquery-ui-1.8.19.custom.min.js'] = 1;
gda_js_includes['/static/js/gda_debug.js'] = 1;
gda_js_includes['/static/js/vk/virtualKeyboard.js'] = 1;
gda_js_includes['/static/js/gda_vk.js'] = 1;
gda_js_includes['/static/js/csa/csa_dialog.js'] = 1;
gda_js_includes['/static/js/gda_csa.js'] = 1;
gda_js_includes['/static/js/gda_dialog.js'] = 1;
gda_js_includes['/static/js/gda_main.js'] = 1;
gda_js_includes['/static/js/error/handleErrorLayer.js'] = 1;
gda_js_includes['/static/js/error/handleErrorLGN.js'] = 1;
gda_js_includes['/static/js/error/handleErrorSAS.js'] = 1;
gda_js_includes['/static/js/vk/config.js'] = 1;
if ($jCsa == null) {
/*!
 * jQuery JavaScript Library v1.4.4
 * http://jquery.com/
 *
 * Copyright 2010, John Resig
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * Includes Sizzle.js
 * http://sizzlejs.com/
 * Copyright 2010, The Dojo Foundation
 * Released under the MIT, BSD, and GPL Licenses.
 *
 * Date: Thu Nov 11 19:04:53 2010 -0500
 */
(function(E,B){function ka(a,b,d){if(d===B&&a.nodeType===1){d=a.getAttribute("data-"+b);if(typeof d==="string"){try{d=d==="true"?true:d==="false"?false:d==="null"?null:!c.isNaN(d)?parseFloat(d):Ja.test(d)?c.parseJSON(d):d}catch(e){}c.data(a,b,d)}else d=B}return d}function U(){return false}function ca(){return true}function la(a,b,d){d[0].type=a;return c.event.handle.apply(b,d)}function Ka(a){var b,d,e,f,h,l,k,o,x,r,A,C=[];f=[];h=c.data(this,this.nodeType?"events":"__events__");if(typeof h==="function")h=
h.events;if(!(a.liveFired===this||!h||!h.live||a.button&&a.type==="click")){if(a.namespace)A=RegExp("(^|\\.)"+a.namespace.split(".").join("\\.(?:.*\\.)?")+"(\\.|$)");a.liveFired=this;var J=h.live.slice(0);for(k=0;k<J.length;k++){h=J[k];h.origType.replace(X,"")===a.type?f.push(h.selector):J.splice(k--,1)}f=c(a.target).closest(f,a.currentTarget);o=0;for(x=f.length;o<x;o++){r=f[o];for(k=0;k<J.length;k++){h=J[k];if(r.selector===h.selector&&(!A||A.test(h.namespace))){l=r.elem;e=null;if(h.preType==="mouseenter"||
h.preType==="mouseleave"){a.type=h.preType;e=c(a.relatedTarget).closest(h.selector)[0]}if(!e||e!==l)C.push({elem:l,handleObj:h,level:r.level})}}}o=0;for(x=C.length;o<x;o++){f=C[o];if(d&&f.level>d)break;a.currentTarget=f.elem;a.data=f.handleObj.data;a.handleObj=f.handleObj;A=f.handleObj.origHandler.apply(f.elem,arguments);if(A===false||a.isPropagationStopped()){d=f.level;if(A===false)b=false;if(a.isImmediatePropagationStopped())break}}return b}}function Y(a,b){return(a&&a!=="*"?a+".":"")+b.replace(La,
"`").replace(Ma,"&")}function ma(a,b,d){if(c.isFunction(b))return c.grep(a,function(f,h){return!!b.call(f,h,f)===d});else if(b.nodeType)return c.grep(a,function(f){return f===b===d});else if(typeof b==="string"){var e=c.grep(a,function(f){return f.nodeType===1});if(Na.test(b))return c.filter(b,e,!d);else b=c.filter(b,e)}return c.grep(a,function(f){return c.inArray(f,b)>=0===d})}function na(a,b){var d=0;b.each(function(){if(this.nodeName===(a[d]&&a[d].nodeName)){var e=c.data(a[d++]),f=c.data(this,
e);if(e=e&&e.events){delete f.handle;f.events={};for(var h in e)for(var l in e[h])c.event.add(this,h,e[h][l],e[h][l].data)}}})}function Oa(a,b){b.src?c.ajax({url:b.src,async:false,dataType:"script"}):c.globalEval(b.text||b.textContent||b.innerHTML||"");b.parentNode&&b.parentNode.removeChild(b)}function oa(a,b,d){var e=b==="width"?a.offsetWidth:a.offsetHeight;if(d==="border")return e;c.each(b==="width"?Pa:Qa,function(){d||(e-=parseFloat(c.css(a,"padding"+this))||0);if(d==="margin")e+=parseFloat(c.css(a,
"margin"+this))||0;else e-=parseFloat(c.css(a,"border"+this+"Width"))||0});return e}function da(a,b,d,e){if(c.isArray(b)&&b.length)c.each(b,function(f,h){d||Ra.test(a)?e(a,h):da(a+"["+(typeof h==="object"||c.isArray(h)?f:"")+"]",h,d,e)});else if(!d&&b!=null&&typeof b==="object")c.isEmptyObject(b)?e(a,""):c.each(b,function(f,h){da(a+"["+f+"]",h,d,e)});else e(a,b)}function S(a,b){var d={};c.each(pa.concat.apply([],pa.slice(0,b)),function(){d[this]=a});return d}function qa(a){if(!ea[a]){var b=c("<"+
a+">").appendTo("body"),d=b.css("display");b.remove();if(d==="none"||d==="")d="block";ea[a]=d}return ea[a]}function fa(a){return c.isWindow(a)?a:a.nodeType===9?a.defaultView||a.parentWindow:false}var t=E.document,c=function(){function a(){if(!b.isReady){try{t.documentElement.doScroll("left")}catch(j){setTimeout(a,1);return}b.ready()}}var b=function(j,s){return new b.fn.init(j,s)},d=E.jQuery,e=E.$,f,h=/^(?:[^<]*(<[\w\W]+>)[^>]*$|#([\w\-]+)$)/,l=/\S/,k=/^\s+/,o=/\s+$/,x=/\W/,r=/\d/,A=/^<(\w+)\s*\/?>(?:<\/\1>)?$/,
C=/^[\],:{}\s]*$/,J=/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,w=/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,I=/(?:^|:|,)(?:\s*\[)+/g,L=/(webkit)[ \/]([\w.]+)/,g=/(opera)(?:.*version)?[ \/]([\w.]+)/,i=/(msie) ([\w.]+)/,n=/(mozilla)(?:.*? rv:([\w.]+))?/,m=navigator.userAgent,p=false,q=[],u,y=Object.prototype.toString,F=Object.prototype.hasOwnProperty,M=Array.prototype.push,N=Array.prototype.slice,O=String.prototype.trim,D=Array.prototype.indexOf,R={};b.fn=b.prototype={init:function(j,
s){var v,z,H;if(!j)return this;if(j.nodeType){this.context=this[0]=j;this.length=1;return this}if(j==="body"&&!s&&t.body){this.context=t;this[0]=t.body;this.selector="body";this.length=1;return this}if(typeof j==="string")if((v=h.exec(j))&&(v[1]||!s))if(v[1]){H=s?s.ownerDocument||s:t;if(z=A.exec(j))if(b.isPlainObject(s)){j=[t.createElement(z[1])];b.fn.attr.call(j,s,true)}else j=[H.createElement(z[1])];else{z=b.buildFragment([v[1]],[H]);j=(z.cacheable?z.fragment.cloneNode(true):z.fragment).childNodes}return b.merge(this,
j)}else{if((z=t.getElementById(v[2]))&&z.parentNode){if(z.id!==v[2])return f.find(j);this.length=1;this[0]=z}this.context=t;this.selector=j;return this}else if(!s&&!x.test(j)){this.selector=j;this.context=t;j=t.getElementsByTagName(j);return b.merge(this,j)}else return!s||s.jquery?(s||f).find(j):b(s).find(j);else if(b.isFunction(j))return f.ready(j);if(j.selector!==B){this.selector=j.selector;this.context=j.context}return b.makeArray(j,this)},selector:"",jquery:"1.4.4",length:0,size:function(){return this.length},
toArray:function(){return N.call(this,0)},get:function(j){return j==null?this.toArray():j<0?this.slice(j)[0]:this[j]},pushStack:function(j,s,v){var z=b();b.isArray(j)?M.apply(z,j):b.merge(z,j);z.prevObject=this;z.context=this.context;if(s==="find")z.selector=this.selector+(this.selector?" ":"")+v;else if(s)z.selector=this.selector+"."+s+"("+v+")";return z},each:function(j,s){return b.each(this,j,s)},ready:function(j){b.bindReady();if(b.isReady)j.call(t,b);else q&&q.push(j);return this},eq:function(j){return j===
-1?this.slice(j):this.slice(j,+j+1)},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},slice:function(){return this.pushStack(N.apply(this,arguments),"slice",N.call(arguments).join(","))},map:function(j){return this.pushStack(b.map(this,function(s,v){return j.call(s,v,s)}))},end:function(){return this.prevObject||b(null)},push:M,sort:[].sort,splice:[].splice};b.fn.init.prototype=b.fn;b.extend=b.fn.extend=function(){var j,s,v,z,H,G=arguments[0]||{},K=1,Q=arguments.length,ga=false;
if(typeof G==="boolean"){ga=G;G=arguments[1]||{};K=2}if(typeof G!=="object"&&!b.isFunction(G))G={};if(Q===K){G=this;--K}for(;K<Q;K++)if((j=arguments[K])!=null)for(s in j){v=G[s];z=j[s];if(G!==z)if(ga&&z&&(b.isPlainObject(z)||(H=b.isArray(z)))){if(H){H=false;v=v&&b.isArray(v)?v:[]}else v=v&&b.isPlainObject(v)?v:{};G[s]=b.extend(ga,v,z)}else if(z!==B)G[s]=z}return G};b.extend({noConflict:function(j){E.$=e;if(j)E.jQuery=d;return b},isReady:false,readyWait:1,ready:function(j){j===true&&b.readyWait--;
if(!b.readyWait||j!==true&&!b.isReady){if(!t.body)return setTimeout(b.ready,1);b.isReady=true;if(!(j!==true&&--b.readyWait>0))if(q){var s=0,v=q;for(q=null;j=v[s++];)j.call(t,b);b.fn.trigger&&b(t).trigger("ready").unbind("ready")}}},bindReady:function(){if(!p){p=true;if(t.readyState==="complete")return setTimeout(b.ready,1);if(t.addEventListener){t.addEventListener("DOMContentLoaded",u,false);E.addEventListener("load",b.ready,false)}else if(t.attachEvent){t.attachEvent("onreadystatechange",u);E.attachEvent("onload",
b.ready);var j=false;try{j=E.frameElement==null}catch(s){}t.documentElement.doScroll&&j&&a()}}},isFunction:function(j){return b.type(j)==="function"},isArray:Array.isArray||function(j){return b.type(j)==="array"},isWindow:function(j){return j&&typeof j==="object"&&"setInterval"in j},isNaN:function(j){return j==null||!r.test(j)||isNaN(j)},type:function(j){return j==null?String(j):R[y.call(j)]||"object"},isPlainObject:function(j){if(!j||b.type(j)!=="object"||j.nodeType||b.isWindow(j))return false;if(j.constructor&&
!F.call(j,"constructor")&&!F.call(j.constructor.prototype,"isPrototypeOf"))return false;for(var s in j);return s===B||F.call(j,s)},isEmptyObject:function(j){for(var s in j)return false;return true},error:function(j){throw j;},parseJSON:function(j){if(typeof j!=="string"||!j)return null;j=b.trim(j);if(C.test(j.replace(J,"@").replace(w,"]").replace(I,"")))return E.JSON&&E.JSON.parse?E.JSON.parse(j):(new Function("return "+j))();else b.error("Invalid JSON: "+j)},noop:function(){},globalEval:function(j){if(j&&
l.test(j)){var s=t.getElementsByTagName("head")[0]||t.documentElement,v=t.createElement("script");v.type="text/javascript";if(b.support.scriptEval)v.appendChild(t.createTextNode(j));else v.text=j;s.insertBefore(v,s.firstChild);s.removeChild(v)}},nodeName:function(j,s){return j.nodeName&&j.nodeName.toUpperCase()===s.toUpperCase()},each:function(j,s,v){var z,H=0,G=j.length,K=G===B||b.isFunction(j);if(v)if(K)for(z in j){if(s.apply(j[z],v)===false)break}else for(;H<G;){if(s.apply(j[H++],v)===false)break}else if(K)for(z in j){if(s.call(j[z],
z,j[z])===false)break}else for(v=j[0];H<G&&s.call(v,H,v)!==false;v=j[++H]);return j},trim:O?function(j){return j==null?"":O.call(j)}:function(j){return j==null?"":j.toString().replace(k,"").replace(o,"")},makeArray:function(j,s){var v=s||[];if(j!=null){var z=b.type(j);j.length==null||z==="string"||z==="function"||z==="regexp"||b.isWindow(j)?M.call(v,j):b.merge(v,j)}return v},inArray:function(j,s){if(s.indexOf)return s.indexOf(j);for(var v=0,z=s.length;v<z;v++)if(s[v]===j)return v;return-1},merge:function(j,
s){var v=j.length,z=0;if(typeof s.length==="number")for(var H=s.length;z<H;z++)j[v++]=s[z];else for(;s[z]!==B;)j[v++]=s[z++];j.length=v;return j},grep:function(j,s,v){var z=[],H;v=!!v;for(var G=0,K=j.length;G<K;G++){H=!!s(j[G],G);v!==H&&z.push(j[G])}return z},map:function(j,s,v){for(var z=[],H,G=0,K=j.length;G<K;G++){H=s(j[G],G,v);if(H!=null)z[z.length]=H}return z.concat.apply([],z)},guid:1,proxy:function(j,s,v){if(arguments.length===2)if(typeof s==="string"){v=j;j=v[s];s=B}else if(s&&!b.isFunction(s)){v=
s;s=B}if(!s&&j)s=function(){return j.apply(v||this,arguments)};if(j)s.guid=j.guid=j.guid||s.guid||b.guid++;return s},access:function(j,s,v,z,H,G){var K=j.length;if(typeof s==="object"){for(var Q in s)b.access(j,Q,s[Q],z,H,v);return j}if(v!==B){z=!G&&z&&b.isFunction(v);for(Q=0;Q<K;Q++)H(j[Q],s,z?v.call(j[Q],Q,H(j[Q],s)):v,G);return j}return K?H(j[0],s):B},now:function(){return(new Date).getTime()},uaMatch:function(j){j=j.toLowerCase();j=L.exec(j)||g.exec(j)||i.exec(j)||j.indexOf("compatible")<0&&n.exec(j)||
[];return{browser:j[1]||"",version:j[2]||"0"}},browser:{}});b.each("Boolean Number String Function Array Date RegExp Object".split(" "),function(j,s){R["[object "+s+"]"]=s.toLowerCase()});m=b.uaMatch(m);if(m.browser){b.browser[m.browser]=true;b.browser.version=m.version}if(b.browser.webkit)b.browser.safari=true;if(D)b.inArray=function(j,s){return D.call(s,j)};if(!/\s/.test("\u00a0")){k=/^[\s\xA0]+/;o=/[\s\xA0]+$/}f=b(t);if(t.addEventListener)u=function(){t.removeEventListener("DOMContentLoaded",u,
false);b.ready()};else if(t.attachEvent)u=function(){if(t.readyState==="complete"){t.detachEvent("onreadystatechange",u);b.ready()}};return E.jQuery=E.$=b}();(function(){c.support={};var a=t.documentElement,b=t.createElement("script"),d=t.createElement("div"),e="script"+c.now();d.style.display="none";d.innerHTML="   <link/><table></table><a href='/a' style='color:red;float:left;opacity:.55;'>a</a><input type='checkbox'/>";var f=d.getElementsByTagName("*"),h=d.getElementsByTagName("a")[0],l=t.createElement("select"),
k=l.appendChild(t.createElement("option"));if(!(!f||!f.length||!h)){c.support={leadingWhitespace:d.firstChild.nodeType===3,tbody:!d.getElementsByTagName("tbody").length,htmlSerialize:!!d.getElementsByTagName("link").length,style:/red/.test(h.getAttribute("style")),hrefNormalized:h.getAttribute("href")==="/a",opacity:/^0.55$/.test(h.style.opacity),cssFloat:!!h.style.cssFloat,checkOn:d.getElementsByTagName("input")[0].value==="on",optSelected:k.selected,deleteExpando:true,optDisabled:false,checkClone:false,
scriptEval:false,noCloneEvent:true,boxModel:null,inlineBlockNeedsLayout:false,shrinkWrapBlocks:false,reliableHiddenOffsets:true};l.disabled=true;c.support.optDisabled=!k.disabled;b.type="text/javascript";try{b.appendChild(t.createTextNode("window."+e+"=1;"))}catch(o){}a.insertBefore(b,a.firstChild);if(E[e]){c.support.scriptEval=true;delete E[e]}try{delete b.test}catch(x){c.support.deleteExpando=false}a.removeChild(b);if(d.attachEvent&&d.fireEvent){d.attachEvent("onclick",function r(){c.support.noCloneEvent=
false;d.detachEvent("onclick",r)});d.cloneNode(true).fireEvent("onclick")}d=t.createElement("div");d.innerHTML="<input type='radio' name='radiotest' checked='checked'/>";a=t.createDocumentFragment();a.appendChild(d.firstChild);c.support.checkClone=a.cloneNode(true).cloneNode(true).lastChild.checked;c(function(){var r=t.createElement("div");r.style.width=r.style.paddingLeft="1px";t.body.appendChild(r);c.boxModel=c.support.boxModel=r.offsetWidth===2;if("zoom"in r.style){r.style.display="inline";r.style.zoom=
1;c.support.inlineBlockNeedsLayout=r.offsetWidth===2;r.style.display="";r.innerHTML="<div style='width:4px;'></div>";c.support.shrinkWrapBlocks=r.offsetWidth!==2}r.innerHTML="<table><tr><td style='padding:0;display:none'></td><td>t</td></tr></table>";var A=r.getElementsByTagName("td");c.support.reliableHiddenOffsets=A[0].offsetHeight===0;A[0].style.display="";A[1].style.display="none";c.support.reliableHiddenOffsets=c.support.reliableHiddenOffsets&&A[0].offsetHeight===0;r.innerHTML="";t.body.removeChild(r).style.display=
"none"});a=function(r){var A=t.createElement("div");r="on"+r;var C=r in A;if(!C){A.setAttribute(r,"return;");C=typeof A[r]==="function"}return C};c.support.submitBubbles=a("submit");c.support.changeBubbles=a("change");a=b=d=f=h=null}})();var ra={},Ja=/^(?:\{.*\}|\[.*\])$/;c.extend({cache:{},uuid:0,expando:"jQuery"+c.now(),noData:{embed:true,object:"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",applet:true},data:function(a,b,d){if(c.acceptData(a)){a=a==E?ra:a;var e=a.nodeType,f=e?a[c.expando]:null,h=
c.cache;if(!(e&&!f&&typeof b==="string"&&d===B)){if(e)f||(a[c.expando]=f=++c.uuid);else h=a;if(typeof b==="object")if(e)h[f]=c.extend(h[f],b);else c.extend(h,b);else if(e&&!h[f])h[f]={};a=e?h[f]:h;if(d!==B)a[b]=d;return typeof b==="string"?a[b]:a}}},removeData:function(a,b){if(c.acceptData(a)){a=a==E?ra:a;var d=a.nodeType,e=d?a[c.expando]:a,f=c.cache,h=d?f[e]:e;if(b){if(h){delete h[b];d&&c.isEmptyObject(h)&&c.removeData(a)}}else if(d&&c.support.deleteExpando)delete a[c.expando];else if(a.removeAttribute)a.removeAttribute(c.expando);
else if(d)delete f[e];else for(var l in a)delete a[l]}},acceptData:function(a){if(a.nodeName){var b=c.noData[a.nodeName.toLowerCase()];if(b)return!(b===true||a.getAttribute("classid")!==b)}return true}});c.fn.extend({data:function(a,b){var d=null;if(typeof a==="undefined"){if(this.length){var e=this[0].attributes,f;d=c.data(this[0]);for(var h=0,l=e.length;h<l;h++){f=e[h].name;if(f.indexOf("data-")===0){f=f.substr(5);ka(this[0],f,d[f])}}}return d}else if(typeof a==="object")return this.each(function(){c.data(this,
a)});var k=a.split(".");k[1]=k[1]?"."+k[1]:"";if(b===B){d=this.triggerHandler("getData"+k[1]+"!",[k[0]]);if(d===B&&this.length){d=c.data(this[0],a);d=ka(this[0],a,d)}return d===B&&k[1]?this.data(k[0]):d}else return this.each(function(){var o=c(this),x=[k[0],b];o.triggerHandler("setData"+k[1]+"!",x);c.data(this,a,b);o.triggerHandler("changeData"+k[1]+"!",x)})},removeData:function(a){return this.each(function(){c.removeData(this,a)})}});c.extend({queue:function(a,b,d){if(a){b=(b||"fx")+"queue";var e=
c.data(a,b);if(!d)return e||[];if(!e||c.isArray(d))e=c.data(a,b,c.makeArray(d));else e.push(d);return e}},dequeue:function(a,b){b=b||"fx";var d=c.queue(a,b),e=d.shift();if(e==="inprogress")e=d.shift();if(e){b==="fx"&&d.unshift("inprogress");e.call(a,function(){c.dequeue(a,b)})}}});c.fn.extend({queue:function(a,b){if(typeof a!=="string"){b=a;a="fx"}if(b===B)return c.queue(this[0],a);return this.each(function(){var d=c.queue(this,a,b);a==="fx"&&d[0]!=="inprogress"&&c.dequeue(this,a)})},dequeue:function(a){return this.each(function(){c.dequeue(this,
a)})},delay:function(a,b){a=c.fx?c.fx.speeds[a]||a:a;b=b||"fx";return this.queue(b,function(){var d=this;setTimeout(function(){c.dequeue(d,b)},a)})},clearQueue:function(a){return this.queue(a||"fx",[])}});var sa=/[\n\t]/g,ha=/\s+/,Sa=/\r/g,Ta=/^(?:href|src|style)$/,Ua=/^(?:button|input)$/i,Va=/^(?:button|input|object|select|textarea)$/i,Wa=/^a(?:rea)?$/i,ta=/^(?:radio|checkbox)$/i;c.props={"for":"htmlFor","class":"className",readonly:"readOnly",maxlength:"maxLength",cellspacing:"cellSpacing",rowspan:"rowSpan",
colspan:"colSpan",tabindex:"tabIndex",usemap:"useMap",frameborder:"frameBorder"};c.fn.extend({attr:function(a,b){return c.access(this,a,b,true,c.attr)},removeAttr:function(a){return this.each(function(){c.attr(this,a,"");this.nodeType===1&&this.removeAttribute(a)})},addClass:function(a){if(c.isFunction(a))return this.each(function(x){var r=c(this);r.addClass(a.call(this,x,r.attr("class")))});if(a&&typeof a==="string")for(var b=(a||"").split(ha),d=0,e=this.length;d<e;d++){var f=this[d];if(f.nodeType===
1)if(f.className){for(var h=" "+f.className+" ",l=f.className,k=0,o=b.length;k<o;k++)if(h.indexOf(" "+b[k]+" ")<0)l+=" "+b[k];f.className=c.trim(l)}else f.className=a}return this},removeClass:function(a){if(c.isFunction(a))return this.each(function(o){var x=c(this);x.removeClass(a.call(this,o,x.attr("class")))});if(a&&typeof a==="string"||a===B)for(var b=(a||"").split(ha),d=0,e=this.length;d<e;d++){var f=this[d];if(f.nodeType===1&&f.className)if(a){for(var h=(" "+f.className+" ").replace(sa," "),
l=0,k=b.length;l<k;l++)h=h.replace(" "+b[l]+" "," ");f.className=c.trim(h)}else f.className=""}return this},toggleClass:function(a,b){var d=typeof a,e=typeof b==="boolean";if(c.isFunction(a))return this.each(function(f){var h=c(this);h.toggleClass(a.call(this,f,h.attr("class"),b),b)});return this.each(function(){if(d==="string")for(var f,h=0,l=c(this),k=b,o=a.split(ha);f=o[h++];){k=e?k:!l.hasClass(f);l[k?"addClass":"removeClass"](f)}else if(d==="undefined"||d==="boolean"){this.className&&c.data(this,
"__className__",this.className);this.className=this.className||a===false?"":c.data(this,"__className__")||""}})},hasClass:function(a){a=" "+a+" ";for(var b=0,d=this.length;b<d;b++)if((" "+this[b].className+" ").replace(sa," ").indexOf(a)>-1)return true;return false},val:function(a){if(!arguments.length){var b=this[0];if(b){if(c.nodeName(b,"option")){var d=b.attributes.value;return!d||d.specified?b.value:b.text}if(c.nodeName(b,"select")){var e=b.selectedIndex;d=[];var f=b.options;b=b.type==="select-one";
if(e<0)return null;var h=b?e:0;for(e=b?e+1:f.length;h<e;h++){var l=f[h];if(l.selected&&(c.support.optDisabled?!l.disabled:l.getAttribute("disabled")===null)&&(!l.parentNode.disabled||!c.nodeName(l.parentNode,"optgroup"))){a=c(l).val();if(b)return a;d.push(a)}}return d}if(ta.test(b.type)&&!c.support.checkOn)return b.getAttribute("value")===null?"on":b.value;return(b.value||"").replace(Sa,"")}return B}var k=c.isFunction(a);return this.each(function(o){var x=c(this),r=a;if(this.nodeType===1){if(k)r=
a.call(this,o,x.val());if(r==null)r="";else if(typeof r==="number")r+="";else if(c.isArray(r))r=c.map(r,function(C){return C==null?"":C+""});if(c.isArray(r)&&ta.test(this.type))this.checked=c.inArray(x.val(),r)>=0;else if(c.nodeName(this,"select")){var A=c.makeArray(r);c("option",this).each(function(){this.selected=c.inArray(c(this).val(),A)>=0});if(!A.length)this.selectedIndex=-1}else this.value=r}})}});c.extend({attrFn:{val:true,css:true,html:true,text:true,data:true,width:true,height:true,offset:true},
attr:function(a,b,d,e){if(!a||a.nodeType===3||a.nodeType===8)return B;if(e&&b in c.attrFn)return c(a)[b](d);e=a.nodeType!==1||!c.isXMLDoc(a);var f=d!==B;b=e&&c.props[b]||b;var h=Ta.test(b);if((b in a||a[b]!==B)&&e&&!h){if(f){b==="type"&&Ua.test(a.nodeName)&&a.parentNode&&c.error("type property can't be changed");if(d===null)a.nodeType===1&&a.removeAttribute(b);else a[b]=d}if(c.nodeName(a,"form")&&a.getAttributeNode(b))return a.getAttributeNode(b).nodeValue;if(b==="tabIndex")return(b=a.getAttributeNode("tabIndex"))&&
b.specified?b.value:Va.test(a.nodeName)||Wa.test(a.nodeName)&&a.href?0:B;return a[b]}if(!c.support.style&&e&&b==="style"){if(f)a.style.cssText=""+d;return a.style.cssText}f&&a.setAttribute(b,""+d);if(!a.attributes[b]&&a.hasAttribute&&!a.hasAttribute(b))return B;a=!c.support.hrefNormalized&&e&&h?a.getAttribute(b,2):a.getAttribute(b);return a===null?B:a}});var X=/\.(.*)$/,ia=/^(?:textarea|input|select)$/i,La=/\./g,Ma=/ /g,Xa=/[^\w\s.|`]/g,Ya=function(a){return a.replace(Xa,"\\$&")},ua={focusin:0,focusout:0};
c.event={add:function(a,b,d,e){if(!(a.nodeType===3||a.nodeType===8)){if(c.isWindow(a)&&a!==E&&!a.frameElement)a=E;if(d===false)d=U;else if(!d)return;var f,h;if(d.handler){f=d;d=f.handler}if(!d.guid)d.guid=c.guid++;if(h=c.data(a)){var l=a.nodeType?"events":"__events__",k=h[l],o=h.handle;if(typeof k==="function"){o=k.handle;k=k.events}else if(!k){a.nodeType||(h[l]=h=function(){});h.events=k={}}if(!o)h.handle=o=function(){return typeof c!=="undefined"&&!c.event.triggered?c.event.handle.apply(o.elem,
arguments):B};o.elem=a;b=b.split(" ");for(var x=0,r;l=b[x++];){h=f?c.extend({},f):{handler:d,data:e};if(l.indexOf(".")>-1){r=l.split(".");l=r.shift();h.namespace=r.slice(0).sort().join(".")}else{r=[];h.namespace=""}h.type=l;if(!h.guid)h.guid=d.guid;var A=k[l],C=c.event.special[l]||{};if(!A){A=k[l]=[];if(!C.setup||C.setup.call(a,e,r,o)===false)if(a.addEventListener)a.addEventListener(l,o,false);else a.attachEvent&&a.attachEvent("on"+l,o)}if(C.add){C.add.call(a,h);if(!h.handler.guid)h.handler.guid=
d.guid}A.push(h);c.event.global[l]=true}a=null}}},global:{},remove:function(a,b,d,e){if(!(a.nodeType===3||a.nodeType===8)){if(d===false)d=U;var f,h,l=0,k,o,x,r,A,C,J=a.nodeType?"events":"__events__",w=c.data(a),I=w&&w[J];if(w&&I){if(typeof I==="function"){w=I;I=I.events}if(b&&b.type){d=b.handler;b=b.type}if(!b||typeof b==="string"&&b.charAt(0)==="."){b=b||"";for(f in I)c.event.remove(a,f+b)}else{for(b=b.split(" ");f=b[l++];){r=f;k=f.indexOf(".")<0;o=[];if(!k){o=f.split(".");f=o.shift();x=RegExp("(^|\\.)"+
c.map(o.slice(0).sort(),Ya).join("\\.(?:.*\\.)?")+"(\\.|$)")}if(A=I[f])if(d){r=c.event.special[f]||{};for(h=e||0;h<A.length;h++){C=A[h];if(d.guid===C.guid){if(k||x.test(C.namespace)){e==null&&A.splice(h--,1);r.remove&&r.remove.call(a,C)}if(e!=null)break}}if(A.length===0||e!=null&&A.length===1){if(!r.teardown||r.teardown.call(a,o)===false)c.removeEvent(a,f,w.handle);delete I[f]}}else for(h=0;h<A.length;h++){C=A[h];if(k||x.test(C.namespace)){c.event.remove(a,r,C.handler,h);A.splice(h--,1)}}}if(c.isEmptyObject(I)){if(b=
w.handle)b.elem=null;delete w.events;delete w.handle;if(typeof w==="function")c.removeData(a,J);else c.isEmptyObject(w)&&c.removeData(a)}}}}},trigger:function(a,b,d,e){var f=a.type||a;if(!e){a=typeof a==="object"?a[c.expando]?a:c.extend(c.Event(f),a):c.Event(f);if(f.indexOf("!")>=0){a.type=f=f.slice(0,-1);a.exclusive=true}if(!d){a.stopPropagation();c.event.global[f]&&c.each(c.cache,function(){this.events&&this.events[f]&&c.event.trigger(a,b,this.handle.elem)})}if(!d||d.nodeType===3||d.nodeType===
8)return B;a.result=B;a.target=d;b=c.makeArray(b);b.unshift(a)}a.currentTarget=d;(e=d.nodeType?c.data(d,"handle"):(c.data(d,"__events__")||{}).handle)&&e.apply(d,b);e=d.parentNode||d.ownerDocument;try{if(!(d&&d.nodeName&&c.noData[d.nodeName.toLowerCase()]))if(d["on"+f]&&d["on"+f].apply(d,b)===false){a.result=false;a.preventDefault()}}catch(h){}if(!a.isPropagationStopped()&&e)c.event.trigger(a,b,e,true);else if(!a.isDefaultPrevented()){var l;e=a.target;var k=f.replace(X,""),o=c.nodeName(e,"a")&&k===
"click",x=c.event.special[k]||{};if((!x._default||x._default.call(d,a)===false)&&!o&&!(e&&e.nodeName&&c.noData[e.nodeName.toLowerCase()])){try{if(e[k]){if(l=e["on"+k])e["on"+k]=null;c.event.triggered=true;e[k]()}}catch(r){}if(l)e["on"+k]=l;c.event.triggered=false}}},handle:function(a){var b,d,e,f;d=[];var h=c.makeArray(arguments);a=h[0]=c.event.fix(a||E.event);a.currentTarget=this;b=a.type.indexOf(".")<0&&!a.exclusive;if(!b){e=a.type.split(".");a.type=e.shift();d=e.slice(0).sort();e=RegExp("(^|\\.)"+
d.join("\\.(?:.*\\.)?")+"(\\.|$)")}a.namespace=a.namespace||d.join(".");f=c.data(this,this.nodeType?"events":"__events__");if(typeof f==="function")f=f.events;d=(f||{})[a.type];if(f&&d){d=d.slice(0);f=0;for(var l=d.length;f<l;f++){var k=d[f];if(b||e.test(k.namespace)){a.handler=k.handler;a.data=k.data;a.handleObj=k;k=k.handler.apply(this,h);if(k!==B){a.result=k;if(k===false){a.preventDefault();a.stopPropagation()}}if(a.isImmediatePropagationStopped())break}}}return a.result},props:"altKey attrChange attrName bubbles button cancelable charCode clientX clientY ctrlKey currentTarget data detail eventPhase fromElement handler keyCode layerX layerY metaKey newValue offsetX offsetY pageX pageY prevValue relatedNode relatedTarget screenX screenY shiftKey srcElement target toElement view wheelDelta which".split(" "),
fix:function(a){if(a[c.expando])return a;var b=a;a=c.Event(b);for(var d=this.props.length,e;d;){e=this.props[--d];a[e]=b[e]}if(!a.target)a.target=a.srcElement||t;if(a.target.nodeType===3)a.target=a.target.parentNode;if(!a.relatedTarget&&a.fromElement)a.relatedTarget=a.fromElement===a.target?a.toElement:a.fromElement;if(a.pageX==null&&a.clientX!=null){b=t.documentElement;d=t.body;a.pageX=a.clientX+(b&&b.scrollLeft||d&&d.scrollLeft||0)-(b&&b.clientLeft||d&&d.clientLeft||0);a.pageY=a.clientY+(b&&b.scrollTop||
d&&d.scrollTop||0)-(b&&b.clientTop||d&&d.clientTop||0)}if(a.which==null&&(a.charCode!=null||a.keyCode!=null))a.which=a.charCode!=null?a.charCode:a.keyCode;if(!a.metaKey&&a.ctrlKey)a.metaKey=a.ctrlKey;if(!a.which&&a.button!==B)a.which=a.button&1?1:a.button&2?3:a.button&4?2:0;return a},guid:1E8,proxy:c.proxy,special:{ready:{setup:c.bindReady,teardown:c.noop},live:{add:function(a){c.event.add(this,Y(a.origType,a.selector),c.extend({},a,{handler:Ka,guid:a.handler.guid}))},remove:function(a){c.event.remove(this,
Y(a.origType,a.selector),a)}},beforeunload:{setup:function(a,b,d){if(c.isWindow(this))this.onbeforeunload=d},teardown:function(a,b){if(this.onbeforeunload===b)this.onbeforeunload=null}}}};c.removeEvent=t.removeEventListener?function(a,b,d){a.removeEventListener&&a.removeEventListener(b,d,false)}:function(a,b,d){a.detachEvent&&a.detachEvent("on"+b,d)};c.Event=function(a){if(!this.preventDefault)return new c.Event(a);if(a&&a.type){this.originalEvent=a;this.type=a.type}else this.type=a;this.timeStamp=
c.now();this[c.expando]=true};c.Event.prototype={preventDefault:function(){this.isDefaultPrevented=ca;var a=this.originalEvent;if(a)if(a.preventDefault)a.preventDefault();else a.returnValue=false},stopPropagation:function(){this.isPropagationStopped=ca;var a=this.originalEvent;if(a){a.stopPropagation&&a.stopPropagation();a.cancelBubble=true}},stopImmediatePropagation:function(){this.isImmediatePropagationStopped=ca;this.stopPropagation()},isDefaultPrevented:U,isPropagationStopped:U,isImmediatePropagationStopped:U};
var va=function(a){var b=a.relatedTarget;try{for(;b&&b!==this;)b=b.parentNode;if(b!==this){a.type=a.data;c.event.handle.apply(this,arguments)}}catch(d){}},wa=function(a){a.type=a.data;c.event.handle.apply(this,arguments)};c.each({mouseenter:"mouseover",mouseleave:"mouseout"},function(a,b){c.event.special[a]={setup:function(d){c.event.add(this,b,d&&d.selector?wa:va,a)},teardown:function(d){c.event.remove(this,b,d&&d.selector?wa:va)}}});if(!c.support.submitBubbles)c.event.special.submit={setup:function(){if(this.nodeName.toLowerCase()!==
"form"){c.event.add(this,"click.specialSubmit",function(a){var b=a.target,d=b.type;if((d==="submit"||d==="image")&&c(b).closest("form").length){a.liveFired=B;return la("submit",this,arguments)}});c.event.add(this,"keypress.specialSubmit",function(a){var b=a.target,d=b.type;if((d==="text"||d==="password")&&c(b).closest("form").length&&a.keyCode===13){a.liveFired=B;return la("submit",this,arguments)}})}else return false},teardown:function(){c.event.remove(this,".specialSubmit")}};if(!c.support.changeBubbles){var V,
xa=function(a){var b=a.type,d=a.value;if(b==="radio"||b==="checkbox")d=a.checked;else if(b==="select-multiple")d=a.selectedIndex>-1?c.map(a.options,function(e){return e.selected}).join("-"):"";else if(a.nodeName.toLowerCase()==="select")d=a.selectedIndex;return d},Z=function(a,b){var d=a.target,e,f;if(!(!ia.test(d.nodeName)||d.readOnly)){e=c.data(d,"_change_data");f=xa(d);if(a.type!=="focusout"||d.type!=="radio")c.data(d,"_change_data",f);if(!(e===B||f===e))if(e!=null||f){a.type="change";a.liveFired=
B;return c.event.trigger(a,b,d)}}};c.event.special.change={filters:{focusout:Z,beforedeactivate:Z,click:function(a){var b=a.target,d=b.type;if(d==="radio"||d==="checkbox"||b.nodeName.toLowerCase()==="select")return Z.call(this,a)},keydown:function(a){var b=a.target,d=b.type;if(a.keyCode===13&&b.nodeName.toLowerCase()!=="textarea"||a.keyCode===32&&(d==="checkbox"||d==="radio")||d==="select-multiple")return Z.call(this,a)},beforeactivate:function(a){a=a.target;c.data(a,"_change_data",xa(a))}},setup:function(){if(this.type===
"file")return false;for(var a in V)c.event.add(this,a+".specialChange",V[a]);return ia.test(this.nodeName)},teardown:function(){c.event.remove(this,".specialChange");return ia.test(this.nodeName)}};V=c.event.special.change.filters;V.focus=V.beforeactivate}t.addEventListener&&c.each({focus:"focusin",blur:"focusout"},function(a,b){function d(e){e=c.event.fix(e);e.type=b;return c.event.trigger(e,null,e.target)}c.event.special[b]={setup:function(){ua[b]++===0&&t.addEventListener(a,d,true)},teardown:function(){--ua[b]===
0&&t.removeEventListener(a,d,true)}}});c.each(["bind","one"],function(a,b){c.fn[b]=function(d,e,f){if(typeof d==="object"){for(var h in d)this[b](h,e,d[h],f);return this}if(c.isFunction(e)||e===false){f=e;e=B}var l=b==="one"?c.proxy(f,function(o){c(this).unbind(o,l);return f.apply(this,arguments)}):f;if(d==="unload"&&b!=="one")this.one(d,e,f);else{h=0;for(var k=this.length;h<k;h++)c.event.add(this[h],d,l,e)}return this}});c.fn.extend({unbind:function(a,b){if(typeof a==="object"&&!a.preventDefault)for(var d in a)this.unbind(d,
a[d]);else{d=0;for(var e=this.length;d<e;d++)c.event.remove(this[d],a,b)}return this},delegate:function(a,b,d,e){return this.live(b,d,e,a)},undelegate:function(a,b,d){return arguments.length===0?this.unbind("live"):this.die(b,null,d,a)},trigger:function(a,b){return this.each(function(){c.event.trigger(a,b,this)})},triggerHandler:function(a,b){if(this[0]){var d=c.Event(a);d.preventDefault();d.stopPropagation();c.event.trigger(d,b,this[0]);return d.result}},toggle:function(a){for(var b=arguments,d=
1;d<b.length;)c.proxy(a,b[d++]);return this.click(c.proxy(a,function(e){var f=(c.data(this,"lastToggle"+a.guid)||0)%d;c.data(this,"lastToggle"+a.guid,f+1);e.preventDefault();return b[f].apply(this,arguments)||false}))},hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)}});var ya={focus:"focusin",blur:"focusout",mouseenter:"mouseover",mouseleave:"mouseout"};c.each(["live","die"],function(a,b){c.fn[b]=function(d,e,f,h){var l,k=0,o,x,r=h||this.selector;h=h?this:c(this.context);if(typeof d===
"object"&&!d.preventDefault){for(l in d)h[b](l,e,d[l],r);return this}if(c.isFunction(e)){f=e;e=B}for(d=(d||"").split(" ");(l=d[k++])!=null;){o=X.exec(l);x="";if(o){x=o[0];l=l.replace(X,"")}if(l==="hover")d.push("mouseenter"+x,"mouseleave"+x);else{o=l;if(l==="focus"||l==="blur"){d.push(ya[l]+x);l+=x}else l=(ya[l]||l)+x;if(b==="live"){x=0;for(var A=h.length;x<A;x++)c.event.add(h[x],"live."+Y(l,r),{data:e,selector:r,handler:f,origType:l,origHandler:f,preType:o})}else h.unbind("live."+Y(l,r),f)}}return this}});
c.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error".split(" "),function(a,b){c.fn[b]=function(d,e){if(e==null){e=d;d=null}return arguments.length>0?this.bind(b,d,e):this.trigger(b)};if(c.attrFn)c.attrFn[b]=true});E.attachEvent&&!E.addEventListener&&c(E).bind("unload",function(){for(var a in c.cache)if(c.cache[a].handle)try{c.event.remove(c.cache[a].handle.elem)}catch(b){}});
(function(){function a(g,i,n,m,p,q){p=0;for(var u=m.length;p<u;p++){var y=m[p];if(y){var F=false;for(y=y[g];y;){if(y.sizcache===n){F=m[y.sizset];break}if(y.nodeType===1&&!q){y.sizcache=n;y.sizset=p}if(y.nodeName.toLowerCase()===i){F=y;break}y=y[g]}m[p]=F}}}function b(g,i,n,m,p,q){p=0;for(var u=m.length;p<u;p++){var y=m[p];if(y){var F=false;for(y=y[g];y;){if(y.sizcache===n){F=m[y.sizset];break}if(y.nodeType===1){if(!q){y.sizcache=n;y.sizset=p}if(typeof i!=="string"){if(y===i){F=true;break}}else if(k.filter(i,
[y]).length>0){F=y;break}}y=y[g]}m[p]=F}}}var d=/((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^\[\]]*\]|['"][^'"]*['"]|[^\[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g,e=0,f=Object.prototype.toString,h=false,l=true;[0,0].sort(function(){l=false;return 0});var k=function(g,i,n,m){n=n||[];var p=i=i||t;if(i.nodeType!==1&&i.nodeType!==9)return[];if(!g||typeof g!=="string")return n;var q,u,y,F,M,N=true,O=k.isXML(i),D=[],R=g;do{d.exec("");if(q=d.exec(R)){R=q[3];D.push(q[1]);if(q[2]){F=q[3];
break}}}while(q);if(D.length>1&&x.exec(g))if(D.length===2&&o.relative[D[0]])u=L(D[0]+D[1],i);else for(u=o.relative[D[0]]?[i]:k(D.shift(),i);D.length;){g=D.shift();if(o.relative[g])g+=D.shift();u=L(g,u)}else{if(!m&&D.length>1&&i.nodeType===9&&!O&&o.match.ID.test(D[0])&&!o.match.ID.test(D[D.length-1])){q=k.find(D.shift(),i,O);i=q.expr?k.filter(q.expr,q.set)[0]:q.set[0]}if(i){q=m?{expr:D.pop(),set:C(m)}:k.find(D.pop(),D.length===1&&(D[0]==="~"||D[0]==="+")&&i.parentNode?i.parentNode:i,O);u=q.expr?k.filter(q.expr,
q.set):q.set;if(D.length>0)y=C(u);else N=false;for(;D.length;){q=M=D.pop();if(o.relative[M])q=D.pop();else M="";if(q==null)q=i;o.relative[M](y,q,O)}}else y=[]}y||(y=u);y||k.error(M||g);if(f.call(y)==="[object Array]")if(N)if(i&&i.nodeType===1)for(g=0;y[g]!=null;g++){if(y[g]&&(y[g]===true||y[g].nodeType===1&&k.contains(i,y[g])))n.push(u[g])}else for(g=0;y[g]!=null;g++)y[g]&&y[g].nodeType===1&&n.push(u[g]);else n.push.apply(n,y);else C(y,n);if(F){k(F,p,n,m);k.uniqueSort(n)}return n};k.uniqueSort=function(g){if(w){h=
l;g.sort(w);if(h)for(var i=1;i<g.length;i++)g[i]===g[i-1]&&g.splice(i--,1)}return g};k.matches=function(g,i){return k(g,null,null,i)};k.matchesSelector=function(g,i){return k(i,null,null,[g]).length>0};k.find=function(g,i,n){var m;if(!g)return[];for(var p=0,q=o.order.length;p<q;p++){var u,y=o.order[p];if(u=o.leftMatch[y].exec(g)){var F=u[1];u.splice(1,1);if(F.substr(F.length-1)!=="\\"){u[1]=(u[1]||"").replace(/\\/g,"");m=o.find[y](u,i,n);if(m!=null){g=g.replace(o.match[y],"");break}}}}m||(m=i.getElementsByTagName("*"));
return{set:m,expr:g}};k.filter=function(g,i,n,m){for(var p,q,u=g,y=[],F=i,M=i&&i[0]&&k.isXML(i[0]);g&&i.length;){for(var N in o.filter)if((p=o.leftMatch[N].exec(g))!=null&&p[2]){var O,D,R=o.filter[N];D=p[1];q=false;p.splice(1,1);if(D.substr(D.length-1)!=="\\"){if(F===y)y=[];if(o.preFilter[N])if(p=o.preFilter[N](p,F,n,y,m,M)){if(p===true)continue}else q=O=true;if(p)for(var j=0;(D=F[j])!=null;j++)if(D){O=R(D,p,j,F);var s=m^!!O;if(n&&O!=null)if(s)q=true;else F[j]=false;else if(s){y.push(D);q=true}}if(O!==
B){n||(F=y);g=g.replace(o.match[N],"");if(!q)return[];break}}}if(g===u)if(q==null)k.error(g);else break;u=g}return F};k.error=function(g){throw"Syntax error, unrecognized expression: "+g;};var o=k.selectors={order:["ID","NAME","TAG"],match:{ID:/#((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,CLASS:/\.((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,NAME:/\[name=['"]*((?:[\w\u00c0-\uFFFF\-]|\\.)+)['"]*\]/,ATTR:/\[\s*((?:[\w\u00c0-\uFFFF\-]|\\.)+)\s*(?:(\S?=)\s*(['"]*)(.*?)\3|)\s*\]/,TAG:/^((?:[\w\u00c0-\uFFFF\*\-]|\\.)+)/,CHILD:/:(only|nth|last|first)-child(?:\((even|odd|[\dn+\-]*)\))?/,
POS:/:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^\-]|$)/,PSEUDO:/:((?:[\w\u00c0-\uFFFF\-]|\\.)+)(?:\((['"]?)((?:\([^\)]+\)|[^\(\)]*)+)\2\))?/},leftMatch:{},attrMap:{"class":"className","for":"htmlFor"},attrHandle:{href:function(g){return g.getAttribute("href")}},relative:{"+":function(g,i){var n=typeof i==="string",m=n&&!/\W/.test(i);n=n&&!m;if(m)i=i.toLowerCase();m=0;for(var p=g.length,q;m<p;m++)if(q=g[m]){for(;(q=q.previousSibling)&&q.nodeType!==1;);g[m]=n||q&&q.nodeName.toLowerCase()===
i?q||false:q===i}n&&k.filter(i,g,true)},">":function(g,i){var n,m=typeof i==="string",p=0,q=g.length;if(m&&!/\W/.test(i))for(i=i.toLowerCase();p<q;p++){if(n=g[p]){n=n.parentNode;g[p]=n.nodeName.toLowerCase()===i?n:false}}else{for(;p<q;p++)if(n=g[p])g[p]=m?n.parentNode:n.parentNode===i;m&&k.filter(i,g,true)}},"":function(g,i,n){var m,p=e++,q=b;if(typeof i==="string"&&!/\W/.test(i)){m=i=i.toLowerCase();q=a}q("parentNode",i,p,g,m,n)},"~":function(g,i,n){var m,p=e++,q=b;if(typeof i==="string"&&!/\W/.test(i)){m=
i=i.toLowerCase();q=a}q("previousSibling",i,p,g,m,n)}},find:{ID:function(g,i,n){if(typeof i.getElementById!=="undefined"&&!n)return(g=i.getElementById(g[1]))&&g.parentNode?[g]:[]},NAME:function(g,i){if(typeof i.getElementsByName!=="undefined"){for(var n=[],m=i.getElementsByName(g[1]),p=0,q=m.length;p<q;p++)m[p].getAttribute("name")===g[1]&&n.push(m[p]);return n.length===0?null:n}},TAG:function(g,i){return i.getElementsByTagName(g[1])}},preFilter:{CLASS:function(g,i,n,m,p,q){g=" "+g[1].replace(/\\/g,
"")+" ";if(q)return g;q=0;for(var u;(u=i[q])!=null;q++)if(u)if(p^(u.className&&(" "+u.className+" ").replace(/[\t\n]/g," ").indexOf(g)>=0))n||m.push(u);else if(n)i[q]=false;return false},ID:function(g){return g[1].replace(/\\/g,"")},TAG:function(g){return g[1].toLowerCase()},CHILD:function(g){if(g[1]==="nth"){var i=/(-?)(\d*)n((?:\+|-)?\d*)/.exec(g[2]==="even"&&"2n"||g[2]==="odd"&&"2n+1"||!/\D/.test(g[2])&&"0n+"+g[2]||g[2]);g[2]=i[1]+(i[2]||1)-0;g[3]=i[3]-0}g[0]=e++;return g},ATTR:function(g,i,n,
m,p,q){i=g[1].replace(/\\/g,"");if(!q&&o.attrMap[i])g[1]=o.attrMap[i];if(g[2]==="~=")g[4]=" "+g[4]+" ";return g},PSEUDO:function(g,i,n,m,p){if(g[1]==="not")if((d.exec(g[3])||"").length>1||/^\w/.test(g[3]))g[3]=k(g[3],null,null,i);else{g=k.filter(g[3],i,n,true^p);n||m.push.apply(m,g);return false}else if(o.match.POS.test(g[0])||o.match.CHILD.test(g[0]))return true;return g},POS:function(g){g.unshift(true);return g}},filters:{enabled:function(g){return g.disabled===false&&g.type!=="hidden"},disabled:function(g){return g.disabled===
true},checked:function(g){return g.checked===true},selected:function(g){return g.selected===true},parent:function(g){return!!g.firstChild},empty:function(g){return!g.firstChild},has:function(g,i,n){return!!k(n[3],g).length},header:function(g){return/h\d/i.test(g.nodeName)},text:function(g){return"text"===g.type},radio:function(g){return"radio"===g.type},checkbox:function(g){return"checkbox"===g.type},file:function(g){return"file"===g.type},password:function(g){return"password"===g.type},submit:function(g){return"submit"===
g.type},image:function(g){return"image"===g.type},reset:function(g){return"reset"===g.type},button:function(g){return"button"===g.type||g.nodeName.toLowerCase()==="button"},input:function(g){return/input|select|textarea|button/i.test(g.nodeName)}},setFilters:{first:function(g,i){return i===0},last:function(g,i,n,m){return i===m.length-1},even:function(g,i){return i%2===0},odd:function(g,i){return i%2===1},lt:function(g,i,n){return i<n[3]-0},gt:function(g,i,n){return i>n[3]-0},nth:function(g,i,n){return n[3]-
0===i},eq:function(g,i,n){return n[3]-0===i}},filter:{PSEUDO:function(g,i,n,m){var p=i[1],q=o.filters[p];if(q)return q(g,n,i,m);else if(p==="contains")return(g.textContent||g.innerText||k.getText([g])||"").indexOf(i[3])>=0;else if(p==="not"){i=i[3];n=0;for(m=i.length;n<m;n++)if(i[n]===g)return false;return true}else k.error("Syntax error, unrecognized expression: "+p)},CHILD:function(g,i){var n=i[1],m=g;switch(n){case "only":case "first":for(;m=m.previousSibling;)if(m.nodeType===1)return false;if(n===
"first")return true;m=g;case "last":for(;m=m.nextSibling;)if(m.nodeType===1)return false;return true;case "nth":n=i[2];var p=i[3];if(n===1&&p===0)return true;var q=i[0],u=g.parentNode;if(u&&(u.sizcache!==q||!g.nodeIndex)){var y=0;for(m=u.firstChild;m;m=m.nextSibling)if(m.nodeType===1)m.nodeIndex=++y;u.sizcache=q}m=g.nodeIndex-p;return n===0?m===0:m%n===0&&m/n>=0}},ID:function(g,i){return g.nodeType===1&&g.getAttribute("id")===i},TAG:function(g,i){return i==="*"&&g.nodeType===1||g.nodeName.toLowerCase()===
i},CLASS:function(g,i){return(" "+(g.className||g.getAttribute("class"))+" ").indexOf(i)>-1},ATTR:function(g,i){var n=i[1];n=o.attrHandle[n]?o.attrHandle[n](g):g[n]!=null?g[n]:g.getAttribute(n);var m=n+"",p=i[2],q=i[4];return n==null?p==="!=":p==="="?m===q:p==="*="?m.indexOf(q)>=0:p==="~="?(" "+m+" ").indexOf(q)>=0:!q?m&&n!==false:p==="!="?m!==q:p==="^="?m.indexOf(q)===0:p==="$="?m.substr(m.length-q.length)===q:p==="|="?m===q||m.substr(0,q.length+1)===q+"-":false},POS:function(g,i,n,m){var p=o.setFilters[i[2]];
if(p)return p(g,n,i,m)}}},x=o.match.POS,r=function(g,i){return"\\"+(i-0+1)},A;for(A in o.match){o.match[A]=RegExp(o.match[A].source+/(?![^\[]*\])(?![^\(]*\))/.source);o.leftMatch[A]=RegExp(/(^(?:.|\r|\n)*?)/.source+o.match[A].source.replace(/\\(\d+)/g,r))}var C=function(g,i){g=Array.prototype.slice.call(g,0);if(i){i.push.apply(i,g);return i}return g};try{Array.prototype.slice.call(t.documentElement.childNodes,0)}catch(J){C=function(g,i){var n=0,m=i||[];if(f.call(g)==="[object Array]")Array.prototype.push.apply(m,
g);else if(typeof g.length==="number")for(var p=g.length;n<p;n++)m.push(g[n]);else for(;g[n];n++)m.push(g[n]);return m}}var w,I;if(t.documentElement.compareDocumentPosition)w=function(g,i){if(g===i){h=true;return 0}if(!g.compareDocumentPosition||!i.compareDocumentPosition)return g.compareDocumentPosition?-1:1;return g.compareDocumentPosition(i)&4?-1:1};else{w=function(g,i){var n,m,p=[],q=[];n=g.parentNode;m=i.parentNode;var u=n;if(g===i){h=true;return 0}else if(n===m)return I(g,i);else if(n){if(!m)return 1}else return-1;
for(;u;){p.unshift(u);u=u.parentNode}for(u=m;u;){q.unshift(u);u=u.parentNode}n=p.length;m=q.length;for(u=0;u<n&&u<m;u++)if(p[u]!==q[u])return I(p[u],q[u]);return u===n?I(g,q[u],-1):I(p[u],i,1)};I=function(g,i,n){if(g===i)return n;for(g=g.nextSibling;g;){if(g===i)return-1;g=g.nextSibling}return 1}}k.getText=function(g){for(var i="",n,m=0;g[m];m++){n=g[m];if(n.nodeType===3||n.nodeType===4)i+=n.nodeValue;else if(n.nodeType!==8)i+=k.getText(n.childNodes)}return i};(function(){var g=t.createElement("div"),
i="script"+(new Date).getTime(),n=t.documentElement;g.innerHTML="<a name='"+i+"'/>";n.insertBefore(g,n.firstChild);if(t.getElementById(i)){o.find.ID=function(m,p,q){if(typeof p.getElementById!=="undefined"&&!q)return(p=p.getElementById(m[1]))?p.id===m[1]||typeof p.getAttributeNode!=="undefined"&&p.getAttributeNode("id").nodeValue===m[1]?[p]:B:[]};o.filter.ID=function(m,p){var q=typeof m.getAttributeNode!=="undefined"&&m.getAttributeNode("id");return m.nodeType===1&&q&&q.nodeValue===p}}n.removeChild(g);
n=g=null})();(function(){var g=t.createElement("div");g.appendChild(t.createComment(""));if(g.getElementsByTagName("*").length>0)o.find.TAG=function(i,n){var m=n.getElementsByTagName(i[1]);if(i[1]==="*"){for(var p=[],q=0;m[q];q++)m[q].nodeType===1&&p.push(m[q]);m=p}return m};g.innerHTML="<a href='#'></a>";if(g.firstChild&&typeof g.firstChild.getAttribute!=="undefined"&&g.firstChild.getAttribute("href")!=="#")o.attrHandle.href=function(i){return i.getAttribute("href",2)};g=null})();t.querySelectorAll&&
function(){var g=k,i=t.createElement("div");i.innerHTML="<p class='TEST'></p>";if(!(i.querySelectorAll&&i.querySelectorAll(".TEST").length===0)){k=function(m,p,q,u){p=p||t;m=m.replace(/\=\s*([^'"\]]*)\s*\]/g,"='$1']");if(!u&&!k.isXML(p))if(p.nodeType===9)try{return C(p.querySelectorAll(m),q)}catch(y){}else if(p.nodeType===1&&p.nodeName.toLowerCase()!=="object"){var F=p.getAttribute("id"),M=F||"__sizzle__";F||p.setAttribute("id",M);try{return C(p.querySelectorAll("#"+M+" "+m),q)}catch(N){}finally{F||
p.removeAttribute("id")}}return g(m,p,q,u)};for(var n in g)k[n]=g[n];i=null}}();(function(){var g=t.documentElement,i=g.matchesSelector||g.mozMatchesSelector||g.webkitMatchesSelector||g.msMatchesSelector,n=false;try{i.call(t.documentElement,"[test!='']:sizzle")}catch(m){n=true}if(i)k.matchesSelector=function(p,q){q=q.replace(/\=\s*([^'"\]]*)\s*\]/g,"='$1']");if(!k.isXML(p))try{if(n||!o.match.PSEUDO.test(q)&&!/!=/.test(q))return i.call(p,q)}catch(u){}return k(q,null,null,[p]).length>0}})();(function(){var g=
t.createElement("div");g.innerHTML="<div class='test e'></div><div class='test'></div>";if(!(!g.getElementsByClassName||g.getElementsByClassName("e").length===0)){g.lastChild.className="e";if(g.getElementsByClassName("e").length!==1){o.order.splice(1,0,"CLASS");o.find.CLASS=function(i,n,m){if(typeof n.getElementsByClassName!=="undefined"&&!m)return n.getElementsByClassName(i[1])};g=null}}})();k.contains=t.documentElement.contains?function(g,i){return g!==i&&(g.contains?g.contains(i):true)}:t.documentElement.compareDocumentPosition?
function(g,i){return!!(g.compareDocumentPosition(i)&16)}:function(){return false};k.isXML=function(g){return(g=(g?g.ownerDocument||g:0).documentElement)?g.nodeName!=="HTML":false};var L=function(g,i){for(var n,m=[],p="",q=i.nodeType?[i]:i;n=o.match.PSEUDO.exec(g);){p+=n[0];g=g.replace(o.match.PSEUDO,"")}g=o.relative[g]?g+"*":g;n=0;for(var u=q.length;n<u;n++)k(g,q[n],m);return k.filter(p,m)};c.find=k;c.expr=k.selectors;c.expr[":"]=c.expr.filters;c.unique=k.uniqueSort;c.text=k.getText;c.isXMLDoc=k.isXML;
c.contains=k.contains})();var Za=/Until$/,$a=/^(?:parents|prevUntil|prevAll)/,ab=/,/,Na=/^.[^:#\[\.,]*$/,bb=Array.prototype.slice,cb=c.expr.match.POS;c.fn.extend({find:function(a){for(var b=this.pushStack("","find",a),d=0,e=0,f=this.length;e<f;e++){d=b.length;c.find(a,this[e],b);if(e>0)for(var h=d;h<b.length;h++)for(var l=0;l<d;l++)if(b[l]===b[h]){b.splice(h--,1);break}}return b},has:function(a){var b=c(a);return this.filter(function(){for(var d=0,e=b.length;d<e;d++)if(c.contains(this,b[d]))return true})},
not:function(a){return this.pushStack(ma(this,a,false),"not",a)},filter:function(a){return this.pushStack(ma(this,a,true),"filter",a)},is:function(a){return!!a&&c.filter(a,this).length>0},closest:function(a,b){var d=[],e,f,h=this[0];if(c.isArray(a)){var l,k={},o=1;if(h&&a.length){e=0;for(f=a.length;e<f;e++){l=a[e];k[l]||(k[l]=c.expr.match.POS.test(l)?c(l,b||this.context):l)}for(;h&&h.ownerDocument&&h!==b;){for(l in k){e=k[l];if(e.jquery?e.index(h)>-1:c(h).is(e))d.push({selector:l,elem:h,level:o})}h=
h.parentNode;o++}}return d}l=cb.test(a)?c(a,b||this.context):null;e=0;for(f=this.length;e<f;e++)for(h=this[e];h;)if(l?l.index(h)>-1:c.find.matchesSelector(h,a)){d.push(h);break}else{h=h.parentNode;if(!h||!h.ownerDocument||h===b)break}d=d.length>1?c.unique(d):d;return this.pushStack(d,"closest",a)},index:function(a){if(!a||typeof a==="string")return c.inArray(this[0],a?c(a):this.parent().children());return c.inArray(a.jquery?a[0]:a,this)},add:function(a,b){var d=typeof a==="string"?c(a,b||this.context):
c.makeArray(a),e=c.merge(this.get(),d);return this.pushStack(!d[0]||!d[0].parentNode||d[0].parentNode.nodeType===11||!e[0]||!e[0].parentNode||e[0].parentNode.nodeType===11?e:c.unique(e))},andSelf:function(){return this.add(this.prevObject)}});c.each({parent:function(a){return(a=a.parentNode)&&a.nodeType!==11?a:null},parents:function(a){return c.dir(a,"parentNode")},parentsUntil:function(a,b,d){return c.dir(a,"parentNode",d)},next:function(a){return c.nth(a,2,"nextSibling")},prev:function(a){return c.nth(a,
2,"previousSibling")},nextAll:function(a){return c.dir(a,"nextSibling")},prevAll:function(a){return c.dir(a,"previousSibling")},nextUntil:function(a,b,d){return c.dir(a,"nextSibling",d)},prevUntil:function(a,b,d){return c.dir(a,"previousSibling",d)},siblings:function(a){return c.sibling(a.parentNode.firstChild,a)},children:function(a){return c.sibling(a.firstChild)},contents:function(a){return c.nodeName(a,"iframe")?a.contentDocument||a.contentWindow.document:c.makeArray(a.childNodes)}},function(a,
b){c.fn[a]=function(d,e){var f=c.map(this,b,d);Za.test(a)||(e=d);if(e&&typeof e==="string")f=c.filter(e,f);f=this.length>1?c.unique(f):f;if((this.length>1||ab.test(e))&&$a.test(a))f=f.reverse();return this.pushStack(f,a,bb.call(arguments).join(","))}});c.extend({filter:function(a,b,d){if(d)a=":not("+a+")";return b.length===1?c.find.matchesSelector(b[0],a)?[b[0]]:[]:c.find.matches(a,b)},dir:function(a,b,d){var e=[];for(a=a[b];a&&a.nodeType!==9&&(d===B||a.nodeType!==1||!c(a).is(d));){a.nodeType===1&&
e.push(a);a=a[b]}return e},nth:function(a,b,d){b=b||1;for(var e=0;a;a=a[d])if(a.nodeType===1&&++e===b)break;return a},sibling:function(a,b){for(var d=[];a;a=a.nextSibling)a.nodeType===1&&a!==b&&d.push(a);return d}});var za=/ jQuery\d+="(?:\d+|null)"/g,$=/^\s+/,Aa=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/ig,Ba=/<([\w:]+)/,db=/<tbody/i,eb=/<|&#?\w+;/,Ca=/<(?:script|object|embed|option|style)/i,Da=/checked\s*(?:[^=]|=\s*.checked.)/i,fb=/\=([^="'>\s]+\/)>/g,P={option:[1,
"<select multiple='multiple'>","</select>"],legend:[1,"<fieldset>","</fieldset>"],thead:[1,"<table>","</table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],col:[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"],area:[1,"<map>","</map>"],_default:[0,"",""]};P.optgroup=P.option;P.tbody=P.tfoot=P.colgroup=P.caption=P.thead;P.th=P.td;if(!c.support.htmlSerialize)P._default=[1,"div<div>","</div>"];c.fn.extend({text:function(a){if(c.isFunction(a))return this.each(function(b){var d=
c(this);d.text(a.call(this,b,d.text()))});if(typeof a!=="object"&&a!==B)return this.empty().append((this[0]&&this[0].ownerDocument||t).createTextNode(a));return c.text(this)},wrapAll:function(a){if(c.isFunction(a))return this.each(function(d){c(this).wrapAll(a.call(this,d))});if(this[0]){var b=c(a,this[0].ownerDocument).eq(0).clone(true);this[0].parentNode&&b.insertBefore(this[0]);b.map(function(){for(var d=this;d.firstChild&&d.firstChild.nodeType===1;)d=d.firstChild;return d}).append(this)}return this},
wrapInner:function(a){if(c.isFunction(a))return this.each(function(b){c(this).wrapInner(a.call(this,b))});return this.each(function(){var b=c(this),d=b.contents();d.length?d.wrapAll(a):b.append(a)})},wrap:function(a){return this.each(function(){c(this).wrapAll(a)})},unwrap:function(){return this.parent().each(function(){c.nodeName(this,"body")||c(this).replaceWith(this.childNodes)}).end()},append:function(){return this.domManip(arguments,true,function(a){this.nodeType===1&&this.appendChild(a)})},
prepend:function(){return this.domManip(arguments,true,function(a){this.nodeType===1&&this.insertBefore(a,this.firstChild)})},before:function(){if(this[0]&&this[0].parentNode)return this.domManip(arguments,false,function(b){this.parentNode.insertBefore(b,this)});else if(arguments.length){var a=c(arguments[0]);a.push.apply(a,this.toArray());return this.pushStack(a,"before",arguments)}},after:function(){if(this[0]&&this[0].parentNode)return this.domManip(arguments,false,function(b){this.parentNode.insertBefore(b,
this.nextSibling)});else if(arguments.length){var a=this.pushStack(this,"after",arguments);a.push.apply(a,c(arguments[0]).toArray());return a}},remove:function(a,b){for(var d=0,e;(e=this[d])!=null;d++)if(!a||c.filter(a,[e]).length){if(!b&&e.nodeType===1){c.cleanData(e.getElementsByTagName("*"));c.cleanData([e])}e.parentNode&&e.parentNode.removeChild(e)}return this},empty:function(){for(var a=0,b;(b=this[a])!=null;a++)for(b.nodeType===1&&c.cleanData(b.getElementsByTagName("*"));b.firstChild;)b.removeChild(b.firstChild);
return this},clone:function(a){var b=this.map(function(){if(!c.support.noCloneEvent&&!c.isXMLDoc(this)){var d=this.outerHTML,e=this.ownerDocument;if(!d){d=e.createElement("div");d.appendChild(this.cloneNode(true));d=d.innerHTML}return c.clean([d.replace(za,"").replace(fb,'="$1">').replace($,"")],e)[0]}else return this.cloneNode(true)});if(a===true){na(this,b);na(this.find("*"),b.find("*"))}return b},html:function(a){if(a===B)return this[0]&&this[0].nodeType===1?this[0].innerHTML.replace(za,""):null;
else if(typeof a==="string"&&!Ca.test(a)&&(c.support.leadingWhitespace||!$.test(a))&&!P[(Ba.exec(a)||["",""])[1].toLowerCase()]){a=a.replace(Aa,"<$1></$2>");try{for(var b=0,d=this.length;b<d;b++)if(this[b].nodeType===1){c.cleanData(this[b].getElementsByTagName("*"));this[b].innerHTML=a}}catch(e){this.empty().append(a)}}else c.isFunction(a)?this.each(function(f){var h=c(this);h.html(a.call(this,f,h.html()))}):this.empty().append(a);return this},replaceWith:function(a){if(this[0]&&this[0].parentNode){if(c.isFunction(a))return this.each(function(b){var d=
c(this),e=d.html();d.replaceWith(a.call(this,b,e))});if(typeof a!=="string")a=c(a).detach();return this.each(function(){var b=this.nextSibling,d=this.parentNode;c(this).remove();b?c(b).before(a):c(d).append(a)})}else return this.pushStack(c(c.isFunction(a)?a():a),"replaceWith",a)},detach:function(a){return this.remove(a,true)},domManip:function(a,b,d){var e,f,h,l=a[0],k=[];if(!c.support.checkClone&&arguments.length===3&&typeof l==="string"&&Da.test(l))return this.each(function(){c(this).domManip(a,
b,d,true)});if(c.isFunction(l))return this.each(function(x){var r=c(this);a[0]=l.call(this,x,b?r.html():B);r.domManip(a,b,d)});if(this[0]){e=l&&l.parentNode;e=c.support.parentNode&&e&&e.nodeType===11&&e.childNodes.length===this.length?{fragment:e}:c.buildFragment(a,this,k);h=e.fragment;if(f=h.childNodes.length===1?h=h.firstChild:h.firstChild){b=b&&c.nodeName(f,"tr");f=0;for(var o=this.length;f<o;f++)d.call(b?c.nodeName(this[f],"table")?this[f].getElementsByTagName("tbody")[0]||this[f].appendChild(this[f].ownerDocument.createElement("tbody")):
this[f]:this[f],f>0||e.cacheable||this.length>1?h.cloneNode(true):h)}k.length&&c.each(k,Oa)}return this}});c.buildFragment=function(a,b,d){var e,f,h;b=b&&b[0]?b[0].ownerDocument||b[0]:t;if(a.length===1&&typeof a[0]==="string"&&a[0].length<512&&b===t&&!Ca.test(a[0])&&(c.support.checkClone||!Da.test(a[0]))){f=true;if(h=c.fragments[a[0]])if(h!==1)e=h}if(!e){e=b.createDocumentFragment();c.clean(a,b,e,d)}if(f)c.fragments[a[0]]=h?e:1;return{fragment:e,cacheable:f}};c.fragments={};c.each({appendTo:"append",
prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(a,b){c.fn[a]=function(d){var e=[];d=c(d);var f=this.length===1&&this[0].parentNode;if(f&&f.nodeType===11&&f.childNodes.length===1&&d.length===1){d[b](this[0]);return this}else{f=0;for(var h=d.length;f<h;f++){var l=(f>0?this.clone(true):this).get();c(d[f])[b](l);e=e.concat(l)}return this.pushStack(e,a,d.selector)}}});c.extend({clean:function(a,b,d,e){b=b||t;if(typeof b.createElement==="undefined")b=b.ownerDocument||
b[0]&&b[0].ownerDocument||t;for(var f=[],h=0,l;(l=a[h])!=null;h++){if(typeof l==="number")l+="";if(l){if(typeof l==="string"&&!eb.test(l))l=b.createTextNode(l);else if(typeof l==="string"){l=l.replace(Aa,"<$1></$2>");var k=(Ba.exec(l)||["",""])[1].toLowerCase(),o=P[k]||P._default,x=o[0],r=b.createElement("div");for(r.innerHTML=o[1]+l+o[2];x--;)r=r.lastChild;if(!c.support.tbody){x=db.test(l);k=k==="table"&&!x?r.firstChild&&r.firstChild.childNodes:o[1]==="<table>"&&!x?r.childNodes:[];for(o=k.length-
1;o>=0;--o)c.nodeName(k[o],"tbody")&&!k[o].childNodes.length&&k[o].parentNode.removeChild(k[o])}!c.support.leadingWhitespace&&$.test(l)&&r.insertBefore(b.createTextNode($.exec(l)[0]),r.firstChild);l=r.childNodes}if(l.nodeType)f.push(l);else f=c.merge(f,l)}}if(d)for(h=0;f[h];h++)if(e&&c.nodeName(f[h],"script")&&(!f[h].type||f[h].type.toLowerCase()==="text/javascript"))e.push(f[h].parentNode?f[h].parentNode.removeChild(f[h]):f[h]);else{f[h].nodeType===1&&f.splice.apply(f,[h+1,0].concat(c.makeArray(f[h].getElementsByTagName("script"))));
d.appendChild(f[h])}return f},cleanData:function(a){for(var b,d,e=c.cache,f=c.event.special,h=c.support.deleteExpando,l=0,k;(k=a[l])!=null;l++)if(!(k.nodeName&&c.noData[k.nodeName.toLowerCase()]))if(d=k[c.expando]){if((b=e[d])&&b.events)for(var o in b.events)f[o]?c.event.remove(k,o):c.removeEvent(k,o,b.handle);if(h)delete k[c.expando];else k.removeAttribute&&k.removeAttribute(c.expando);delete e[d]}}});var Ea=/alpha\([^)]*\)/i,gb=/opacity=([^)]*)/,hb=/-([a-z])/ig,ib=/([A-Z])/g,Fa=/^-?\d+(?:px)?$/i,
jb=/^-?\d/,kb={position:"absolute",visibility:"hidden",display:"block"},Pa=["Left","Right"],Qa=["Top","Bottom"],W,Ga,aa,lb=function(a,b){return b.toUpperCase()};c.fn.css=function(a,b){if(arguments.length===2&&b===B)return this;return c.access(this,a,b,true,function(d,e,f){return f!==B?c.style(d,e,f):c.css(d,e)})};c.extend({cssHooks:{opacity:{get:function(a,b){if(b){var d=W(a,"opacity","opacity");return d===""?"1":d}else return a.style.opacity}}},cssNumber:{zIndex:true,fontWeight:true,opacity:true,
zoom:true,lineHeight:true},cssProps:{"float":c.support.cssFloat?"cssFloat":"styleFloat"},style:function(a,b,d,e){if(!(!a||a.nodeType===3||a.nodeType===8||!a.style)){var f,h=c.camelCase(b),l=a.style,k=c.cssHooks[h];b=c.cssProps[h]||h;if(d!==B){if(!(typeof d==="number"&&isNaN(d)||d==null)){if(typeof d==="number"&&!c.cssNumber[h])d+="px";if(!k||!("set"in k)||(d=k.set(a,d))!==B)try{l[b]=d}catch(o){}}}else{if(k&&"get"in k&&(f=k.get(a,false,e))!==B)return f;return l[b]}}},css:function(a,b,d){var e,f=c.camelCase(b),
h=c.cssHooks[f];b=c.cssProps[f]||f;if(h&&"get"in h&&(e=h.get(a,true,d))!==B)return e;else if(W)return W(a,b,f)},swap:function(a,b,d){var e={},f;for(f in b){e[f]=a.style[f];a.style[f]=b[f]}d.call(a);for(f in b)a.style[f]=e[f]},camelCase:function(a){return a.replace(hb,lb)}});c.curCSS=c.css;c.each(["height","width"],function(a,b){c.cssHooks[b]={get:function(d,e,f){var h;if(e){if(d.offsetWidth!==0)h=oa(d,b,f);else c.swap(d,kb,function(){h=oa(d,b,f)});if(h<=0){h=W(d,b,b);if(h==="0px"&&aa)h=aa(d,b,b);
if(h!=null)return h===""||h==="auto"?"0px":h}if(h<0||h==null){h=d.style[b];return h===""||h==="auto"?"0px":h}return typeof h==="string"?h:h+"px"}},set:function(d,e){if(Fa.test(e)){e=parseFloat(e);if(e>=0)return e+"px"}else return e}}});if(!c.support.opacity)c.cssHooks.opacity={get:function(a,b){return gb.test((b&&a.currentStyle?a.currentStyle.filter:a.style.filter)||"")?parseFloat(RegExp.$1)/100+"":b?"1":""},set:function(a,b){var d=a.style;d.zoom=1;var e=c.isNaN(b)?"":"alpha(opacity="+b*100+")",f=
d.filter||"";d.filter=Ea.test(f)?f.replace(Ea,e):d.filter+" "+e}};if(t.defaultView&&t.defaultView.getComputedStyle)Ga=function(a,b,d){var e;d=d.replace(ib,"-$1").toLowerCase();if(!(b=a.ownerDocument.defaultView))return B;if(b=b.getComputedStyle(a,null)){e=b.getPropertyValue(d);if(e===""&&!c.contains(a.ownerDocument.documentElement,a))e=c.style(a,d)}return e};if(t.documentElement.currentStyle)aa=function(a,b){var d,e,f=a.currentStyle&&a.currentStyle[b],h=a.style;if(!Fa.test(f)&&jb.test(f)){d=h.left;
e=a.runtimeStyle.left;a.runtimeStyle.left=a.currentStyle.left;h.left=b==="fontSize"?"1em":f||0;f=h.pixelLeft+"px";h.left=d;a.runtimeStyle.left=e}return f===""?"auto":f};W=Ga||aa;if(c.expr&&c.expr.filters){c.expr.filters.hidden=function(a){var b=a.offsetHeight;return a.offsetWidth===0&&b===0||!c.support.reliableHiddenOffsets&&(a.style.display||c.css(a,"display"))==="none"};c.expr.filters.visible=function(a){return!c.expr.filters.hidden(a)}}var mb=c.now(),nb=/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,
ob=/^(?:select|textarea)/i,pb=/^(?:color|date|datetime|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i,qb=/^(?:GET|HEAD)$/,Ra=/\[\]$/,T=/\=\?(&|$)/,ja=/\?/,rb=/([?&])_=[^&]*/,sb=/^(\w+:)?\/\/([^\/?#]+)/,tb=/%20/g,ub=/#.*$/,Ha=c.fn.load;c.fn.extend({load:function(a,b,d){if(typeof a!=="string"&&Ha)return Ha.apply(this,arguments);else if(!this.length)return this;var e=a.indexOf(" ");if(e>=0){var f=a.slice(e,a.length);a=a.slice(0,e)}e="GET";if(b)if(c.isFunction(b)){d=b;b=null}else if(typeof b===
"object"){b=c.param(b,c.ajaxSettings.traditional);e="POST"}var h=this;c.ajax({url:a,type:e,dataType:"html",data:b,complete:function(l,k){if(k==="success"||k==="notmodified")h.html(f?c("<div>").append(l.responseText.replace(nb,"")).find(f):l.responseText);d&&h.each(d,[l.responseText,k,l])}});return this},serialize:function(){return c.param(this.serializeArray())},serializeArray:function(){return this.map(function(){return this.elements?c.makeArray(this.elements):this}).filter(function(){return this.name&&
!this.disabled&&(this.checked||ob.test(this.nodeName)||pb.test(this.type))}).map(function(a,b){var d=c(this).val();return d==null?null:c.isArray(d)?c.map(d,function(e){return{name:b.name,value:e}}):{name:b.name,value:d}}).get()}});c.each("ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(" "),function(a,b){c.fn[b]=function(d){return this.bind(b,d)}});c.extend({get:function(a,b,d,e){if(c.isFunction(b)){e=e||d;d=b;b=null}return c.ajax({type:"GET",url:a,data:b,success:d,dataType:e})},
getScript:function(a,b){return c.get(a,null,b,"script")},getJSON:function(a,b,d){return c.get(a,b,d,"json")},post:function(a,b,d,e){if(c.isFunction(b)){e=e||d;d=b;b={}}return c.ajax({type:"POST",url:a,data:b,success:d,dataType:e})},ajaxSetup:function(a){c.extend(c.ajaxSettings,a)},ajaxSettings:{url:location.href,global:true,type:"GET",contentType:"application/x-www-form-urlencoded",processData:true,async:true,xhr:function(){return new E.XMLHttpRequest},accepts:{xml:"application/xml, text/xml",html:"text/html",
script:"text/javascript, application/javascript",json:"application/json, text/javascript",text:"text/plain",_default:"*/*"}},ajax:function(a){var b=c.extend(true,{},c.ajaxSettings,a),d,e,f,h=b.type.toUpperCase(),l=qb.test(h);b.url=b.url.replace(ub,"");b.context=a&&a.context!=null?a.context:b;if(b.data&&b.processData&&typeof b.data!=="string")b.data=c.param(b.data,b.traditional);if(b.dataType==="jsonp"){if(h==="GET")T.test(b.url)||(b.url+=(ja.test(b.url)?"&":"?")+(b.jsonp||"callback")+"=?");else if(!b.data||
!T.test(b.data))b.data=(b.data?b.data+"&":"")+(b.jsonp||"callback")+"=?";b.dataType="json"}if(b.dataType==="json"&&(b.data&&T.test(b.data)||T.test(b.url))){d=b.jsonpCallback||"jsonp"+mb++;if(b.data)b.data=(b.data+"").replace(T,"="+d+"$1");b.url=b.url.replace(T,"="+d+"$1");b.dataType="script";var k=E[d];E[d]=function(m){if(c.isFunction(k))k(m);else{E[d]=B;try{delete E[d]}catch(p){}}f=m;c.handleSuccess(b,w,e,f);c.handleComplete(b,w,e,f);r&&r.removeChild(A)}}if(b.dataType==="script"&&b.cache===null)b.cache=
false;if(b.cache===false&&l){var o=c.now(),x=b.url.replace(rb,"$1_="+o);b.url=x+(x===b.url?(ja.test(b.url)?"&":"?")+"_="+o:"")}if(b.data&&l)b.url+=(ja.test(b.url)?"&":"?")+b.data;b.global&&c.active++===0&&c.event.trigger("ajaxStart");o=(o=sb.exec(b.url))&&(o[1]&&o[1].toLowerCase()!==location.protocol||o[2].toLowerCase()!==location.host);if(b.dataType==="script"&&h==="GET"&&o){var r=t.getElementsByTagName("head")[0]||t.documentElement,A=t.createElement("script");if(b.scriptCharset)A.charset=b.scriptCharset;
A.src=b.url;if(!d){var C=false;A.onload=A.onreadystatechange=function(){if(!C&&(!this.readyState||this.readyState==="loaded"||this.readyState==="complete")){C=true;c.handleSuccess(b,w,e,f);c.handleComplete(b,w,e,f);A.onload=A.onreadystatechange=null;r&&A.parentNode&&r.removeChild(A)}}}r.insertBefore(A,r.firstChild);return B}var J=false,w=b.xhr();if(w){b.username?w.open(h,b.url,b.async,b.username,b.password):w.open(h,b.url,b.async);try{if(b.data!=null&&!l||a&&a.contentType)w.setRequestHeader("Content-Type",
b.contentType);if(b.ifModified){c.lastModified[b.url]&&w.setRequestHeader("If-Modified-Since",c.lastModified[b.url]);c.etag[b.url]&&w.setRequestHeader("If-None-Match",c.etag[b.url])}o||w.setRequestHeader("X-Requested-With","XMLHttpRequest");w.setRequestHeader("Accept",b.dataType&&b.accepts[b.dataType]?b.accepts[b.dataType]+", */*; q=0.01":b.accepts._default)}catch(I){}if(b.beforeSend&&b.beforeSend.call(b.context,w,b)===false){b.global&&c.active--===1&&c.event.trigger("ajaxStop");w.abort();return false}b.global&&
c.triggerGlobal(b,"ajaxSend",[w,b]);var L=w.onreadystatechange=function(m){if(!w||w.readyState===0||m==="abort"){J||c.handleComplete(b,w,e,f);J=true;if(w)w.onreadystatechange=c.noop}else if(!J&&w&&(w.readyState===4||m==="timeout")){J=true;w.onreadystatechange=c.noop;e=m==="timeout"?"timeout":!c.httpSuccess(w)?"error":b.ifModified&&c.httpNotModified(w,b.url)?"notmodified":"success";var p;if(e==="success")try{f=c.httpData(w,b.dataType,b)}catch(q){e="parsererror";p=q}if(e==="success"||e==="notmodified")d||
c.handleSuccess(b,w,e,f);else c.handleError(b,w,e,p);d||c.handleComplete(b,w,e,f);m==="timeout"&&w.abort();if(b.async)w=null}};try{var g=w.abort;w.abort=function(){w&&Function.prototype.call.call(g,w);L("abort")}}catch(i){}b.async&&b.timeout>0&&setTimeout(function(){w&&!J&&L("timeout")},b.timeout);try{w.send(l||b.data==null?null:b.data)}catch(n){c.handleError(b,w,null,n);c.handleComplete(b,w,e,f)}b.async||L();return w}},param:function(a,b){var d=[],e=function(h,l){l=c.isFunction(l)?l():l;d[d.length]=
encodeURIComponent(h)+"="+encodeURIComponent(l)};if(b===B)b=c.ajaxSettings.traditional;if(c.isArray(a)||a.jquery)c.each(a,function(){e(this.name,this.value)});else for(var f in a)da(f,a[f],b,e);return d.join("&").replace(tb,"+")}});c.extend({active:0,lastModified:{},etag:{},handleError:function(a,b,d,e){a.error&&a.error.call(a.context,b,d,e);a.global&&c.triggerGlobal(a,"ajaxError",[b,a,e])},handleSuccess:function(a,b,d,e){a.success&&a.success.call(a.context,e,d,b);a.global&&c.triggerGlobal(a,"ajaxSuccess",
[b,a])},handleComplete:function(a,b,d){a.complete&&a.complete.call(a.context,b,d);a.global&&c.triggerGlobal(a,"ajaxComplete",[b,a]);a.global&&c.active--===1&&c.event.trigger("ajaxStop")},triggerGlobal:function(a,b,d){(a.context&&a.context.url==null?c(a.context):c.event).trigger(b,d)},httpSuccess:function(a){try{return!a.status&&location.protocol==="file:"||a.status>=200&&a.status<300||a.status===304||a.status===1223}catch(b){}return false},httpNotModified:function(a,b){var d=a.getResponseHeader("Last-Modified"),
e=a.getResponseHeader("Etag");if(d)c.lastModified[b]=d;if(e)c.etag[b]=e;return a.status===304},httpData:function(a,b,d){var e=a.getResponseHeader("content-type")||"",f=b==="xml"||!b&&e.indexOf("xml")>=0;a=f?a.responseXML:a.responseText;f&&a.documentElement.nodeName==="parsererror"&&c.error("parsererror");if(d&&d.dataFilter)a=d.dataFilter(a,b);if(typeof a==="string")if(b==="json"||!b&&e.indexOf("json")>=0)a=c.parseJSON(a);else if(b==="script"||!b&&e.indexOf("javascript")>=0)c.globalEval(a);return a}});
if(E.ActiveXObject)c.ajaxSettings.xhr=function(){if(E.location.protocol!=="file:")try{return new E.XMLHttpRequest}catch(a){}try{return new E.ActiveXObject("Microsoft.XMLHTTP")}catch(b){}};c.support.ajax=!!c.ajaxSettings.xhr();var ea={},vb=/^(?:toggle|show|hide)$/,wb=/^([+\-]=)?([\d+.\-]+)(.*)$/,ba,pa=[["height","marginTop","marginBottom","paddingTop","paddingBottom"],["width","marginLeft","marginRight","paddingLeft","paddingRight"],["opacity"]];c.fn.extend({show:function(a,b,d){if(a||a===0)return this.animate(S("show",
3),a,b,d);else{d=0;for(var e=this.length;d<e;d++){a=this[d];b=a.style.display;if(!c.data(a,"olddisplay")&&b==="none")b=a.style.display="";b===""&&c.css(a,"display")==="none"&&c.data(a,"olddisplay",qa(a.nodeName))}for(d=0;d<e;d++){a=this[d];b=a.style.display;if(b===""||b==="none")a.style.display=c.data(a,"olddisplay")||""}return this}},hide:function(a,b,d){if(a||a===0)return this.animate(S("hide",3),a,b,d);else{a=0;for(b=this.length;a<b;a++){d=c.css(this[a],"display");d!=="none"&&c.data(this[a],"olddisplay",
d)}for(a=0;a<b;a++)this[a].style.display="none";return this}},_toggle:c.fn.toggle,toggle:function(a,b,d){var e=typeof a==="boolean";if(c.isFunction(a)&&c.isFunction(b))this._toggle.apply(this,arguments);else a==null||e?this.each(function(){var f=e?a:c(this).is(":hidden");c(this)[f?"show":"hide"]()}):this.animate(S("toggle",3),a,b,d);return this},fadeTo:function(a,b,d,e){return this.filter(":hidden").css("opacity",0).show().end().animate({opacity:b},a,d,e)},animate:function(a,b,d,e){var f=c.speed(b,
d,e);if(c.isEmptyObject(a))return this.each(f.complete);return this[f.queue===false?"each":"queue"](function(){var h=c.extend({},f),l,k=this.nodeType===1,o=k&&c(this).is(":hidden"),x=this;for(l in a){var r=c.camelCase(l);if(l!==r){a[r]=a[l];delete a[l];l=r}if(a[l]==="hide"&&o||a[l]==="show"&&!o)return h.complete.call(this);if(k&&(l==="height"||l==="width")){h.overflow=[this.style.overflow,this.style.overflowX,this.style.overflowY];if(c.css(this,"display")==="inline"&&c.css(this,"float")==="none")if(c.support.inlineBlockNeedsLayout)if(qa(this.nodeName)===
"inline")this.style.display="inline-block";else{this.style.display="inline";this.style.zoom=1}else this.style.display="inline-block"}if(c.isArray(a[l])){(h.specialEasing=h.specialEasing||{})[l]=a[l][1];a[l]=a[l][0]}}if(h.overflow!=null)this.style.overflow="hidden";h.curAnim=c.extend({},a);c.each(a,function(A,C){var J=new c.fx(x,h,A);if(vb.test(C))J[C==="toggle"?o?"show":"hide":C](a);else{var w=wb.exec(C),I=J.cur()||0;if(w){var L=parseFloat(w[2]),g=w[3]||"px";if(g!=="px"){c.style(x,A,(L||1)+g);I=(L||
1)/J.cur()*I;c.style(x,A,I+g)}if(w[1])L=(w[1]==="-="?-1:1)*L+I;J.custom(I,L,g)}else J.custom(I,C,"")}});return true})},stop:function(a,b){var d=c.timers;a&&this.queue([]);this.each(function(){for(var e=d.length-1;e>=0;e--)if(d[e].elem===this){b&&d[e](true);d.splice(e,1)}});b||this.dequeue();return this}});c.each({slideDown:S("show",1),slideUp:S("hide",1),slideToggle:S("toggle",1),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(a,b){c.fn[a]=function(d,e,f){return this.animate(b,
d,e,f)}});c.extend({speed:function(a,b,d){var e=a&&typeof a==="object"?c.extend({},a):{complete:d||!d&&b||c.isFunction(a)&&a,duration:a,easing:d&&b||b&&!c.isFunction(b)&&b};e.duration=c.fx.off?0:typeof e.duration==="number"?e.duration:e.duration in c.fx.speeds?c.fx.speeds[e.duration]:c.fx.speeds._default;e.old=e.complete;e.complete=function(){e.queue!==false&&c(this).dequeue();c.isFunction(e.old)&&e.old.call(this)};return e},easing:{linear:function(a,b,d,e){return d+e*a},swing:function(a,b,d,e){return(-Math.cos(a*
Math.PI)/2+0.5)*e+d}},timers:[],fx:function(a,b,d){this.options=b;this.elem=a;this.prop=d;if(!b.orig)b.orig={}}});c.fx.prototype={update:function(){this.options.step&&this.options.step.call(this.elem,this.now,this);(c.fx.step[this.prop]||c.fx.step._default)(this)},cur:function(){if(this.elem[this.prop]!=null&&(!this.elem.style||this.elem.style[this.prop]==null))return this.elem[this.prop];var a=parseFloat(c.css(this.elem,this.prop));return a&&a>-1E4?a:0},custom:function(a,b,d){function e(l){return f.step(l)}
var f=this,h=c.fx;this.startTime=c.now();this.start=a;this.end=b;this.unit=d||this.unit||"px";this.now=this.start;this.pos=this.state=0;e.elem=this.elem;if(e()&&c.timers.push(e)&&!ba)ba=setInterval(h.tick,h.interval)},show:function(){this.options.orig[this.prop]=c.style(this.elem,this.prop);this.options.show=true;this.custom(this.prop==="width"||this.prop==="height"?1:0,this.cur());c(this.elem).show()},hide:function(){this.options.orig[this.prop]=c.style(this.elem,this.prop);this.options.hide=true;
this.custom(this.cur(),0)},step:function(a){var b=c.now(),d=true;if(a||b>=this.options.duration+this.startTime){this.now=this.end;this.pos=this.state=1;this.update();this.options.curAnim[this.prop]=true;for(var e in this.options.curAnim)if(this.options.curAnim[e]!==true)d=false;if(d){if(this.options.overflow!=null&&!c.support.shrinkWrapBlocks){var f=this.elem,h=this.options;c.each(["","X","Y"],function(k,o){f.style["overflow"+o]=h.overflow[k]})}this.options.hide&&c(this.elem).hide();if(this.options.hide||
this.options.show)for(var l in this.options.curAnim)c.style(this.elem,l,this.options.orig[l]);this.options.complete.call(this.elem)}return false}else{a=b-this.startTime;this.state=a/this.options.duration;b=this.options.easing||(c.easing.swing?"swing":"linear");this.pos=c.easing[this.options.specialEasing&&this.options.specialEasing[this.prop]||b](this.state,a,0,1,this.options.duration);this.now=this.start+(this.end-this.start)*this.pos;this.update()}return true}};c.extend(c.fx,{tick:function(){for(var a=
c.timers,b=0;b<a.length;b++)a[b]()||a.splice(b--,1);a.length||c.fx.stop()},interval:13,stop:function(){clearInterval(ba);ba=null},speeds:{slow:600,fast:200,_default:400},step:{opacity:function(a){c.style(a.elem,"opacity",a.now)},_default:function(a){if(a.elem.style&&a.elem.style[a.prop]!=null)a.elem.style[a.prop]=(a.prop==="width"||a.prop==="height"?Math.max(0,a.now):a.now)+a.unit;else a.elem[a.prop]=a.now}}});if(c.expr&&c.expr.filters)c.expr.filters.animated=function(a){return c.grep(c.timers,function(b){return a===
b.elem}).length};var xb=/^t(?:able|d|h)$/i,Ia=/^(?:body|html)$/i;c.fn.offset="getBoundingClientRect"in t.documentElement?function(a){var b=this[0],d;if(a)return this.each(function(l){c.offset.setOffset(this,a,l)});if(!b||!b.ownerDocument)return null;if(b===b.ownerDocument.body)return c.offset.bodyOffset(b);try{d=b.getBoundingClientRect()}catch(e){}var f=b.ownerDocument,h=f.documentElement;if(!d||!c.contains(h,b))return d||{top:0,left:0};b=f.body;f=fa(f);return{top:d.top+(f.pageYOffset||c.support.boxModel&&
h.scrollTop||b.scrollTop)-(h.clientTop||b.clientTop||0),left:d.left+(f.pageXOffset||c.support.boxModel&&h.scrollLeft||b.scrollLeft)-(h.clientLeft||b.clientLeft||0)}}:function(a){var b=this[0];if(a)return this.each(function(x){c.offset.setOffset(this,a,x)});if(!b||!b.ownerDocument)return null;if(b===b.ownerDocument.body)return c.offset.bodyOffset(b);c.offset.initialize();var d,e=b.offsetParent,f=b.ownerDocument,h=f.documentElement,l=f.body;d=(f=f.defaultView)?f.getComputedStyle(b,null):b.currentStyle;
for(var k=b.offsetTop,o=b.offsetLeft;(b=b.parentNode)&&b!==l&&b!==h;){if(c.offset.supportsFixedPosition&&d.position==="fixed")break;d=f?f.getComputedStyle(b,null):b.currentStyle;k-=b.scrollTop;o-=b.scrollLeft;if(b===e){k+=b.offsetTop;o+=b.offsetLeft;if(c.offset.doesNotAddBorder&&!(c.offset.doesAddBorderForTableAndCells&&xb.test(b.nodeName))){k+=parseFloat(d.borderTopWidth)||0;o+=parseFloat(d.borderLeftWidth)||0}e=b.offsetParent}if(c.offset.subtractsBorderForOverflowNotVisible&&d.overflow!=="visible"){k+=
parseFloat(d.borderTopWidth)||0;o+=parseFloat(d.borderLeftWidth)||0}d=d}if(d.position==="relative"||d.position==="static"){k+=l.offsetTop;o+=l.offsetLeft}if(c.offset.supportsFixedPosition&&d.position==="fixed"){k+=Math.max(h.scrollTop,l.scrollTop);o+=Math.max(h.scrollLeft,l.scrollLeft)}return{top:k,left:o}};c.offset={initialize:function(){var a=t.body,b=t.createElement("div"),d,e,f,h=parseFloat(c.css(a,"marginTop"))||0;c.extend(b.style,{position:"absolute",top:0,left:0,margin:0,border:0,width:"1px",
height:"1px",visibility:"hidden"});b.innerHTML="<div style='position:absolute;top:0;left:0;margin:0;border:5px solid #000;padding:0;width:1px;height:1px;'><div></div></div><table style='position:absolute;top:0;left:0;margin:0;border:5px solid #000;padding:0;width:1px;height:1px;' cellpadding='0' cellspacing='0'><tr><td></td></tr></table>";a.insertBefore(b,a.firstChild);d=b.firstChild;e=d.firstChild;f=d.nextSibling.firstChild.firstChild;this.doesNotAddBorder=e.offsetTop!==5;this.doesAddBorderForTableAndCells=
f.offsetTop===5;e.style.position="fixed";e.style.top="20px";this.supportsFixedPosition=e.offsetTop===20||e.offsetTop===15;e.style.position=e.style.top="";d.style.overflow="hidden";d.style.position="relative";this.subtractsBorderForOverflowNotVisible=e.offsetTop===-5;this.doesNotIncludeMarginInBodyOffset=a.offsetTop!==h;a.removeChild(b);c.offset.initialize=c.noop},bodyOffset:function(a){var b=a.offsetTop,d=a.offsetLeft;c.offset.initialize();if(c.offset.doesNotIncludeMarginInBodyOffset){b+=parseFloat(c.css(a,
"marginTop"))||0;d+=parseFloat(c.css(a,"marginLeft"))||0}return{top:b,left:d}},setOffset:function(a,b,d){var e=c.css(a,"position");if(e==="static")a.style.position="relative";var f=c(a),h=f.offset(),l=c.css(a,"top"),k=c.css(a,"left"),o=e==="absolute"&&c.inArray("auto",[l,k])>-1;e={};var x={};if(o)x=f.position();l=o?x.top:parseInt(l,10)||0;k=o?x.left:parseInt(k,10)||0;if(c.isFunction(b))b=b.call(a,d,h);if(b.top!=null)e.top=b.top-h.top+l;if(b.left!=null)e.left=b.left-h.left+k;"using"in b?b.using.call(a,
e):f.css(e)}};c.fn.extend({position:function(){if(!this[0])return null;var a=this[0],b=this.offsetParent(),d=this.offset(),e=Ia.test(b[0].nodeName)?{top:0,left:0}:b.offset();d.top-=parseFloat(c.css(a,"marginTop"))||0;d.left-=parseFloat(c.css(a,"marginLeft"))||0;e.top+=parseFloat(c.css(b[0],"borderTopWidth"))||0;e.left+=parseFloat(c.css(b[0],"borderLeftWidth"))||0;return{top:d.top-e.top,left:d.left-e.left}},offsetParent:function(){return this.map(function(){for(var a=this.offsetParent||t.body;a&&!Ia.test(a.nodeName)&&
c.css(a,"position")==="static";)a=a.offsetParent;return a})}});c.each(["Left","Top"],function(a,b){var d="scroll"+b;c.fn[d]=function(e){var f=this[0],h;if(!f)return null;if(e!==B)return this.each(function(){if(h=fa(this))h.scrollTo(!a?e:c(h).scrollLeft(),a?e:c(h).scrollTop());else this[d]=e});else return(h=fa(f))?"pageXOffset"in h?h[a?"pageYOffset":"pageXOffset"]:c.support.boxModel&&h.document.documentElement[d]||h.document.body[d]:f[d]}});c.each(["Height","Width"],function(a,b){var d=b.toLowerCase();
c.fn["inner"+b]=function(){return this[0]?parseFloat(c.css(this[0],d,"padding")):null};c.fn["outer"+b]=function(e){return this[0]?parseFloat(c.css(this[0],d,e?"margin":"border")):null};c.fn[d]=function(e){var f=this[0];if(!f)return e==null?null:this;if(c.isFunction(e))return this.each(function(l){var k=c(this);k[d](e.call(this,l,k[d]()))});if(c.isWindow(f))return f.document.compatMode==="CSS1Compat"&&f.document.documentElement["client"+b]||f.document.body["client"+b];else if(f.nodeType===9)return Math.max(f.documentElement["client"+
b],f.body["scroll"+b],f.documentElement["scroll"+b],f.body["offset"+b],f.documentElement["offset"+b]);else if(e===B){f=c.css(f,d);var h=parseFloat(f);return c.isNaN(h)?f:h}else return this.css(d,typeof e==="string"?e:e+"px")}})})(window);

}
/*! jQuery UI - v1.8.19 - 2012-04-16
* https://github.com/jquery/jquery-ui
* Includes: jquery.ui.core.js
* Copyright (c) 2012 AUTHORS.txt; Licensed MIT, GPL */
(function(a,b){function c(b,c){var e=b.nodeName.toLowerCase();if("area"===e){var f=b.parentNode,g=f.name,h;return!b.href||!g||f.nodeName.toLowerCase()!=="map"?!1:(h=a("img[usemap=#"+g+"]")[0],!!h&&d(h))}return(/input|select|textarea|button|object/.test(e)?!b.disabled:"a"==e?b.href||c:c)&&d(b)}function d(b){return!a(b).parents().andSelf().filter(function(){return a.curCSS(this,"visibility")==="hidden"||a.expr.filters.hidden(this)}).length}a.ui=a.ui||{};if(a.ui.version)return;a.extend(a.ui,{version:"1.8.19",keyCode:{ALT:18,BACKSPACE:8,CAPS_LOCK:20,COMMA:188,COMMAND:91,COMMAND_LEFT:91,COMMAND_RIGHT:93,CONTROL:17,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,INSERT:45,LEFT:37,MENU:93,NUMPAD_ADD:107,NUMPAD_DECIMAL:110,NUMPAD_DIVIDE:111,NUMPAD_ENTER:108,NUMPAD_MULTIPLY:106,NUMPAD_SUBTRACT:109,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SHIFT:16,SPACE:32,TAB:9,UP:38,WINDOWS:91}}),a.fn.extend({propAttr:a.fn.prop||a.fn.attr,_focus:a.fn.focus,focus:function(b,c){return typeof b=="number"?this.each(function(){var d=this;setTimeout(function(){a(d).focus(),c&&c.call(d)},b)}):this._focus.apply(this,arguments)},scrollParent:function(){var b;return a.browser.msie&&/(static|relative)/.test(this.css("position"))||/absolute/.test(this.css("position"))?b=this.parents().filter(function(){return/(relative|absolute|fixed)/.test(a.curCSS(this,"position",1))&&/(auto|scroll)/.test(a.curCSS(this,"overflow",1)+a.curCSS(this,"overflow-y",1)+a.curCSS(this,"overflow-x",1))}).eq(0):b=this.parents().filter(function(){return/(auto|scroll)/.test(a.curCSS(this,"overflow",1)+a.curCSS(this,"overflow-y",1)+a.curCSS(this,"overflow-x",1))}).eq(0),/fixed/.test(this.css("position"))||!b.length?a(document):b},zIndex:function(c){if(c!==b)return this.css("zIndex",c);if(this.length){var d=a(this[0]),e,f;while(d.length&&d[0]!==document){e=d.css("position");if(e==="absolute"||e==="relative"||e==="fixed"){f=parseInt(d.css("zIndex"),10);if(!isNaN(f)&&f!==0)return f}d=d.parent()}}return 0},disableSelection:function(){return this.bind((a.support.selectstart?"selectstart":"mousedown")+".ui-disableSelection",function(a){a.preventDefault()})},enableSelection:function(){return this.unbind(".ui-disableSelection")}}),a.each(["Width","Height"],function(c,d){function h(b,c,d,f){return a.each(e,function(){c-=parseFloat(a.curCSS(b,"padding"+this,!0))||0,d&&(c-=parseFloat(a.curCSS(b,"border"+this+"Width",!0))||0),f&&(c-=parseFloat(a.curCSS(b,"margin"+this,!0))||0)}),c}var e=d==="Width"?["Left","Right"]:["Top","Bottom"],f=d.toLowerCase(),g={innerWidth:a.fn.innerWidth,innerHeight:a.fn.innerHeight,outerWidth:a.fn.outerWidth,outerHeight:a.fn.outerHeight};a.fn["inner"+d]=function(c){return c===b?g["inner"+d].call(this):this.each(function(){a(this).css(f,h(this,c)+"px")})},a.fn["outer"+d]=function(b,c){return typeof b!="number"?g["outer"+d].call(this,b):this.each(function(){a(this).css(f,h(this,b,!0,c)+"px")})}}),a.extend(a.expr[":"],{data:function(b,c,d){return!!a.data(b,d[3])},focusable:function(b){return c(b,!isNaN(a.attr(b,"tabindex")))},tabbable:function(b){var d=a.attr(b,"tabindex"),e=isNaN(d);return(e||d>=0)&&c(b,!e)}}),a(function(){var b=document.body,c=b.appendChild(c=document.createElement("div"));c.offsetHeight,a.extend(c.style,{minHeight:"100px",height:"auto",padding:0,borderWidth:0}),a.support.minHeight=c.offsetHeight===100,a.support.selectstart="onselectstart"in c,b.removeChild(c).style.display="none"}),a.extend(a.ui,{plugin:{add:function(b,c,d){var e=a.ui[b].prototype;for(var f in d)e.plugins[f]=e.plugins[f]||[],e.plugins[f].push([c,d[f]])},call:function(a,b,c){var d=a.plugins[b];if(!d||!a.element[0].parentNode)return;for(var e=0;e<d.length;e++)a.options[d[e][0]]&&d[e][1].apply(a.element,c)}},contains:function(a,b){return document.compareDocumentPosition?a.compareDocumentPosition(b)&16:a!==b&&a.contains(b)},hasScroll:function(b,c){if(a(b).css("overflow")==="hidden")return!1;var d=c&&c==="left"?"scrollLeft":"scrollTop",e=!1;return b[d]>0?!0:(b[d]=1,e=b[d]>0,b[d]=0,e)},isOverAxis:function(a,b,c){return a>b&&a<b+c},isOver:function(b,c,d,e,f,g){return a.ui.isOverAxis(b,d,f)&&a.ui.isOverAxis(c,e,g)}})})(jQuery);/*! jQuery UI - v1.8.19 - 2012-04-16
* https://github.com/jquery/jquery-ui
* Includes: jquery.ui.widget.js
* Copyright (c) 2012 AUTHORS.txt; Licensed MIT, GPL */
(function(a,b){if(a.cleanData){var c=a.cleanData;a.cleanData=function(b){for(var d=0,e;(e=b[d])!=null;d++)try{a(e).triggerHandler("remove")}catch(f){}c(b)}}else{var d=a.fn.remove;a.fn.remove=function(b,c){return this.each(function(){return c||(!b||a.filter(b,[this]).length)&&a("*",this).add([this]).each(function(){try{a(this).triggerHandler("remove")}catch(b){}}),d.call(a(this),b,c)})}}a.widget=function(b,c,d){var e=b.split(".")[0],f;b=b.split(".")[1],f=e+"-"+b,d||(d=c,c=a.Widget),a.expr[":"][f]=function(c){return!!a.data(c,b)},a[e]=a[e]||{},a[e][b]=function(a,b){arguments.length&&this._createWidget(a,b)};var g=new c;g.options=a.extend(!0,{},g.options),a[e][b].prototype=a.extend(!0,g,{namespace:e,widgetName:b,widgetEventPrefix:a[e][b].prototype.widgetEventPrefix||b,widgetBaseClass:f},d),a.widget.bridge(b,a[e][b])},a.widget.bridge=function(c,d){a.fn[c]=function(e){var f=typeof e=="string",g=Array.prototype.slice.call(arguments,1),h=this;return e=!f&&g.length?a.extend.apply(null,[!0,e].concat(g)):e,f&&e.charAt(0)==="_"?h:(f?this.each(function(){var d=a.data(this,c),f=d&&a.isFunction(d[e])?d[e].apply(d,g):d;if(f!==d&&f!==b)return h=f,!1}):this.each(function(){var b=a.data(this,c);b?b.option(e||{})._init():a.data(this,c,new d(e,this))}),h)}},a.Widget=function(a,b){arguments.length&&this._createWidget(a,b)},a.Widget.prototype={widgetName:"widget",widgetEventPrefix:"",options:{disabled:!1},_createWidget:function(b,c){a.data(c,this.widgetName,this),this.element=a(c),this.options=a.extend(!0,{},this.options,this._getCreateOptions(),b);var d=this;this.element.bind("remove."+this.widgetName,function(){d.destroy()}),this._create(),this._trigger("create"),this._init()},_getCreateOptions:function(){return a.metadata&&a.metadata.get(this.element[0])[this.widgetName]},_create:function(){},_init:function(){},destroy:function(){this.element.unbind("."+this.widgetName).removeData(this.widgetName),this.widget().unbind("."+this.widgetName).removeAttr("aria-disabled").removeClass(this.widgetBaseClass+"-disabled "+"ui-state-disabled")},widget:function(){return this.element},option:function(c,d){var e=c;if(arguments.length===0)return a.extend({},this.options);if(typeof c=="string"){if(d===b)return this.options[c];e={},e[c]=d}return this._setOptions(e),this},_setOptions:function(b){var c=this;return a.each(b,function(a,b){c._setOption(a,b)}),this},_setOption:function(a,b){return this.options[a]=b,a==="disabled"&&this.widget()[b?"addClass":"removeClass"](this.widgetBaseClass+"-disabled"+" "+"ui-state-disabled").attr("aria-disabled",b),this},enable:function(){return this._setOption("disabled",!1)},disable:function(){return this._setOption("disabled",!0)},_trigger:function(b,c,d){var e,f,g=this.options[b];d=d||{},c=a.Event(c),c.type=(b===this.widgetEventPrefix?b:this.widgetEventPrefix+b).toLowerCase(),c.target=this.element[0],f=c.originalEvent;if(f)for(e in f)e in c||(c[e]=f[e]);return this.element.trigger(c,d),!(a.isFunction(g)&&g.call(this.element[0],c,d)===!1||c.isDefaultPrevented())}}})(jQuery);/*! jQuery UI - v1.8.19 - 2012-04-16
* https://github.com/jquery/jquery-ui
* Includes: jquery.ui.position.js
* Copyright (c) 2012 AUTHORS.txt; Licensed MIT, GPL */
(function(a,b){a.ui=a.ui||{};var c=/left|center|right/,d=/top|center|bottom/,e="center",f={},g=a.fn.position,h=a.fn.offset;a.fn.position=function(b){if(!b||!b.of)return g.apply(this,arguments);b=a.extend({},b);var h=a(b.of),i=h[0],j=(b.collision||"flip").split(" "),k=b.offset?b.offset.split(" "):[0,0],l,m,n;return i.nodeType===9?(l=h.width(),m=h.height(),n={top:0,left:0}):i.setTimeout?(l=h.width(),m=h.height(),n={top:h.scrollTop(),left:h.scrollLeft()}):i.preventDefault?(b.at="left top",l=m=0,n={top:b.of.pageY,left:b.of.pageX}):(l=h.outerWidth(),m=h.outerHeight(),n=h.offset()),a.each(["my","at"],function(){var a=(b[this]||"").split(" ");a.length===1&&(a=c.test(a[0])?a.concat([e]):d.test(a[0])?[e].concat(a):[e,e]),a[0]=c.test(a[0])?a[0]:e,a[1]=d.test(a[1])?a[1]:e,b[this]=a}),j.length===1&&(j[1]=j[0]),k[0]=parseInt(k[0],10)||0,k.length===1&&(k[1]=k[0]),k[1]=parseInt(k[1],10)||0,b.at[0]==="right"?n.left+=l:b.at[0]===e&&(n.left+=l/2),b.at[1]==="bottom"?n.top+=m:b.at[1]===e&&(n.top+=m/2),n.left+=k[0],n.top+=k[1],this.each(function(){var c=a(this),d=c.outerWidth(),g=c.outerHeight(),h=parseInt(a.curCSS(this,"marginLeft",!0))||0,i=parseInt(a.curCSS(this,"marginTop",!0))||0,o=d+h+(parseInt(a.curCSS(this,"marginRight",!0))||0),p=g+i+(parseInt(a.curCSS(this,"marginBottom",!0))||0),q=a.extend({},n),r;b.my[0]==="right"?q.left-=d:b.my[0]===e&&(q.left-=d/2),b.my[1]==="bottom"?q.top-=g:b.my[1]===e&&(q.top-=g/2),f.fractions||(q.left=Math.round(q.left),q.top=Math.round(q.top)),r={left:q.left-h,top:q.top-i},a.each(["left","top"],function(c,e){a.ui.position[j[c]]&&a.ui.position[j[c]][e](q,{targetWidth:l,targetHeight:m,elemWidth:d,elemHeight:g,collisionPosition:r,collisionWidth:o,collisionHeight:p,offset:k,my:b.my,at:b.at})}),a.fn.bgiframe&&c.bgiframe(),c.offset(a.extend(q,{using:b.using}))})},a.ui.position={fit:{left:function(b,c){var d=a(window),e=c.collisionPosition.left+c.collisionWidth-d.width()-d.scrollLeft();b.left=e>0?b.left-e:Math.max(b.left-c.collisionPosition.left,b.left)},top:function(b,c){var d=a(window),e=c.collisionPosition.top+c.collisionHeight-d.height()-d.scrollTop();b.top=e>0?b.top-e:Math.max(b.top-c.collisionPosition.top,b.top)}},flip:{left:function(b,c){if(c.at[0]===e)return;var d=a(window),f=c.collisionPosition.left+c.collisionWidth-d.width()-d.scrollLeft(),g=c.my[0]==="left"?-c.elemWidth:c.my[0]==="right"?c.elemWidth:0,h=c.at[0]==="left"?c.targetWidth:-c.targetWidth,i=-2*c.offset[0];b.left+=c.collisionPosition.left<0?g+h+i:f>0?g+h+i:0},top:function(b,c){if(c.at[1]===e)return;var d=a(window),f=c.collisionPosition.top+c.collisionHeight-d.height()-d.scrollTop(),g=c.my[1]==="top"?-c.elemHeight:c.my[1]==="bottom"?c.elemHeight:0,h=c.at[1]==="top"?c.targetHeight:-c.targetHeight,i=-2*c.offset[1];b.top+=c.collisionPosition.top<0?g+h+i:f>0?g+h+i:0}}},a.offset.setOffset||(a.offset.setOffset=function(b,c){/static/.test(a.curCSS(b,"position"))&&(b.style.position="relative");var d=a(b),e=d.offset(),f=parseInt(a.curCSS(b,"top",!0),10)||0,g=parseInt(a.curCSS(b,"left",!0),10)||0,h={top:c.top-e.top+f,left:c.left-e.left+g};"using"in c?c.using.call(b,h):d.css(h)},a.fn.offset=function(b){var c=this[0];return!c||!c.ownerDocument?null:b?this.each(function(){a.offset.setOffset(this,b)}):h.call(this)}),function(){var b=document.getElementsByTagName("body")[0],c=document.createElement("div"),d,e,g,h,i;d=document.createElement(b?"div":"body"),g={visibility:"hidden",width:0,height:0,border:0,margin:0,background:"none"},b&&a.extend(g,{position:"absolute",left:"-1000px",top:"-1000px"});for(var j in g)d.style[j]=g[j];d.appendChild(c),e=b||document.documentElement,e.insertBefore(d,e.firstChild),c.style.cssText="position: absolute; left: 10.7432222px; top: 10.432325px; height: 30px; width: 201px;",h=a(c).offset(function(a,b){return b}).offset(),d.innerHTML="",e.removeChild(d),i=h.top+h.left+(b?2e3:0),f.fractions=i>21&&i<22}()})(jQuery);/*! jQuery UI - v1.8.19 - 2012-04-16
* https://github.com/jquery/jquery-ui
* Includes: jquery.ui.dialog.js
* Copyright (c) 2012 AUTHORS.txt; Licensed MIT, GPL */
(function(a,b){var c="ui-dialog ui-widget ui-widget-content ui-corner-all ",d={buttons:!0,height:!0,maxHeight:!0,maxWidth:!0,minHeight:!0,minWidth:!0,width:!0},e={maxHeight:!0,maxWidth:!0,minHeight:!0,minWidth:!0},f=a.attrFn||{val:!0,css:!0,html:!0,text:!0,data:!0,width:!0,height:!0,offset:!0,click:!0};a.widget("ui.dialog",{options:{autoOpen:!0,buttons:{},closeOnEscape:!0,closeText:"close",dialogClass:"",draggable:!0,hide:null,height:"auto",maxHeight:!1,maxWidth:!1,minHeight:150,minWidth:150,modal:!1,position:{my:"center",at:"center",collision:"fit",using:function(b){var c=a(this).css(b).offset().top;c<0&&a(this).css("top",b.top-c)}},resizable:!0,show:null,stack:!0,title:"",width:300,zIndex:1e3},_create:function(){this.originalTitle=this.element.attr("title"),typeof this.originalTitle!="string"&&(this.originalTitle=""),this.options.title=this.options.title||this.originalTitle;var b=this,d=b.options,e=d.title||"&#160;",f=a.ui.dialog.getTitleId(b.element),g=(b.uiDialog=a("<div></div>")).appendTo(document.body).hide().addClass(c+d.dialogClass).css({zIndex:d.zIndex}).attr("tabIndex",-1).css("outline",0).keydown(function(c){d.closeOnEscape&&!c.isDefaultPrevented()&&c.keyCode&&c.keyCode===a.ui.keyCode.ESCAPE&&(b.close(c),c.preventDefault())}).attr({role:"dialog","aria-labelledby":f}).mousedown(function(a){b.moveToTop(!1,a)}),h=b.element.show().removeAttr("title").addClass("ui-dialog-content ui-widget-content").appendTo(g),i=(b.uiDialogTitlebar=a("<div></div>")).addClass("ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix").prependTo(g),j=a('<a href="#"></a>').addClass("ui-dialog-titlebar-close ui-corner-all").attr("role","button").hover(function(){j.addClass("ui-state-hover")},function(){j.removeClass("ui-state-hover")}).focus(function(){j.addClass("ui-state-focus")}).blur(function(){j.removeClass("ui-state-focus")}).click(function(a){return b.close(a),!1}).appendTo(i),k=(b.uiDialogTitlebarCloseText=a("<span></span>")).addClass("ui-icon ui-icon-closethick").text(d.closeText).appendTo(j),l=a("<span></span>").addClass("ui-dialog-title").attr("id",f).html(e).prependTo(i);a.isFunction(d.beforeclose)&&!a.isFunction(d.beforeClose)&&(d.beforeClose=d.beforeclose),i.find("*").add(i).disableSelection(),d.draggable&&a.fn.draggable&&b._makeDraggable(),d.resizable&&a.fn.resizable&&b._makeResizable(),b._createButtons(d.buttons),b._isOpen=!1,a.fn.bgiframe&&g.bgiframe()},_init:function(){this.options.autoOpen&&this.open()},destroy:function(){var a=this;return a.overlay&&a.overlay.destroy(),a.uiDialog.hide(),a.element.unbind(".dialog").removeData("dialog").removeClass("ui-dialog-content ui-widget-content").hide().appendTo("body"),a.uiDialog.remove(),a.originalTitle&&a.element.attr("title",a.originalTitle),a},widget:function(){return this.uiDialog},close:function(b){var c=this,d,e;if(!1===c._trigger("beforeClose",b))return;return c.overlay&&c.overlay.destroy(),c.uiDialog.unbind("keypress.ui-dialog"),c._isOpen=!1,c.options.hide?c.uiDialog.hide(c.options.hide,function(){c._trigger("close",b)}):(c.uiDialog.hide(),c._trigger("close",b)),a.ui.dialog.overlay.resize(),c.options.modal&&(d=0,a(".ui-dialog").each(function(){this!==c.uiDialog[0]&&(e=a(this).css("z-index"),isNaN(e)||(d=Math.max(d,e)))}),a.ui.dialog.maxZ=d),c},isOpen:function(){return this._isOpen},moveToTop:function(b,c){var d=this,e=d.options,f;return e.modal&&!b||!e.stack&&!e.modal?d._trigger("focus",c):(e.zIndex>a.ui.dialog.maxZ&&(a.ui.dialog.maxZ=e.zIndex),d.overlay&&(a.ui.dialog.maxZ+=1,d.overlay.$el.css("z-index",a.ui.dialog.overlay.maxZ=a.ui.dialog.maxZ)),f={scrollTop:d.element.scrollTop(),scrollLeft:d.element.scrollLeft()},a.ui.dialog.maxZ+=1,d.uiDialog.css("z-index",a.ui.dialog.maxZ),d.element.attr(f),d._trigger("focus",c),d)},open:function(){if(this._isOpen)return;var b=this,c=b.options,d=b.uiDialog;return b.overlay=c.modal?new a.ui.dialog.overlay(b):null,b._size(),b._position(c.position),d.show(c.show),b.moveToTop(!0),c.modal&&d.bind("keydown.ui-dialog",function(b){if(b.keyCode!==a.ui.keyCode.TAB)return;var c=a(":tabbable",this),d=c.filter(":first"),e=c.filter(":last");if(b.target===e[0]&&!b.shiftKey)return d.focus(1),!1;if(b.target===d[0]&&b.shiftKey)return e.focus(1),!1}),a(b.element.find(":tabbable").get().concat(d.find(".ui-dialog-buttonpane :tabbable").get().concat(d.get()))).eq(0).focus(),b._isOpen=!0,b._trigger("open"),b},_createButtons:function(b){var c=this,d=!1,e=a("<div></div>").addClass("ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"),g=a("<div></div>").addClass("ui-dialog-buttonset").appendTo(e);c.uiDialog.find(".ui-dialog-buttonpane").remove(),typeof b=="object"&&b!==null&&a.each(b,function(){return!(d=!0)}),d&&(a.each(b,function(b,d){d=a.isFunction(d)?{click:d,text:b}:d;var e=a('<button type="button"></button>').click(function(){d.click.apply(c.element[0],arguments)}).appendTo(g);a.each(d,function(a,b){if(a==="click")return;a in f?e[a](b):e.attr(a,b)}),a.fn.button&&e.button()}),e.appendTo(c.uiDialog))},_makeDraggable:function(){function f(a){return{position:a.position,offset:a.offset}}var b=this,c=b.options,d=a(document),e;b.uiDialog.draggable({cancel:".ui-dialog-content, .ui-dialog-titlebar-close",handle:".ui-dialog-titlebar",containment:"document",start:function(d,g){e=c.height==="auto"?"auto":a(this).height(),a(this).height(a(this).height()).addClass("ui-dialog-dragging"),b._trigger("dragStart",d,f(g))},drag:function(a,c){b._trigger("drag",a,f(c))},stop:function(g,h){c.position=[h.position.left-d.scrollLeft(),h.position.top-d.scrollTop()],a(this).removeClass("ui-dialog-dragging").height(e),b._trigger("dragStop",g,f(h)),a.ui.dialog.overlay.resize()}})},_makeResizable:function(c){function h(a){return{originalPosition:a.originalPosition,originalSize:a.originalSize,position:a.position,size:a.size}}c=c===b?this.options.resizable:c;var d=this,e=d.options,f=d.uiDialog.css("position"),g=typeof c=="string"?c:"n,e,s,w,se,sw,ne,nw";d.uiDialog.resizable({cancel:".ui-dialog-content",containment:"document",alsoResize:d.element,maxWidth:e.maxWidth,maxHeight:e.maxHeight,minWidth:e.minWidth,minHeight:d._minHeight(),handles:g,start:function(b,c){a(this).addClass("ui-dialog-resizing"),d._trigger("resizeStart",b,h(c))},resize:function(a,b){d._trigger("resize",a,h(b))},stop:function(b,c){a(this).removeClass("ui-dialog-resizing"),e.height=a(this).height(),e.width=a(this).width(),d._trigger("resizeStop",b,h(c)),a.ui.dialog.overlay.resize()}}).css("position",f).find(".ui-resizable-se").addClass("ui-icon ui-icon-grip-diagonal-se")},_minHeight:function(){var a=this.options;return a.height==="auto"?a.minHeight:Math.min(a.minHeight,a.height)},_position:function(b){var c=[],d=[0,0],e;if(b){if(typeof b=="string"||typeof b=="object"&&"0"in b)c=b.split?b.split(" "):[b[0],b[1]],c.length===1&&(c[1]=c[0]),a.each(["left","top"],function(a,b){+c[a]===c[a]&&(d[a]=c[a],c[a]=b)}),b={my:c.join(" "),at:c.join(" "),offset:d.join(" ")};b=a.extend({},a.ui.dialog.prototype.options.position,b)}else b=a.ui.dialog.prototype.options.position;e=this.uiDialog.is(":visible"),e||this.uiDialog.show(),this.uiDialog.css({top:0,left:0}).position(a.extend({of:window},b)),e||this.uiDialog.hide()},_setOptions:function(b){var c=this,f={},g=!1;a.each(b,function(a,b){c._setOption(a,b),a in d&&(g=!0),a in e&&(f[a]=b)}),g&&this._size(),this.uiDialog.is(":data(resizable)")&&this.uiDialog.resizable("option",f)},_setOption:function(b,d){var e=this,f=e.uiDialog;switch(b){case"beforeclose":b="beforeClose";break;case"buttons":e._createButtons(d);break;case"closeText":e.uiDialogTitlebarCloseText.text(""+d);break;case"dialogClass":f.removeClass(e.options.dialogClass).addClass(c+d);break;case"disabled":d?f.addClass("ui-dialog-disabled"):f.removeClass("ui-dialog-disabled");break;case"draggable":var g=f.is(":data(draggable)");g&&!d&&f.draggable("destroy"),!g&&d&&e._makeDraggable();break;case"position":e._position(d);break;case"resizable":var h=f.is(":data(resizable)");h&&!d&&f.resizable("destroy"),h&&typeof d=="string"&&f.resizable("option","handles",d),!h&&d!==!1&&e._makeResizable(d);break;case"title":a(".ui-dialog-title",e.uiDialogTitlebar).html(""+(d||"&#160;"))}a.Widget.prototype._setOption.apply(e,arguments)},_size:function(){var b=this.options,c,d,e=this.uiDialog.is(":visible");this.element.show().css({width:"auto",minHeight:0,height:0}),b.minWidth>b.width&&(b.width=b.minWidth),c=this.uiDialog.css({height:"auto",width:b.width}).height(),d=Math.max(0,b.minHeight-c);if(b.height==="auto")if(a.support.minHeight)this.element.css({minHeight:d,height:"auto"});else{this.uiDialog.show();var f=this.element.css("height","auto").height();e||this.uiDialog.hide(),this.element.height(Math.max(f,d))}else this.element.height(Math.max(b.height-c,0));this.uiDialog.is(":data(resizable)")&&this.uiDialog.resizable("option","minHeight",this._minHeight())}}),a.extend(a.ui.dialog,{version:"1.8.19",uuid:0,maxZ:0,getTitleId:function(a){var b=a.attr("id");return b||(this.uuid+=1,b=this.uuid),"ui-dialog-title-"+b},overlay:function(b){this.$el=a.ui.dialog.overlay.create(b)}}),a.extend(a.ui.dialog.overlay,{instances:[],oldInstances:[],maxZ:0,events:a.map("focus,mousedown,mouseup,keydown,keypress,click".split(","),function(a){return a+".dialog-overlay"}).join(" "),create:function(b){this.instances.length===0&&(setTimeout(function(){a.ui.dialog.overlay.instances.length&&a(document).bind(a.ui.dialog.overlay.events,function(b){if(a(b.target).zIndex()<a.ui.dialog.overlay.maxZ)return!1})},1),a(document).bind("keydown.dialog-overlay",function(c){b.options.closeOnEscape&&!c.isDefaultPrevented()&&c.keyCode&&c.keyCode===a.ui.keyCode.ESCAPE&&(b.close(c),c.preventDefault())}),a(window).bind("resize.dialog-overlay",a.ui.dialog.overlay.resize));var c=(this.oldInstances.pop()||a("<div></div>").addClass("ui-widget-overlay")).appendTo(document.body).css({width:this.width(),height:this.height()});return a.fn.bgiframe&&c.bgiframe(),this.instances.push(c),c},destroy:function(b){var c=a.inArray(b,this.instances);c!=-1&&this.oldInstances.push(this.instances.splice(c,1)[0]),this.instances.length===0&&a([document,window]).unbind(".dialog-overlay"),b.remove();var d=0;a.each(this.instances,function(){d=Math.max(d,this.css("z-index"))}),this.maxZ=d},height:function(){var b,c;return a.browser.msie&&a.browser.version<7?(b=Math.max(document.documentElement.scrollHeight,document.body.scrollHeight),c=Math.max(document.documentElement.offsetHeight,document.body.offsetHeight),b<c?a(window).height()+"px":b+"px"):a(document).height()+"px"},width:function(){var b,c;return a.browser.msie?(b=Math.max(document.documentElement.scrollWidth,document.body.scrollWidth),c=Math.max(document.documentElement.offsetWidth,document.body.offsetWidth),b<c?a(window).width()+"px":b+"px"):a(document).width()+"px"},resize:function(){var b=a([]);a.each(a.ui.dialog.overlay.instances,function(){b=b.add(this)}),b.css({width:0,height:0}).css({width:a.ui.dialog.overlay.width(),height:a.ui.dialog.overlay.height()})}}),a.extend(a.ui.dialog.overlay.prototype,{destroy:function(){a.ui.dialog.overlay.destroy(this.$el)}})})(jQuery);/*
 * GDA Virtual Keyboard configuration for NGO
 */

var gda_vk_config = {
	switch_button_left: 102,
	switch_button_top: 8,
	opacity: 0.56,
	zones: [ {x:150, y:230, height:270, width:610} ],
	overwriteImages: { "tc_ombre": "/static/img/vk/tc_ombre.gif" },
	faqURI: 'javascript: showFAQ("/aide/clavier_virtuel.html", "CodeSecret")',
	faqNVURI: 'javascript:showFAQ("/aide/clavier_sonore.html", "CodeSecret")',
	useDisableLayer: true,
	disableLayerOpacity: 0.55,
	closefn: function() { if (typeof(fermerCV) == "function") fermerCV(); }
 };/*
 * gda.js
 *
 * JS entry point for GDA
 */

/* Dynamic JS include function */
function gda_js_include(URI) {
	if (typeof(gda_js_includes) != 'undefined' && gda_js_includes[URI] == 1)
		return;
	/* XXX: we SHOULD use DOM here, but IE won't wait for the includes to be parsed and fail if the HTML contains calls to the included scripts */
	if (typeof(gda_base_uri) == "string")
		URI = gda_base_uri + URI;
	document.write('<script type="text/javascript" src="' + URI + '"></script>');
	if (typeof(gda_js_includes) == 'undefined') {
		gda_js_includes = new Array();
	}
	gda_js_includes[URI] = 1;
}

/* Dynamic CSS include function */
function gda_css_include(URI) {
	if (typeof(gda_css_includes) != 'undefined' && gda_css_includes[URI] == 1)
		return;
	
	if (document.createStyleSheet) { /* IE */
		document.createStyleSheet(URI);
	}
	else {
		var linkTag = $jGda(document.createElement('link')).attr('rel', 'stylesheet').attr('type', 'text/css').attr('media', 'screen').attr('href', URI);
		$jGda($jGda('head')[0]).append(linkTag);
	}
	
	if (typeof(gda_css_includes) == 'undefined') {
		gda_css_includes = new Array();
	}
	gda_css_includes[URI] = 1;
}

function gda_js_getURL(URI) {
	if (typeof(gda_base_uri) == "string")
		return gda_base_uri + URI;
	else
		return URI;
}


if ($jCsa == null) {
	gda_js_include('/static/js/jquery-1.4.4.min.js');
	/* Include jquery */
	var $jCsa = jQuery.noConflict();
	$jCsa.ajaxSetup({cache:true});
	var $jGda = jQuery.noConflict();
	$jGda.ajaxSetup({cache:true});
}

/* Include main JS */
gda_js_include('/static/js/gda_main.js');

/*
 * gda_main.js
 *
 * Main JS for GDA
 */


/* Include auth modules */
var gda_modules = {};

var gda_auth_module = 'vk';
var gda_sign_module = 'vk';
var gda_origin_lgn = 'LGN';
var gda_origin_sas = 'SAS';

gda_js_include('/static/js/gda_' + gda_auth_module + '.js');
if (gda_auth_module != gda_sign_module)
	gda_js_include('/static/js/gda_' + gda_sign_module + '.js');

var gda_auth_fort_module = 'csa';
gda_js_include('/static/js/gda_' + gda_auth_fort_module + '.js');

/* include dialog gda */
gda_js_include('/static/js/gda_dialog.js');

/* include handler error */
gda_js_include('/static/js/error/handleErrorLayer.js');
gda_js_include('/static/js/error/handleErrorLGN.js');
gda_js_include('/static/js/error/handleErrorSAS.js');

/* gda functions */
function gda_auth(form_id) {
	gda_modules[gda_auth_module].auth(form_id);
}

function gda_auth_json(params) {
	gda_modules[gda_auth_module].auth_json(params);
}


function prepare_gda_auth(form_id) {
	if ((__getCookie('vk_mode')!= constModeDefaut) && (__getCookie('vk_mode')!= constModeMalVoyant)) __setCookie('vk_mode', constModeDefaut);
}

function prepare_gda_auth_nonvoyant(form_id) {
	__setCookie('vk_mode', constModeNonVoyant);
}

function is_gda_auth_nonvoyant() {
	return (__getCookie('vk_mode')== constModeNonVoyant);
}

function gda_secure(form_id) {
	gda_modules[gda_sign_module].sign(form_id);
}

function gda_secure_json(params) {
	gda_modules[gda_sign_module].sign_json(params);
}

function gda_auth_close() {
	if (gda_modules) gda_modules[gda_auth_module].close();
}

function gda_secure_close() {
	if (gda_modules) gda_modules[gda_sign_module].close();
}

function gda_auth_setopts(options) {
	if (gda_modules) gda_modules[gda_auth_module].setopts(options);
}

function gda_secure_setopts(options) {
	if (gda_modules) gda_modules[gda_sign_module].setopts(options);
}

var gda_levels = {
	"ANON": 0,
	"SIMPLE": 300,
	"STRONG": 500
};

var gda_chg_lvl_callback_func;
var gda_chg_lvl_callback_func_args;

function gda_chg_level(level, form_id, derog){
	var resp = get_auth_infos(level);
	if (resp.status == 'OK') {
		gda_chg_level_submit(form_id);
	} else if (resp.status == 'NOK') { // Niveau insuffisant
		var tmp_module = gda_modules[resp.auth_module];
		if (tmp_module != null) {
			tmp_module.init(resp);
			tmp_module.chg_level_set_callback(gda_chg_level_callback);
			tmp_module.chg_level(level, form_id, derog);
		} else {
			//alert('erreur technique module secu introuvable : '+resp.auth_module);
			gda_showErreurTech();
		}
	} else if (resp.status='ERR') { // Erreur sur transition
		if (resp.reason == 'noTrans') {
			gda_showErreurNoTrans();
		} else if (resp.reason == 'noTransSup') {
			gda_showErreurNoTransSup();
		} else {
			gda_showErreurTech();
		}
	} else { // Erreur
		//alert('erreur technique sur appel get_auth_infos');
		gda_showErreurTech();
	}
}

var gda_dialog;

function gda_showErreurTech() {
	gda_dialog = new GdaDialog();
    gda_dialog.showLayer();
    gda_dialog.showErreur('Une erreur technique nous emp&ecirc;che de donner suite &agrave; votre demande.', null,
		'Fermer', 
		'javascript:gda_dialog.closeDialog();gda_dialog.hideLayer();'
	);
}

function gda_showErreurNoTrans() {
	gda_dialog = new GdaDialog();
    gda_dialog.showLayer();
    gda_dialog.showErreur('Votre session n\'est plus valide.', 
		'Pour vous reconnecter, cliquez sur <span style="font-weight: bold;">Retour</span>.',
		'Retour', 
		'/'
	);
}

function gda_showErreurNoTransSup() {
	gda_dialog = new GdaDialog();
    gda_dialog.showLayer();
    gda_dialog.showErreur('Les contr&ocirc;les sont corrects, mais dans le cas d\'une supervision client, la mise &agrave; jour des donn&eacute;es n\'a pas &eacute;t&eacute; effectu&eacute;e.', 
		null,
		'Fermer', 
		'javascript:gda_dialog.closeDialog();gda_dialog.hideLayer();'
	);
}

function gda_chg_level_submit(form_id) {
	if (!gda_chg_level_callback() && form_id != null) document.getElementById(form_id).submit();
}

function gda_chg_level_set_callback(func) {
	gda_chg_lvl_callback_func = func;
	var args_size = arguments.length;
	if (args_size > 0) {
		gda_chg_lvl_callback_func = arguments[0];	
		if (args_size > 1) {
			gda_chg_lvl_callback_func_args = new Array(args_size-1);
			for (var i = 0;i < args_size;i++) {
				gda_chg_lvl_callback_func_args[i] = arguments[i+1];
			}
		} else {
			gda_chg_lvl_callback_func_args = null;
		}
	}
}

function gda_chg_level_callback() {
	if (typeof(gda_chg_lvl_callback_func) == 'function') {
		if (gda_chg_lvl_callback_func_args != null && gda_chg_lvl_callback_func_args.length > 0) {
			gda_chg_lvl_callback_func.apply(null, gda_chg_lvl_callback_func_args);
		} else {
			gda_chg_lvl_callback_func();
		}
		return true;
	} else {
		return false;
	}
}


function get_auth_infos(level) {
	var sUrl = gda_js_getURL('/sec/get_auth_infos?target_level='+level);
	return call_json('GET', sUrl);
}

function call_json(method, sUrl) {
	if (window.XMLHttpRequest) {
		xhr = new XMLHttpRequest();
	}
	else if (window.ActiveXObject) {
		xhr = new ActiveXObject("Microsoft.XMLHTTP"); 
	}
	else {
		alert('Votre navigateur est incompatible');
		return false;
	}
	xhr.open(method, sUrl, false);
	xhr.send(null);
	if (xhr.readyState == 4) {
		if (xhr.status == 200) {
			var resp = xhr.responseText;
			var tmp;
			try {
				tmp = eval('(' + resp + ')');
			} catch(exc) {
//				alert('Erreur technique JSON');
				return false;
			}
			return tmp;
//		} else {
//			alert('Erreur technique HTTP');
		}
	}
	return false;
}

function gda_StringToXML(txt) {
  if (typeof(DOMParser) == 'undefined') {
    var xml = new ActiveXObject('Microsoft.XMLDOM');
    xml.async = false;
    xml.loadXML(txt);
    return xml;
    
  } else {
    var p   = new DOMParser();
    var doc = p.parseFromString(txt, 'text/xml');
    return doc;
    
  }
}

function gda_stringToJSON(jsonTxt) {
	// we read the json string and convert it into an object
	var docJSON = eval('(' + jsonTxt + ')') ;
	return docJSON;
}


function gda_handleResponse(params) {
	var result = false;
	
	if (gda_isGDAResponse(params.contentType, params.responseBody)) {
		if (params.contentType.indexOf("application/xml") != -1) {
			var xml;
			
			if (typeof(params.responseBody) == 'string') {
				xml = gda_StringToXML(params.responseBody);
			} else {
				xml = params.responseBody;
			}
			
			gda_handleErrorXML(params.success, params.error, xml);
			result = true;

	    } else if (params.contentType.indexOf("application/json") != -1) {
	    	var respJSON;
	    	if( typeof(params.responseBody) == 'string' ) {
	    		respJSON = gda_stringToJSON(params.responseBody);
	    	} else {
	    		respJSON = params.responseBody;
	    	}
	    	gda_handleErrorJSON(params.success, params.error, respJSON);
	        result = true;
	 
	    } else {
	        // TODO Cas d'erreur, ajouter une trace ?...
	    	result = false;
	 
	    }
	}
	
	return result;
	
	function gda_isGDAResponse(contentType, responseBody) {
		
		function gda_getOrigin(contentType, responseBody) {
		   var origin = null;
		   
		   if (contentType.indexOf("application/xml") != -1) {
		       var xml;
		       
		       if (typeof(responseBody) == 'string') {
		       	xml = gda_StringToXML(responseBody);
		       } else {
		       	xml = responseBody;
		       }
		       
		       var root = xml.documentElement;
		       origin = root.getElementsByTagName('origin').item(0).firstChild.data;
		    
		   } else if (contentType.indexOf("application/json") != -1) {
		       var respJSON;
		       if (typeof(responseBody) == 'string') {
		       	respJSON = JSON.parse(responseBody);
		       } else {
		       	respJSON = responseBody;
		       }
		       
		       if (respJSON && respJSON.commun && respJSON.commun.origine && respJSON.commun.origine) {
		           var origin = respJSON.commun.origine;
		    
		       } else {
		           // Format JSON incorrect, il ne s'agit pas d'une r�ponse GDA
		           origin = null;
		    
		       }
		    
		   } else {
		       // Il ne s'agit pas d'une r�ponse avec origine
		       origin = null;
		    
		   }
		   
		   return origin;
		}
		
		function gda_isGDAOrigin(origin) {
			   return origin != null && 
			   	(origin.toLowerCase() == gda_origin_sas.toLowerCase() 
						|| origin.toLowerCase() == gda_origin_lgn.toLowerCase());
		}
		
		var origin = gda_getOrigin(contentType, responseBody);
		return gda_isGDAOrigin(origin);
	}
	
	function gda_handleErrorXML(onSuccessCallback, onFailureCallback, xml) {
		var root = xml.documentElement;
		var origin = root.getElementsByTagName('origin').item(0).firstChild.data;
		if (origin == gda_origin_lgn) {
			gda_lgn_handleErrorXML(onSuccessCallback, onFailureCallback, xml);
			
		} else if (origin == gda_origin_sas) {
			gda_sas_handleErrorXML(onSuccessCallback, onFailureCallback, xml);
			
		} else {
	        // TODO Cas d'erreur, ajouter une trace ?...
		}
	}

	function gda_handleErrorJSON(onSuccessCallback, onFailureCallback, respJSON) {
		if (respJSON && respJSON.commun && respJSON.commun.origine && respJSON.commun.origine) {
			var origin = respJSON.commun.origine;
			if (origin == gda_origin_lgn) {
				gda_lgn_handleErrorJSON(onSuccessCallback, onFailureCallback, respJSON);
				
			} else if (origin == gda_origin_sas) {
				gda_sas_handleErrorJSON(onSuccessCallback, onFailureCallback, respJSON);

			} else {
		        // TODO Cas d'erreur, ajouter une trace ?...
			}
		} else {
	        // TODO Cas d'erreur, ajouter une trace ?...
		}
	}
	
}

	
function gda_getAuthInfo(params) {
	function gda_paramsToString(params) {
	   var result = null;
	   
	   if (params) {
	       result = '';
	       for(attrName in params) {
	           if (result.length > 0) {
	               result += ':';
	           }
	           result += attrName + '=' + params[attrName]
	       }
	   }
	   
	   return result;	
	}
	
	var authInfoParams = {
		'codcli': '00000000',
		'crypto': params['cryptocvcs'],
		'codsec': params['codsec']
	};
	
	return gda_paramsToString(authInfoParams);
}
//**********************************************************
// Constantes
//**********************************************************

var playTimeout = null;

/* For dragStart, dragStop, etc. */
var dragObj = new Object();
var haveqt = false;

/* Insert VBScript code detecting whether the plugin we need for blind people. */
document.writeln('<script language="VBscript">');
document.writeln('detectableWithVB = False');
document.writeln('If ScriptEngineMajorVersion >= 2 then');
document.writeln('  detectableWithVB = True');
document.writeln('End If');
document.writeln('  on error resume next');
document.writeln('  detectActiveXControl = False');
document.writeln('  If detectableWithVB Then');
document.writeln('     detectActiveXControl = IsObject(CreateObject(activeXControlName))');
document.writeln('  End If');
document.writeln('  on error resume next');
document.writeln('  detectQuickTimeActiveXControl = False');
document.writeln('  If detectableWithVB Then');
document.writeln('    detectQuickTimeActiveXControl = False');
document.writeln('    hasQuickTimeChecker = false');
document.writeln('    Set hasQuickTimeChecker = CreateObject("QuickTimeCheckObject.QuickTimeCheck.1")');
document.writeln('    If IsObject(hasQuickTimeChecker) Then');
document.writeln('      If hasQuickTimeChecker.IsQuickTimeAvailable(0) Then ');
document.writeln('        haveqt = True');
document.writeln('      End If');
document.writeln('    End If');
document.writeln('  End If');
document.writeln('</script>');

if (navigator.plugins) {
	for (var i = 0; i < navigator.plugins.length; i++) {
		if (navigator.plugins[i].name.indexOf('QuickTime') != -1) {
			haveqt = true;
			break;
		}
	}
}

if (navigator.appVersion.indexOf("Mac") != -1 && navigator.appName.substring(0,9) == "Microsoft" && parseInt(navigator.appVersion) < 5)
	haveqt = true;

/*
 * Nouveau clavier virtuel utilisant uniquement l'API cross-browser jQuery.
 *
 * Documentation de l'API:
 *	http://docs.jquery.com/
 */

/* A few constants. */
var constModeDefaut = 0;
var constModeMalVoyant = 1;
var constModeNonVoyant = 2;
var constSessionDuration = 600;

function showFAQ(url,name) {
  window.open(url, name,
    'width=780,height=540,top=0,left=0,toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,alwaysRaised=yes,resizable=yes');
}

function VirtualKeyboard(width, height, inSession, codeFieldId, options) {
	if (!$jGda('#'+codeFieldId)[0]) {
		alert("Erreur: le champ de code secret d'identifiant '" + codeFieldId + "' n'existe pas dans la page!");
		return;
	}

	this.fieldId = codeFieldId;
	this.inSession = inSession;

	
	this.initParams = function(options) {
		/* Default values. */
		this.pwdlen = 6;	/* Secret code length */
		this.allowSwitch = true;
		this.opacity = 0.56;
		this.switch_button_left = 102;	/* pixels */
		this.switch_button_top = 0;		/* pixels */
		this.urlBase = '';
		this.urlStaticBase = '';
		this.offx = 0;
		this.offy = 0;
		this.emptyCodeMsg = "La saisie de votre Code Secret est obligatoire.";
		this.incompleteCodeMsg = "Le Code Secret saisi est incorrect.\r\n" +
		    "Merci de bien vouloir ressaisir votre Code Secret " + unescape("compos%E9") + " de " +
		    this.pwdlen + " chiffres.";
		this.showHelpButton = true;
		this.faqURI = "javascript:showFAQ('" + this.urlBase + "/codeSecret.html', 'CodeSecret')";
		this.faqNVURI = "javascript:showFAQ('" + this.urlBase + "/html/faq/faq43.html', 'CodeSecret')";
		this.overwriteImages = {};
		this.useDisableLayer = false;
		this.disableLayerOpacity = 0.05;

		var mode = __getCookie('vk_mode');
		this.wantMode = mode ? parseInt(mode) : constModeDefaut;
		if (isNaN(this.wantMode))
		   this.wantMode=constModeDefaut;

		/* Override with supplied options. */
		if (options != undefined)
			$jGda.extend(this, options);
	}
	
	this.initParams(options);

	this.mode = constModeDefaut;
	this.loadingCrypto = false;
	this.numKeys = 0;
	this.state = '';
	this.codsec = '';
	this.urlError = this.urlBase + '/pageErreur.html';
	this.urlKeyboard = this.urlBase + '/sec/vk/gen_crypto?estSession=';
	this.urlImage = this.urlBase + '/sec/vk/gen_ui?modeClavier='; 
	this.urlCSS = this.urlStaticBase + '/static/styles/vk/vk.css';
	this.urlParamCrypto = '&cryptogramme=';
	this.urlParamError = 'cvcsCodeErreur';

	/* Drag'n'drop handling. */
	dragObj.elNode = $jGda('#tc_divdeplace');
	$jGda('#img_tc_haut').bind('mousedown', function(event) {
		dragStart.call(event.currentTarget, event);
	});
	$jGda('#img_tc_haut').bind('mouseup', function(event) {
		dragStop.call(event.currentTarget, event);
	});

	/* Path to images. */
	this.images = {};
	this.images["img_tc_haut"] = "/static/img/vk/tc_haut_sans.gif";
	this.images["tc_fond_img"] = "/static/img/vk/tc_fond.gif";
	this.images["legendeInput"] = "/static/img/vk/tc_votre_code.gif";
	this.images["tc_aideimg"] = "/static/img/vk/bouton_question.gif";
	this.images["tc_corriger"] = "/static/img/vk/tc_corriger.gif";
	this.images["tc_valider"] = "/static/img/vk/tc_valider.gif";
	this.images["surlignage"] = "/static/img/vk/tc_touche_cache_hover.gif";
	this.images["trans"] = "/static/img/vk/trans.gif"

	/* Overwrite with user supplied data */
	update_object(this.images, this.overwriteImages);

	/* Make images in the HTML code point to the correct location. */
	var urlBase = this.urlBase;
	var urlStaticBase = this.urlStaticBase;
	var images = this.images;
	$jGda('#tc_cvcs').find('img').each(function(index, img) {
		if (images[$jGda(img).attr('id')])
			$jGda(img).attr('src', urlStaticBase + images[$jGda(img).attr('id')]);
	});
	/* Create the appropriate link for the help button. */
	$jGda('#tc_aidelien').attr('href', this.faqURI);

	if (this.allowSwitch) {
		var img_mv = $jGda(document.createElement('img')).attr('id', 'tc_haut_switch');
		img_mv.attr('class', 'tc_haut_switch');
		img_mv.attr('src', this.urlStaticBase + '/static/img/vk/tc_agrandir.gif');
		img_mv.attr('style', 'cursor: pointer');
		img_mv.attr('alt', 'AGRANDIR');
		img_mv.attr('title', 'AGRANDIR');
		img_mv.removeAttr('height');
		img_mv.removeAttr('width');
		$jGda('#img_tc_haut').after(img_mv);
		img_mv.bind('click', {thisObj: this}, function(event) {
			event.data.thisObj.toggleMode(event);
		});
	}

	if (this.showHelpButton)
		$jGda('#tc_aidelien').toggle();

	$jGda('#tc_fond_img').css({opacity:this.opacity});
	$jGda('#vk_layer').css({opacity:this.disableLayerOpacity});
		


	/* Reset internal state and use another input. */
	this.reset = function(codeFieldId, options) {
		this.emptyFields();
		this.fieldId = codeFieldId;
		this.initParams(options);
	}

	this.getWidth = function() {
		return $jGda('#tc_fond_img').width();
	}

	this.getHeight = function() {
		return $jGda('#img_tc_haut').height() + $jGda('#tc_fond_img').height();
	}

	this.onKeyClicked = function(event) {
		if (this.mode == constModeDefaut)
			$jGda(event.currentTarget).attr('src', this.urlStaticBase + '/static/img/vk/tc_touche_cache_click_1s2.gif');
		else
			$jGda(event.currentTarget).attr('src', this.urlStaticBase + '/static/img/vk/tc_touche_cache_click_1s2_mv.gif');
		this.clickKeyX = numCurX;
		this.clickKeyY = numCurY;
	}

	this.onKeyUnclicked = function(event) {
		if (this.mode == constModeDefaut)
			$jGda(event.currentTarget).attr('src', this.urlStaticBase + '/static/img/vk/tc_touche_cache_hover.gif');
		else
			$jGda(event.currentTarget).attr('src', this.urlStaticBase + '/static/img/vk/tc_touche_cache_hover_mv.gif');
		if (this.clickKeyX == numCurX && this.clickKeyY == numCurY)
			this.updateValue(parseInt((numCurY - 1) * this.nbc + parseInt(numCurX)) - 1);
	}

	this.show = function() {
		/* Attach events to anything clickable. */
		$jGda('#shapeHautFermer').unbind('click').bind('click', {thisObj: this}, function(event) {
			event.data.thisObj.close();
		});
		$jGda('#tc_corriger').unbind('click').bind('click', {thisObj: this}, function(event) {
			event.data.thisObj.emptyCode();
		});
		$jGda('#tc_valider').unbind('click').bind('click', {thisObj: this}, function(event) {
			event.data.thisObj.validate(event);
		});

		$jGda('#surlignage').unbind('mousedown').bind('mousedown', {thisObj: this}, function(event) {
			event.data.thisObj.onKeyClicked(event);
		});
		$jGda('#surlignage').unbind('mouseup').bind('mouseup', {thisObj: this}, function(event) {
			event.data.thisObj.onKeyUnclicked(event);
		});
		/* This event is for unhilighting the keys. */
		$jGda('#surlignage').unbind('mouseout').bind('mouseout', {thisObj: this}, function(event) {
			desactiverSurlignage.call(event.data.thisObj, event);
		});
		
		if (this.wantMode == constModeNonVoyant)
			this.enableSound();
		else
			this.enableVisual();
	}

	this.enableVisual = function() {
		if (this.crypto != undefined) {
			this.showKeyboard();
			return;
		}
		this.generateKeyboardCoords();
		if (this.state == "") {
			if (this.wantMode != this.mode) {
				this.setMode(this.wantMode);
				this.updateKeyboard($jGda('#tc_cvcs'));
			}
			var vk = this;
			this.getCryptogram(function() {
				vk.state = 'i';
				vk.generateKeyboard();
				if (!vk.inSession) {
					vk.timer = (new Date()).getTime();
				}
				vk.showKeyboard();
			});
		}
	}

	this.showKeyboard = function() {
		if (this.mode != constModeNonVoyant) {
			if (this.useDisableLayer) {
				/*this.vk_layer.show();*/
				 this.vk_layer.fadeIn('300');
				 this.vk_layer.unbind('click').bind('click', function() {
					if (gda_auth_close) {
						gda_auth_close();
					}
				 });
			}

			var div = $jGda('#tc_divdeplace');
			if (typeof(N2GHideFlash) == 'function')
				N2GHideFlash(); // masks flash objects
			div.css('left', $jGda(window).scrollLeft() + this.posx + 'px');
			div.css('top', $jGda(window).scrollTop() + this.posy + 'px');

			this.state = 'v';

//			$('tc_enablesound').focus();
		}
	}

	/* XXX - looks like this one should go now. */
	this.emptyFields = function() {
		this.emptyCode();
		this.state = ''; // XXX - Was in vider_champs() but looks misplaced.
	}

	this.emptyCode = function() {
		if (this.mode != constModeNonVoyant) {
			$jGda('#tc_visu_saisie').val('');
			$jGda('#cvcs_visu_cache').val('');
		} else {
			$jGda('#debutClavier').focus();
		}
		this.numKeys = 0;
		this.codsec = '';
	}

	/* Callback for handling the JSON data returned by the server. */
	this.JSONHandler = function(onSuccess, vkdata) {
		if (vkdata.code != 0) {
			window.location = this.urlError + '?' + this.urlParamError + '=104'; // + codeR;
			return false;
		}
		this.crypto = vkdata.crypto;
		$jGda('#cryptocvcs').val(this.crypto);
		this.nbl = vkdata.nbrows;
		this.nbc = vkdata.nbcols;
		this.adaptKeyboard();

		if (this.crypto == "" || this.nbl <= 0 || this.nbc <= 0) {
			window.location = this.urlError + '?' + this.urlParamError + '=106'; // + codeERR_GEN;
			return false;
		}
		var ndata = this.nbl * this.nbc;
		if (vkdata.grid.length != this.pwdlen * ndata) {
			window.location = this.urlError + '?' + this.urlParamError + '=105'; // + codeERR_GEN;
			return false;
		}
		if (this.mode == constModeNonVoyant)
			this.keyCodes = $jGda.grep(vkdata.grid, function(v) {
				return v != 0;
			});
		else
			this.keyCodes = vkdata.grid;
		onSuccess();
		this.loadingCrypto = false;
	}

	/*
	 * Call server to get cryptogramme and size of the virtual keyboard.
	 * This is needed later on to construct an URL to the server-generated
	 * image, and to send the secret code in encoded form.
	 */
	this.getCryptogram = function(onSuccess) {
		if (this.loadingCrypto)
			return;

		if (this.scriptTag)
			this.scriptTag.remove();

		this.loadingCrypto = true;
		var url = this.urlKeyboard;
		if (this.inSession)
			url += '1';
		else
			url += '0';

		_vkCallback = (function(func, thisObj, callback) {
			return function() {
				func.call(thisObj, callback, arguments[0]);
			}
		})(this.JSONHandler, this, onSuccess);
		
		var scriptTag=document.createElement('script')
	 	if (typeof scriptTag!="undefined")
	 	{
  			scriptTag.setAttribute("type","text/javascript")
  			scriptTag.setAttribute("src", url)
  			document.getElementsByTagName("head")[0].appendChild(scriptTag)

				this.scriptTag = $jGda(scriptTag);
		}
		
	}

	this.hide = function() {
		if (this.mode != constModeNonVoyant) {
			if (this.state != "" || $jGda('#tc_cvcs').is(':visible')) {
				if (this.useDisableLayer) {
					this.vk_layer.hide();
				}
				var div = $jGda('#tc_divdeplace');
				div.css('left', '-600px');
				div.css('top', '-600px');
				this.state = '';
			}
		}
		if (typeof(N2GShowFlash) == 'function')
			N2GShowFlash(); // shows Flash objects
	}

	this.setMode = function(mode) {
		this.mode = mode;
		/* Save mode */
		__setCookie('vk_mode', String(this.mode));
	}

	this.toggleMode = function(event) {
		this.hide();
		if (this.mode == constModeDefaut)
			this.setMode(constModeMalVoyant);
		else
			this.setMode(constModeDefaut);

		__setCookie('vk_mode', String(this.mode));

		this.generateKeyboard();

		this.updateKeyboard($jGda('#tc_cvcs'));
		this.adaptKeyboard();
		/* The delay helps for displaying only when the browser is done rendering. */
		(function(func, objThis) {
			setTimeout(function() {
				func.call(objThis);
			}, 300);
		})(this.showKeyboard, this);

		this.state = 'i';
		event.preventDefault();
	}

	this.getKeyboardMode = function() {
		return this.mode;
	}

	this.generateKeyboard = function() {
		var ndt = this.nbl * this.nbc;
		var k = 0;

		var kbdmap = $jGda('#tc_tclavier');
		if (this.kbdmapIdUnique != undefined) kbdmap = $jGda('#'+this.kbdmapIdUnique);

		/* First, remove any <area> element contained within the <map>. */
		kbdmap.find('area').remove();

		this.kbdmapIdUnique = 'tc_tclavier' + new Date().getTime();
		kbdmap.attr('id', this.kbdmapIdUnique).attr('name', this.kbdmapIdUnique);
		kbdmap = $jGda('#'+this.kbdmapIdUnique);
		
		/* Then, add back all the <area> elements matching this keyboard. */
		for (i = 1; i <= this.nbl; i++) {
			for (j = 1; j <= this.nbc; j++) {
				var keyId = 'touche' + i + j;
				var factor = 24;
				if (this.mode == constModeMalVoyant)
					factor *= 2;
				var coords = (j - 1) * factor + "," + (i - 1) * factor + "," + j * factor + "," + i * factor;
				var area = $jGda(document.createElement('area')).attr('id', keyId).attr('coords', coords).attr('style', 'cursor: pointer;display:block;');
				/* Insert element in the DOM before registering events. */
				kbdmap.append(area);
				if (this.keyCodes[k] != 0) {
					area.addClass('touche');
					area.bind('mouseout', {thisObj: this}, function(event) {
						desactiverSurlignage.call(event.data.thisObj, event);
					});
					area.bind('mouseover', {thisObj:this, jArg:j, iArg:i}, function(event) {
						activerSurlignage.call(event.data.thisObj, event, event.data.jArg, event.data.iArg);
					});
				} else {
					area.addClass('toucheVide');
					area.bind('mouseover', {thisObj:this}, function(event) {
						desactiverSurlignage.call(event.data.thisObj, event);
					});
				}
				k++;
			}
		}

		$jGda('#img_clavier').attr('useMap', '#'+this.kbdmapIdUnique);
		$jGda('#img_clavier').attr('src', this.urlImage + this.mode + this.urlParamCrypto + this.crypto);
	}

	this.close = function() {
		this.hide();
		this.emptyFields();
		this.crypto = null;
		if (this.mode == constModeNonVoyant) {
			$jGda('#pluginQt').remove();
		}
		if (this.closefn)
			this.closefn();
	}

	/* Update keyboard tags to point to the correct images and update CSS classes. */
	this.updateKeyboard = function(elem) {
		if (this.mode == constModeDefaut)
			$jGda('#tc_fond_img').css({opacity:this.opacity});
		else
			$jGda('#tc_fond_img').css({opacity:1});
		var children = $jGda(elem).find('*');
		var numchilds = children.length;
		for (var i = 0; i < numchilds; ++i) {
			var child = children[i];
			if (child.nodeName.toLowerCase() == 'img' && $jGda(child).attr('src')) {
				var src = child.src;
				if (src != "") {
					if (this.mode == constModeDefaut)
						child.src = src.replace(/_mv(\.\w+)$/, '$1');
					else
						child.src = src.replace(/(\.\w+)$/, '_mv$1');
				}
			}
			if (child.className && child.className != "") {
				if (this.mode == constModeDefaut)
					child.className = child.className.replace(/_mv$/, '');
				else
					child.className = child.className.replace(/$/, '_mv');
			}
		}
	}

	this.adaptKeyboard = function() {
		if (this.mode == constModeNonVoyant)
			return;
		var extOpera = '';
		if ($jGda.browser.opera) {
			/* Get the major version number. */
			var version = navigator.appVersion.charAt(0);
			if (version < 9)
				extOpera = '_1s2';
		}

		var img_tc_haut = this.urlStaticBase + '/static/img/vk/tc_haut_sans' + (this.nbc > 4 ? '_' + this.nbc : '') + '.gif';
		var img_tc_fond = this.urlStaticBase + '/static/img/vk/tc_fond.gif';
		if (this.nbc == 4 && this.nbl == 4)
			img_tc_fond = this.urlStaticBase + '/static/img/vk/tc_fond' + extOpera + '.gif';
		var img_tc_haut_mv = this.urlStaticBase + '/static/img/vk/tc_haut_sans' + (this.nbc > 4 ? '_' + this.nbc : '') + '_mv.gif';
		var img_tc_fond_mv = this.urlStaticBase + '/static/img/vk/tc_fond_mv.gif';
		if (this.nbc > 4) {
			if (this.nbl > 4){
				img_tc_fond = this.urlStaticBase + '/static/img/vk/tc_fond' + extOpera + '_5x5.gif';
				img_tc_fond_mv = this.urlStaticBase + '/static/img/vk/tc_fond_5x5_mv.gif';
			} else {
				img_tc_fond = this.urlStaticBase + '/static/img/vk/tc_fond' + extOpera + '_5x4.gif';
				img_tc_fond_mv = this.urlStaticBase + '/static/img/vk/tc_fond_5x4_mv.gif';
			}
		} else if (this.nbl > 4) {
			img_tc_fond = this.urlStaticBase + '/static/img/vk/tc_fond' + extOpera + '_4x5.gif';
			img_tc_fond_mv = this.urlStaticBase + '/static/img/vk/tc_fond_4x5_mv.gif';
		}
		if (this.mode == constModeDefaut) {
			$jGda('#img_tc_haut').attr('src', img_tc_haut);
			$jGda('#tc_fond_img').attr('src', img_tc_fond);
			$jGda('#shapeHautFermer').attr('coords', this.nbc > 4 ? '192,6,241,16' : '167,3,230,16');
			$jGda('#tc_boutons').css('left', 139 + (this.nbc - 4) * 24 + 'px');
			$jGda('#tc_boutons').css('top', 80 + (this.nbl - 4) * 24 + 'px');
			if ($jGda('#tc_haut_switch').size() > 0) {
				$jGda('#tc_haut_switch').css('left', this.switch_button_left + (this.nbc - 4) * 25 + 'px');
				$jGda('#tc_haut_switch').css('top', this.switch_button_top + 'px');
				$jGda('#tc_haut_switch').attr('alt', 'AGRANDIR');
				$jGda('#tc_haut_switch').attr('title', 'AGRANDIR');
			}
		} else {
			$jGda('#img_tc_haut').attr('src', img_tc_haut_mv);
			$jGda('#tc_fond_img').attr('src', img_tc_fond_mv);
			/* The next two lines are needed for Safari. */
			$jGda('#tc_fond_img').removeAttr('height');
			$jGda('#tc_fond_img').removeAttr('width');
			$jGda('#shapeHautFermer').attr('coords', this.nbc > 4 ? '325,4,498,40' : '275,4,448,40');
			$jGda('#tc_boutons').css('left', 245 + (this.nbc - 4) * 48 + 'px');
			$jGda('#tc_boutons').css('top', 115 + (this.nbl - 4) * 48 + 'px');
			if ($jGda('#tc_haut_switch').size() > 0) {
				$jGda('#tc_haut_switch').css('left', 80 + (this.nbc - 4) * 30 + 'px');
				$jGda('#tc_haut_switch').attr('alt', 'REDUIRE');
				$jGda('#tc_haut_switch').attr('title', 'REDUIRE');
			}
		}
		
		var oldMapHautClose = $jGda('#img_tc_haut').attr('usemap');
		var mapHautClose = 'maphaut'+new Date().getTime();
		$jGda('#img_tc_haut').attr('usemap', '#'+mapHautClose);
		$jGda(oldMapHautClose).attr('id', mapHautClose).attr('name', mapHautClose);
	}

	this.generateKeyboardCoords = function() {
		if (this.zones == undefined) {
			this.posx = genererCoordAleatoire(this.offx, this.getWidth(), $jGda(window).width());
			this.posy = genererCoordAleatoire(this.offy, this.getHeight(), $jGda(window).height());
		} else {
			var n = Math.floor(Math.random() * this.zones.length);
			var zone = this.zones[n];
			/* If the width or height aren't defined, or if we are given numbers that
                           exceed the size of the viewport, restrict the zone to the viewport. */
			if (zone.width == undefined || zone.width > $jGda(window).width())
				zone.width = $jGda(window).width();
			if (zone.height == undefined || zone.height > $jGda(window).height())
				zone.height = $jGda(window).height();
			this.posx = genererCoordAleatoire(zone.x, this.getWidth(), zone.width);
			this.posy = genererCoordAleatoire(zone.y, this.getHeight(), zone.height);
		}
	}

	this.updateValue = function(index) {
		var vi = $jGda('#tc_visu_saisie');
		var vc = $jGda('#cvcs_visu_cache');
		var ndata = this.nbl * this.nbc;
		if (this.numKeys < this.pwdlen) {
			vi.val(vi.val() + '*');
			vc.val(vi.val());
			this.codsec += this.keyCodes[this.numKeys * ndata + index] ;
			if (this.numKeys < this.pwdlen - 1)
				this.codsec += ',';
			this.numKeys++;
		}
	}

	this.validate = function(event) {
		$jGda('#'+this.fieldId).val(this.codsec);

		if (!this.inSession && this.timer != null && ((new Date()).getTime() - this.timer)/1000 > constSessionDuration) {
			alert("Attention, vous avez " + unescape("d%E9pass%E9") + " la " +
			    unescape("dur%E9e") + " maximale pour valider votre Code Secret. " +
			    "Merci de recommencer la saisie.");
			this.close(false);
			return;
		}

		if (this.numKeys == 0) {
			alert(this.emptyCodeMsg);
			if (this.mode == constModeNonVoyant) {
				$jGda('#debutClavier').focus();
			}
			return;
		} else if (this.numKeys != this.pwdlen) {
			alert(this.incompleteCodeMsg);
			this.emptyCode();
			return;
		}

		if (this.validatefn) {
			this.state = 'f';
			this.emptyCode();
			this.hide();
			/* If the validation function tries to submit a form, it won't work with IE6 unless
			 * we defer this function for some reason. */
			(function(func, objThis) {
				setTimeout(function() {
					func.call(objThis, objThis);
				}, 1);
			})(this.validatefn, this);
		} else
			alert("Le nom de la fonction de validation n'est pas " + unescape("renseign%E9."));
	}

	this.enableSound = function() {
		if (this.mode == constModeNonVoyant)
			return;

		var res = confirm("Vous avez " + unescape("activ%E9") + " le clavier virtuel sonore.");
		if (!res)
			return;

		if (!haveqt) {
			res = confirm("Le plugin Quicktime n'est pas " + unescape("install%E9") +
			    " sur votre navigateur. Pour le " + unescape("t%E9l%E9charger") +
			    ", choisissez OK (ouverture d'une nouvelle " + unescape("fen%EAtre") + ").");
			if (res)
				window.open("http://www.apple.com/quicktime/download", "_blank", "");
			else
				window.history.go(0);
			return;
		}

		this.hide();

		this.setMode(constModeNonVoyant);
		
		if (!vk.inSession) {
			vk.timer = (new Date()).getTime();
		}

		if (this.crypto != undefined)
			this.buildSoundKeyboard();
		else {
			var onSuccess = (function(func, thisObj) { return function() { func.call(thisObj); } })(this.buildSoundKeyboard, this);
			this.getCryptogram(onSuccess);
		}
	}

	this.buildSoundKeyboard = function() {
		var pluginHTML = '<object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" width="1" height="1" id="sonCode"><param name="src" value="';
		pluginHTML += this.urlImage + this.mode + this.urlParamCrypto + this.crypto;
		pluginHTML += '"><param name="name" value="sonCode"><param name="id" value="sonCode"><param name="autostart" value="false"><param name="type" value="audio/mpeg"><embed width="1" height="1" src="';
		pluginHTML += this.urlImage + this.mode + this.urlParamCrypto + this.crypto;
		pluginHTML += '" name="sonCode" id="sonCode" autostart="false" type="audio/mpeg" enablejavascript="true"></embed></object>';
		var p = $jGda('#pluginQt');
		if (p.size() == 0) {
			p = $jGda(document.createElement('p')).attr('id', 'pluginQt').attr('class', 'noMarge');
			if (this.form.size() == 0) {
				$jGda('#tc_cvcs').after(p);
			} else {
				this.form.after(p);
			}
		}
		p.html(pluginHTML);

		var help = $jGda(document.createElement('a')).attr('href', this.faqNVURI).attr('id', 'lienAideNonVoyant');
		var help_img = $jGda(document.createElement('img')).attr('id', 'aideNonVoyantClav');
		help_img.attr('src', this.urlStaticBase + this.images["trans"]);
		help_img.attr('class', 'noMarge');
		help_img.attr('alt', 'Aide ' + unescape('%E0') + ' la solution accessible nonvoyant nouvelle ' + unescape('fen%EAtre'));
		help_img.attr('title', 'Aide ' + unescape('%E0') + ' la solution accessible nonvoyant nouvelle ' + unescape('fen%EAtre'));
		help.append(help_img);
		p.append(help);

		var a0 = $jGda(document.createElement('a')).attr('href', 'javascript:void(0)').attr('name', 'Ensemble').attr('id', 'Ensemble');
		var img0 = $jGda(document.createElement('img')).attr('src', this.urlStaticBase + this.images["trans"]);
		img0.attr('class', 'noMarge');
		img0.attr('alt', 'Ecoutez l\'ensemble de la suite de chiffres');
		img0.attr('title', 'Ecoutez l\'ensemble de la suite de chiffres');
		img0.attr('id', 'ListenAll');
		a0.append(img0);
		p.append(a0);
		a0.bind('click', playAllSelection);

		for (var i = 0; i <= 9; i++) {
			/* Used to construct the alt attribute. */
			var digitsAlt = ['premier', 'second', 'troisi%E8me', 'quatri%E8me',
				'cinqui%E8me', 'sixi%E8me', 'septi%E8me', 'huiti%E8me',
				'neuvi%E8me', 'dernier'];

			var a = $jGda(document.createElement('a')).attr('href', 'javascript:void(0)');
			if (i == 0)
				a.attr('id', 'debutClavier').attr('name', 'debutClavier');
				
			var img = $jGda(document.createElement('img')).attr('src', this.urlStaticBase + this.images["trans"]);
			img.attr('class', 'noMarge');
			img.attr('alt', unescape(digitsAlt[i]) + ' chiffre du clavier virtuel');
			img.attr('title', unescape(digitsAlt[i]) + ' chiffre du clavier virtuel');
			a.append(img);
			p.append(a);
			a.bind('focus', (function(func, arg) { return function() { func(arg); } })(armPlayNV, i));
			a.bind('blur', disarmPlayNV);
			a.bind('click', {thisObj:this, iArg:i}, function(event) {
				event.data.thisObj.keyPressedNV(event, event.data.iArg);
			});
		}

		/* Add the 'corriger' link. */
		var correct = $jGda(document.createElement('a')).attr('href', 'javascript:void(0)').attr('name', 'corriger').attr('id', 'corriger');
		var cimg = $jGda(document.createElement('img')).attr('src', this.urlStaticBase + this.images["trans"]);
		cimg.attr('class', 'noMarge');
		cimg.attr('alt', 'Corriger le code secret');
		cimg.attr('title', 'Corriger le code secret');
		correct.append(cimg);
		p.append(correct);
		correct.bind('click', {thisObj: this}, function(event) {
			event.data.thisObj.emptyCode();
		});

		/* Add the 'valider' link. */
		var submit = $jGda(document.createElement('a')).attr('href', 'javascript:void(42)');
		var simg = $jGda(document.createElement('img')).attr('src', this.urlStaticBase + this.images["trans"]);
		simg.attr('class', 'noMarge');
		simg.attr('name', 'validation');
		simg.attr('id', 'validation');
		simg.attr('alt', 'Valider le code secret');
		simg.attr('title', 'Valider le code secret');
		submit.append(simg);
		p.append(submit);
		submit.bind('click', {thisObj: this}, function(event) {
			event.data.thisObj.validate(event);
		});

		help.focus();
	}

	this.keyPressedNV = function(event, codeInterrupt) {
		if (this.mode == constModeNonVoyant && this.numKeys >= this.pwdlen) {
			alert(this.incompleteCodeMsg);
			this.emptyCode();
			$jGda('#lienAideNonVoyant').focus();
			return;
		}

		this.codsec += this.keyCodes[this.numKeys * 10 + codeInterrupt] ;
		if (this.numKeys < this.pwdlen - 1)
			this.codsec += ',';
		this.numKeys++;
		if (this.numKeys == this.pwdlen && $jGda('#corriger'))
			$jGda('#corriger').focus();
		newPlayNV(codeInterrupt);
	}

	this.showZone = function() {
		var zone_div = $jGda('#vk_debug_zone');
		if (zone_div.size() == 0) {
			zone_div = $jGda(document.createElement('div')).attr('id', 'vk_debug_zone');
			zone_div.css('position', 'absolute');
			zone_div.css('border', '1px solid red');
			zone_div.css('left', this.zones[0].x + 'px');
			zone_div.css('top', this.zones[0].y + 'px');
			zone_div.css('height', this.zones[0].height + 'px');
			zone_div.css('width', this.zones[0].width + 'px');
			$jGda('body').append(zone_div);
		}
		zone_div.show();
	}

	this.hideZone = function() {
		var zone_div = $jGda('#vk_debug_zone');
		if (zone_div.size() > 0)
			zone_div.hide();
	}
	
};

function VirtualKeyboard2(inSession, codeFieldId, options) {
	VirtualKeyboard.call(this, null, null, inSession, codeFieldId, options);
};

var kbdHTML =  '<div id="tc_cvcs" class="clcvcs">\n' +
  '<div id="tc_divdeplace" style="position:absolute; left:-10000px; top:-10000px;" class="tc_fond">\n' +
//	'<a id="tc_enablesound" alt="Activer le clavier sonore" href="javascript:void(0);"></a>\n' +
    '<map name="maphaut" id="maphaut" style="display:block">\n' +
      '<area id="shapeHautFermer" shape="rect" style="cursor:pointer;display:block" coords="167,6,230,16" alt="FERMER" title="FERMER">\n' +
    '</map>\n' +
    '<img id="img_tc_haut" usemap="#maphaut">\n' +
    '<img id="tc_fond_img" class="tc_fond_img">\n' +
    '<div id="tc_text" class="text_div">\n' +
      '<div id="tc_votre_code" class="code_div">\n' +
        '<img id="legendeInput" alt="VOTRE CODE SECRET" title="VOTRE CODE SECRET">\n' +
        '<div id="tc_pass" class="password_div">\n' +
          '<input type="text" id="tc_visu_saisie" value="" readonly size="6">\n' +
          '<a id="tc_aidelien" style="display: none">\n' +
            '<img id="tc_aideimg">\n' +
          '</a>\n' +
        '</div>\n' +
      '</div>\n' +
    '</div>\n' +
    '<map id="tc_tclavier" name="tc_tclavier" style="display:block"></map>\n' +
    '<img id="img_clavier" class="keyboard" usemap="#tc_tclavier">\n' +
    '<div id="tc_boutons" class="buttons">\n' +
      '<img id="tc_corriger" class="correct_img" style="cursor:pointer" alt="CORRIGER" title="CORRIGER"><br>\n' +
      '<img id="tc_valider" class="valid_img" style="cursor:pointer" alt="VALIDER" title="VALIDER">\n' +
    '</div>\n' +
    '<img id="surlignage" style="cursor:pointer; position:absolute; left:-10000px; top:-10000px;">\n' +
  '</div>\n' +
'</div>\n';

function VirtualKeyboardHTML(elemId, formId, inSession, codeFieldId, options) {
	if ($jGda('#vk_layer').size() == 0) {
		$jGda('body').append(this.vk_layer = $jGda('<div id="vk_layer" class="vk_layer"></div>'));
		this.iframePourIE = $jGda('<iframe style="width:100%; height: 100%; filter:alpha(opacity=0); border: 0; pointer-events: none;" />');
		this.iframePourIE.attr("src",(typeof(gda_static_base_uri) == "string" ? gda_static_base_uri : '')+"/static/img/vide.gif");
		this.vk_layer.append(this.iframePourIE);
		this.vk_move = $jGda(document.createElement('div')).attr('id', 'vk_move').attr('class', 'vk_move');
		this.vk_layer.append(this.vk_move);
		this.vk_layer.hide();
	} else if (this.vk_layer == null) {
		this.vk_layer = $jGda('#vk_layer');
		this.vk_move = $jGda('#vk_move');

	}
	var elem = $jGda('#'+elemId);
	elem.html(kbdHTML);
	if ($jGda('#cryptocvcs').size() == 0) {
		var inputCVCS = $jGda(document.createElement('input')).attr('type', 'hidden').attr('name', 'cryptocvcs').attr('id', 'cryptocvcs').attr('value', '');
		$jGda('#'+formId).append(inputCVCS);
		var inputCVCSCache = $jGda(document.createElement('input')).attr('type', 'hidden').attr('id', 'cvcs_visu_cache').attr('value', '');
		$jGda('#'+formId).append(inputCVCSCache);
	}
	this.form = $jGda('#'+formId);
	VirtualKeyboard2.call(this, inSession, codeFieldId, options);
	
	gda_css_include(this.urlCSS);
};


function VirtualKeyboardHTML2(elemId, formId, inSession, codeFieldId, options) {
	VirtualKeyboardHTML.call(this, elemId, formId, inSession, codeFieldId, options);
};

function dragStart(event) {
	if (event.which == 1) {
		/* Get cursor position with respect to the page. */
		dragObj.cursorStartX = event.pageX;
		dragObj.cursorStartY = event.pageY;

		/* Save starting positions of element. */
		dragObj.elStartLeft  = parseInt(dragObj.elNode.css('left'));
		dragObj.elStartTop   = parseInt(dragObj.elNode.css('top'));

		if (isNaN(dragObj.elStartLeft)) dragObj.elStartLeft = 0;
		if (isNaN(dragObj.elStartTop))  dragObj.elStartTop  = 0;

		/* Capture mousemove and mouseup events on the page. */
		if (this.vk_move == null) {
			$jGda(document).bind('mousemove', dragGo);
			$jGda(document).bind('mouseup', dragStop);
		} else {
			this.vk_move.bind('mousemove', dragGo);
			this.vk_move.bind('mouseup', dragStop);
		}
		event.preventDefault();
	}
}

function dragGo(event) {
	var x, y;

	/* Get cursor position with respect to the page. */
	x = event.pageX;
	y = event.pageY;

	/* Move drag element by the same amount the cursor has moved. */
	if (x > 0 && y > 0) {
		px = dragObj.elStartLeft + x - dragObj.cursorStartX;
		py = dragObj.elStartTop  + y - dragObj.cursorStartY;

		if (px > 0) dragObj.elNode.css('left', px + 'px');
		if (py > 0) dragObj.elNode.css('top', py + 'px');
	}

	event.preventDefault();
}

function dragStop(event) {
	/* Stop capturing mousemove and mouseup events. */
	$jGda(document).unbind('mousemove');
	$jGda(document).unbind('mouseup');
}

function activerSurlignage(event) {
	var data = Array.prototype.slice.call(arguments);
	data.shift();
	var x = data.shift();
	var y = data.shift();

	this.curKey = event.currentTarget;

	var surlignage = $jGda('#surlignage');
	var imageClavierOffSet = $jGda('#img_clavier').position();
	
	if (this.mode == constModeDefaut) {
		surlignage.css('top', ((y * 23) + imageClavierOffSet.top - 23 + 1) + 'px');
	//	surlignage.style.top = ((y * 23) + 53) + 'px';
		surlignage.css('left', ((x * 24) + imageClavierOffSet.left - 24 + 1 ) + 'px');
	//	surlignage.style.left = ((x * 24) - 3) + 'px';
	} else {
		surlignage.css('top', ((y * 46) + imageClavierOffSet.top - 46 + 1 ) + 'px');
	//	surlignage.style.top = ((y * 46) + 122) + 'px';
		surlignage.css('left', ((x * 48) + imageClavierOffSet.left -48 + 1) + 'px');
	//	surlignage.style.left = ((x * 48) - 24) + 'px';
	}
	numCurX = x;
	numCurY = y;
}

function desactiverSurlignage(event) {
	var surlignage = $jGda('#surlignage');
	var off = surlignage.offset();
	var posx = event.pageX;
	var posy = event.pageY;
	if (!(posx > off.left && posx < off.left + surlignage[0].offsetWidth)
	    || !(posy > off.top && posy < off.top + surlignage[0].offsetHeight)) {
		surlignage.css('left', '-5000px');
		surlignage.css('top', '-5000px');
	}
	/* XXX duplicated with this.onKeyUnclicked() */
	if (this.mode == constModeDefaut)
		surlignage.src = this.urlStaticBase + '/static/img/vk/tc_touche_cache_hover.gif';
	else
		surlignage.src = this.urlStaticBase + '/static/img/vk/tc_touche_cache_hover_mv.gif';
}

function genererCoordAleatoire(coordOrigine, tailleClavier, tailleTotale)
{
	return Math.round(Math.random() * (tailleTotale - tailleClavier)) + coordOrigine;
}

function playAllSelection() {
	//var sonCode = $('pluginQt').down('object');
	var sonCode = document.sonCode;
	if (sonCode) {
		sonCode.Rewind();
		sonCode.SetStartTime(0);
		sonCode.SetEndTime(10 * 666 - 50);
		sonCode.Play();
	}
}

function playSelection(digitIndex) {
	//var sonCode = $('pluginQt').down('object');
	var sonCode = document.sonCode;
	if (sonCode) {
		sonCode.Rewind();
		sonCode.SetStartTime(0);
		sonCode.SetEndTime((digitIndex + 1) * 666 - 50);
		sonCode.SetStartTime(digitIndex * 666);
		sonCode.Play();
	}
}

function armPlayNV(index) {
	disarmPlayNV();
	playTimeout = (function(func, param) {
			return setTimeout(function() {
				func(param);
			}, 2000);
		})(newPlayNV, index);
}

function newPlayNV(index) {
	playSelection(index);
	playTimeout = null;
}

function disarmPlayNV() {
	if (playTimeout) {
		window.clearTimeout(playTimeout);
		playTimeout = null;
	}
}

/*
 * Backported utility functions from LGN
 */

function __getCookie(name)
{
	var rg = new RegExp(name+"=([^;]*)");
	var r = rg.exec(document.cookie);
	return (r ? unescape(r[1]) : null);
}

/* No support for expire/domain/path/secure */
function __setCookie(name,value)
{
	document.cookie = name + "=" + escape(value)+"; path=/";	
}

/*
 * gda_vk.js
 *
 * VK implementation of gda_ functions
 */

gda_js_include('/static/js/vk/virtualKeyboard.js');
gda_js_include('/static/js/vk/config.js');

var vk;
var gda_vk_div = 'gda_vk';
function GDAVK() {
	return {
		/* Authentification */
		auth: function(form_id) {
			GDAVK.spawn(form_id, gda_vk_div, 'auth', true);
		},
		
		/* Authentification JSON */
		auth_json: function(params) {
			GDAVK.spawn_json(gda_vk_div, 'auth', params, true);
		},

		/* Operation signing */
		sign: function(form_id) {
			GDAVK.spawn(form_id, gda_vk_div, 'sign', false);
		},
		
		/* Operation signing JSON */
		sign_json: function(params) {
			GDAVK.spawn_json(gda_vk_div, 'sign', params, false);
		},
		
		close: function() {
			GDAVK.close();
		},
		
		setopts: function(options) {
			GDAVK.setopts(options);
		}
	}
};

GDAVK.getopts = function(validate_fn, allow_rebase, custom_opts)
{
	var opts = typeof(gda_vk_config) == "object" ? clone_object(gda_vk_config) : {};
	update_object(opts, {validatefn: validate_fn, 
						urlBase: allow_rebase && (typeof(gda_base_uri) == "string") ? gda_base_uri : '',
						urlStaticBase: typeof(gda_static_base_uri) == "string" ? gda_static_base_uri : '' });

	if (typeof(custom_opts) == "object")
		update_object(opts, custom_opts);

	var localUrlBase = opts.urlBase;
	var localurlStaticBase = opts.urlStaticBase;
	if (!localurlStaticBase && localUrlBase) {
		update_object(opts, {validatefn: validate_fn, urlStaticBase: localUrlBase });
	}

	return opts;
}

GDAVK.setopts = function(options)
{
	this.options = options;
}

GDAVK.spawn = function(form_id, vk_id, vk_op, allow_rebase)
{
	/* Insert keyboard <div> and hidden <input> */
	if (document.getElementById(vk_id) == null) {
		$jGda('body').append($jGda(document.createElement('div')).attr('id', vk_id));
		var input = $jGda(document.createElement('input')).attr('type', 'hidden').attr('name', 'codsec').attr('id', 'codsec').attr('value', '');
		$jGda(document.getElementById(form_id)).append(input);
	}
		
	/* Define validation function */
	function gda_vk_validate(kbd) {
		var form = $jGda(document.getElementById(form_id));
		if (document.getElementById('vk_op') == null) {
			var input = $jGda(document.createElement('input')).attr('type', 'hidden').attr('name', 'vk_op').attr('id', 'vk_op').attr('value', vk_op);
			form.append(input);
		}
		form[0].submit();
	}
	
	/* Create virtual keyboard */
	vk = new VirtualKeyboardHTML2(vk_id, form_id, false, 'codsec', GDAVK.getopts(gda_vk_validate, allow_rebase, this.options));

	/* Show the virtual keyboard */
	vk.show();
}

GDAVK.spawn_json = function(vk_id, vk_op, params, allow_rebase)
{
	/* Insert keyboard <div> and hidden <input> */
	if ($jGda('#' + vk_id) == null || $jGda('#' + vk_id).length == 0) {
		var divVk = $jGda('<div />').attr('id', vk_id);
		$jGda('body').append(divVk);
		divVk.append($jGda('<input />').attr('type', 'hidden').attr('name', 'codsec').attr('id', 'codsec'));
		var divVkInner = $jGda('<div />').attr('id', vk_id + '_inner');  
		divVk.append(divVkInner);
		var formVk = $jGda('<form />').attr('id', vk_id + '_formVK');
		divVk.append(formVk);
	}
		
	/* Define validation function */
	function gda_vk_validate() {
		var secureParams = {
				'cryptocvcs': vkJSON.crypto,
				'codsec' : $jGda("#codsec").val(),
				'vk_op' : vk_op
		};
		
		if (vk_op = 'auth') {
			secureParams['user_id'] = params['idUser'];
		}
		
		params.success(secureParams);
	}
	
   /* Create virtual keyboard */
	vkJSON = new VirtualKeyboardHTML(
				vk_id + '_inner', 
				vk_id + '_formVK',
				false, 
				'codsec', 
				GDAVK.getopts(gda_vk_validate, allow_rebase));

   /* Show the virtual keyboard */
   vkJSON.show();
}

GDAVK.close = function()
{
	if (vk) vk.close();
}

/* 
 * Code change
 */

var gda_vk_chcode_errors = {
    ERROR_STEP2_CODE_SHOULD_BE_NEW: 1,
    ERROR_STEP3_CODE_CONFIRMATION_FAILED: 2
}


/* Internal stuff */
var gda_vk_chcode_step;
var gda_vk_chcode_step_handler;
var gda_vk_chcode_error_handler;
var gda_vk_chcode_form;
		
function gda_vk_chcode_validate(vk)
{
	switch (gda_vk_chcode_step) {
		case 1:
			$jGda('#codsec_old').val($jGda('#codsec').val());
			break;

		case 2:
			$jGda('#codsec_new').val($jGda('#codsec').val());
			if ($jGda('#codsec_new').val() == $jGda('#codsec_old').val() && typeof(gda_vk_chcode_error_handler) == 'function')  {
                        	gda_vk_chcode_error_handler(gda_vk_chcode_errors.ERROR_STEP2_CODE_SHOULD_BE_NEW);
				vk.emptyFields()
				vk.show();
				return;
			}
			break;

		case 3:
			if ($jGda('#codsec').val() != $jGda('#codsec_new').val()) {
				if (typeof(gda_vk_chcode_error_handler) == 'function')
					gda_vk_chcode_error_handler(gda_vk_chcode_errors.ERROR_STEP3_CODE_CONFIRMATION_FAILED);
					//gda_vk_chcode_error_handler();
				else
					alert('VK new code confirm mismatch!');
				vk.emptyFields()
				vk.show();
				//vk.hide();
				return;
			}
			break;

		default:
			alert('Internal error: unknown step ' + gda_vk_chcode_step);
			return;
	}

	/* Clear VK input buffer */
	$jGda('#codsec').val('');
	/* Call step handler, if any */
	if (typeof(gda_vk_chcode_step_handler) == 'function')
		gda_vk_chcode_step_handler();

	/* Step 1 or 2 => advance 1 step, reset VK */
	if (gda_vk_chcode_step == 1 || gda_vk_chcode_step == 2) {
		gda_vk_chcode_step++;
		vk.emptyFields()
		vk.show();
	}

	/* Step 3 => submit */
	else if (gda_vk_chcode_step == 3)
		document.forms[gda_vk_chcode_form].submit();	
}


/* API */
function gda_vk_chcode_init(form_id)
{
	/* Insert keyboard and hidden tags if needed */
	if ($jGda('#gda_chcode_vk').size() == 0) {
		$jGda('body').append($jGda(document.createElement('div')).attr('id', 'gda_chcode_vk'));
		var form = $jGda(document.getElementById(form_id));
		var inputCodsec = $jGda(document.createElement('input')).attr('type', 'hidden').attr('name', 'codsec').attr('id', 'codsec').attr('value', '');
		var inputCodsecOld = $jGda(document.createElement('input')).attr('type', 'hidden').attr('name', 'codsec_old').attr('id', 'codsec_old').attr('value', '');
		var inputCodsecNew = $jGda(document.createElement('input')).attr('type', 'hidden').attr('name', 'codsec_new').attr('id', 'codsec_new').attr('value', '');
		var inputVKOP = $jGda(document.createElement('input')).attr('type', 'hidden').attr('name', 'vk_op').attr('id', 'vk_op').attr('value', 'chcode');
		form.append(inputCodsec);
		form.append(inputCodsecOld);
		form.append(inputCodsecNew);
		form.append(inputVKOP);
		
		gda_vk_chcode_form = form_id;
		gda_vk_chcode_step_handler = null;
		gda_vk_chcode_error_handler = null;

		vk = new VirtualKeyboardHTML2('gda_chcode_vk', form_id, false, 'codsec', GDAVK.getopts(gda_vk_chcode_validate, false, this.options));
	}
	else {
		$jGda('#codsec').val('');
		$jGda('#codsec_old').val('');
		$jGda('#codsec_new').val('');
		vk.reset('codsec');
	}

	gda_vk_chcode_step = 0;
}

function gda_vk_chcode_start()
{
	gda_vk_chcode_step = 1;
	vk.show();
}


function gda_vk_chcode_set_step_handler(func)
{
	gda_vk_chcode_step_handler = func;
}

function gda_vk_chcode_set_error_handler(func)
{
	gda_vk_chcode_error_handler = func;
}


function gda_vk_suspend(form_id)
{
	GDAVK.spawn(form_id, gda_vk_div, 'susp', false);
}


function update_object(objDest, objSrc)
{
	for (var property in objSrc) {
		objDest[property] = objSrc[property];
	}
	return objDest;
}


function clone_object(obj)
{
	if(typeof(obj) != 'object' || obj == null) {
		return obj;
	}
	
	var newObj = obj.constructor();
	for (var property in obj) {
		newObj[property] = clone_object(obj[property]);
	}
	
	return newObj;
}

/* Register with SAS */
gda_modules['vk'] = new GDAVK();

/*
 * gda_csa.js
 *
 * VK implementation of gda_ functions
 */

gda_js_include('/static/js/csa/csa_dialog.js');

function GDACSA() {
	
	this.libSaisieCsa1Sms = 'Un Code S&eacute;curit&eacute; vous a &eacute;t&eacute; transmis par SMS &agrave; HHhMMmSSs.';
	this.libSaisieCsa1Svi = 'Un Code S&eacute;curit&eacute; vous a &eacute;t&eacute; transmis par SVI &agrave; HHhMMmSSs.';
	this.libSaisieCsa2Sms = 'Si vous ne le recevez pas dans un d&eacute;lai de 2 minutes, nous vous remercions de v&eacute;rifier le num&eacute;ro de mobile enregistr&eacute; aupr&egrave;s de votre Conseiller de client&egrave;le.';
	this.libSaisieCsa2Svi = 'Si vous ne le recevez pas dans un d&eacute;lai de 2 minutes, nous vous remercions de v&eacute;rifier le num&eacute;ro de t&eacute;l&eacute;phone enregistr&eacute; aupr&egrave;s de votre Conseiller de client&egrave;le.';
	this.libSaisieCsa3 = 'Valider la saisie';

	this.libEnvoiCsa1Sms = 'Cette op&eacute;ration doit &ecirc;tre valid&eacute;e par un Code S&eacute;curit&eacute;.';
	this.libEnvoiCsa1Svi = 'Cette op&eacute;ration doit &ecirc;tre valid&eacute;e par un Code S&eacute;curit&eacute;.';
	this.libEnvoiCsa2Sms = 'Le code s&eacute;curit&eacute; est envoy&eacute; par SMS sur votre t&eacute;l&eacute;phone<br/>Sa dur&eacute;e de validit&eacute; est de 20 minutes.';
	this.libEnvoiCsa2Svi = 'Le code s&eacute;curit&eacute; est envoy&eacute; par SVI sur votre t&eacute;l&eacute;phone<br/>Sa dur&eacute;e de validit&eacute; est de 20 minutes.';
	this.libEnvoiCsa3 = 'Envoyer maintenant';

	this.libErrDerog1 = 'Cette op&eacute;ration doit &ecirc;tre valid&eacute;e par un Code S&eacute;curit&eacute; mais aucun num&eacute;ro de t&eacute;l&eacute;phone mobile n\'est enregistr&eacute;.';
	this.libErrDerog2 = 'Un num&eacute;ro de t&eacute;l&eacute;phone mobile doit &ecirc;tre pr&eacute;alablement enregistr&eacute; pour recevoir un Code S&eacute;curit&eacute;. Nous vous remercions de bien vouloir contacter votre Conseiller de client&egrave;le.';

	this.libErrUnregistred1 = 'Cette op&eacute;ration doit &ecirc;tre valid&eacute;e par un Code S&eacute;curit&eacute; mais aucun num&eacute;ro de t&eacute;l&eacute;phone mobile n\'est enregistr&eacute;.';
	this.libErrUnregistred2 = 'Un num&eacute;ro de t&eacute;l&eacute;phone mobile doit &ecirc;tre pr&eacute;alablement enregistr&eacute; pour recevoir un Code S&eacute;curit&eacute;. Nous vous remercions de bien vouloir contacter votre Conseiller de client&egrave;le.';

	this.libErrAlreadySent1 = 'Un code secret vous a d&eacute;j&agrave; &eacute;t&eacute; envoy&eacute; r&eacute;cement. Veuillez r&eacute;utiliser ce mot de passe.';
	this.libErrAlreadySent2 = 'Pour saisir ce dernier, cliquez sur <span style="font-weight: bold;">Continuer</span>';

	this.libErrEnvoiCsa1 = 'Une erreur est survenue.';
	this.libErrEnvoiCsa2 = null;

	this.libErrBadCode1 = 'Vous avez saisi l\'mauvais mot de passe';
	this.libErrBadCode2 = 'Pour effectuer une nouvelle tentative, cliquez sur <span style="font-weight: bold;">Retour</span>.';

	this.libErrSaisieCode1 = 'Le champ Code s&eacute;curit&eacute; n\'est pas renseign&eacute; ou contient des caract&rgrave;res interdits.';
	this.libErrSaisieCode2 = null;

	this.libErrTooMany1 = 'Vous avez saisi trop de mots de passe erron&eacute;s, votre session est maintenant d&eacute;sactiv&eacute;e';
	this.libErrTooMany2 = 'Pour revenir sur la page d\'accueil, cliquez sur <span style="font-weight: bold;">Retour</span>.';

	this.libErrExpired1 = 'Votre CSA est expir&eacute;';
	this.libErrExpired2 = 'Pour recommencer la proc&eacute;dure, cliquez sur <span style="font-weight: bold;">Retour</span>.';

	this.libErrVerifCsa1 = 'Une erreur est survenue, ou vous n\'avez pas saisi le bon code secret';
	this.libErrVerifCsa2 = 'Vous pouvez r&eacute;essayer de saisir un autre code ou fermer cette fen&ecirc;tre.';
	
	this.urlAide = '/aide-csa.html';

	/* Authentification */
	this.auth = function(form_id) {
		/* Initialisation de la saisie du CSA en mode compatibilite */
		alert('Not yet implemented');
	}

	this.chg_level = function(level, form_id, derog) {
		this.level = level;
		this.form_id = form_id;
		this.derog = derog;
		
		dialog = new CsaDialog(this.urlAide);
	    dialog.showLayer();

		/* changement de niveau gere par CSA */
		/* declenchement de la proc d'auth */
		// L'utilisateur n'a pas enregistre de telephone
		if (!this.registered) {
			if (derog) {
				// Affichage mode derogatoire
				// Message d'erreur : pas de telephone, mais derogation
				dialog.showErreur(this.libErrDerog1, this.libErrDerog2, 
					'Continuer',
					'javascript:csa_module.csaDerog();'
					); 

			} else {
				// Affichage impossibilite de continuer
				// Message d'erreur : pas de telephone, pas de derogation
				dialog.showErreur(this.libErrUnregistred1,this.libErrUnregistred2,
						null,
						'javascript:csa_module.csaDismiss()'
						);
			}
		} else {
			// Affichage premier calque de saisie CSA
			if (this.already_sent) {
				this.saisieCsa();
			} else {
				this.demanderCsa();
			}
		}
		
	}

	this.chg_level_set_callback = function(func) {
		this.chg_level_callback = func;
	}

	this.demanderCsa = function() {
        	dialog.showEnvoiCsa(
				(this.method == 'SMS'?this.libEnvoiCsa1Sms:this.libEnvoiCsa1Svi), 
				(this.method == 'SMS'?this.libEnvoiCsa2Sms:this.libEnvoiCsa2Svi), 
				this.libEnvoiCsa3, 
				'javascript:csa_module.envoyerCsa()'
				);
	}


	this.envoyerCsa = function() {
		// Appel de la fonction d'envoi CSA, interpretation du retour et affichage du calque d'envoi ou du calque d'erreur	
        var sUrl = '/sec/csa/send';
   		var csaCallback = call_json('GET', sUrl);
        if (!csaCallback || csaCallback.status != 'OK') {
			if (csaCallback.reason == 'alreadySent') {
    	        dialog.showErreur(this.libErrAlreadySent1, this.libErrAlreadySent2, 
					'Continuer', 
					'javascript:csa_module.saisieCsa()'
					);
				
			} else if (csaCallback.reason == 'noTrans') {
				this.csaDismiss();
				gda_showErreurNoTrans();
				
			} else {
    	        dialog.showErreur(this.libErrEnvoiCsa1, this.libErrEnvoiCsa2,
					'Fermer', 
					'javascript:csa_module.csaDismiss()'
					);
			}
    	} else {
			this.heureEnvoiCSA = csaCallback.sent_date_hh+'h'+csaCallback.sent_date_mm+'m'+csaCallback.sent_date_ss;
			this.saisieCsa();
		}	
	}

	this.saisieCsa = function() {

			if (this.method =='SMS') {
				dialog.showSaisieCsa(this.libSaisieCsa1Sms.replace(/HHhMMmSSs/,this.heureEnvoiCSA) , 
					this.libSaisieCsa2Sms, 
					this.libSaisieCsa3,
					'javascript:csa_module.verifierCsa()'
					);
			} else {
				dialog.showSaisieCsa(this.libSaisieCsa1Svi.replace(/HHhMMmSSs/,this.heureEnvoiCSA) , 
					this.libSaisieCsa2Svi, 
					this.libSaisieCsa3,
					'javascript:csa_module.verifierCsa()'
					);
			}
				  
	}

	this.verifierCsa = function() {
		var csa = dialog.getCsa();
       	if (!csa.match(new RegExp("^[0-9A-Za-z]{1,6}$"))) {
	        dialog.showErreur(this.libErrSaisieCode1, this.libErrSaisieCode2, 
				'Retour', 
				'javascript:csa_module.saisieCsa()'
				);
			return;
       	}

		var sUrl = '/sec/csa/check?code=' + csa;
		var csaCallback = call_json('GET',sUrl);
		if (!csaCallback || csaCallback.status != 'OK') {

			if (csaCallback.reason == 'badCode') {
				// Message d'erreur : mauvais mot de passe
  				dialog.showErreur(this.libErrBadCode1, this.libErrBadCode2, 
					'Retour', 
					'javascript:csa_module.saisieCsa()'
				);

			} else if (csaCallback.reason == 'tooMany') {
				/* TODO: message d'erreur non conforme aux specification... A modifier. */
  				dialog.showErreur(this.libErrTooMany1, this.libErrTooMany2, 
					'Retour', 
					'/'
				);

			} else if (csaCallback.reason == 'expired') {
  				dialog.showErreur(this.libErrExpired1, this.libErrExpired2, 
					'Retour', 
					'javascript:csa_module.demanderCsa()'
				);

			}  else if (csaCallback.reason == 'noTrans') {
				this.csaDismiss();
				gda_showErreurNoTrans();
				
			} else {
				dialog.showErreur(this.libErrVerifCsa1,this.libErrVerifCsa2,
					'R&eacute;essayer', 
					'javascript:csa_module.saisieCsa()'
					);
			}
			
			
            return;
		}

		this.csaDismiss();
		gda_chg_level(this.level, this.form_id, this.derog);

	}

	this.csaDismiss = function() {
		dialog.closeDialog();
		dialog.hideLayer();
	}

	this.csaDerog = function() {
		if (this.form_id != null) {
			var input = $jCsa(document.createElement('input')).attr('type', 'hidden').attr('name', 'derog').attr('id', 'derog').attr('value', 'O');
			$jCsa(document.getElementById(this.form_id)).append(input);
		}
		this.csaDismiss();
		gda_chg_level_submit(this.form_id);
	}

	this.init = function(auth_infos) {
		this.registered = (auth_infos != null && auth_infos.registered != null ? (auth_infos.registered=="true") : false);
		this.tempo_end = (auth_infos != null && auth_infos.tempo_end != null ? auth_infos.tempo_end : null);
		this.tempo = (auth_infos != null && auth_infos.tempo != null ? (auth_infos.tempo=="true") : false);
		this.method = (auth_infos != null && auth_infos.method != null ? auth_infos.method : null);
		this.retry_delay = (auth_infos != null && auth_infos.retry_delay != null ? auth_infos.retry_delay : null);
		this.callstatus_available = (auth_infos != null && auth_infos.callstatus_available != null ? auth_infos.callstatus_available : false);
		this.already_sent = (auth_infos != null && auth_infos.already_sent != null ? (auth_infos.already_sent=="true") : false);
		if (this.already_sent) {
				this.heureEnvoiCSA = auth_infos.sent_date_hh+'h'+auth_infos.sent_date_mm+'m'+auth_infos.sent_date_ss;
		}

		/* Ajout options specifiques chantier */
		try {
			var gda_csa_config = call_json("GET",gda_js_getURL("/static/js/csa/config.json"));
			if (gda_csa_config != undefined) {
				$jCsa.extend(this, gda_csa_config);
			}
		} catch(ex) {
//			alert(ex);
		}


	}
	
}


var csa_module =  new GDACSA();
/* Register with SAS */
gda_modules['csa'] = csa_module;

//gda_js_include('/static/js/csa/jquery-1.3.2.min.js');

var urlCSS = (typeof(gda_static_base_uri) == "string" ? gda_static_base_uri : '')+'/static/styles/csa/csa.css';

var csaDialogUrlAide = '';

var CsaDialog = function(argUrlAide) {
	
		this.urlAide = argUrlAide;
		csaDialogUrlAide = this.urlAide;

        this.isIE6 = !$jCsa.support.opacity && !window.XMLHttpRequest;

        // Chargement des CSS
		gda_css_include(urlCSS);

        $jCsa('body').append(
                this.overlay = $jCsa('<div class="csa_overlay"></div>')
        );
		
		this.iframePourIE = $jGda('<iframe style="width:100%; height: 100%; filter:alpha(opacity=0); border: 0; pointer-events: none;" />');
		this.iframePourIE.attr("src",(typeof(gda_static_base_uri) == "string" ? gda_static_base_uri : '')+"/static/img/vide.gif");
		this.overlay.append(this.iframePourIE);
		
        $jCsa('body').append(
                this.dialogDiv = $jCsa('<div id="csa_dialog" style="visibility:hidden;position:absolute;width:730px;height:320px;top:-5000px;left:-5000px"></div>')
        );

        this.overlay.css({
                'background-color'      : '#666',
                'opacity'                       : 0.7
                });

        this.overlay.hide();

        this.showLayer = function() {
                this.overlay.fadeIn('300');
        };

        this.hideLayer = function() {
                this.closeDialog();
                this.overlay.fadeOut('300');
        };

        this.centerDialog = function (element) {

                var windowHeight = $jCsa(window).height();
                var windowWidth = $jCsa(window).width();
                var elementHeight = $jCsa(element).height();
                var elementWidth = $jCsa(element).width();
                
                var elementTop, elementLeft;
					//alert("winwow = " + windowWidth + "x" + windowHeight + ", element = " + elementWidth + "x" + elementHeight);

                if (windowHeight <= elementHeight) {
                        elementTop = $jCsa(window).scrollTop();
                } else {
                        elementTop = ((windowHeight - elementHeight) / 2) + $jCsa(window).scrollTop();
                }
                if (windowWidth <= elementWidth) {
                        elementLeft = $jCsa(window).scrollLeft();
                } else {
                        elementLeft = ((windowWidth - elementWidth) / 2) + $jCsa(window).scrollLeft();
                }

                this.dialogDiv.css({
                        'top': elementTop,
                        'left': elementLeft
                        });
        };



        this.showDialog = function(html) {
                this.closeDialog();
                this.dialogDiv.html(html);
		          this.dialogDiv.css({'visibility':'visible'});

                this.centerDialog('#csa_dialog');

                $jCsa('.csa_entete_fermer').click(function() {
                        csa_module.csaDismiss();
                        });

                $jCsa('.csa_entete_fermer').attr('title', 'Fermer');

                $jCsa('.csa_entete_aide').click(function() {
                        window.open(csaDialogUrlAide);
                        });
                $jCsa('.csa_entete_aide').attr('title', 'Aide (nouvelle fen&ecirc;tre)');

        };

        this.closeDialog = function() {
                this.dialogDiv.css({'visibility':'hidden'});
                this.dialogDiv.html('');
                };

		this.showEnvoiCsa = function(message1, message2, actionLabel, actionUrl) {
									var htmlEnvoiCsa =
										'<div class="csa_mobile_1">' +
										'  	    <div class="csa_entete"> ' +
										'    		<div class="csa_entete_barre"><span>Fermer la fen&ecirc;tre de connexion</span></div>' +
										'    		<div class="csa_entete_fermer"><span>Fermer la fen&ecirc;tre de connexion</span></div>' +
											'		<div class="csa_entete_aide"><span>Aide (nouvelle fen&ecirc;tre)</span></div>' +
										' 	   </div>' +
										'       <div class="csa_conteneur_ombre">' +
										'  	     <div class="csa_conteneur">' +
										'		 	     <div class="csa_onglet_conteneur">' +
										'		 	     <div class="csa_onglet  csa_onglet_actif">' +
										'					<div class="csa_onglet_etape_1"></div>' +
										'						Envoi du Code S&eacute;curit&eacute; </div>' +
										'				 <div class="csa_onglet ">' +
										'<div class="csa_onglet_etape_2 "></div>' +
										'				 Saisie du Code S&eacute;curit&eacute; </div>' +
										'			</div>' +
										'		 <div class="csa_titre_rouge">' + message1 + '</div>' +
										'		 <div class="csa_details">' + message2 + '</div>' +
										'		 <div class="csa_muBtn_conteneur_bouton csa_bouton">' +
										'    <a class="csa_muBtn_button_suite" href="' + actionUrl + '"> <span><span>' + actionLabel + '</span></span></a>' +
										' </div>' +
										'		 </div>' +
										'       </div>' +
										'<div class="csa_bloc_bas_ombre"></div>' +
										'</div>'
										;
									this.showDialog(htmlEnvoiCsa);
								};

		this.showSaisieCsa = function(message1, message2, actionLabel, actionUrl) {
										var htmlSaisieCsa = 
											'<div class="csa_mobile_2">' +
											'	<div class="csa_entete">' +
											'		<div class="csa_entete_barre"><span>Fermer la fen&ecirc;tre de connexion</span></div>' +
											'		<div class="csa_entete_fermer"><span>Fermer la fen&ecirc;tre de connexion</span></div>' +
											'		<div class="csa_entete_aide"><span>Aide (nouvelle fen&ecirc;tre)</span></div>' +
											'	</div>' +
											'	<div class="csa_conteneur_ombre">' +
											'		<div class="csa_conteneur">' +
											'			<div class="csa_onglet_conteneur">' +
											'				<div class="csa_onglet ">' +
											'					<div class="csa_onglet_etape_1"></div>' +
											'					Envoi du Code S&eacute;curit&eacute;' +
											'				</div>' +
											'				<div class="csa_onglet csa_onglet_actif ">' +
											'					<div class="csa_onglet_etape_2 "></div>' +
											'					Saisie du Code S&eacute;curit&eacute;' +
											'				</div>' +
											'			</div>' +
											'			<div class="csa_titre_rouge">' + message1 + '</div>' +
											'			<div class="csa_details">' + message2 + '</div>' +
											'			<div class="csa_input"><form id="csa_form" onSubmit="return false;">Veuillez saisir le Code S&eacute;curit&eacute; : <input type="text" maxlength="6" autocomplete="off" name="code_secu"/></form></div>' +
											'			<div class="csa_muBtn_conteneur_bouton csa_bouton"><a class="csa_muBtn_button_suite" href="' + actionUrl + '"> <span><span>' + actionLabel + '</span></span></a></div>' +
											'		</div>' +
											'	</div>' +
											'	<div class="csa_bloc_bas_ombre"></div>' +
										'	</div>';
									this.showDialog(htmlSaisieCsa);
								};

		this.showErreur = function(message1, message2, actionLabel, actionUrl) {
										var htmlSaisieCsa = 
											'<div class="csa_mobile_1">' +
											'	<div class="csa_entete">' +
											'		<div class="csa_entete_barre"><span>Fermer la fen&ecirc;tre de connexion</span></div>' +
											'		<div class="csa_entete_fermer"><span>Fermer la fen&ecirc;tre de connexion</span></div>' +
											'		<div class="csa_entete_aide"><span>Aide (nouvelle fen&ecirc;tre)</span></div>' +
											'	</div>' +
											'	<div class="csa_conteneur_ombre">' +
											'		<div class="csa_conteneur_erreur">' +
											'			<div class="csa_titre_rouge">' + message1 + '</div>' +
											((message2 != null) ? '			<div class="csa_details">' + message2 + '</div>' : '') +
											((actionLabel != null) ? '			<div class="csa_muBtn_conteneur_bouton csa_bouton"><a class="csa_muBtn_button_suite" href="' + (actionUrl != null ? actionUrl : '#') + '"> <span><span>' + actionLabel + '</span></span></a></div>' : '')+
											'		</div>' +
											'	</div>' +
											'	<div class="csa_bloc_bas_ombre"></div>' +
										'	</div>';
									this.showDialog(htmlSaisieCsa);
								};


			this.getCsa = function() {
                        return document.getElementById('csa_form').code_secu.value;
                };

}

var gdaUrlCSS = (typeof(gda_static_base_uri) == "string" ? gda_static_base_uri : '')+'/static/styles/gda/gda.css';

var GdaDialog = function() {

        // Chargement des CSS
		gda_css_include(gdaUrlCSS);

        $jGda('body').append(
                this.overlay = $jGda('<div class="gda_overlay"></div>')
        );
		
		this.iframePourIE = $jGda('<iframe style="width:100%; height: 100%; filter:alpha(opacity=0); border: 0; pointer-events: none;" />');
		this.iframePourIE.attr("src",(typeof(gda_static_base_uri) == "string" ? gda_static_base_uri : '')+"/static/img/vide.gif");
		this.overlay.append(this.iframePourIE);

        $jGda('body').append(
                this.dialogDiv = $jGda('<div id="gda_dialog" style="visibility:hidden;position:absolute;width:730px;height:320px;top:-5000px;left:-5000px"></div>')
        );

        this.overlay.css({
                'background-color'      : '#666',
                'opacity'                       : 0.7
                });

        this.overlay.hide();

        this.showLayer = function() {
                this.overlay.fadeIn('300');
        };

        this.hideLayer = function() {
                this.closeDialog();
                this.overlay.fadeOut('300');
        };

        this.centerDialog = function (element) {

                var windowHeight = $jGda(window).height();
                var windowWidth = $jGda(window).width();
                var elementHeight = $jGda(element).height();
                var elementWidth = $jGda(element).width();
                
                var elementTop, elementLeft;

                if (windowHeight <= elementHeight) {
                        elementTop = $jGda(window).scrollTop();
                } else {
                        elementTop = ((windowHeight - elementHeight) / 2) + $jGda(window).scrollTop();
                }
                if (windowWidth <= elementWidth) {
                        elementLeft = $jGda(window).scrollLeft();
                } else {
                        elementLeft = ((windowWidth - elementWidth) / 2) + $jGda(window).scrollLeft();
                }

                this.dialogDiv.css({
                        'top': elementTop,
                        'left': elementLeft
                        });
        };



        this.showDialog = function(html) {
                this.closeDialog();
                this.dialogDiv.html(html);
		          this.dialogDiv.css({'visibility':'visible'});

                this.centerDialog('#gda_dialog');

                $jGda('.gda_entete_fermer').click(function() {
                        gda_dialog.closeDialog();
						gda_dialog.hideLayer();
                        });

                $jGda('.gda_entete_fermer').attr('title', 'Fermer');

        };

        this.closeDialog = function() {
                this.dialogDiv.css({'visibility':'hidden'});
                this.dialogDiv.html('');
                };


		this.showErreur = function(message1, message2, actionLabel, actionUrl) {
										var htmlGda = 
											'<div class="gda_mobile_1">' +
											'	<div class="gda_entete">' +
											'		<div class="gda_entete_barre"><span>Fermer la fen&ecirc;tre de connexion</span></div>' +
											'		<div class="gda_entete_fermer"><span>Fermer la fen&ecirc;tre de connexion</span></div>' +
											'	</div>' +
											'	<div class="gda_conteneur_ombre">' +
											'		<div class="gda_conteneur_erreur">' +
											'			<div class="gda_titre_rouge">' + message1 + '</div>' +
											((message2 != null) ? '			<div class="gda_details">' + message2 + '</div>' : '') +
											((actionLabel != null) ? '			<div class="gda_muBtn_conteneur_bouton gda_bouton"><a class="gda_muBtn_button_suite" href="' + (actionUrl != null ? actionUrl : '#') + '"> <span><span>' + actionLabel + '</span></span></a></div>' : '')+
											'		</div>' +
											'	</div>' +
											'	<div class="gda_bloc_bas_ombre"></div>' +
										'	</div>';
									this.showDialog(htmlGda);
								};



}

var gda_layer_UrlCSS = (typeof(gda_static_base_uri) == "string" ? gda_static_base_uri : '')+'/static/styles/gda/gda.css';

var gda_layer;

var gda_layer_defaultTxt = "Le service est momentan&eacute;ment indisponible. Merci de bien vouloir nous en excuser.";

var gda_layer_dialog=
	'<div class="gda_layer_ctn">' +
	'<div class="gda_layer_ctn_header"><a id="gda_layer_btn_fermer">FERMER</a></div>' +
	'<div id="gda_layer_ctn_content" class="gda_layer_ctn_content">';
	
var gda_layer_ctn_auth=
	'<div class="gda_layer_auth">'+
	'	<div class="gda_layer_auth_client">'+
	'		<p class="gda_layer_titre">Votre session a expiré. Merci de bien vouloir vous identifier à nouveau.</p>' +
	'		<p id="gda_layer_auth_error" class="gda_layer_error"></p>' +
	'		<span class="gda_layer_auth_form"><input id="gda_layer_auth_idUser" placeholder="Votre code client" maxlength="8" title="Saisissez votre code client"><span id="gda_layer_auth_btn"><a>OK</a></span></span>'+
	'		<p class="gda_layer_texte"><span class="gda_layer_important">Vous n' + "'" +'avez pas vos codes d' + "'" +'accès ?</span>' +
	'		<br/><a id="gda_layer_auth_obtcode" class="gda_layer_auth_obtcode" href="/votre_site/configuration_securite/mesures_securite/attribution_codes_acces.html">Obtenir vos codes d' + "'" +'accès</a></p>' +
	'	</div>' +
	'	<div class="gda_layer_auth_nonclient">' +
	'		<p class="gda_layer_titre">Vous n' + "'" +'êtes pas client</p>' +
	'		<div class="gda_layer_auth_devclient"><a href="https://particuliers.societegenerale.fr/devenir_client.html">Devenir client</a></div>' +
	'	</div>' +
	'	<div class="gda_layer_auth_reset" />' +
	'</div>';

var gda_layer_ctn_msg=
	'<div class="gda_layer_ctnmsg">' +
	'	<div id="gda_layer_msg" class="gda_layer_msg" />' +
	'</div>';

/*
 * Basic function : Small API to display a layer on top of another page
 */
function gda_layer_createLayer(layerHTMLContent, fermerCallback) {
    // Chargement des CSS
	gda_css_include(gda_layer_UrlCSS);
	
	/* The first div covers the whole viewport to grey the screen. */
	if (!gda_layer) {
		gda_layer = $jGda(gda_layer_dialog);
		gda_layer.dialog({
			width: 700,
			modal: true,
			dialogClass: 'gda_layer_dialog',
			closeOnEscape: false,
			autoOpen: false,
			zIndex: 1005
		});
		$jGda('.gda_layer_dialog').css('position', 'fixed');
		gda_layer.dialog("open");
	} else {
		gda_layer.dialog("open");
	}
	
	$jGda('#gda_layer_btn_fermer').unbind('click').click(fermerCallback);
	$jGda('#gda_layer_ctn_content').html(layerHTMLContent);
    	
	return gda_layer;
}

function gda_layer_openMessage(message, fermerCallback) {
	gda_layer_createLayer(gda_layer_ctn_msg, fermerCallback);
    $jGda('#gda_layer_msg').html(message);
}

function gda_layer_openAuth(fermerCallback, authCallback, errorMessage) {
	gda_layer_createLayer(gda_layer_ctn_auth, fermerCallback);
	$jGda('#gda_layer_auth_btn').unbind('click').click(
			function() {
				var idUser = $jGda('#gda_layer_auth_idUser').val();
				if (!idUser || $jGda.trim(idUser) == '') {
					alert("Vous devez saisir votre identifiant.");
					return false;
				}	else if (!idUser.match(new RegExp("^[0-9]{8}$"))) {
					alert("Votre identifiant est incorrect.");
					return false;
				} 				
				authCallback(idUser); 
			}
		);
	if (errorMessage) {
		$jGda('#gda_layer_auth_error').html(errorMessage);
	}
}

function gda_layer_closeLayer() {
	if (gda_layer) {
		gda_layer.dialog("close");
		gda_layer.dialog("destroy").remove();
		gda_layer = null;
	}
}
var gda_lgn_DEBUG = 0;
var gda_lgn_successCallback=null;
var gda_lgn_failureCallback=null;
var gda_lgn_donneesIncorrecteMsg = 'Les donn&eacute;es saisies sont incorrectes. Veuillez les saisir &agrave; nouveau.';
var gda_lgn_compteBloqueMsg = 'Suite &agrave; trois saisies erron&eacute;es de vos codes, l&rsquo;acc&egrave;s &agrave; vos comptes est bloqu&eacute; jusqu&rsquo;&agrave; demain pour des raisons de s&eacute;curit&eacute;.';

function gda_lgn_authValidate(secureParams) {
	$jGda.ajax({
		'url': "/acces/authlgn.xml",
    	'type': "POST",
    	'contentType': 'application/x-www-form-urlencoded; charset=UTF-8',
    	'data': secureParams,
    	'cache': false,
    	'error': function(jqXHR, textStatus, errorThrown) {
			if (gda_lgn_DEBUG) {
	    		alert("Une erreur s'est produite : <" + jqXHR.status + "> <" + jqXHR.statusText + ">");
	    		alert("textStatus : " + textStatus);
	    		alert("errorThrown : " + errorThrown);
			}
			gda_lgn_displayMessage(gda_layer_defaultTxt, 99);
		},
    	success: function(respData, textStatus, jqXHR) {
			var responseContentType = jqXHR.getResponseHeader('Content-Type');
			if (!responseContentType|| responseContentType == null) responseContentType = "";

			var gda_lgn_successCallback_curr = gda_lgn_successCallback;
			var gda_lgn_failureCallback_curr = gda_lgn_failureCallback;
			gda_handleResponse({
				'contentType': responseContentType,
				'responseBody' : respData,
				'success' : gda_lgn_successCallback_curr,
				'error' : gda_lgn_failureCallback_curr
			    });
		}
	});
}

function gda_lgn_authenticate(idUser) {
	gda_auth_json({
		'idUser' : idUser,
        'success' : gda_lgn_authValidate,
        'error' : null // Gérer le cas null au niveau du js GDA
    });
}

function gda_lgn_displayMessage(message, code) {
	if (code==1) { /* Authentification requise */
		if (message != gda_lgn_donneesIncorrecteMsg) {
			gda_layer_openAuth(function() {gda_lgn_exitHandle(false)}, gda_lgn_authenticate);
		} else {
			gda_layer_openAuth(function() {gda_lgn_exitHandle(false)}, gda_lgn_authenticate, gda_lgn_donneesIncorrecteMsg);
			
		}
	} else if (code==5) { /* Compte bloqué */
		gda_layer_openMessage(gda_lgn_compteBloqueMsg, function() {location.href='/'}) 
	} else if (code==10) { /* Code client ou code secret invalide */ 
		gda_layer_openAuth(function() {gda_lgn_exitHandle(false)}, gda_lgn_authenticate, gda_lgn_donneesIncorrecteMsg); 
	} else {
		gda_layer_openMessage(message, function() {gda_lgn_exitHandle(false)})
	}
}

/*
 * Exit => hide and callback call
 */
function gda_lgn_exitHandle(callSuccessCallBack) {
	gda_layer_closeLayer();
	
	if (callSuccessCallBack)	{
		if (gda_lgn_successCallback) gda_lgn_successCallback();
	} else {
		if (gda_lgn_failureCallback) gda_lgn_failureCallback();
	}
}

/*
 * Main function - internal
 */
function gda_lgn_error(code, clientMsg, onSuccessCallback, onFailureCallback) {
	gda_lgn_successCallback = onSuccessCallback;
	gda_lgn_failureCallback = onFailureCallback;

	// TODO : check des fonctions js
	// TODO : texte message d'erreur session expiree ...

    switch (code) {

		case 0:  /* Success */
			gda_lgn_exitHandle(true);
			break;

		case 1:  /* Authentification requise */
		    gda_lgn_displayMessage(clientMsg, code);
			break;

		case 98: /* Cas interne LGN/GDA */
		    gda_lgn_displayMessage(gda_layer_defaultTxt, code);
			break;

		case 99: /* Erreur non recouvrable */
		    gda_lgn_displayMessage(clientMsg, code);
			break;

		case 2:  /* CSA requis */
			gda_lgn_displayMessage(gda_layer_defaultTxt, code);
			break;

		case 5: /* Deconnexion + compte bloque */
		case 6: /* SAS CSA faux */
		case 7: /* SAS CSA - CSA en cours d'envoi (SMS ou APS/SVI) */
		   gda_lgn_displayMessage(clientMsg, code);
			break;

		case 10:  /* Authentification requise */
		    gda_lgn_displayMessage(clientMsg, code);
				break;

		case 3:  /* Certificat requis */
		case 4:  /* CConf faux sur validation d'operation */
		default: /* Cas inconnu */
		    gda_lgn_displayMessage(gda_layer_defaultTxt, 99);
			break;

	}
}

// Private functions above 
// Don't call these functions from somewhere else
//#############################################################################
//#############################################################################
//#############################################################################
// Public functions below
// Only call these functions from somewhere else

function gda_lgn_handleErrorXML(onSuccessCallback, onFailureCallback, xml) {
	var code = 99;
	var clientMsg = gda_layer_defaultTxt;

	try {
		var root = xml.documentElement;
		code = parseInt(root.getElementsByTagName('code').item(0).firstChild.data);
   		clientMsg = root.getElementsByTagName('functionalMsg').item(0).firstChild.data;
	} catch (e) {
		// TODO Logguer l'erreur
	}
	
	return gda_lgn_error(code, clientMsg, onSuccessCallback, onFailureCallback);
}

function gda_lgn_handleErrorJSON(onSuccessCallback, onFailureCallback, respJSON) {
	var code = 99;
	var clientMsg = gda_layer_defaultTxt;

	try {
		code = parseInt(respJSON.commun.raison);
		clientMsg = respJSON.donnees.message;
	} catch (e) {
		// TODO Logguer l'erreur
	}
	
	return gda_lgn_error(code, clientMsg, onSuccessCallback, onFailureCallback);
}
var gda_sas_DEBUG = 0;
var gda_sas_successCallback=null;
var gda_sas_failureCallback=null;

function gda_sas_authValidate(secureParams) {
	$jGda.ajax({
		'url': "/acces/authlgn.xml",
    	'type': "POST",
    	'contentType': 'application/x-www-form-urlencoded; charset=UTF-8',
    	'data': secureParams,
    	'cache': false,
    	'error': function(jqXHR, textStatus, errorThrown) {
			if (gda_sas_DEBUG) {
	    		alert("Une erreur s'est produite : <" + jqXHR.status + "> <" + jqXHR.statusText + ">");
	    		alert("textStatus : " + textStatus);
	    		alert("errorThrown : " + errorThrown);
			}
			gda_sas_displayMessage(gda_layer_defaultTxt, 99);
		},
    	success: function(respData, textStatus, jqXHR) {
			var responseContentType = jqXHR.getResponseHeader('Content-Type');
			if (!responseContentType|| responseContentType == null) responseContentType = "";

			var gda_sas_successCallback_curr = gda_sas_successCallback;
			var gda_sas_failureCallback_curr = gda_sas_failureCallback;
			gda_handleResponse({
				'contentType': responseContentType,
				'responseBody' : respData,
				'success' : gda_sas_successCallback_curr,
				'error' : gda_sas_failureCallback_curr
			    });
		}
	});
}

function gda_sas_authenticate(idUser) {
	gda_auth_json({
		'idUser' : idUser,
        'success' : gda_sas_authValidate,
        'error' : null // Gérer le cas null au niveau du js GDA
    });
}

function gda_sas_displayMessage(message, code) {
	switch (code) {
		case 'niv_auth_insuff':
			gda_layer_openAuth(function() {gda_sas_exitHandle(false)}, gda_sas_authenticate);
			break;
		default: /* Cas inconnu */
			gda_layer_openMessage(message, function() {gda_sas_exitHandle(false)})
		break;
	}
}

/*
 * Exit => hide and callback call
 */
function gda_sas_exitHandle(callSuccessCallBack) {
	gda_layer_closeLayer();
	
	if (callSuccessCallBack)	{
		if (gda_sas_successCallback) gda_sas_successCallback();
	} else {
		if (gda_sas_failureCallback) gda_sas_failureCallback();
	}
}


function gda_sas_error(code, message, onSuccessCallback, onFailureCallback) {
	gda_sas_successCallback = onSuccessCallback;
	gda_sas_failureCallback = onFailureCallback;
	
	if (code == 'ok') {
		gda_sas_exitHandle(true);
		
	} else {
		switch (message) {
			case 'niv_auth_insuff':
				gda_sas_displayMessage(null, message);
				break;
				
			default: /* Cas inconnu */
				gda_sas_displayMessage(gda_layer_defaultTxt, message);
				break;
		}
	}
}

function gda_sas_handleErrorXML(onSuccessCallback, onFailureCallback, xml) {
	var code;
	
	try {
		var root = xml.documentElement;
		code = root.getElementsByTagName('code').item(0).firstChild.data;
		message = root.getElementsByTagName('functionalMsg').item(0).firstChild.data;
	} catch (e) {
		// TODO Logguer l'erreur
	}

	return gda_sas_error(code, message, onSuccessCallback, onFailureCallback);
}

function gda_sas_handleErrorJSON(onSuccessCallback, onFailureCallback, respJSON) {
	var code;
	
	try {
		code = respJSON.commun.statut;
		message = respJSON.commun.raison;
	} catch (e) {
		// TODO Logguer l'erreur
	}
	
	return gda_sas_error(code, message, onSuccessCallback, onFailureCallback);
}