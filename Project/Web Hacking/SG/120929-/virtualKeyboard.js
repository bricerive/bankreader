//**********************************************************
// Constantes
//**********************************************************

var playTimeout = null;

/* For dragStart, dragStop, etc. */
var dragObj = new Object();
var haveqt = false;

/* Insert VBScript code detecting whether the plugin we need for blind people. */
document.writeln('<script language="VBscript">');
document.writeln('detectableWithVB = False');
document.writeln('If ScriptEngineMajorVersion >= 2 then');
document.writeln('  detectableWithVB = True');
document.writeln('End If');
document.writeln('  on error resume next');
document.writeln('  detectActiveXControl = False');
document.writeln('  If detectableWithVB Then');
document.writeln('     detectActiveXControl = IsObject(CreateObject(activeXControlName))');
document.writeln('  End If');
document.writeln('  on error resume next');
document.writeln('  detectQuickTimeActiveXControl = False');
document.writeln('  If detectableWithVB Then');
document.writeln('    detectQuickTimeActiveXControl = False');
document.writeln('    hasQuickTimeChecker = false');
document.writeln('    Set hasQuickTimeChecker = CreateObject("QuickTimeCheckObject.QuickTimeCheck.1")');
document.writeln('    If IsObject(hasQuickTimeChecker) Then');
document.writeln('      If hasQuickTimeChecker.IsQuickTimeAvailable(0) Then ');
document.writeln('        haveqt = True');
document.writeln('      End If');
document.writeln('    End If');
document.writeln('  End If');
document.writeln('</script>');

if (navigator.plugins) {
	for (var i = 0; i < navigator.plugins.length; i++) {
		if (navigator.plugins[i].name.indexOf('QuickTime') != -1) {
			haveqt = true;
			break;
		}
	}
}

if (navigator.appVersion.indexOf("Mac") != -1 && navigator.appName.substring(0,9) == "Microsoft" && parseInt(navigator.appVersion) < 5)
	haveqt = true;

/*
 * Nouveau clavier virtuel utilisant uniquement l'API cross-browser jQuery.
 *
 * Documentation de l'API:
 *	http://docs.jquery.com/
 */

/* A few constants. */
var constModeDefaut = 0;
var constModeMalVoyant = 1;
var constModeNonVoyant = 2;
var constSessionDuration = 600;

function showFAQ(url,name) {
  window.open(url, name,
    'width=780,height=540,top=0,left=0,toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,alwaysRaised=yes,resizable=yes');
}

function VirtualKeyboard(width, height, inSession, codeFieldId, options) {
	if (!$jGda('#'+codeFieldId)[0]) {
		alert("Erreur: le champ de code secret d'identifiant '" + codeFieldId + "' n'existe pas dans la page!");
		return;
	}

	this.fieldId = codeFieldId;
	this.inSession = inSession;

	
	this.initParams = function(options) {
		/* Default values. */
		this.pwdlen = 6;	/* Secret code length */
		this.allowSwitch = true;
		this.opacity = 0.56;
		this.switch_button_left = 102;	/* pixels */
		this.switch_button_top = 0;		/* pixels */
		this.urlBase = '';
		this.urlStaticBase = '';
		this.offx = 0;
		this.offy = 0;
		this.emptyCodeMsg = "La saisie de votre Code Secret est obligatoire.";
		this.incompleteCodeMsg = "Le Code Secret saisi est incorrect.\r\n" +
		    "Merci de bien vouloir ressaisir votre Code Secret " + unescape("compos%E9") + " de " +
		    this.pwdlen + " chiffres.";
		this.showHelpButton = true;
		this.faqURI = "javascript:showFAQ('" + this.urlBase + "/codeSecret.html', 'CodeSecret')";
		this.faqNVURI = "javascript:showFAQ('" + this.urlBase + "/html/faq/faq43.html', 'CodeSecret')";
		this.overwriteImages = {};
		this.useDisableLayer = false;
		this.disableLayerOpacity = 0.05;

		var mode = __getCookie('vk_mode');
		this.wantMode = mode ? parseInt(mode) : constModeDefaut;
		if (isNaN(this.wantMode))
		   this.wantMode=constModeDefaut;

		/* Override with supplied options. */
		if (options != undefined)
			$jGda.extend(this, options);
	}
	
	this.initParams(options);

	this.mode = constModeDefaut;
	this.loadingCrypto = false;
	this.numKeys = 0;
	this.state = '';
	this.codsec = '';
	this.urlError = this.urlBase + '/pageErreur.html';
	this.urlKeyboard = this.urlBase + '/sec/vk/gen_crypto?estSession=';
	this.urlImage = this.urlBase + '/sec/vk/gen_ui?modeClavier='; 
	this.urlCSS = this.urlStaticBase + '/static/styles/vk/vk.css';
	this.urlParamCrypto = '&cryptogramme=';
	this.urlParamError = 'cvcsCodeErreur';

	/* Drag'n'drop handling. */
	dragObj.elNode = $jGda('#tc_divdeplace');
	$jGda('#img_tc_haut').bind('mousedown', function(event) {
		dragStart.call(event.currentTarget, event);
	});
	$jGda('#img_tc_haut').bind('mouseup', function(event) {
		dragStop.call(event.currentTarget, event);
	});

	/* Path to images. */
	this.images = {};
	this.images["img_tc_haut"] = "/static/img/vk/tc_haut_sans.gif";
	this.images["tc_fond_img"] = "/static/img/vk/tc_fond.gif";
	this.images["legendeInput"] = "/static/img/vk/tc_votre_code.gif";
	this.images["tc_aideimg"] = "/static/img/vk/bouton_question.gif";
	this.images["tc_corriger"] = "/static/img/vk/tc_corriger.gif";
	this.images["tc_valider"] = "/static/img/vk/tc_valider.gif";
	this.images["surlignage"] = "/static/img/vk/tc_touche_cache_hover.gif";
	this.images["trans"] = "/static/img/vk/trans.gif"

	/* Overwrite with user supplied data */
	update_object(this.images, this.overwriteImages);

	/* Make images in the HTML code point to the correct location. */
	var urlBase = this.urlBase;
	var urlStaticBase = this.urlStaticBase;
	var images = this.images;
	$jGda('#tc_cvcs').find('img').each(function(index, img) {
		if (images[$jGda(img).attr('id')])
			$jGda(img).attr('src', urlStaticBase + images[$jGda(img).attr('id')]);
	});
	/* Create the appropriate link for the help button. */
	$jGda('#tc_aidelien').attr('href', this.faqURI);

	if (this.allowSwitch) {
		var img_mv = $jGda(document.createElement('img')).attr('id', 'tc_haut_switch');
		img_mv.attr('class', 'tc_haut_switch');
		img_mv.attr('src', this.urlStaticBase + '/static/img/vk/tc_agrandir.gif');
		img_mv.attr('style', 'cursor: pointer');
		img_mv.attr('alt', 'AGRANDIR');
		img_mv.attr('title', 'AGRANDIR');
		img_mv.removeAttr('height');
		img_mv.removeAttr('width');
		$jGda('#img_tc_haut').after(img_mv);
		img_mv.bind('click', {thisObj: this}, function(event) {
			event.data.thisObj.toggleMode(event);
		});
	}

	if (this.showHelpButton)
		$jGda('#tc_aidelien').toggle();

	$jGda('#tc_fond_img').css({opacity:this.opacity});
	$jGda('#vk_layer').css({opacity:this.disableLayerOpacity});
		


	/* Reset internal state and use another input. */
	this.reset = function(codeFieldId, options) {
		this.emptyFields();
		this.fieldId = codeFieldId;
		this.initParams(options);
	}

	this.getWidth = function() {
		return $jGda('#tc_fond_img').width();
	}

	this.getHeight = function() {
		return $jGda('#img_tc_haut').height() + $jGda('#tc_fond_img').height();
	}

	this.onKeyClicked = function(event) {
		if (this.mode == constModeDefaut)
			$jGda(event.currentTarget).attr('src', this.urlStaticBase + '/static/img/vk/tc_touche_cache_click_1s2.gif');
		else
			$jGda(event.currentTarget).attr('src', this.urlStaticBase + '/static/img/vk/tc_touche_cache_click_1s2_mv.gif');
		this.clickKeyX = numCurX;
		this.clickKeyY = numCurY;
	}

	this.onKeyUnclicked = function(event) {
		if (this.mode == constModeDefaut)
			$jGda(event.currentTarget).attr('src', this.urlStaticBase + '/static/img/vk/tc_touche_cache_hover.gif');
		else
			$jGda(event.currentTarget).attr('src', this.urlStaticBase + '/static/img/vk/tc_touche_cache_hover_mv.gif');
		if (this.clickKeyX == numCurX && this.clickKeyY == numCurY)
			this.updateValue(parseInt((numCurY - 1) * this.nbc + parseInt(numCurX)) - 1);
	}

	this.show = function() {
		/* Attach events to anything clickable. */
		$jGda('#shapeHautFermer').unbind('click').bind('click', {thisObj: this}, function(event) {
			event.data.thisObj.close();
		});
		$jGda('#tc_corriger').unbind('click').bind('click', {thisObj: this}, function(event) {
			event.data.thisObj.emptyCode();
		});
		$jGda('#tc_valider').unbind('click').bind('click', {thisObj: this}, function(event) {
			event.data.thisObj.validate(event);
		});

		$jGda('#surlignage').unbind('mousedown').bind('mousedown', {thisObj: this}, function(event) {
			event.data.thisObj.onKeyClicked(event);
		});
		$jGda('#surlignage').unbind('mouseup').bind('mouseup', {thisObj: this}, function(event) {
			event.data.thisObj.onKeyUnclicked(event);
		});
		/* This event is for unhilighting the keys. */
		$jGda('#surlignage').unbind('mouseout').bind('mouseout', {thisObj: this}, function(event) {
			desactiverSurlignage.call(event.data.thisObj, event);
		});
		
		if (this.wantMode == constModeNonVoyant)
			this.enableSound();
		else
			this.enableVisual();
	}

	this.enableVisual = function() {
		if (this.crypto != undefined) {
			this.showKeyboard();
			return;
		}
		this.generateKeyboardCoords();
		if (this.state == "") {
			if (this.wantMode != this.mode) {
				this.setMode(this.wantMode);
				this.updateKeyboard($jGda('#tc_cvcs'));
			}
			var vk = this;
			this.getCryptogram(function() {
				vk.state = 'i';
				vk.generateKeyboard();
				if (!vk.inSession) {
					vk.timer = (new Date()).getTime();
				}
				vk.showKeyboard();
			});
		}
	}

	this.showKeyboard = function() {
		if (this.mode != constModeNonVoyant) {
			if (this.useDisableLayer) {
				/*this.vk_layer.show();*/
				 this.vk_layer.fadeIn('300');
				 this.vk_layer.unbind('click').bind('click', function() {
					if (gda_auth_close) {
						gda_auth_close();
					}
				 });
			}

			var div = $jGda('#tc_divdeplace');
			if (typeof(N2GHideFlash) == 'function')
				N2GHideFlash(); // masks flash objects
			div.css('left', $jGda(window).scrollLeft() + this.posx + 'px');
			div.css('top', $jGda(window).scrollTop() + this.posy + 'px');

			this.state = 'v';

//			$('tc_enablesound').focus();
		}
	}

	/* XXX - looks like this one should go now. */
	this.emptyFields = function() {
		this.emptyCode();
		this.state = ''; // XXX - Was in vider_champs() but looks misplaced.
	}

	this.emptyCode = function() {
		if (this.mode != constModeNonVoyant) {
			$jGda('#tc_visu_saisie').val('');
			$jGda('#cvcs_visu_cache').val('');
		} else {
			$jGda('#debutClavier').focus();
		}
		this.numKeys = 0;
		this.codsec = '';
	}

	/* Callback for handling the JSON data returned by the server. */
	this.JSONHandler = function(onSuccess, vkdata) {
		if (vkdata.code != 0) {
			window.location = this.urlError + '?' + this.urlParamError + '=104'; // + codeR;
			return false;
		}
		this.crypto = vkdata.crypto;
		$jGda('#cryptocvcs').val(this.crypto);
		this.nbl = vkdata.nbrows;
		this.nbc = vkdata.nbcols;
		this.adaptKeyboard();

		if (this.crypto == "" || this.nbl <= 0 || this.nbc <= 0) {
			window.location = this.urlError + '?' + this.urlParamError + '=106'; // + codeERR_GEN;
			return false;
		}
		var ndata = this.nbl * this.nbc;
		if (vkdata.grid.length != this.pwdlen * ndata) {
			window.location = this.urlError + '?' + this.urlParamError + '=105'; // + codeERR_GEN;
			return false;
		}
		if (this.mode == constModeNonVoyant)
			this.keyCodes = $jGda.grep(vkdata.grid, function(v) {
				return v != 0;
			});
		else
			this.keyCodes = vkdata.grid;
		onSuccess();
		this.loadingCrypto = false;
	}

	/*
	 * Call server to get cryptogramme and size of the virtual keyboard.
	 * This is needed later on to construct an URL to the server-generated
	 * image, and to send the secret code in encoded form.
	 */
	this.getCryptogram = function(onSuccess) {
		if (this.loadingCrypto)
			return;

		if (this.scriptTag)
			this.scriptTag.remove();

		this.loadingCrypto = true;
		var url = this.urlKeyboard;
		if (this.inSession)
			url += '1';
		else
			url += '0';

		_vkCallback = (function(func, thisObj, callback) {
			return function() {
				func.call(thisObj, callback, arguments[0]);
			}
		})(this.JSONHandler, this, onSuccess);
		
		var scriptTag=document.createElement('script')
	 	if (typeof scriptTag!="undefined")
	 	{
  			scriptTag.setAttribute("type","text/javascript")
  			scriptTag.setAttribute("src", url)
  			document.getElementsByTagName("head")[0].appendChild(scriptTag)

				this.scriptTag = $jGda(scriptTag);
		}
		
	}

	this.hide = function() {
		if (this.mode != constModeNonVoyant) {
			if (this.state != "" || $jGda('#tc_cvcs').is(':visible')) {
				if (this.useDisableLayer) {
					this.vk_layer.hide();
				}
				var div = $jGda('#tc_divdeplace');
				div.css('left', '-600px');
				div.css('top', '-600px');
				this.state = '';
			}
		}
		if (typeof(N2GShowFlash) == 'function')
			N2GShowFlash(); // shows Flash objects
	}

	this.setMode = function(mode) {
		this.mode = mode;
		/* Save mode */
		__setCookie('vk_mode', String(this.mode));
	}

	this.toggleMode = function(event) {
		this.hide();
		if (this.mode == constModeDefaut)
			this.setMode(constModeMalVoyant);
		else
			this.setMode(constModeDefaut);

		__setCookie('vk_mode', String(this.mode));

		this.generateKeyboard();

		this.updateKeyboard($jGda('#tc_cvcs'));
		this.adaptKeyboard();
		/* The delay helps for displaying only when the browser is done rendering. */
		(function(func, objThis) {
			setTimeout(function() {
				func.call(objThis);
			}, 300);
		})(this.showKeyboard, this);

		this.state = 'i';
		event.preventDefault();
	}

	this.getKeyboardMode = function() {
		return this.mode;
	}

	this.generateKeyboard = function() {
		var ndt = this.nbl * this.nbc;
		var k = 0;

		var kbdmap = $jGda('#tc_tclavier');
		if (this.kbdmapIdUnique != undefined) kbdmap = $jGda('#'+this.kbdmapIdUnique);

		/* First, remove any <area> element contained within the <map>. */
		kbdmap.find('area').remove();

		this.kbdmapIdUnique = 'tc_tclavier' + new Date().getTime();
		kbdmap.attr('id', this.kbdmapIdUnique).attr('name', this.kbdmapIdUnique);
		kbdmap = $jGda('#'+this.kbdmapIdUnique);
		
		/* Then, add back all the <area> elements matching this keyboard. */
		for (i = 1; i <= this.nbl; i++) {
			for (j = 1; j <= this.nbc; j++) {
				var keyId = 'touche' + i + j;
				var factor = 24;
				if (this.mode == constModeMalVoyant)
					factor *= 2;
				var coords = (j - 1) * factor + "," + (i - 1) * factor + "," + j * factor + "," + i * factor;
				var area = $jGda(document.createElement('area')).attr('id', keyId).attr('coords', coords).attr('style', 'cursor: pointer;display:block;');
				/* Insert element in the DOM before registering events. */
				kbdmap.append(area);
				if (this.keyCodes[k] != 0) {
					area.addClass('touche');
					area.bind('mouseout', {thisObj: this}, function(event) {
						desactiverSurlignage.call(event.data.thisObj, event);
					});
					area.bind('mouseover', {thisObj:this, jArg:j, iArg:i}, function(event) {
						activerSurlignage.call(event.data.thisObj, event, event.data.jArg, event.data.iArg);
					});
				} else {
					area.addClass('toucheVide');
					area.bind('mouseover', {thisObj:this}, function(event) {
						desactiverSurlignage.call(event.data.thisObj, event);
					});
				}
				k++;
			}
		}

		$jGda('#img_clavier').attr('useMap', '#'+this.kbdmapIdUnique);
		$jGda('#img_clavier').attr('src', this.urlImage + this.mode + this.urlParamCrypto + this.crypto);
	}

	this.close = function() {
		this.hide();
		this.emptyFields();
		this.crypto = null;
		if (this.mode == constModeNonVoyant) {
			$jGda('#pluginQt').remove();
		}
		if (this.closefn)
			this.closefn();
	}

	/* Update keyboard tags to point to the correct images and update CSS classes. */
	this.updateKeyboard = function(elem) {
		if (this.mode == constModeDefaut)
			$jGda('#tc_fond_img').css({opacity:this.opacity});
		else
			$jGda('#tc_fond_img').css({opacity:1});
		var children = $jGda(elem).find('*');
		var numchilds = children.length;
		for (var i = 0; i < numchilds; ++i) {
			var child = children[i];
			if (child.nodeName.toLowerCase() == 'img' && $jGda(child).attr('src')) {
				var src = child.src;
				if (src != "") {
					if (this.mode == constModeDefaut)
						child.src = src.replace(/_mv(\.\w+)$/, '$1');
					else
						child.src = src.replace(/(\.\w+)$/, '_mv$1');
				}
			}
			if (child.className && child.className != "") {
				if (this.mode == constModeDefaut)
					child.className = child.className.replace(/_mv$/, '');
				else
					child.className = child.className.replace(/$/, '_mv');
			}
		}
	}

	this.adaptKeyboard = function() {
		if (this.mode == constModeNonVoyant)
			return;
		var extOpera = '';
		if ($jGda.browser.opera) {
			/* Get the major version number. */
			var version = navigator.appVersion.charAt(0);
			if (version < 9)
				extOpera = '_1s2';
		}

		var img_tc_haut = this.urlStaticBase + '/static/img/vk/tc_haut_sans' + (this.nbc > 4 ? '_' + this.nbc : '') + '.gif';
		var img_tc_fond = this.urlStaticBase + '/static/img/vk/tc_fond.gif';
		if (this.nbc == 4 && this.nbl == 4)
			img_tc_fond = this.urlStaticBase + '/static/img/vk/tc_fond' + extOpera + '.gif';
		var img_tc_haut_mv = this.urlStaticBase + '/static/img/vk/tc_haut_sans' + (this.nbc > 4 ? '_' + this.nbc : '') + '_mv.gif';
		var img_tc_fond_mv = this.urlStaticBase + '/static/img/vk/tc_fond_mv.gif';
		if (this.nbc > 4) {
			if (this.nbl > 4){
				img_tc_fond = this.urlStaticBase + '/static/img/vk/tc_fond' + extOpera + '_5x5.gif';
				img_tc_fond_mv = this.urlStaticBase + '/static/img/vk/tc_fond_5x5_mv.gif';
			} else {
				img_tc_fond = this.urlStaticBase + '/static/img/vk/tc_fond' + extOpera + '_5x4.gif';
				img_tc_fond_mv = this.urlStaticBase + '/static/img/vk/tc_fond_5x4_mv.gif';
			}
		} else if (this.nbl > 4) {
			img_tc_fond = this.urlStaticBase + '/static/img/vk/tc_fond' + extOpera + '_4x5.gif';
			img_tc_fond_mv = this.urlStaticBase + '/static/img/vk/tc_fond_4x5_mv.gif';
		}
		if (this.mode == constModeDefaut) {
			$jGda('#img_tc_haut').attr('src', img_tc_haut);
			$jGda('#tc_fond_img').attr('src', img_tc_fond);
			$jGda('#shapeHautFermer').attr('coords', this.nbc > 4 ? '192,6,241,16' : '167,3,230,16');
			$jGda('#tc_boutons').css('left', 139 + (this.nbc - 4) * 24 + 'px');
			$jGda('#tc_boutons').css('top', 80 + (this.nbl - 4) * 24 + 'px');
			if ($jGda('#tc_haut_switch').size() > 0) {
				$jGda('#tc_haut_switch').css('left', this.switch_button_left + (this.nbc - 4) * 25 + 'px');
				$jGda('#tc_haut_switch').css('top', this.switch_button_top + 'px');
				$jGda('#tc_haut_switch').attr('alt', 'AGRANDIR');
				$jGda('#tc_haut_switch').attr('title', 'AGRANDIR');
			}
		} else {
			$jGda('#img_tc_haut').attr('src', img_tc_haut_mv);
			$jGda('#tc_fond_img').attr('src', img_tc_fond_mv);
			/* The next two lines are needed for Safari. */
			$jGda('#tc_fond_img').removeAttr('height');
			$jGda('#tc_fond_img').removeAttr('width');
			$jGda('#shapeHautFermer').attr('coords', this.nbc > 4 ? '325,4,498,40' : '275,4,448,40');
			$jGda('#tc_boutons').css('left', 245 + (this.nbc - 4) * 48 + 'px');
			$jGda('#tc_boutons').css('top', 115 + (this.nbl - 4) * 48 + 'px');
			if ($jGda('#tc_haut_switch').size() > 0) {
				$jGda('#tc_haut_switch').css('left', 80 + (this.nbc - 4) * 30 + 'px');
				$jGda('#tc_haut_switch').attr('alt', 'REDUIRE');
				$jGda('#tc_haut_switch').attr('title', 'REDUIRE');
			}
		}
		
		var oldMapHautClose = $jGda('#img_tc_haut').attr('usemap');
		var mapHautClose = 'maphaut'+new Date().getTime();
		$jGda('#img_tc_haut').attr('usemap', '#'+mapHautClose);
		$jGda(oldMapHautClose).attr('id', mapHautClose).attr('name', mapHautClose);
	}

	this.generateKeyboardCoords = function() {
		if (this.zones == undefined) {
			this.posx = genererCoordAleatoire(this.offx, this.getWidth(), $jGda(window).width());
			this.posy = genererCoordAleatoire(this.offy, this.getHeight(), $jGda(window).height());
		} else {
			var n = Math.floor(Math.random() * this.zones.length);
			var zone = this.zones[n];
			/* If the width or height aren't defined, or if we are given numbers that
                           exceed the size of the viewport, restrict the zone to the viewport. */
			if (zone.width == undefined || zone.width > $jGda(window).width())
				zone.width = $jGda(window).width();
			if (zone.height == undefined || zone.height > $jGda(window).height())
				zone.height = $jGda(window).height();
			this.posx = genererCoordAleatoire(zone.x, this.getWidth(), zone.width);
			this.posy = genererCoordAleatoire(zone.y, this.getHeight(), zone.height);
		}
	}

	this.updateValue = function(index) {
		var vi = $jGda('#tc_visu_saisie');
		var vc = $jGda('#cvcs_visu_cache');
		var ndata = this.nbl * this.nbc;
		if (this.numKeys < this.pwdlen) {
			vi.val(vi.val() + '*');
			vc.val(vi.val());
			this.codsec += this.keyCodes[this.numKeys * ndata + index] ;
			if (this.numKeys < this.pwdlen - 1)
				this.codsec += ',';
			this.numKeys++;
		}
	}

	this.validate = function(event) {
		$jGda('#'+this.fieldId).val(this.codsec);

		if (!this.inSession && this.timer != null && ((new Date()).getTime() - this.timer)/1000 > constSessionDuration) {
			alert("Attention, vous avez " + unescape("d%E9pass%E9") + " la " +
			    unescape("dur%E9e") + " maximale pour valider votre Code Secret. " +
			    "Merci de recommencer la saisie.");
			this.close(false);
			return;
		}

		if (this.numKeys == 0) {
			alert(this.emptyCodeMsg);
			if (this.mode == constModeNonVoyant) {
				$jGda('#debutClavier').focus();
			}
			return;
		} else if (this.numKeys != this.pwdlen) {
			alert(this.incompleteCodeMsg);
			this.emptyCode();
			return;
		}

		if (this.validatefn) {
			this.state = 'f';
			this.emptyCode();
			this.hide();
			/* If the validation function tries to submit a form, it won't work with IE6 unless
			 * we defer this function for some reason. */
			(function(func, objThis) {
				setTimeout(function() {
					func.call(objThis, objThis);
				}, 1);
			})(this.validatefn, this);
		} else
			alert("Le nom de la fonction de validation n'est pas " + unescape("renseign%E9."));
	}

	this.enableSound = function() {
		if (this.mode == constModeNonVoyant)
			return;

		var res = confirm("Vous avez " + unescape("activ%E9") + " le clavier virtuel sonore.");
		if (!res)
			return;

		if (!haveqt) {
			res = confirm("Le plugin Quicktime n'est pas " + unescape("install%E9") +
			    " sur votre navigateur. Pour le " + unescape("t%E9l%E9charger") +
			    ", choisissez OK (ouverture d'une nouvelle " + unescape("fen%EAtre") + ").");
			if (res)
				window.open("http://www.apple.com/quicktime/download", "_blank", "");
			else
				window.history.go(0);
			return;
		}

		this.hide();

		this.setMode(constModeNonVoyant);
		
		if (!vk.inSession) {
			vk.timer = (new Date()).getTime();
		}

		if (this.crypto != undefined)
			this.buildSoundKeyboard();
		else {
			var onSuccess = (function(func, thisObj) { return function() { func.call(thisObj); } })(this.buildSoundKeyboard, this);
			this.getCryptogram(onSuccess);
		}
	}

	this.buildSoundKeyboard = function() {
		var pluginHTML = '<object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" width="1" height="1" id="sonCode"><param name="src" value="';
		pluginHTML += this.urlImage + this.mode + this.urlParamCrypto + this.crypto;
		pluginHTML += '"><param name="name" value="sonCode"><param name="id" value="sonCode"><param name="autostart" value="false"><param name="type" value="audio/mpeg"><embed width="1" height="1" src="';
		pluginHTML += this.urlImage + this.mode + this.urlParamCrypto + this.crypto;
		pluginHTML += '" name="sonCode" id="sonCode" autostart="false" type="audio/mpeg" enablejavascript="true"></embed></object>';
		var p = $jGda('#pluginQt');
		if (p.size() == 0) {
			p = $jGda(document.createElement('p')).attr('id', 'pluginQt').attr('class', 'noMarge');
			if (this.form.size() == 0) {
				$jGda('#tc_cvcs').after(p);
			} else {
				this.form.after(p);
			}
		}
		p.html(pluginHTML);

		var help = $jGda(document.createElement('a')).attr('href', this.faqNVURI).attr('id', 'lienAideNonVoyant');
		var help_img = $jGda(document.createElement('img')).attr('id', 'aideNonVoyantClav');
		help_img.attr('src', this.urlStaticBase + this.images["trans"]);
		help_img.attr('class', 'noMarge');
		help_img.attr('alt', 'Aide ' + unescape('%E0') + ' la solution accessible nonvoyant nouvelle ' + unescape('fen%EAtre'));
		help_img.attr('title', 'Aide ' + unescape('%E0') + ' la solution accessible nonvoyant nouvelle ' + unescape('fen%EAtre'));
		help.append(help_img);
		p.append(help);

		var a0 = $jGda(document.createElement('a')).attr('href', 'javascript:void(0)').attr('name', 'Ensemble').attr('id', 'Ensemble');
		var img0 = $jGda(document.createElement('img')).attr('src', this.urlStaticBase + this.images["trans"]);
		img0.attr('class', 'noMarge');
		img0.attr('alt', 'Ecoutez l\'ensemble de la suite de chiffres');
		img0.attr('title', 'Ecoutez l\'ensemble de la suite de chiffres');
		img0.attr('id', 'ListenAll');
		a0.append(img0);
		p.append(a0);
		a0.bind('click', playAllSelection);

		for (var i = 0; i <= 9; i++) {
			/* Used to construct the alt attribute. */
			var digitsAlt = ['premier', 'second', 'troisi%E8me', 'quatri%E8me',
				'cinqui%E8me', 'sixi%E8me', 'septi%E8me', 'huiti%E8me',
				'neuvi%E8me', 'dernier'];

			var a = $jGda(document.createElement('a')).attr('href', 'javascript:void(0)');
			if (i == 0)
				a.attr('id', 'debutClavier').attr('name', 'debutClavier');
				
			var img = $jGda(document.createElement('img')).attr('src', this.urlStaticBase + this.images["trans"]);
			img.attr('class', 'noMarge');
			img.attr('alt', unescape(digitsAlt[i]) + ' chiffre du clavier virtuel');
			img.attr('title', unescape(digitsAlt[i]) + ' chiffre du clavier virtuel');
			a.append(img);
			p.append(a);
			a.bind('focus', (function(func, arg) { return function() { func(arg); } })(armPlayNV, i));
			a.bind('blur', disarmPlayNV);
			a.bind('click', {thisObj:this, iArg:i}, function(event) {
				event.data.thisObj.keyPressedNV(event, event.data.iArg);
			});
		}

		/* Add the 'corriger' link. */
		var correct = $jGda(document.createElement('a')).attr('href', 'javascript:void(0)').attr('name', 'corriger').attr('id', 'corriger');
		var cimg = $jGda(document.createElement('img')).attr('src', this.urlStaticBase + this.images["trans"]);
		cimg.attr('class', 'noMarge');
		cimg.attr('alt', 'Corriger le code secret');
		cimg.attr('title', 'Corriger le code secret');
		correct.append(cimg);
		p.append(correct);
		correct.bind('click', {thisObj: this}, function(event) {
			event.data.thisObj.emptyCode();
		});

		/* Add the 'valider' link. */
		var submit = $jGda(document.createElement('a')).attr('href', 'javascript:void(42)');
		var simg = $jGda(document.createElement('img')).attr('src', this.urlStaticBase + this.images["trans"]);
		simg.attr('class', 'noMarge');
		simg.attr('name', 'validation');
		simg.attr('id', 'validation');
		simg.attr('alt', 'Valider le code secret');
		simg.attr('title', 'Valider le code secret');
		submit.append(simg);
		p.append(submit);
		submit.bind('click', {thisObj: this}, function(event) {
			event.data.thisObj.validate(event);
		});

		help.focus();
	}

	this.keyPressedNV = function(event, codeInterrupt) {
		if (this.mode == constModeNonVoyant && this.numKeys >= this.pwdlen) {
			alert(this.incompleteCodeMsg);
			this.emptyCode();
			$jGda('#lienAideNonVoyant').focus();
			return;
		}

		this.codsec += this.keyCodes[this.numKeys * 10 + codeInterrupt] ;
		if (this.numKeys < this.pwdlen - 1)
			this.codsec += ',';
		this.numKeys++;
		if (this.numKeys == this.pwdlen && $jGda('#corriger'))
			$jGda('#corriger').focus();
		newPlayNV(codeInterrupt);
	}

	this.showZone = function() {
		var zone_div = $jGda('#vk_debug_zone');
		if (zone_div.size() == 0) {
			zone_div = $jGda(document.createElement('div')).attr('id', 'vk_debug_zone');
			zone_div.css('position', 'absolute');
			zone_div.css('border', '1px solid red');
			zone_div.css('left', this.zones[0].x + 'px');
			zone_div.css('top', this.zones[0].y + 'px');
			zone_div.css('height', this.zones[0].height + 'px');
			zone_div.css('width', this.zones[0].width + 'px');
			$jGda('body').append(zone_div);
		}
		zone_div.show();
	}

	this.hideZone = function() {
		var zone_div = $jGda('#vk_debug_zone');
		if (zone_div.size() > 0)
			zone_div.hide();
	}
	
};

function VirtualKeyboard2(inSession, codeFieldId, options) {
	VirtualKeyboard.call(this, null, null, inSession, codeFieldId, options);
};

var kbdHTML =  '<div id="tc_cvcs" class="clcvcs">\n' +
  '<div id="tc_divdeplace" style="position:absolute; left:-10000px; top:-10000px;" class="tc_fond">\n' +
//	'<a id="tc_enablesound" alt="Activer le clavier sonore" href="javascript:void(0);"></a>\n' +
    '<map name="maphaut" id="maphaut" style="display:block">\n' +
      '<area id="shapeHautFermer" shape="rect" style="cursor:pointer;display:block" coords="167,6,230,16" alt="FERMER" title="FERMER">\n' +
    '</map>\n' +
    '<img id="img_tc_haut" usemap="#maphaut">\n' +
    '<img id="tc_fond_img" class="tc_fond_img">\n' +
    '<div id="tc_text" class="text_div">\n' +
      '<div id="tc_votre_code" class="code_div">\n' +
        '<img id="legendeInput" alt="VOTRE CODE SECRET" title="VOTRE CODE SECRET">\n' +
        '<div id="tc_pass" class="password_div">\n' +
          '<input type="text" id="tc_visu_saisie" value="" readonly size="6">\n' +
          '<a id="tc_aidelien" style="display: none">\n' +
            '<img id="tc_aideimg">\n' +
          '</a>\n' +
        '</div>\n' +
      '</div>\n' +
    '</div>\n' +
    '<map id="tc_tclavier" name="tc_tclavier" style="display:block"></map>\n' +
    '<img id="img_clavier" class="keyboard" usemap="#tc_tclavier">\n' +
    '<div id="tc_boutons" class="buttons">\n' +
      '<img id="tc_corriger" class="correct_img" style="cursor:pointer" alt="CORRIGER" title="CORRIGER"><br>\n' +
      '<img id="tc_valider" class="valid_img" style="cursor:pointer" alt="VALIDER" title="VALIDER">\n' +
    '</div>\n' +
    '<img id="surlignage" style="cursor:pointer; position:absolute; left:-10000px; top:-10000px;">\n' +
  '</div>\n' +
'</div>\n';

function VirtualKeyboardHTML(elemId, formId, inSession, codeFieldId, options) {
	if ($jGda('#vk_layer').size() == 0) {
		$jGda('body').append(this.vk_layer = $jGda('<div id="vk_layer" class="vk_layer"></div>'));
		this.iframePourIE = $jGda('<iframe style="width:100%; height: 100%; filter:alpha(opacity=0); border: 0; pointer-events: none;" />');
		this.iframePourIE.attr("src",(typeof(gda_static_base_uri) == "string" ? gda_static_base_uri : '')+"/static/img/vide.gif");
		this.vk_layer.append(this.iframePourIE);
		this.vk_move = $jGda(document.createElement('div')).attr('id', 'vk_move').attr('class', 'vk_move');
		this.vk_layer.append(this.vk_move);
		this.vk_layer.hide();
	} else if (this.vk_layer == null) {
		this.vk_layer = $jGda('#vk_layer');
		this.vk_move = $jGda('#vk_move');

	}
	var elem = $jGda('#'+elemId);
	elem.html(kbdHTML);
	if ($jGda('#cryptocvcs').size() == 0) {
		var inputCVCS = $jGda(document.createElement('input')).attr('type', 'hidden').attr('name', 'cryptocvcs').attr('id', 'cryptocvcs').attr('value', '');
		$jGda('#'+formId).append(inputCVCS);
		var inputCVCSCache = $jGda(document.createElement('input')).attr('type', 'hidden').attr('id', 'cvcs_visu_cache').attr('value', '');
		$jGda('#'+formId).append(inputCVCSCache);
	}
	this.form = $jGda('#'+formId);
	VirtualKeyboard2.call(this, inSession, codeFieldId, options);
	
	gda_css_include(this.urlCSS);
};


function VirtualKeyboardHTML2(elemId, formId, inSession, codeFieldId, options) {
	VirtualKeyboardHTML.call(this, elemId, formId, inSession, codeFieldId, options);
};

function dragStart(event) {
	if (event.which == 1) {
		/* Get cursor position with respect to the page. */
		dragObj.cursorStartX = event.pageX;
		dragObj.cursorStartY = event.pageY;

		/* Save starting positions of element. */
		dragObj.elStartLeft  = parseInt(dragObj.elNode.css('left'));
		dragObj.elStartTop   = parseInt(dragObj.elNode.css('top'));

		if (isNaN(dragObj.elStartLeft)) dragObj.elStartLeft = 0;
		if (isNaN(dragObj.elStartTop))  dragObj.elStartTop  = 0;

		/* Capture mousemove and mouseup events on the page. */
		if (this.vk_move == null) {
			$jGda(document).bind('mousemove', dragGo);
			$jGda(document).bind('mouseup', dragStop);
		} else {
			this.vk_move.bind('mousemove', dragGo);
			this.vk_move.bind('mouseup', dragStop);
		}
		event.preventDefault();
	}
}

function dragGo(event) {
	var x, y;

	/* Get cursor position with respect to the page. */
	x = event.pageX;
	y = event.pageY;

	/* Move drag element by the same amount the cursor has moved. */
	if (x > 0 && y > 0) {
		px = dragObj.elStartLeft + x - dragObj.cursorStartX;
		py = dragObj.elStartTop  + y - dragObj.cursorStartY;

		if (px > 0) dragObj.elNode.css('left', px + 'px');
		if (py > 0) dragObj.elNode.css('top', py + 'px');
	}

	event.preventDefault();
}

function dragStop(event) {
	/* Stop capturing mousemove and mouseup events. */
	$jGda(document).unbind('mousemove');
	$jGda(document).unbind('mouseup');
}

function activerSurlignage(event) {
	var data = Array.prototype.slice.call(arguments);
	data.shift();
	var x = data.shift();
	var y = data.shift();

	this.curKey = event.currentTarget;

	var surlignage = $jGda('#surlignage');
	var imageClavierOffSet = $jGda('#img_clavier').position();
	
	if (this.mode == constModeDefaut) {
		surlignage.css('top', ((y * 23) + imageClavierOffSet.top - 23 + 1) + 'px');
	//	surlignage.style.top = ((y * 23) + 53) + 'px';
		surlignage.css('left', ((x * 24) + imageClavierOffSet.left - 24 + 1 ) + 'px');
	//	surlignage.style.left = ((x * 24) - 3) + 'px';
	} else {
		surlignage.css('top', ((y * 46) + imageClavierOffSet.top - 46 + 1 ) + 'px');
	//	surlignage.style.top = ((y * 46) + 122) + 'px';
		surlignage.css('left', ((x * 48) + imageClavierOffSet.left -48 + 1) + 'px');
	//	surlignage.style.left = ((x * 48) - 24) + 'px';
	}
	numCurX = x;
	numCurY = y;
}

function desactiverSurlignage(event) {
	var surlignage = $jGda('#surlignage');
	var off = surlignage.offset();
	var posx = event.pageX;
	var posy = event.pageY;
	if (!(posx > off.left && posx < off.left + surlignage[0].offsetWidth)
	    || !(posy > off.top && posy < off.top + surlignage[0].offsetHeight)) {
		surlignage.css('left', '-5000px');
		surlignage.css('top', '-5000px');
	}
	/* XXX duplicated with this.onKeyUnclicked() */
	if (this.mode == constModeDefaut)
		surlignage.src = this.urlStaticBase + '/static/img/vk/tc_touche_cache_hover.gif';
	else
		surlignage.src = this.urlStaticBase + '/static/img/vk/tc_touche_cache_hover_mv.gif';
}

function genererCoordAleatoire(coordOrigine, tailleClavier, tailleTotale)
{
	return Math.round(Math.random() * (tailleTotale - tailleClavier)) + coordOrigine;
}

function playAllSelection() {
	//var sonCode = $('pluginQt').down('object');
	var sonCode = document.sonCode;
	if (sonCode) {
		sonCode.Rewind();
		sonCode.SetStartTime(0);
		sonCode.SetEndTime(10 * 666 - 50);
		sonCode.Play();
	}
}

function playSelection(digitIndex) {
	//var sonCode = $('pluginQt').down('object');
	var sonCode = document.sonCode;
	if (sonCode) {
		sonCode.Rewind();
		sonCode.SetStartTime(0);
		sonCode.SetEndTime((digitIndex + 1) * 666 - 50);
		sonCode.SetStartTime(digitIndex * 666);
		sonCode.Play();
	}
}

function armPlayNV(index) {
	disarmPlayNV();
	playTimeout = (function(func, param) {
			return setTimeout(function() {
				func(param);
			}, 2000);
		})(newPlayNV, index);
}

function newPlayNV(index) {
	playSelection(index);
	playTimeout = null;
}

function disarmPlayNV() {
	if (playTimeout) {
		window.clearTimeout(playTimeout);
		playTimeout = null;
	}
}

/*
 * Backported utility functions from LGN
 */

function __getCookie(name)
{
	var rg = new RegExp(name+"=([^;]*)");
	var r = rg.exec(document.cookie);
	return (r ? unescape(r[1]) : null);
}

/* No support for expire/domain/path/secure */
function __setCookie(name,value)
{
	document.cookie = name + "=" + escape(value)+"; path=/";	
}

