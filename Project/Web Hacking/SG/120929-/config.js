/*
 * GDA Virtual Keyboard configuration for NGO
 */

var gda_vk_config = {
	switch_button_left: 102,
	switch_button_top: 8,
	opacity: 0.56,
	zones: [ {x:150, y:230, height:270, width:610} ],
	overwriteImages: { "tc_ombre": "/static/img/vk/tc_ombre.gif" },
	faqURI: 'javascript: showFAQ("/aide/clavier_virtuel.html", "CodeSecret")',
	faqNVURI: 'javascript:showFAQ("/aide/clavier_sonore.html", "CodeSecret")',
	useDisableLayer: true,
	disableLayerOpacity: 0.55,
	closefn: function() { if (typeof(fermerCV) == "function") fermerCV(); }
 };