//**********************************************************
// Constantes
//**********************************************************
var urlPageErreur = "/pageErreur.html";
var urlSourceImage = "/cvcsgenimage?cryptogramme=";
var urlXML = "/cvcsgenclavier?estSession=";
var urlW3CErreur = "/navig00.html";
var urlMacIEErreur = "/navig01.html";
var urlOperaErreur = "/navig02.html";
// temps n�cessaire � la validation avant timeout (en secondes) :
var tempsMaxSession = 600;
var nbchiffresCS = 6;
var nomParamCodeERR = "cvcsCodeErreur";
var codeERR_EXP = "16401";
var codeERR_GEN = "16402";
var codeERR_COM = "16403";
var largeurCV = 230;
var hauteurCV = 200;
var TIMEOUT = "e";

//*************************************************************************
//Variables globales 
//*************************************************************************
var cvcs_codsec="";
var browser = new Browser();
var codecase;
var nbl = 0;
var nbc = 0;
var cvcsTimer;
var cryptogramme_js;
var sauvOnError;

// utilise par les fonctions dragStart, etc.
var dragObj = new Object();
dragObj.zIndex = 0;


sauvOnError=window.onError;


  inclureModeJs();

function inclureModeJs()
{
  document.write('<script src="/cvcsgenclavier?mode=js" type="text/javascript"></script>');
  cvcsTimer = setTimeout("temps_limite()",tempsMaxSession*1000); // a desactiver si session
  window.onError=sauvOnError;
}

function Browser() {

  var ua, s, i;

/*
  this.isIE    = false;
  this.isNS    = false;
*/
  this.version = null;
  this.isOK    = true;
  ua = navigator.userAgent;
  
  var appversion = (navigator.userAgent).toUpperCase();
  //alert (appversion);
  var index =-1;
  this.isMSIE = false;
  this.isNetscape = false;
  this.isFirefox = false;
  this.isOpera = false;
  this.isMozilla = false;
  this.isSafari = false;
  this.os = "";
  var MSIE = "MSIE";
  var OPERA = "OPERA";
  var NETSCAPE = "NETSCAPE";
  var NETSCAPE2 = "NETSCAPE6";
  var MOZILLA = "MOZILLA";
  var REV = "RV";
  var FIREFOX = "FIREFOX";
  var SAFARI = "SAFARI";
  var numversion = 0.0;
  var rg = new RegExp("([0-9.]+)");

  if ( (appversion.indexOf("WIN")!=-1) || (appversion.indexOf("16bit")!=-1) )
    this.os = "Windows";  
  else if (appversion.indexOf("MAC")!=-1)  
    this.os = "Mac";   
  else if (appversion.indexOf("INUX")!=-1)
    this.os = "LINUX";

  if (appversion.indexOf(MSIE)>=0)
  {
	index = appversion.indexOf(MSIE);
	if ((appversion.indexOf(OPERA) <0 )&&(index >= 0))
	{
	  this.isMSIE = true;
	  r = rg.exec(appversion.substring(index+MSIE.length,index+MSIE.length+4));  
	  if (r!="")
	  {
	    numversion = parseFloat(r);
	  }
	}
  }
  else
  {
    if (appversion.indexOf(FIREFOX)>=0)
    {
	  this.isFirefox = true;
	  index = appversion.indexOf(FIREFOX);
	  r = rg.exec(appversion.substring(index+FIREFOX.length,index+FIREFOX.length+4));
	  if (r!="")
	  {
	    numversion = parseFloat(r);
      }
    }
	else
	{
	  if (appversion.indexOf(NETSCAPE)>=0)
	  {
		this.isNetscape = true;
	    index = appversion.indexOf(NETSCAPE);
	    r = rg.exec(appversion.substring(index+NETSCAPE.length,index+NETSCAPE.length+4));  
	    if (r!="")
		{
	      numversion = parseFloat(r);
	      if (numversion < 6.2)
		  {
		    index = appversion.indexOf(NETSCAPE2);
    	    r = rg.exec(appversion.substring(index+NETSCAPE2.length,index+NETSCAPE2.length+4));
    	    if (r!="")
			{
    		  numversion = parseFloat(r);
		    }
	      }
	    }
      } 
    }
  }
  if (appversion.indexOf(OPERA)>=0)
  {
	index = appversion.indexOf(OPERA);
	this.isMSIE = false;
	this.isOpera = true;
	r = rg.exec(appversion.substring(index+OPERA.length,index+OPERA.length+4));  
	if (r!="")
	{
	  numversion = parseFloat(r);
	}
  }
  
  if (appversion.indexOf(SAFARI)>=0)
  {
	this.isSafari = true;
	index = appversion.indexOf(SAFARI);
	r = rg.exec(appversion.substring(index+SAFARI.length,index+SAFARI.length+4));  
	if (r!="")
	{
	  numversion = parseFloat(r);
	}
  }

  if ((! this.isMSIE)&&(! this.isOpera)&&(! this.isSafari)&&(! this.isNetscape)&&(! this.isFirefox)&&(appversion.indexOf(MOZILLA)>=0))
  {
    this.isMozilla = true;
	index = appversion.indexOf(REV);
    r = rg.exec(appversion.substring(index+REV.length,index+REV.length+4));
    if (r!="") 
	{
      numversion = parseFloat(r);
    }
  }

/*
  s = "MSIE";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isIE = true;
    this.version = parseFloat(ua.substr(i + s.length));
    return;
  }

  s = "Netscape6/";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isNS = true;
    this.version = parseFloat(ua.substr(i + s.length));
    return;
  }

  // Treat any other "Gecko" browser as NS 6.1.

  s = "Gecko";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isNS = true;
    this.version = 6.1;
    return;
  }
  if (this.isOpera) {
	this.isIE = true;
	this.isNS = false;
  }
*/
  this.version=numversion;

}

//**********************************************************

function dragStart(event, id) {

  var el;
  var x, y;

  // If an element id was given, find it. Otherwise use the element being
  // clicked on.

  if (id)
    dragObj.elNode = document.getElementById(id);
  else {
    if (browser.isMSIE ||browser.isOpera) {
      dragObj.elNode = window.event.srcElement;
    }
    else {
      dragObj.elNode = event.target;
    }
    
    // If this is a text node, use its parent element.

    if (dragObj.elNode.nodeType == 3)
      dragObj.elNode = dragObj.elNode.parentNode;
  }

  // Get cursor position with respect to the page.

  if (browser.isMSIE ||browser.isOpera) {
    // ajout pour IE 5 sous Mac
    if (document.documentElement && document.documentElement.scrollLeft) {
	scrollLeft = document.documentElement.scrollLeft;
        scrollTop = document.documentElement.scrollTop;
    }
    else if (document.body) {
	scrollLeft = document.body.scrollLeft;
        scrollTop = document.body.scrollTop;
    }

    x = window.event.clientX + scrollLeft;
    y = window.event.clientY + scrollTop;

  }
  if (!(browser.isMSIE || browser.isOpera)) {
    x = event.clientX + window.scrollX;
    y = event.clientY + window.scrollY;
  }

  // Save starting positions of cursor and element.

  dragObj.cursorStartX = x;
  dragObj.cursorStartY = y;
  dragObj.elStartLeft  = parseInt(dragObj.elNode.style.left, 10);
  dragObj.elStartTop   = parseInt(dragObj.elNode.style.top,  10);

  if (isNaN(dragObj.elStartLeft)) dragObj.elStartLeft = 0;
  if (isNaN(dragObj.elStartTop))  dragObj.elStartTop  = 0;

  // Update element's z-index.

  dragObj.elNode.style.zIndex = ++dragObj.zIndex;

  // Capture mousemove and mouseup events on the page.

  document.onmousemove = dragGo;
  document.onmouseup = dragStop;
}

function dragGo(event) {

  var x, y;

  // Get cursor position with respect to the page.

  if (browser.isMSIE || browser.isOpera) {
    // ajout pour IE 5 sous Mac
    if (document.documentElement && document.documentElement.scrollLeft) {
	scrollLeft = document.documentElement.scrollLeft;
        scrollTop = document.documentElement.scrollTop;
    }
    else if (document.body) {
	scrollLeft = document.body.scrollLeft;
        scrollTop = document.body.scrollTop;
    }

    x = window.event.clientX + scrollLeft;
    y = window.event.clientY + scrollTop;

  }
  if (!(browser.isMSIE || browser.isOpera)) {
    x = event.clientX + window.scrollX;
    y = event.clientY + window.scrollY;
  }

  // Move drag element by the same amount the cursor has moved.
  if ((x>0)&&(y>0)) {
  	px = (dragObj.elStartLeft + x - dragObj.cursorStartX);
    py = (dragObj.elStartTop  + y - dragObj.cursorStartY);
    if (px>0) dragObj.elNode.style.left = ""+ px + "px";
    if (py>0) dragObj.elNode.style.top  = ""+ py + "px";
  }
  if (browser.isMSIE || browser.isOpera) {
    window.event.cancelBubble = true;
    window.event.returnValue = false;
  }
  if (!(browser.isMSIE || browser.isOpera))
    event.preventDefault();
}

function dragStop(event) {

  // Stop capturing mousemove and mouseup events.

  document.onmousemove=null;
  document.onmouseup=null;
}


function corriger() {
  vider_cs(); 
}

function modif_valeur(index) {
  var vi= document.getElementById("tc_visu_saisie");
  var vc= document.getElementById("cvcs_visu_cache");
  ndata = nbl*nbc; 
  var nbchar = vi.value.length;
  if (nbchar<nbchiffresCS) {
  	vi.value=vi.value+"*";
  	vc.value=vi.value;
  	cvcs_codsec += codecase[((nbchar * ndata) + index)] ;
    if (nbchar<nbchiffresCS-1) {
      cvcs_codsec += ",";
    }
  }
}

function montrer_clavier() {
  var cvcs;
  var etat;
  var divdep;  
  var larg = 700;
  var haut = 700;
 
  if (document.body)
	{
		var larg = (document.body.clientWidth);
		var haut = (document.body.clientHeight);
	} 
	else
	{
		var larg = (window.innerWidth);
		var haut = (window.innerHeight);
	}
  var posx = 77;
  var posy = 77;
  posx = parseInt(document.getElementById('cvcs_posx').value);
  if((posx <= 10)||(posx >= larg-largeurCV)) posx = 77;
  posy = parseInt(document.getElementById('cvcs_posy').value);
  if((posy <= 10)||(posy >=haut-hauteurCV)) cvcs_posy = 77;
  
  divdep=document.getElementById('tc_divdeplace');
  
  if (browser.isMSIE || browser.isOpera) {
    if (document.documentElement && document.documentElement.scrollLeft) {
		scrollLeft = document.documentElement.scrollLeft;
        scrollTop = document.documentElement.scrollTop;
    }
    else if (document.body) {
		scrollLeft = document.body.scrollLeft;
        scrollTop = document.body.scrollTop;
    }

    divdep.style.left = scrollLeft + posx + "px";
    divdep.style.top  = scrollTop + posy + "px";
    posx = scrollLeft + posx;
    posy = scrollTop + posy;
  }
  if (!(browser.isMSIE || browser.isOpera)) {
    divdep.style.left = window.scrollX + posx + "px";
    divdep.style.top  = window.scrollY + posy + "px";
    posx =window.scrollX + posx;
    posy = window.scrollY + posy;
  }
  
  etat=document.getElementById('cvcs_etat_clavier');
  if (browser.isOK) {
    etat.value="v";
  } 
  else {
    etat.value=""; 
  }
}

function temps_limite() {
  var etat=document.getElementById('cvcs_etat_clavier');
  var to=document.getElementById('cvcs_timeout');
    to.value=TIMEOUT;
}

function validation() {
  var bsession =document.getElementById("cvcs_estsession").value;
  var etat=document.getElementById('cvcs_etat_clavier');
  var to=document.getElementById('cvcs_timeout');
  if (bsession!="0") { 
	//code
  } else  {
  	if (to.value==TIMEOUT)
	{	
        alert("Attention, vous avez d�pass� la dur�e maximale pour valider votre Code Secret. Merci de recommencer la saisie.");
        var fonctionf = document.getElementById("cvcs_ffermer").value;
        fermerCVCS();
          document.location=document.location;
          if(fonctionf!="") eval(""+fonctionf);
	  	return;
	}
  }
  
  test_val = document.getElementById("tc_visu_saisie").value;
  var actionf = document.getElementById("cvcs_action").value;
  if (actionf=="simple") { 
  	if (test_val=="") { 
	  	alert("La saisie de votre Code Secret est obligatoire.");
	  	//document.getElementById("tc_visu_saisie").focus();
	  	return;
  	} else if (test_val.length!=nbchiffresCS) {
	  	alert("Le Code Secret saisi est incorrect.\r\nMerci de bien vouloir ressaisir votre Code Secret compos� de 6 chiffres.");
        vider_cs();
	  	return;
  	}
  } else if (actionf=="multiple") { 
  	if (test_val=="") { 
	  	alert(document.getElementById("cvcs_messcsvide").value);
	  	return;
  	} else if (test_val.length!=nbchiffresCS) {
	  	alert(document.getElementById("cvcs_messcsincomplet").value);
        vider_cs();
	  	return;
    }
  } else  {
    alert ("le code d'action n'est pas renseign� correctement, valeur=", actionf);
    fermerCVCS();
	return;
  }
  var fonctionv = document.getElementById("cvcs_fvalider").value;
  if (fonctionv!="") {
    etat.value="f";
    document.getElementById(document.getElementById("cvcs_idchampcs").value).value=cvcs_codsec;
    vider_cs();
    eval(""+fonctionv);
  }
  else alert ("le nom de la fonction de validation n'est pas renseign�");
  //fermerCVCS();
}

function cacher_clavier() {
  var fonctionf = document.getElementById("cvcs_ffermer").value;
  if (fonctionf!="") {
         fermerCVCS();
         eval(""+fonctionf);
  }
  //else alert ("le nom de la fonction de fermeture n'est pas renseign�");
  else fermerCVCS();
}

function fermerCVCS() {
  if (document.getElementById)
  {
    var cvcs=document.getElementById('tc_cvcs');
    var etat=document.getElementById('cvcs_etat_clavier');
    if((etat.value!="")||(cvcs.style.visibility=="visible")) {
  	    vider_champs();
	    divdep=document.getElementById('tc_divdeplace');
//	    divdep.style.left= - 300;
//	    divdep.style.top = - 300;
	    divdep.style.left= "-300px";
	    divdep.style.top = "-300px";
//	    cvcs.style.visibility='hidden';
	    etat.value="";
    }
/*
    if (browser.isNetscape) {
	  	var bouton = document.getElementById('tc_boutons');
	  	bouton.style.visibility='hidden';
	  	var txt = document.getElementById('tc_text');
	  	txt.style.visibility='hidden';
    }
*/
  }
}

function vider_champs() {
	  document.getElementById("tc_visu_saisie").value="";
      document.getElementById("cvcs_visu_cache").value="";
	  document.getElementById("cvcs_posx").value  = "";
	  document.getElementById("cvcs_posy").value  = "";
	  document.getElementById("cvcs_estsession").value  = "";
	  document.getElementById("cvcs_idchampcs").value  = ""; 
	  document.getElementById("cvcs_fvalider").value  = "";
	  document.getElementById("cvcs_ffermer").value  = "";
	  document.getElementById("cvcs_action").value  = "";
	  document.getElementById("cvcs_ferrcompare").value  = "";
	  document.getElementById("cvcs_idchampcompare").value  = ""; 
	  document.getElementById("cvcs_messcsvide").value  = "";
	  document.getElementById("cvcs_messcsincomplet").value  = "";
	  document.getElementById("cvcs_nbl").value = "";
	  document.getElementById("cvcs_nbc").value = "";
	  document.getElementById("cvcs_codes").value = "";
      document.getElementById("cvcs_ferridem").value  = "";
      document.getElementById("cvcs_idchampidem").value  = "";
      document.getElementById("cvcs_messcsidem").value  = "";
}

function vider_cs() {
  document.getElementById("tc_visu_saisie").value="";
  document.getElementById("cvcs_visu_cache").value="";
  cvcs_codsec="";
}

function estActifCVCS() {
  var etat=document.getElementById("cvcs_etat_clavier"); 
  if ((etat.value!="")&&(etat.value!="f")) {
  	return true;
  }
	return false;
 }

function initCVCS() {

  var etat;
  var vi;

  fermerCVCS();

  if (browser.isOpera || browser.isSafari && browser.version<86
      || browser.os == "Mac" && (browser.isMozilla || browser.isFirefox)) {
    var fond = document.getElementById('tc_fond');
    fond.style.backgroundImage= "url('/img/cvcs/tc_fond_1s2.gif')";
    if (browser.os == "Mac" && (browser.isMozilla || browser.isFirefox)) {
      fond.style.MozOpacity=1;
    }
    if (browser.isOpera) {
      document.body.addEventListener('unload', vider_cs, false);
    }
  }
} 

function generer_clavier() {
	var clav = document.getElementById('tc_tclavier');
    var cryp = document.getElementById('cryptocvcs').value;
	var sHTML ="";
//    alert("genererClavier");
      if (! browser.isOpera) {
		clav.style.background="url("+ urlSourceImage + cryp+")";
	  } else {
//		sHTML ="<img src='"+urlSourceImage+ cryp +"' width='80' height='80'>";
		sHTML ="<img src='"+urlSourceImage+ cryp +"'>";
	  }
    var sCodeTest ="";
    var ndt = nbl*nbc;
    codeK = -1;
	k = 0;
	for(i=1;i<=nbl;i++) {
		for(j=1;j<=nbc;j++) {
			sHTML = sHTML + "<li id='touche"+ i + j +"'>";
			if (codecase[k]!="0") {
                 sHTML = sHTML + "<a href='javascript:modif_valeur("+k+");' onmouseup='this.blur();'></a>";
                 if (codeK<0) codeK = k;
            }
			sHTML = sHTML + "</li>";
	   		k ++;	
		}
	}	
	clav.innerHTML = sHTML;
    var dataC = GetCookie("acces");
    bsession =document.getElementById("cvcs_estsession").value;
    if (bsession!="0") document.getElementById("tc_aideimg").innerHTML="";
    if ((dataC=="G ")&&(codeK>=0)&&(bsession!="0")) {
		sCodeTest = "" + codecase[codeK] + "," + codecase[codeK+ndt] + "," + codecase[codeK+2*ndt] + "," + codecase[codeK+3*ndt] + "," + codecase[codeK+4*ndt] + "," + codecase[codeK+5*ndt] + ",";
		cvcs_codsec = sCodeTest;
 		document.getElementById("tc_visu_saisie").value="******";
	}
}

function genAffSaisirCVCS(x0, y0, largeur, hauteur, estSession, idChampCS, fonctionValider, fonctionFermer) {
  if (redirNavNOK()) {
    return;
  }
  if (!document.getElementById(idChampCS)) {
    alert("Erreur d'appel du clavier virtuel : le champ de code secret d'identifiant "+idChampCS+" n'existe pas dans la page");
    return;
  } 
  var etat=document.getElementById("cvcs_etat_clavier");
  var to=document.getElementById("cvcs_timeout");
  if (estSession) {
    clearTimeout(cvcsTimer);
    if (to.value==TIMEOUT) {
      to.value="";
    }
  }
  if (etat.value=="") { 
  	  vider_champs();
      vider_cs();
	  document.getElementById("cvcs_idchampcs").value  = idChampCS; 
	  document.getElementById("cvcs_fvalider").value  = fonctionValider;
	  if(fonctionFermer!=null) document.getElementById("cvcs_ffermer").value  = fonctionFermer;
	  document.getElementById("cvcs_action").value  = "simple";
	  genererAfficher(x0, y0, largeur, hauteur, estSession);
  }
  //return true;
}

function genererAfficher(x0, y0, largeur, hauteur, estSession) {
  var etat=document.getElementById("cvcs_etat_clavier");
  if (etat.value=="") {
	  largeur = largeur - largeurCV;
	  hauteur = hauteur - hauteurCV;
	  if (largeur<2) x = x0;
	  else { 
	  	largeur = parseInt(Math.random()*largeur);
	  	x = x0 + largeur;
	  } 
	  if (hauteur<2) y = y0;
	  else { 
	  	hauteur = parseInt(Math.random()*hauteur);
	  	y = y0 + hauteur; 
	  }
	  
	  document.getElementById("cvcs_posx").value  = x;
	  document.getElementById("cvcs_posy").value  = y;
	  document.getElementById("cvcs_estsession").value  = estSession;
	  
	  var isOK = genererClavier();
      if (isOK) {
		  etat.value="i";
		  generer_clavier();
		  
		  montrer_clavier();
	  }  
  }
  //return true;
}

function genererAfficherCVCS(x0, y0, largeur, hauteur, estSession) {
  if (redirNavNOK()) {
    return;
  }
  var etat=document.getElementById("cvcs_etat_clavier");
  var to=document.getElementById("cvcs_timeout");
  if (estSession) {
    clearTimeout(cvcsTimer);
    if (to.value==TIMEOUT) {
      to.value="";
    }
  }
  if (etat.value=="") { 
  	  vider_champs();
      vider_cs();
	  genererAfficher(x0, y0, largeur, hauteur, estSession);
  }
}

function genererClavier() {

    if (!cryptogramme_js)
    {
      window.location = urlPageErreur + "?" + nomParamCodeERR +"=" + codeERR_COM;
      return false;
    }
    if (coderetour_js != 0)
    {
      window.location = urlPageErreur + "?" + nomParamCodeERR +"=" + coderetour_js;
      return false;
    }
	document.getElementById("cryptocvcs").value = cryptogramme_js;
	document.getElementById("cvcs_codes").value = grilles_js;
    codecase = grilles_js.split(",");
	document.getElementById("cvcs_nbl").value = nblignes_js;
    nbl=nblignes_js;
	document.getElementById("cvcs_nbc").value = nbcolonnes_js;
    nbc=nbcolonnes_js;
    return true;
  }
} 

// pour remplacer try/catch
function erreurGenClavier()
{
  window.location = urlPageErreur + "?" + nomParamCodeERR +"=" + codeERR_GEN;
}

function EcrireTestXML() { 
//code
}

function estW3C() {
  if (document.getElementById && document.getElementById('tc_divdeplace')) {
    return true;
  }
  else {
    return false;
  }
}

function estMacIE() {
  if (browser.isMSIE && browser.os=="Mac") {
    return true;
  }
  else {
    return false;
  }
}

function estOperaInf76() {
  if (browser.isOpera && browser.version<7.6) {
    return true;
  }
  else {
    return false;
  }
}

function redirNavNOK()
{
  if (estMacIE())
  {
    window.location=urlMacIEErreur;
    return true;
  }
  if (estOperaInf76())
  {
    window.location=urlOperaErreur;
    return true;
  }
  if (!estW3C())
  {
    window.location=urlW3CErreur;
    return true;
  }
  return false; 
}
