var estOpera = (navigator.userAgent.toLowerCase().indexOf("opera")) != -1 ? 1 : 0;

function fctfalse(){return false;};
function fcttrue(){return true;};

// NS 4.X
if(navigator.appName.indexOf("Netscape") >= 0)
document.captureEvents(Event.KEYDOWN);

function fctOnKeyDown(e)
{
  var key = e ? (e.which ? e.which : e.keyCode) : event.keyCode;
  var elm = e ? e.target : event.srcElement;
  if(key == 13 && elm.form && elm.type!="button" && elm.type!="textarea")
  return document.onkeydown13(elm);
}

function fctClickLink(fct)
{
  for(var i=0; i<document.links.length; i++ )
  if(fct) document.links[i].onclick = fct;
}

function fctClickForm(fct,fct2)
{
  for(var i=0; i<document.forms.length; i++ )
  { document.forms[i].onsubmit = fct2?fct2:fctfalse;
    if(fct) for(var j=0;j<document.forms[i].elements.length; j++ )
    {
      if(document.forms[i].elements[j].type=="button")
      document.forms[i].elements[j].onclick = fct;
} } }

function fctKeyDown13(fct)
{
  document.onkeydown13 = fct?fct:fcttrue;
  document.onkeydown = fctOnKeyDown;
}

function initForm(fct)
{
  fctClickForm(0,fctfalse);
  fctKeyDown13(fct);
}

function disableAllAction()
{
  fctClickLink(fctfalse);
  if (estOpera) fctClickForm(0,fcttrue);
  setTimeout("fctClickForm(fctfalse,fctfalse)",1);
  fctKeyDown13(fctfalse);
}
