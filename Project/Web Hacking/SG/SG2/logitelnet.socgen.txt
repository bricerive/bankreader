<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
<TITLE>Soci&eacute;t&eacute; G&eacute;n&eacute;rale : produits et services pour la client&egrave;le de particuliers, gestion de compte en ligne via Logitel Net</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="0">
<META NAME="description" CONTENT="Société Générale : Gérez vos comptes en ligne avec Logitel Net, consultez et réservez nos produits de placement, prêt, assurance, recherchez une agence ou un distributeur">
<META NAME="keywords" CONTENT="Abonnement gratuit, Actions, Agence, Argent, Assurance Auto, Assurance Habitation, Assurance Vie, Banque à distance, Banque à domicile, Banque en ligne, Bourse, Carte bleue,Carte de paiement, Carte espèces, Chèque bancaire, Chèques, Chéquier, Compte, Compte à vue, Compte bancaire, Compte chèque, Compte courant, Compte titres, Consultation, Courtage, Crédit Auto, Crédits immobiliers, Distributeur de billets, Epargne, Etablissement bancaire, Expatriés, FCP, Finance, Fiscalité, Gestion de patrimoine, gestion de portefeuille, Home banking, Immobilier, Investissement, Livret, Logitel Net, OPCVM, Opérations bancaires, Patrimoine, PEA, PEL, Placement, Placements en bourse, Portefeuille titres, Prélèvement bancaire, Prêts Auto, Prêts étudiants, Prêts immobiliers, Prévoyance, Produits bancaires, Service en ligne, Services bancaires, SG, SICAV, Simulations bancaires, Socgen, Société Générale, Solde de compte, Tarifs bancaires, Téléchargement, Télépaiement, Virement, Virement bancaire">
<LINK REL="SHORTCUT ICON" HREF="/img/pac/favicon.ico">
<LINK HREF="/styles/pac.css" REL="stylesheet" TYPE="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="/js/commun/fw_form.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/commun/fw_cs.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/commun/fw_cookies.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/commun/fw_popup.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/commun/fw_secu.js"></SCRIPT>
<script language="javaScript" src="/js/commun/fw_statclient.js"></script>
<SCRIPT LANGUAGE=JavaScript SRC="/js/commun/fw_xml.js"></SCRIPT>
<SCRIPT LANGUAGE=JavaScript SRC="/js/cvcs/ClavierVirtuel.js"></SCRIPT>
<LINK HREF="/styles/LogitelNetClavier.css" REL="stylesheet" TYPE="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="/js/pac/pac.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/pac/pacAuth.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">frame_shield();</SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
EcrireAppelJSBack("/paceix/js/EIFPAC2.js");
function ouvrirFenetre(url,name,options)
{
  window.open(url,name,options);
}
</SCRIPT>
</HEAD>
<BODY BGCOLOR=#FFFFFF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 onUnLoad="javascript:fermerCV();">
<TABLE BORDER=0 WIDTH=971 CELLSPACING=0 CELLPADDING=0>
<!-- BANDEAU DU HAUT -->
  <TR>
    <TD WIDTH=1><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1></TD>            
    <TD WIDTH=129><A HREF="http://www.societegenerale.fr/"><IMG SRC="img/pac/logoSG.gif" BORDER=0 ALIGN=RIGHT VALIGN=MIDDLE ALT="Banque en ligne Société Générale"></A></TD>
    <TD WIDTH=841>	
      <TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0>
      <TR>		  
    	  <TD WIDTH=837 COLSPAN=5 ALIGN=RIGHT VALIGN=TOP class="bandeaunoir">
    	    <IMG BORDER=0 ALIGN=LEFT SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=16>
    	    <A class="bandeaunoir" HREF="http://par.societegenerale.fr/EIP/resources/production/contacter_un_conseiller_par_mail/">Contacter un Conseiller</A> 
    	    | <A class="bandeaunoir" HREF="javascript:ouvrirFenetre('http://www.societegenerale.fr/agence/1,,,00.html','Trouver_une_Agence','width=750,height=480,toolbar=no,scrollbars=yes,resizable=yes,statusbar=no,top=0,left=0')">Trouver une Agence</A> 
    	    | <A class="bandeaunoir" HREF="javascript:ouvrirFenetre('http://par.societegenerale.fr/EIP/resources/recherche/rech_popup.html','Trouver_dans_le_site','width=660,height=540,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,alwaysRaised=yes,system=no,resizable=no,top=0,left=0')">Trouver dans le site</A>
    	    &nbsp;
    	  </TD>
        <TD WIDTH=4><IMG BORDER=0 ALIGN=LEFT SRC="img/pac/magic.gif" WIDTH=4 HEIGHT=1></TD>	  	  
    	</TR>
    	<TR>
    	  <TD WIDTH=19><IMG SRC="img/pac/magic.gif" BORDER=0 WIDTH=19 HEIGHT=1></TD>
    	  <TD WIDTH=388 COLSPAN=3>
    	    <TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH=388 HEIGHT=74>
    	      <TR><TD><IMG SRC="img/pac/magic.gif" BORDER=0 WIDTH=1 HEIGHT=25></TD></TR>
    	      <TR><TD><IMG SRC="img/pac/banque_en_ligne.gif" BORDER=0 ALIGN=LEFT VALIGN=TOP alt="Toute votre banque en ligne"></TD></TR>
    	    </TABLE>
    	  </TD>
    	  <TD><A HREF="http://par.societegenerale.fr/EIP/resources/production/devenir_client/"><IMG SRC="img/pac/devenir_client.jpg" BORDER=0 FLOAT=RIGHT alt="DEVENIR CLIENT"></A></TD>
        <TD WIDTH=4><IMG BORDER=0 ALIGN=LEFT SRC="img/pac/magic.gif" WIDTH=4 HEIGHT=1></TD>
    	</TR>	
    	<TR><TD WIDTH=21 COLSPAN=2><IMG SRC="img/pac/magic.gif" BORDER=0 WIDTH=1 HEIGHT=10></TD><TD WIDTH=816 COLSPAN=3 background="img/pac/degrade_bleu.jpg"><IMG SRC="img/pac/magic.gif" BORDER=0></TD><TD WIDTH=4><IMG BORDER=0 ALIGN=LEFT SRC="img/pac/magic.gif" WIDTH=4 HEIGHT=1></TD></TR>
    	<TR><TD WIDTH=130 COLSPAN=3><IMG SRC="img/pac/magic.gif" BORDER=0 WIDTH=1 HEIGHT=20></TD><TD WIDTH=707 COLSPAN=2 background="img/pac/degrade_rouge.jpg"><IMG SRC="img/pac/magic.gif" BORDER=0></TD><TD WIDTH=4><IMG BORDER=0 ALIGN=LEFT SRC="img/pac/magic.gif" WIDTH=4 HEIGHT=1></TD></TR>
      </TABLE>	  
    </TD>	
  </TR>
<!-- /BANDEAU DU HAUT -->
  <SCRIPT LANGUAGE="JavaScript">TestModeDegrade();</SCRIPT>
  <TR>
<!-- BANDEAU DE GAUCHE -->
    <TD WIDTH=1>
	  <IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1>
	</TD>
    <TD WIDTH=129 background="img/pac/bandeau_gauche_fond.gif" VALIGN=TOP>      
      <TABLE WIDTH=129 BORDER=0 CELLSPACING=0 CELLPADDING=0 background="img/pac/bandeau_gauche_fond.gif">
	    <TR>
		  <TD>
		    <TABLE WIDTH=129 BORDER=0 CELLSPACING=0 CELLPADDING=0 background="img/pac/magic.gif">
			  <TR>
			    <TD WIDTH=129>
				  <TABLE BORDER=0 WIDTH=129 HEIGHT=51 CELLSPACING=0 CELLPADDING=0
				    background="img/pac/menu_gauche_haut.gif">S	ESSS	
					<TR>
					  <TD>
					    <TABLE BORDER=0 WIDTH=129 HEIGHT=51 CELLSPACING=0 CELLPADDING=0
						background="img/pac/magic.gif">
						  <TR>
						    <TD WIDTH=124 ALIGN=RIGHT VALIGN=BOTTOM>
							  <A HREF="/codeClient.html">
							    <IMG border=0 SRC="img/pac/bouton_question.gif" alt="?">
							  </A>
							</TD>
							<TD WIDTH=5>
							  <IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=5 HEIGHT=49>
							</TD>
						  </TR>
						  <TR>
						    <TD WIDTH=129 COLSPAN=2>
							  <IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=2>
							</TD>
						  </TR>
						</TABLE>
					  </TD>
					</TR>
				  </TABLE>
				</TD>
			  </TR>             
			  <TR>
			    <TD>
				  <TABLE BORDER=0 WIDTH=129 HEIGHT=25 CELLSPACING=0 CELLPADDING=0
				  background="img/pac/menu_gauche_fond.gif">
				    <TR>
					  <TD>
					    <TABLE BORDER=0 WIDTH=129 HEIGHT=25 CELLSPACING=0 CELLPADDING=0
						background="img/pac/magic.gif">                                                  
						<form name=authentification method="POST" action="/acces/authlgn.html">
						  <TR>
						    <TD WIDTH=129 COLSPAN=3>

<div id="tc_divdeplace" style="position:absolute; left:-300px; top:-300px;">
<!-- les coordonnées doivent être laissées dans l\'attribut style et non pas dans class
  pour qu\'elles soient bien prises en compte lors du deplacement -->
<table id="tc_cvcs" class="clcvcs">
<tr>
   <td onmousedown="dragStart(event, \'tc_divdeplace\')" class="haut">
    <div>
       <a href="javascript:cacher_clavier()">
	     <img src="/img/cvcs/tc_fermer.gif" alt="FERMER">
	  </a>
   </div>
  </td>
</tr>
<tr>
  <td>
	<div id="tc_text">
	  <div id="tc_votre_code">
	    <img src="/img/cvcs/tc_votre_code.gif" alt="VOTRE CODE SECRET">
	  </div>
	  <div id="tc_pass">
		<table>
	      <tr>
		    <td>
			  <input type="text" id="tc_visu_saisie" value="" readonly size="6">
			</td>
			<td id="tc_aideimg">
			  <a href="/codeSecret.html">
			    <img src="/img/cvcs/bouton_question.gif" alt="?">
			  </a>
		    </td>
		  </tr>
		</table>
	  </div>
	  <!-- CLAVIER -->
	  <table id="tc_clavier">
		<tr>
	      <td>
		    <div class="dropshadow">
		      <div class="innerbox">
		        <ul id="tc_tclavier"></ul>
		      </div>
		    </div>
		  </td>
		</tr>
	  </table>
	  <!-- /CLAVIER -->
	  <div id="tc_boutons">
	    <a class="cvcslienbouton" href="javascript:corriger()">
		  <img id="tc_corriger" src="/img/cvcs/tc_corriger.gif" alt="CORRIGER">
		</a><br/>
		<a class="cvcslienbouton" href="javascript:validation()">
		  <img id="tc_valider" src="/img/cvcs/tc_valider.gif" alt="VALIDER">
		</a>
	  </div>
	</div>
    <div id="tc_fond"></div>
  </td>
</tr>
</table>
</div>



<!-- Codage du clavier virtuel : 
************************************************************************************* -->

<script>
if (document.getElementById)
{
  document.write('<div id="tc_divdeplace" style="position:absolute; left:-300px; top:-300px;">');
  document.write('<!-- les coordonnées doivent être laissées dans l\'attribut style et non pas dans class pour qu\'elles soient bien prises en compte lors du deplacement -->');
//  document.write('<table id="tc_cvcs" class="clcvcs" cellpadding=\'0\' cellspacing=\'0\' height="192" width="229" style="visibility:hidden;">');
  document.write('<table id="tc_cvcs" class="clcvcs">');
  document.write('<tr>');
  document.write('   <td onmousedown="dragStart(event, \'tc_divdeplace\')" class="haut">');
  document.write('    <div>');
  document.write('       <a href="javascript:cacher_clavier()"><img src="/img/cvcs/tc_fermer.gif" alt="FERMER"></a>');
  document.write('   </div>');
  document.write('  </td>');
  document.write('</tr>');
  document.write('<tr><td>');
  document.write('		<div id="tc_text">');
  document.write('			<div id="tc_votre_code"><img src="/img/cvcs/tc_votre_code.gif" alt="VOTRE CODE SECRET"></div>');
  document.write('			<div id="tc_pass"><table><tr><td><input type="text" id="tc_visu_saisie" value="" readonly size="6"></td><td id="tc_aideimg"><a href="/codeSecret.html"><img src="/img/cvcs/bouton_question.gif" alt="?"></a></td></tr></table></div>');
  document.write('			<!-- CLAVIER -->');
  document.write('			<table id="tc_clavier">');
  document.write('			<tr><td>');
  document.write('					<div class="dropshadow">');
  document.write('					<div class="innerbox">');
  document.write('						<ul id="tc_tclavier">');
  document.write('						</ul>');
  document.write('					</div>');
  document.write('					</div>');
  document.write('				</td>');
  document.write('			</tr>');
  document.write('			</table>');
  document.write('			<!-- /CLAVIER -->');
  document.write('			<div id="tc_boutons">');
  document.write('				<a class="cvcslienbouton" href="javascript:corriger()"><img id="tc_corriger" src="/img/cvcs/tc_corriger.gif" alt="CORRIGER"></a><br/>');
  document.write('				<a class="cvcslienbouton" href="javascript:validation()"><img id="tc_valider" src="/img/cvcs/tc_valider.gif" alt="VALIDER"></a>');
  document.write('			</div>');
  document.write('		</div>');
  document.write('	<div id="tc_fond"></div> ');
  document.write('</td></tr>');
  document.write('</table>');
  document.write('</div>');

}
</script>

<!-- ************************************************************************************ -->

<!-- Définition des champs cachés : -->
<input id="cvcs_etat_clavier" type="hidden" value="">
<input id="cvcs_visu_cache" type="hidden" value="">
<input id="cvcs_action" type="hidden" value=""> 
<input id="cvcs_idchampcs" type="hidden" value="">
<input id="cvcs_fvalider" type="hidden" value="">
<input id="cvcs_ffermer" type="hidden" value="">
<input id="cvcs_posx" type="hidden" value="">
<input id="cvcs_posy" type="hidden" value=""> 
<input id="cvcs_estsession" type="hidden" value="">
<input id="cvcs_ferrcompare" type="hidden" value=""> 
<input id="cvcs_idchampcompare" type="hidden" value="">
<input id="cvcs_messcsvide" type="hidden" value=""> 
<input id="cvcs_messcsincomplet" type="hidden" value="">
<input id="cvcs_ferridem" type="hidden" value="">
<input id="cvcs_idchampidem" type="hidden" value="">
<input id="cvcs_messcsidem" type="hidden" value="">
<input id="cryptocvcs" name="cryptocvcs" type="hidden" value="">
<input id="cvcs_nbl" type="hidden" value="">
<input id="cvcs_nbc" type="hidden" value="">
<input id="cvcs_codes" type="hidden" value="">
<input id="cvcs_timeout" type="hidden" value="">

<IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=3></TD></TR>
        <TR>
          <TD WIDTH=8><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=8 HEIGHT=1></TD>
          <TD WIDTH=97>            
            <INPUT class="champscode" ID="codcli" NAME="codcli" TYPE="text" SIZE="8" MAXLENGTH="8" VALUE="" AUTOCOMPLETE="off" TABINDEX="1" onKeyDown="javascript:if(ccSaisi){fermerCV();}"/>      
            <INPUT TYPE="hidden" NAME="codsec" id="codsec" VALUE="">
            <INPUT TYPE="hidden" NAME="codeaction" VALUE="1">
            <INPUT TYPE="hidden" NAME="x" VALUE="">
            <INPUT TYPE="hidden" NAME="y" VALUE="">
            <INPUT TYPE="hidden" NAME="categNav" VALUE="">
            <INPUT TYPE="hidden" NAME="os" VALUE="">
            <INPUT TYPE="hidden" NAME="typeNav" VALUE="">
            <INPUT TYPE="hidden" NAME="versionNav" VALUE="">
          </TD>          
          <TD WIDTH=24 ALIGN=LEFT VALIGN=BOTTOM><A HREF="javascript:controlerPac();" TABINDEX="2"><IMG BORDER=0 SRC="img/pac/bouton_ok.gif" ALT="OK"></A></TD>
        </TR>
        </form>
        <TR><TD WIDTH=129 COLSPAN=3><IMG border=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=3></TD></TR>                                        
      </TABLE></TD></TR></TABLE></TD></TR>            
      <!--Bandeau gifs animés spéciaux (ex : interruption)--><SCRIPT LANGUAGE="JavaScript">EcrireBandeau40();</SCRIPT>
      <TR><TD WIDTH=129><TABLE BORDER=0 WIDTH=129 HEIGHT=14 CELLSPACING=0 CELLPADDING=0 background="img/pac/menu_gauche_fond.gif"><TR><TD><TABLE BORDER=0 WIDTH=129 HEIGHT=14 CELLSPACING=0 CELLPADDING=0 background="img/pac/magic.gif">
        <TR><TD WIDTH=129 COLSPAN=4><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=5></TD></TR>
        <TR>
          <TD WIDTH=19 HEIGHT=14><IMG border=0 SRC="img/pac/bouton_fleche.gif"></TD>
          <TD WIDTH=3><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=3 HEIGHT=1></TD>
          <TD class="itemcell" WIDTH=100><A HREF="http://par.societegenerale.fr/EIP/resources/production/decouvrir_service/" class="item">Découvrir le service</A></TD>
          <TD WIDTH=7><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=7 HEIGHT=1></TD>
        </TR>
      </TABLE></TD></TR></TABLE></TD></TR>            
       <!--Non affiché pour l'instant
       <TR><TD><TABLE BORDER=0 WIDTH=129 HEIGHT=14 CELLSPACING=0 CELLPADDING=0 background="img/pac/menu_gauche_fond.gif"><TR><TD><TABLE BORDER=0 WIDTH=129 HEIGHT=14 CELLSPACING=0 CELLPADDING=0 background="img/pac/magic.gif">
         <TR><TD><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=5></TD></TR>
         <TR>
           <TD WIDTH=19 HEIGHT=14><IMG border=0 SRC="img/pac/bouton_fleche.gif"></TD>
           <TD WIDTH=3><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=3 HEIGHT=1></TD>
           <TD class="itemcell" WIDTH=100><A HREF="http://par.societegenerale.fr/EIP/resources/production/abonnement_BAD" class="item">S'abonner en ligne</A></TD>
           <TD WIDTH=7><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=7 HEIGHT=1></TD>
         </TR>         
         </TABLE></TD></TR></TABLE></TD></TR>
       -->     
       <TR><TD><TABLE BORDER=0 WIDTH=129 HEIGHT=14 CELLSPACING=0 CELLPADDING=0 background="img/pac/menu_gauche_fond.gif"><TR><TD><TABLE BORDER=0 WIDTH=129 HEIGHT=14 CELLSPACING=0 CELLPADDING=0 background="img/pac/magic.gif">
         <TR><TD><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=5></TD></TR>
         <TR>
           <TD WIDTH=19 HEIGHT=14><IMG border=0 SRC="img/pac/bouton_fleche.gif"></TD>
           <TD WIDTH=3><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=3 HEIGHT=1></TD>
           <TD class="itemcell" WIDTH=100><A HREF="javascript:valider('/acces/ia13.html')" class="item">Première connexion</A></TD>
           <TD WIDTH=7><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=7 HEIGHT=1></TD>
         </TR>         
         </TABLE></TD></TR></TABLE></TD></TR>
       <TR><TD><TABLE BORDER=0 WIDTH=129 HEIGHT=14 CELLSPACING=0 CELLPADDING=0 background="img/pac/menu_gauche_fond.gif"><TR><TD><TABLE BORDER=0 WIDTH=129 HEIGHT=14 CELLSPACING=0 CELLPADDING=0 background="img/pac/magic.gif">
         <TR><TD><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=5></TD></TR>
         <TR>
           <TD WIDTH=19 HEIGHT=14><IMG border=0 SRC="img/pac/bouton_fleche.gif"></TD>
           <TD WIDTH=3><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=3 HEIGHT=1></TD>
           <TD class="itemcell" WIDTH=100><A HREF="javascript:valider('/gererCode.html')" class="item">Gérer vos codes</A></TD>
           <TD WIDTH=7><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=7 HEIGHT=1></TD>
         </TR>
       </TABLE></TD></TR></TABLE></TD></TR>
       <TR><TD WIDTH=129><TABLE BORDER=0 WIDTH=129 HEIGHT=38 CELLSPACING=0 CELLPADDING=0 background="img/pac/menu_gauche_bas.gif"><TR><TD><TABLE BORDER=0 WIDTH=129 HEIGHT=38 CELLSPACING=0 CELLPADDING=0 background="img/pac/magic.gif">           
         <TR>           
           <TD WIDTH=123 ALIGN=RIGHT VALIGN=BOTTOM><A HREF="javascript:OpenWindowFocus('/html/faq/faq01.html','FAQ','width=780,height=540,top=0,left=0,toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,alwaysRaised=yes,resizable=yes')"><IMG border=0 SRC="img/pac/bouton_aide.gif" alt="?"></A></TD>
           <TD><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=6 HEIGHT=28></TD>
         </TR>
         <TR><TD COLSPAN=2><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=10></TD></TR>                                 
       </TABLE></TD></TR></TABLE></TD></TR>        
       <TR><TD WIDTH=129><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=5></TD></TR>
       <TR><TD WIDTH=129><A HREF="http://par.societegenerale.fr/EIP/resources/production/souscrire_en_ligne/"><IMG BORDER=0 SRC="img/pac/menu_bg_souscrire.gif" alt="Souscrire en ligne"></A></TD></TR>
       <TR><TD WIDTH=129><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=1></TD></TR>
       <TR><TD WIDTH=129><A HREF="http://www.jazz.societegenerale.fr/"><IMG BORDER=0 SRC="img/pac/menu_bg_jazz.gif" alt="Adhérents JAZZ"></A></TD></TR>
       <TR><TD WIDTH=129><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=1></TD></TR>
       <TR><TD WIDTH=129><A HREF="http://par.societegenerale.fr/EIP/resources/production/services_urgence/"><IMG BORDER=0 SRC="img/pac/menu_bg_urgence.gif" alt="Services d'urgence"></A></TD></TR>
       <TR><TD WIDTH=129><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=5></TD></TR>       
       <SCRIPT LANGUAGE="JavaScript">EcrireBandeau20();</SCRIPT>
       <TR><TD WIDTH=129><IMG SRC="img/pac/indicPacSecure.gif" width="1" height="3"></TR></TD>
      </TABLE></TD></TR></TABLE>
    </TD>
<!-- /BANDEAU DE GAUCHE -->
<!-- CORPS -->
    <TD WIDTH=841 VALIGN=TOP><TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0>
      <TR>    
        <TD WIDTH=710 VALIGN=TOP><TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0>
           <TR>
             <TD WIDTH=1><IMG SRC="img/pac/magic.gif" BORDER=0 WIDTH=1 HEIGHT=1></TD>  
             <TD WIDTH=709 COLSPAN=3><SCRIPT LANGUAGE="JavaScript">EcrireBandeau10();</SCRIPT></TD>
           </TR>
           <TR><TD WIDTH=710 COLSPAN=4><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=8></TD></TR>                                 
           <TR>
             <TD WIDTH=1><IMG SRC="img/pac/magic.gif" BORDER=0 WIDTH=1 HEIGHT=1></TD> 
             <TD WIDTH=263 VALIGN=TOP><TABLE BORDER=0 WIDTH=263><TR><TD>
               <SCRIPT LANGUAGE="JavaScript">PreparerActus();</SCRIPT>
        	     <SCRIPT LANGUAGE="JavaScript">EcrireActu(0);</SCRIPT>        
		           </TD></TR></TABLE> 	
		         </TD>
		         <TD WIDTH=263 VALIGN=TOP><TABLE BORDER=0 WIDTH=263><TR><TD>
 	             <SCRIPT LANGUAGE="JavaScript">EcrireActu(1);</SCRIPT>
               </TD></TR></TABLE>
		         </TD>
		         <TD WIDTH=183 ALIGN=CENTER VALIGN=TOP><A HREF="http://par.societegenerale.fr/EIP/resources/production/contacter_un_conseiller_par_mail/"><IMG WIDTH=132 HEIGHT=101 BORDER=0 SRC="img/pac/contacter.jpg" alt="Contacter un Conseiller" /></A></TD>		
	         </TR>
	         <TR><TD WIDTH=710 COLSPAN=4><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=12></TD></TR>
 	         <TR>
             <TD WIDTH=1><IMG SRC="img/pac/magic.gif" BORDER=0 WIDTH=1 HEIGHT=1></TD> 
         	   <TD WIDTH=263 VALIGN=TOP><TABLE BORDER=0 WIDTH=263><TR><TD>
  	           <SCRIPT LANGUAGE="JavaScript">EcrireActu(2);</SCRIPT>
		           </TD></TR></TABLE>	
		         </TD>
		         <TD WIDTH=263 VALIGN=TOP><TABLE BORDER=0 WIDTH=263><TR><TD>
	             <SCRIPT LANGUAGE="JavaScript">EcrireActu(3);</SCRIPT>
		           </TD></TR></TABLE>	
		         </TD>
		         <TD WIDTH=183 ALIGN=CENTER VALIGN=TOP><A HREF="javascript:ouvrirFenetre('http://www.societegenerale.fr/agence/1,,,00.html','Trouver_une_Agence','width=750,height=480,toolbar=no,scrollbars=yes,resizable=yes,statusbar=no,top=0,left=0')"><IMG WIDTH=132 HEIGHT=101 BORDER=0 SRC="img/pac/trouver.jpg" alt="Trouver une agence, un distributeur" /></A></TD>
	         </TR>
	         <TR><TD WIDTH=710 COLSPAN=4><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=10></TD></TR>                              
        </TABLE></TD>
        <TD VALIGN=TOP><TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 background="img/pac/menu_bd_fond.gif"><TR><TD><TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 background="img/pac/magic.gif">
<!-- /BANDEAU DE DROITE -->                        
          <TR><TD COLSPAN=5><A HREF="http://par.societegenerale.fr/EIP/resources/production/Home/quotidien/"><IMG BORDER=0 SRC="img/pac/menu_droit_votre_argent.gif" alt="Votre argent au quotidien" /></A></TD></TR>
	        <TR><TD COLSPAN=5><A HREF="http://par.societegenerale.fr/EIP/resources/production/Home/prets_conso/"><IMG BORDER=0 SRC="img/pac/menu_droit_prets_conso.gif" alt="Prêts à la consommation" /></A></TD></TR>
	        <TR><TD COLSPAN=5><A HREF="http://par.societegenerale.fr/EIP/resources/production/Home/prets_immo/"><IMG BORDER=0 SRC="img/pac/menu_droit_prets_immo.gif" alt="Prêts immobiliers" /></A></TD></TR>         
	        <TR><TD COLSPAN=5><A HREF="http://par.societegenerale.fr/EIP/resources/production/Home/placements/"><IMG BORDER=0 SRC="img/pac/menu_droit_epargne.gif" alt="Epargne et placements" /></A></TD></TR>
	        <TR><TD COLSPAN=5><A HREF="http://par.societegenerale.fr/EIP/resources/production/Home/bourse/"><IMG BORDER=0 SRC="img/pac/menu_droit_bourse.gif" alt="Investir en Bourse" /></A></TD></TR>             
	        <TR><TD COLSPAN=5><A HREF="http://par.societegenerale.fr/EIP/resources/production/Home/assurances/"><IMG BORDER=0 SRC="img/pac/menu_droit_assurance.gif" alt="Assurance et prévoyance" /></A></TD></TR>                         
	        <TR><TD COLSPAN=5><A HREF="http://par.societegenerale.fr/EIP/resources/production/tous_produits/"><IMG BORDER=0 SRC="img/pac/menu_bd_produits.gif" alt="Tous les produits" /></A></TD></TR>
	        <TR><TD COLSPAN=5><A HREF="http://par.societegenerale.fr/EIP/resources/production/tous_tarifs/"><IMG BORDER=0 SRC="img/pac/menu_bd_tarifs.gif" alt="Tous les tarifs" /></A></TD></TR>	
          <SCRIPT LANGUAGE="JavaScript">EcrireFocus();</SCRIPT>		
<!-- /BANDEAU DE DROITE -->
        </TABLE></TR></TD></TABLE></TD>
     </TR></TABLE></TD>    
<!-- /CORPS -->
  </TR>
<!-- BANDEAU DU BAS -->
  <TR>     
  	<TD COLSPAN=2 WIDTH=130><A HREF="http://www.socgen.com"><IMG BORDER=0 SRC="img/pac/logoSGbas.gif" ALT="GROUPE SOCIETE GENERALE" /></A></TD>    
	  <TD>
       <TABLE>
	     <TR>
         <TD WIDTH=711 ALIGN=RIGHT VALIGN=TOP class="bandeaunoir"><IMG BORDER=0 ALIGN=LEFT SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=15><SCRIPT LANGUAGE="JavaScript">EcrireStat();</SCRIPT><A class="bandeaunoir" HREF="http://par.societegenerale.fr/EIP/resources/production/configuration_securite/">Configuration et sécurité</A> | <A class="bandeaunoir" HREF="http://par.societegenerale.fr/EIP/resources/production/nos_engagements/">Nos engagements</A> | <A class="bandeaunoir" HREF="http://par.societegenerale.fr/EIP/resources/production/mentions_legales/">Mentions légales et conditions générales</A>&nbsp;&nbsp;</TD>	
	       <TD WIDTH=131 ALIGN=LEFT VALIGN=TOP class="copyright">&nbsp;&copy; Soci&eacute;t&eacute; G&eacute;n&eacute;rale 2005</TD>		
	     </TR>  
	     </TABLE>
	  </TD>
  </TR>
<!-- /BANDEAU DU BAS -->
</TABLE>
<SCRIPT LANGUAGE="JavaScript">
var frm = document.forms[0];
function initFocus()
{
  if(frm)
  {
    if(frm.codcli) {frm.codcli.value="";frm.codcli.focus();}
  }
}
function validerCV()
{
  initCVCS();
  genAffSaisirCVCS(142,125,562,214,(GetCookie("idUser")!=null)?1:0,'codsec','envoyerCV()','fermerCV(1)');
}
initForm();
initFocus();
</SCRIPT>
</BODY>
</HTML>

