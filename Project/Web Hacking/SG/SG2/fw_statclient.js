var os = "Autres";
var typeNav = "Autres";
var versionNav = "Autres";

function detectClient()
{
  var agent = navigator.userAgent.toLowerCase();
  if ( (agent.indexOf("win")!=-1) || (agent.indexOf("16bit")!=-1) )
    os = "Windows";
  else if (agent.indexOf("mac")!=-1)
    os = "Mac";
  else if ( (agent.indexOf("os/2")!=-1) || (navigator.appVersion.indexOf("OS/2")!=-1) || (agent.indexOf("ibm-webexplorer")!=-1))
    os = "Os2";
  else if ( (agent.indexOf("inux")!=-1) || (agent.indexOf("x11")!=-1) ||
(agent.indexOf("sunos")!=-1) || (agent.indexOf("bsd")!=-1) ||
(agent.indexOf("irix") !=-1) || (agent.indexOf("hp-ux")!=-1) ||
(agent.indexOf("aix")!=-1) || (agent.indexOf("sco")!=-1) ||
(agent.indexOf("unix_sv")!=-1) || (agent.indexOf("unix_system_v")!=-1) ||
(agent.indexOf("ncr")!=-1) || (agent.indexOf("reliantunix")!=-1) ||
(agent.indexOf("dec")!=-1) || (agent.indexOf("osf1")!=-1) ||
(agent.indexOf("dec_alpha")!=-1) || (agent.indexOf("alphaserver")!=-1) ||
(agent.indexOf("ultrix")!=-1) || (agent.indexOf("alphastation")!=-1) ||
(agent.indexOf("sinix")!=-1) )
    os = "Unix";
  else if ( (agent.indexOf("vax")!=-1) || (agent.indexOf("openvms")!=-1))
    os = "Vms";

  if (agent.indexOf("opera")!=-1)
  {
    typeNav = "Opera";
    if ( (agent.indexOf("opera 2")!=-1) || (agent.indexOf("opera/2") != -1) )
      versionNav = "2";
    else if ( (agent.indexOf("opera 3")!=-1) || (agent.indexOf("opera/3")!=-1) )
      versionNav = "3";
    else if ( (agent.indexOf("opera 4")!=-1) || (agent.indexOf("opera/4")!=-1) )
      versionNav = "4";
    else if ( (agent.indexOf("opera 5")!=-1) || (agent.indexOf("opera/5")!=-1) )
      versionNav = "5";
    else if ( (agent.indexOf("opera 6")!=-1) || (agent.indexOf("opera/6")!=-1) )
      versionNav = "6";
    else if ( (agent.indexOf("opera 7")!=-1) || (agent.indexOf("opera/7")!=-1) )
      versionNav = "7";
  }
  else if (agent.indexOf('gecko')!=-1)
  {
    typeNav = "Gecko";
    var rvValue = 0;
    var rvStart = agent.indexOf('rv:');
    var rvEnd   = agent.indexOf(')', rvStart);
    var rv      = agent.substring(rvStart+3, rvEnd);
    var rvParts = rv.split('.');
    var exp     = 1;

    for (var i = 0; i < rvParts.length; i++)
    {
      var val = parseInt(rvParts[i]);
      rvValue += val / exp;
      exp *= 100;
    }
    versionNav = rvValue;
  }
  else if (agent.indexOf("msie")!=-1)
  {
    typeNav = "Msie";
    var rg = new RegExp("msie ([^;]*)");
    var r = rg.exec(agent);
    var versionIE = r? r[1]: "";
    if (versionIE != "")
      versionNav = parseFloat(versionIE);
  }
  else if ( (agent.indexOf("webtv")!=-1) || (agent.indexOf("navio")!=-1) || (agent.indexOf("navio_aoltv")!=-1) )
  {
    typeNav = "TV";
    if (agent.indexOf("webtv")!=-1)
    {
      versionNav = "Webtv";
    }
    else if (agent.indexOf("navio_aoltv")!=-1)
    {
      versionNav = "Aol";
    }
  }
}

function StatConnect()
{
  var indicConnect = new Image;
  var indicConnectType = new Image;
  if (document.images)
  {
    indicConnect.src = "/img/acces/indicConnect.gif?x=" + screen.width + "&y=" + screen.height;
    detectClient();
    var agent = navigator.userAgent.toLowerCase();
    var isNS4 = (agent.indexOf("mozilla/4.") != -1 && agent.indexOf("mozilla/4.0") == -1) ? 1 : 0;
    if (isNS4)
    {
      typeNav = "NS4";
      var rg = new RegExp("mozilla/([0-9.]+) ");
      var r = rg.exec(agent);
      var versionNS = r? r[1]: "";
      if (versionNS != "")
        versionNav = parseFloat(versionNS);
      indicConnectType.src = "/img/acces/indicConnectNS4.gif?os=" + os + "&typeNav=" + typeNav + "&versionNav=" + versionNav;
    }
    else if (document.getElementById)
      indicConnectType.src = "/img/acces/indicConnectW3C.gif?os=" + os + "&typeNav=" + typeNav + "&versionNav=" + versionNav;
    else
      indicConnectType.src = "/img/acces/indicConnectNonW3C.gif?os=" + os + "&typeNav=" + typeNav + "&versionNav=" + versionNav;
  }
}

function redirectMacIe()
{
    detectClient();
    if ((os == "Mac") && (typeNav == "Msie")) 
    {
    	window.document.location.replace("/navig01.html");	
    }
}

function redirectOpera_under_7_6()
{
    //detectClient();
    var top=0;    
		if (typeNav == "Opera")
    {
      var op_v=0;
      if( window.opera ) {
        try {      
          op_v=parseFloat(opera.version());
          if (op_v < 7.6) { 
             top=1; 
          }
        }
        catch(ex) {
          top=1;
        }            
      }
			else top=1;
			if (top == 1) {
          window.document.location.replace("/navig02.html");			
			} 
    }			
}


