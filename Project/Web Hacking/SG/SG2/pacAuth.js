var MSG_PAC_001="Vous devez saisir votre code client.";
var MSG_PAC_002="La saisie de votre code client est incorrecte. Pour en savoir plus, cliquez sur le ? � c�t� du code client.";
var MSG_PAC_003="Vous devez saisir votre code secret.";
var MSG_PAC_004="La saisie de votre code secret est incorrecte. Pour en savoir plus, cliquez sur le ? � c�t� du code secret.";
var lien_secu = "http://par.societegenerale.fr/EIP/resources/production/actu_logitelnet/";

var acces = GetCookie("acces");
if (acces == null || acces == "NN")
{
SetCookie("acces","NN",null,"/",null,true);
SetCookie("type","NN",null,"/",null,true);
DeleteCookie("idUser",null,null,"/",null,true);
DeleteCookie("Crypto",null,null,"/",null,true);
}

var onverif = false;
var ccSaisi = false;

function controlerPac()
{
  var ret = false;
  var categNav= "";
  var frm = document.forms["authentification"];

  if(frm && !onverif)
  {
    onverif = true;
    ret = ctrlSaisie(frm.codcli,MSG_PAC_001,MSG_PAC_002,"",false,estNumString,8);
    //if(ret) ret = ctrlSaisie(frm.codsec,MSG_PAC_003,MSG_PAC_004,"",false,estNumString,6);
    if(ret)
    {
      detectClient();
      var agent = navigator.userAgent.toLowerCase();
      var isNS4 = (agent.indexOf("mozilla/4.") != -1 && agent.indexOf("mozilla/4.0") == -1) ? 1 : 0;
      if (isNS4)
      {
        var rg = new RegExp("mozilla/([0-9.]+) ");
        var r = rg.exec(agent);
        versionNav = r? r[1]: "";
        categNav= "NS4";
        typeNav = "NS4";
      }
      else if (document.getElementById)
      { categNav="W3C";}
      else
      { categNav= "NonW3C";}
      frm.x.value=screen.width;
      frm.y.value=screen.height;
      frm.os.value=os;
      frm.categNav.value=categNav;
      frm.typeNav.value=typeNav;
      frm.versionNav.value=versionNav;
	if (isNS4)
	{window.document.location.href = "/navig00.html";}
	else if(!ccSaisi)
	{ ccSaisi=true; validerCV();}
    }
    onverif = false;
  }
}
var os = "Autres";
var typeNav = "Autres";
var versionNav = "Autres";

function detectClient()
{
	var agent = navigator.userAgent.toLowerCase();
	if ( (agent.indexOf("win")!=-1) || (agent.indexOf("16bit")!=-1) )
		os = "Windows";
	else if (agent.indexOf("mac")!=-1)
		os = "Mac";
	else if ( (agent.indexOf("os/2")!=-1) || (navigator.appVersion.indexOf("OS/2")!=-1) || (agent.indexOf("ibm-webexplorer")!=-1))
		os = "Os2";
	else if ( (agent.indexOf("inux")!=-1) || (agent.indexOf("x11")!=-1) ||
			  (agent.indexOf("sunos")!=-1) || (agent.indexOf("bsd")!=-1) ||
			  (agent.indexOf("irix") !=-1) || (agent.indexOf("hp-ux")!=-1) ||
			  (agent.indexOf("aix")!=-1) || (agent.indexOf("sco")!=-1) ||
			  (agent.indexOf("unix_sv")!=-1) || (agent.indexOf("unix_system_v")!=-1) ||
			  (agent.indexOf("ncr")!=-1) || (agent.indexOf("reliantunix")!=-1) ||
			  (agent.indexOf("dec")!=-1) || (agent.indexOf("osf1")!=-1) ||
			  (agent.indexOf("dec_alpha")!=-1) || (agent.indexOf("alphaserver")!=-1) ||
			  (agent.indexOf("ultrix")!=-1) || (agent.indexOf("alphastation")!=-1) ||
			  (agent.indexOf("sinix")!=-1) )
		os = "Unix";
	else if ( (agent.indexOf("vax")!=-1) || (agent.indexOf("openvms")!=-1))
		os = "Vms";
	
	if (agent.indexOf("opera")!=-1)
	{
		typeNav = "Opera";
		if ( (agent.indexOf("opera 2")!=-1) || (agent.indexOf("opera/2") != -1) )
			versionNav = "2";
		else if ( (agent.indexOf("opera 3")!=-1) || (agent.indexOf("opera/3")!=-1) )
			versionNav = "3";
		else if ( (agent.indexOf("opera 4")!=-1) || (agent.indexOf("opera/4")!=-1) )
			versionNav = "4";
		else if ( (agent.indexOf("opera 5")!=-1) || (agent.indexOf("opera/5")!=-1) )
			versionNav = "5";
		else if ( (agent.indexOf("opera 6")!=-1) || (agent.indexOf("opera/6")!=-1) )
			versionNav = "6";
		else if ( (agent.indexOf("opera 7")!=-1) || (agent.indexOf("opera/7")!=-1) )
			versionNav = "7";
	}
	else if (agent.indexOf('gecko')!=-1)
	{
		typeNav = "Gecko";
		var rvValue = 0;
		var rvStart = agent.indexOf('rv:');
		var rvEnd   = agent.indexOf(')', rvStart);
		var rv      = agent.substring(rvStart+3, rvEnd);
		var rvParts = rv.split('.');
		var exp     = 1;
		
		for (var i = 0; i < rvParts.length; i++)
		{
			var val = parseInt(rvParts[i]);
			rvValue += val / exp;
			exp *= 100;
		}
		versionNav = rvValue;
	}
	else if (agent.indexOf("msie")!=-1)
	{
		typeNav = "Msie";
		var rg = new RegExp("msie ([^;]*)");
		var r = rg.exec(agent);
		var versionIE = r? r[1]: "";
		if (versionIE != "")
			versionNav = parseFloat(versionIE);
	}
	else if ( (agent.indexOf("webtv")!=-1) || (agent.indexOf("navio")!=-1) || (agent.indexOf("navio_aoltv")!=-1) )
	{
		typeNav = "TV";
		if (agent.indexOf("webtv")!=-1)
		{
			versionNav = "Webtv";
		}
		else if (agent.indexOf("navio_aoltv")!=-1)
		{
			versionNav = "Aol";
		}
	}
}

function controlerCV()
{ var ret = false;
  if(frm && !onverif)
  {onverif = true;
   if(document.getElementById)
   {
     if(ctrlSaisie(frm.codcli,MSG_PAC_001,MSG_PAC_002,'',false,estNumString,8))
     { if(!ccSaisi){ccSaisi=true; validerCV();}}
   }else 
   {window.document.location.href = '/navig00.html';}
   onverif = false;
  }
}
function envoyerCV()
{
  fermerCVCS();
  if(frm){frm.submit();disableAllAction();}
}
function fermerCV(vider)
{
  fermerCVCS();
  ccSaisi=false;
  if(vider)initFocus();
}
function activTexte(i,on)
{
    var etape = document.getElementById("etape" + i);
    var nombre = document.getElementById("nombre" + i);

    if (etape && nombre)
    {
      if(on)
      { 
	etape.className = "titrenoir";
	nombre.className = "nombrerouge";
      }
      else
      {
	etape.className = "textegris";
	nombre.className = "textegris";
      } 
    }
}
