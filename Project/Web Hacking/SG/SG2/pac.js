var dayJour;
var modeDegrade = false;
var msg = "";

function GoLGN(page)
{
  var hostLGN;
  if (window.location.host.search("logitelnet.socgen.com") != -1)
    hostLGN = "https://" + window.location.host.replace("www.","");
  else
    hostLGN = "https://logitelnet.socgen.com";
  window.location.href = hostLGN + page;
}

function valider(page)
{
  if (document.getElementById)
    GoLGN(page);
  else
    window.location.href = "/navig00.html";
}

function HostNameBackEi()
{
  var protocol = document.location.protocol;
  var hostname = document.location.hostname;
  var hostnamebackei;
  if (hostname.search("acs") != -1)
    hostnamebackei = "";
  else if (document.location.port != "")
  {
    if (protocol == "https:")
      hostnamebackei = "https://eifv6.cypher.secure.socgen.com:4443";
    if (protocol == "http:")
      hostnamebackei = "http://eifv6.cypher.socgen.com:4080";
  }
  else if (hostname.search("homo") != -1)
    hostnamebackei = protocol + "//www.homooffre2.societegenerale.fr";
  else
    hostnamebackei = protocol + "//www.offre2.societegenerale.fr";
  return hostnamebackei;
}

function EcrireAppelJSBack(urlJS)
{
  var lien = HostNameBackEi();
  var urlJSBack = lien + urlJS;
  document.write('<SCRIPT LANGUAGE="JavaScript" SRC="' + urlJSBack + '"></SCRIPT>');
}

function TestModeDegrade()
{
  if (TableauActus.length==0)
    modeDegrade = true;
}

var TableauActus = new Array();
var j = 0;
function Actu(id,titre,texte,url,image)
{
  this.id = id;
  this.titre = titre;
  this.texte = texte;
  this.url = url;
  this.image = image;
}

var bandeau_10;
var bandeau_20;
var bandeau_40;

function Bandeau(swf,gif,url,title)
{
  this.swf = swf;
  this.gif = gif;
  this.url = url;
  this.title = title;
}

function EcrireBandeau(bandeau,pref_code,suff_code,width,height,top_force)
{
  // - width & height obligatoires
  // - attention au param�tre top_force : si on �crit un bandeau flash, width et height sont forc�s de toute fa�on,
  //   en revanche si on �crit un bandeau image, on ne force width et height que si top_force diff�rent de 0 
  var isflash=false;
  var swf_url="";
  var gif_url="";
  var title_bandeau="";
  var Code="";
  var lien=HostNameBackEi();
  if ((bandeau != null) && ((bandeau.swf != "") || (bandeau.gif != "")))
  {
    if (bandeau.swf != "") isflash=true;
    if (bandeau.title) title_bandeau=bandeau.title;
    if (modeDegrade)
    {
      if (isflash) swf_url = bandeau.swf;
      else gif_url = bandeau.gif;
    }
    else
    { 	        
      if (isflash) swf_url = lien + bandeau.swf;
      else gif_url = lien + bandeau.gif;
    }    
		Code += pref_code;
    if (isflash) Code += EcrireBandeauFlash(swf_url,width,height);      
    else Code += EcrireBandeauImg(gif_url,bandeau.url,title_bandeau,width,height,top_force);               
    Code += suff_code;                    
    document.write(Code) ;
  }  
}

function EcrireBandeauFlash(swf_url,width,height)
{
  var Code="";
  var W="";
  var H="";
  if (width != "") W = ' width=' + width ;
  if (height != "") H = ' height=' + height ;    
  Code += '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="https://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"';
  Code += W + H;
  Code += '>\n' ;
  Code += '<param name=movie value="' + swf_url + '">\n' ;
  Code += '<param name=quality value=high>\n';
  Code += '<embed src="' + swf_url + '" quality=high pluginspage="https://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash"';
  Code += W + H;
  Code += '>\n';
  Code += '</embed>\n';
  Code += '</object>\n';  
  return Code;
}

function EcrireBandeauImg(img_url,target_url,title,width,height,top_force)
{
  var Code="";
  var W="";
  var H="";
  if ((top_force != 0) && (width != "")) W = ' width=' + width ;
  if ((top_force != 0) && (height != "")) H = ' height=' + height ;  
  Code += '<a href="' + target_url + '"><img border=0' + W + H + ' src="' + img_url +'" title="' + title + '" /></a>';
  return Code;
}

function EcrireBandeau10()
{
  if (modeDegrade) 
  {
    bandeau_10 = new Bandeau("", "/img/pac/bandeau_promo_d.jpg", "http://par.societegenerale.fr/EIP/resources/production/Home/prets/prets_perso/expresso_prets_perso/", "Pr&ecirc;t Expresso");        
  }
  EcrireBandeau(bandeau_10,'','\n',709,189,1);  
}

function EcrireBandeau20()
{
  if (modeDegrade) 
  {
    bandeau_20 = new Bandeau("", "/img/pac/menu_bg_mobile_d.gif", "http://par.societegenerale.fr/EIP/resources/production/Home/quotidien/rechargement_mobile", "Recharger votre mobile");
  }
  EcrireBandeau(bandeau_20,'<TR><TD WIDTH=129>','</TD></TR>\n<TR><TD WIDTH=129><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=5></TD></TR>\n',129,35,1);  
}

function EcrireBandeau40()
{
  EcrireBandeau(bandeau_40,'<TR><TD WIDTH=129>\n<TABLE BORDER=0 WIDTH=129 CELLSPACING=0 CELLPADDING=0 background="img/pac/menu_gauche_fond.gif">\n<TR><TD WIDTH=129>\n<TABLE BORDER=0 WIDTH=129 CELLSPACING=0 CELLPADDING=0 background="img/pac/magic.gif">\n<TR><TD WIDTH=129><IMG border=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=5></TD></TR>\n<TR><TD WIDTH=129 ALIGN=CENTER>','</TD></TR>\n<TR><TD WIDTH=129><IMG border=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=5></TD></TR>\n</TABLE></TD></TR>\n</TABLE>\n</TD></TR>\n',129,63,0);  
}

function RotationActus()
{
  var dateJour = new Date();
  dayJour = dateJour.getDate();
  var test = Math.ceil(dayJour/3);
  if (test>5)
  {
    if (test==11)
      test=5;
    else
      test=test-5;
  }
  test=test-1;
  var TableauTampon = new Array();
  for (var cpt=0; cpt<5; cpt++)
  {
    TableauTampon[cpt] = new Actu(cpt, TableauActus[cpt].titre, TableauActus[cpt].texte, TableauActus[cpt].url, TableauActus[cpt].image);
  }
  for (var cpt=0; cpt<5; cpt++)
  {
    var indice = cpt-test;
    if (indice<0)
      indice = indice + 5;  
    TableauActus[cpt] = TableauTampon[indice];    
  }
}
function PreparerActus()
{
  if (modeDegrade)
  {  
    j=0;
    TableauActus[j] = new Actu(j++, "Indisponibilit&eacute; des formulaires", "Nos formulaires de contact et de souscription sont actuellement indisponibles. Veuillez nous excuser pour la g&ecirc;ne occasionn&eacute;e.", "","/img/pac/formulaire_d.jpg");		    
    TableauActus[j] = new Actu(j++, "Un projet immobilier ?", "Calculez votre capacit&eacute; d'acquisition et d&eacute;couvrez l'ensemble de nos solutions, pour pr&eacute;parer ou financer votre projet immobilier.", "http://par.societegenerale.fr/EIP/resources/production/Home/prets/prets_immo/","/img/pac/immobilier_d.jpg");
    TableauActus[j] = new Actu(j++, "Vocalia, pour garder le contact", "Avec le 0892 707 707, vous suivez vos comptes 24h/24, vous disposez de services d'assistance et pouvez contacter un Conseiller du lundi au vendredi, de 8 heures &agrave; 22 heures.", "http://par.societegenerale.fr/EIP/resources/production/Home/quotidien/suivre_comptes_quotidien/vocalia_quotidien/","/img/pac/vocalia_d.jpg");
    TableauActus[j] = new Actu(j++, "Sp&eacute;cial automobile", "Avez-vous pens&eacute; &agrave; la formule &laquo; tout compris &raquo; pour mensualiser l'ensemble de vos d&eacute;penses : financement, assurance, entretien, assistance 24h24 ?", "http://par.societegenerale.fr/EIP/resources/production/Home/prets/special_auto/cle_contact/","/img/pac/automobile_d.jpg");
  }
  if (!modeDegrade)
  {     
    RotationActus();
  } 
}

function EcrireActu(num_actu)
{  
  var Code = "";
  var lien=HostNameBackEi(); 
  Code += '<TR>\n';
  Code += '<TD WIDTH=23><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=23 HEIGHT=1></TD>\n';
  Code += '<TD class="titactu" COLSPAN=2><B>' + TableauActus[num_actu].titre + '</B></TD></TR>\n';
  Code += '<TR><TD COLSPAN=3><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=5></TD></TR>\n';
  Code += '<TR>\n';
  Code += '<TD WIDTH=23><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=23 HEIGHT=1></TD>\n';
  Code += '<TD WIDTH=70 VALIGN=TOP><img border=0 WIDTH=65 src="';
  if (!modeDegrade)
  {
    Code += lien;
  } 
  Code += TableauActus[num_actu].image +'" alt=""></TD>\n';
  
  Code += '<TD WIDTH=170 VALIGN=TOP>' + TableauActus[num_actu].texte + '<BR>\n';
  if (TableauActus[num_actu].url != "")
  {
    Code += '<A class="titactu" href="' + TableauActus[num_actu].url + '">&gt;&gt;&gt;</A>\n' ;
  }
  Code += '</TD></TR>\n';            
  document.write(Code);                        		      		      		    		    		      		     			        
}
                        
var TableauFocus = new Array();
var i = 0;
function Focus(id,titre,url,title)
{
  this.id = id;
  this.titre = titre;
  this.url = url;
  this.title = title;
}
function EcrireFocus()
{
  if (modeDegrade)
  {
    i=0;
    TableauFocus[i] = new Focus(i++, "Moins de 25 ans", "http://jeunes.societegenerale.fr/home/","Site jeunes Soci�t� G�n�rale");
    TableauFocus[i] = new Focus(i++, "Client�les Expatri�es", "http://par.societegenerale.fr/EIP/resources/production/THEME/clienteles_expatriees/","Client�les expatri�es");
    TableauFocus[i] = new Focus(i++, "Le Club Soci�t� G�n�rale American Express", "http://par.societegenerale.fr/EIP/resources/production/club/","Club Soci�t� G�n�rale American Express");
    TableauFocus[i] = new Focus(i++, "Gestion de patrimoine", "http://par.societegenerale.fr/EIP/resources/production/THEME/espace_patrimoine/","Gestion de Patrimoine");
    TableauFocus[i] = new Focus(i++, "Soci�t� G�n�rale Gestion Priv�e", "http://par.societegenerale.fr/EIP/resources/production/THEME/gestion_privee/","Soci�t� G�n�rale Gestion Priv�e");
  }
  if (TableauFocus.length!=0)
  {
    var Code = "";
    var title = "";              
    for (var i in TableauFocus)
    {
      if (!TableauFocus[i].title)
        title="";
      else
        title=TableauFocus[i].title;
      if (i == 0)
      {
        Code += '<TR><TD COLSPAN=5>\n';
        Code += '<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 background="img/pac/menu_droit_ombre_haut.gif"><TR><TD><TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 background="img/pac/magic.gif">\n';
        Code += '<TR>\n';
        Code += '<TD WIDTH=1><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=2></TD>\n';
        Code += '<TD WIDTH=126 COLSPAN=3 class="premier_focus"><IMG BORDER=0 SRC="img/pac/magic.gif" HEIGHT=5></TD>\n';
        Code += '<TD WIDTH=4><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=4 HEIGHT=2></TD>\n';
        Code += '</TR>\n';
        Code += '<TR>\n';
        Code += '<TD WIDTH=1><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=1></TD>\n';
        Code += '<TD WIDTH=16 VALIGN=TOP class="premier_focus">&nbsp;&nbsp;&gt;</TD>\n';
        Code += '<TD WIDTH=106 class="premier_focus">\n';
        Code += '<A class="focus" HREF="' + TableauFocus[i].url + '" TITLE="' + title + '">' + TableauFocus[i].titre + '</A>\n';
        Code += '</TD>\n';
        Code += '<TD WIDTH=4 class="premier_focus">&nbsp;</TD>\n';
        Code += '<TD WIDTH=4><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=4 HEIGHT=1></TD>\n';	  	    
        Code += '</TR>\n';
        Code += '</TABLE></TD></TR></TABLE>\n';
        Code += '</TD></TR>\n';	        
      }
      else
      {
        Code += '<TR>\n';
        Code += '<TD WIDTH=1><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=2></TD>\n';	  
        Code += '<TD WIDTH=16 class="focus"><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=16 HEIGHT=2></TD>\n';
        Code += '<TD WIDTH=106 class="focus"><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=106 HEIGHT=2></TD>\n';
        Code += '<TD WIDTH=4 class="focus"><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=4 HEIGHT=2></TD>\n';	  	  
        Code += '<TD WIDTH=4><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=4 HEIGHT=2></TD>\n';
        Code += '</TR>\n';
        Code += '<TR>\n';
        Code += '<TD WIDTH=1><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=1></TD>\n';
        Code += '<TD WIDTH=16 VALIGN=TOP class="focus">&nbsp;&nbsp;&gt;</TD>\n';
        Code += '<TD WIDTH=106 class="focus">\n';
        Code += '<A class="focus" HREF="' + TableauFocus[i].url + '" TITLE="' + title + '">' + TableauFocus[i].titre + '</A>\n';
        Code += '</TD>\n';
        Code += '<TD WIDTH=4 class="focus">&nbsp;</TD>\n';
        Code += '<TD WIDTH=4><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=4 HEIGHT=1></TD>\n';
        Code += '</TR>\n';             
      }
      Code += '<TR>\n';
      Code += '<TD WIDTH=1><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=2></TD>\n';	  
      Code += '<TD WIDTH=16 class="focus"><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=16 HEIGHT=2></TD>\n';
      Code += '<TD WIDTH=106 class="focus"><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=106 HEIGHT=2></TD>\n';
      Code += '<TD WIDTH=4 class="focus"><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=4 HEIGHT=2></TD>\n';	  	  
      Code += '<TD WIDTH=4><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=4 HEIGHT=2></TD>\n';
      Code += '</TR>\n';	
      Code += '<TR><TD WIDTH=1><IMG BORDER=0 SRC="img/pac/magic.gif" WIDTH=1 HEIGHT=1></TD></TR>\n';        
    } 
    document.write(Code);
  }  
}

function webo_zpi(_WEBOZONE,_WEBOPAGE,_WEBOID)
{
  var result = "";
  var wbs_da = new Date();
  wbs_da = parseInt(wbs_da.getTime()/1000 - 60*wbs_da.getTimezoneOffset());
  var wbs_ref = '' + escape(document.referrer);
  var wbs_ta = '0x0';
  var wbs_co = 0;
  var wbs_nav = navigator.appName;
  if (parseInt(navigator.appVersion)>=4)
  {
    wbs_ta = screen.width + "x" + screen.height;
    wbs_co = (wbs_nav!="Netscape") ? screen.colorDepth : screen.pixelDepth;
  }
  var wbs_arg = ".weborama.fr/fcgi-bin/comptage.fcgi?ID=" + _WEBOID;
  if (location.protocol == 'https:')
    wbs_arg = "https://ssl" + wbs_arg; 
  else 
    wbs_arg =  "http://pro" + wbs_arg;
  wbs_arg += "&ZONE=" + _WEBOZONE + "&PAGE=" + _WEBOPAGE;
  wbs_arg += "&ver=2&da2=" + wbs_da + "&ta=" + wbs_ta + "&co=" + wbs_co + "&ref=" + wbs_ref;
  if (parseInt(navigator.appVersion)>=3)
  {
    webo_compteur = new Image(1,1);
    webo_compteur.src = wbs_arg;
  }else{
    result = '<IMG SRC=' + wbs_arg + ' border="0" height="1" width="1" alt="">';
  }
  return result;
}

function EcrireStat()
{
  var stat = webo_zpi(1,1,83083);
  document.write(stat);
}


