var lotUrlAide = "";
var rubriqueUrlAide = "";
var PremiereCnx = false;
var estOpera = (navigator.userAgent.toLowerCase().indexOf("opera")) != -1 ? 1 : 0;

function init_statusDefileMsg()
{
  var canal = GetCookie("lastMedia");
  var date = GetCookie("lastDate");
  if ((canal != null) && (date != null))
  {
    statusDefileMsg(canal,date,150);
    DeleteCookie("lastMedia",null,null,"/",null,true);
    DeleteCookie("lastDate",null,null,"/",null,true);
  }
}

function UnSelectSave()
{
  var objClickSave =  GetCookie("objClickSave");
  if (objClickSave != null)
  {
    window.frames[0].document.getElementById("point_" + objClickSave + "_select").style.display = "none";
    window.frames[0].document.getElementById("point_" + objClickSave + "_default").style.display = "";
    window.frames[0].document.getElementById("span_" + objClickSave).className = "arial"; 
    DeleteCookie("objClickSave",null,null,"/",null,true);
  }
}

function UnSelectLot(lot,boolSelect)
{
  var objClickSave =  GetCookie("objClickSave");
  if (lot != objClickSave)
  {
    window.frames[0].document.getElementById("point_" + lot + "_select").style.display = "none";
    window.frames[0].document.getElementById("point_" + lot + "_default").style.display = "";
    if (boolSelect)
    {window.frames[0].document.getElementById("span_" + lot).className = "arial";}
  }
}

function SelectLot(lot,boolSelect)
{
  var objClickSave =  GetCookie("objClickSave");
  window.frames[0].document.getElementById("point_" + lot +"_default").style.display = "none";
  window.frames[0].document.getElementById("point_" + lot +"_select").style.display = "";
  if (boolSelect)
  {
    window.frames[0].document.getElementById("span_" + lot).className = "arial2";
    SetCookie("objClickSave",lot,null,"/",null,true);
    SelectGroupe(Lot2Groupe(lot),true);
  }
  if ((objClickSave != null) && (boolSelect))
  {
    UnSelectLot(objClickSave,boolSelect);
  }
}

function MM_swapClickImage(objClick,ImageDest,ImageRestore)
{
  SelectLot(objClick,true);
}

function RestoreMenu()
{
  var objClickSave =  GetCookie("objClickSave");
  if (objClickSave != null)
  {
    SelectLot(objClickSave,true);
  }
}

function InitSwapMenu()
{
  RestoreMenu();
  DeleteCookie("objClickSave",null,null,"/",null,true);
}

function ChangeFrameDroit(Url)
{
   if ((document.all) && (!estOpera))
     document.getElementById("H_win").src = Url;
   else
     window.frames[1].document.location = Url;
   SetCookie("UrlFrameDroit",Url,null,"/",null,true);
}

function RestoreFrameDroit()
{
  var UrlFrameDroit = GetCookie("UrlFrameDroit");
  var OptionConnect = GetCookie("OptionConnect");
  if (OptionConnect != null)
  {
    if (OptionConnect == "1")
    {
      ChangeFrameDroit('/restitution/cns_listeprestation.html');
      SelectLot('consultation',true);
    }
    else if (OptionConnect == "2")
    {
      ChangeFrameDroit('/virement/pas_vipon_saisie.html');
      SelectLot('passerordrevir',true);
    }
    else if (OptionConnect == "3")
    {
      ChangeFrameDroit('/brs/cct/comti10.html');
      UnSelectSave();
    }
    DeleteCookie("OptionConnect",null,null,"/",null,true);
    PremiereCnx = true;
  }
  else if ((UrlFrameDroit != null) && (window.frames[1].document.location.pathname != UrlFrameDroit) && (window.frames[1].document.location.href != UrlFrameDroit))
  {
    window.frames[1].document.location = UrlFrameDroit;
    PremiereCnx = true;
  }
}

function RestoreIndex()
{
  RestoreFrameDroit();
  init_statusDefileMsg();
}

function SauvegardeUrlFrameDroit(Url)
{
  SetCookie("UrlFrameDroit",Url,null,"/",null,true);
}

function deconnexion(url,message,campagne,nom,page)
{
if(message!=null){	
  if (confirm(message) == true)
  {
    DeleteCookie("UrlFrameDroit",null,null,"/",null,true);
    DeleteCookie("objClickSave",null,null,"/",null,true);
    DeleteCookie("NbMessages",null,null,"/",null,true);
    DeleteCookie("lastMedia",null,null,"/",null,true);
    DeleteCookie("lastDate",null,null,"/",null,true);
    if(campagne!=null&&nom!=null){
    	var indicCampagne = new Image;
    	var idUser = GetCookie("idUser");
    	indicCampagne.src = "/img/acces/indic"+campagne+".gif?nom="+nom+"&page="+page;
    }
    window.location.href=url;
  }
 }else{
 if (confirm('Souhaitez-vous quitter Logitel Net ?')==true)
 {
  DeleteCookie("UrlFrameDroit",null,null,"/",null,true);
  DeleteCookie("objClickSave",null,null,"/",null,true);
  DeleteCookie("NbMessages",null,null,"/",null,true);
  DeleteCookie("lastMedia",null,null,"/",null,true);
  DeleteCookie("lastDate",null,null,"/",null,true);
  window.location.href=url;
 } 
 }
  return;
}

function test_conversion_mue()
{
  var conv=new String(window.frames[1].convertible);
  var url_conv=new String(window.frames[1].url_conv);

  if ((conv != "undefined")
			 && (conv == "OUI")
			 && (url_conv != "undefined")
		 )
  {
    window.frames[1].location.href=url_conv;
  }
}

function testLotetRubrique()
{
  var objClickSave = GetCookie("objClickSave");
  if (objClickSave == null)
  {
    lotUrlAide = "";
    rubriqueUrlAide = "";
    return;
  } 
  switch (objClickSave)
  {
    case "consultation" : 
    {
      lotUrlAide = "comptes";
      rubriqueUrlAide = "consultation";
      break;
    }
    case "personnalisation" :
    {
      lotUrlAide = "personnalisation";
      rubriqueUrlAide = "comptes";
      break;
    }
    case "impressionrib" :
    {
      lotUrlAide = "comptes";
      rubriqueUrlAide = "impression";
      break;
    }
    case "telechargement" :
    {
      lotUrlAide = "comptes";
      rubriqueUrlAide = "telechargement";
      break;
    }
    case "passerordrevir" :
    {
      lotUrlAide = "virements";
      rubriqueUrlAide = "passer";
      break;
    }
    case "suiviordresvir" :
    {
      lotUrlAide = "virements";
      rubriqueUrlAide = "suivi";
      break;
    }
    case "listecomptes" :
    {
      lotUrlAide = "virements";
      rubriqueUrlAide = "liste";
      break;
    }
    case "comptestitres" :
    {
      lotUrlAide = "bourse";
      rubriqueUrlAide = "comptes";
      break;
    }
    case "passerordrebou" :
    {
      lotUrlAide = "bourse";
      rubriqueUrlAide = "passer";
      break;
    }
    case "suiviordresbou" :
    {
      lotUrlAide = "bourse";
      rubriqueUrlAide = "suivi";
      break;
    }
    case "contrat" :
    {
      lotUrlAide = "personnalisation";
      rubriqueUrlAide = "contrat";
      break;
    }		
    case "aidedecision" :
    {
      lotUrlAide = "bourse";
      rubriqueUrlAide = "decision";
      break;
    }
    case "fiscalite" :
    {
      lotUrlAide = "bourse";
      rubriqueUrlAide = "fiscalite";
      break;
    }
    case "coursmarche" :
    {
      lotUrlAide = "bourse";
      rubriqueUrlAide = "lexique";
      break;
    }
    default :
    {
      lotUrlAide = "";
      rubriqueUrlAide = "";
      break;
    }
  }
  return;
}

function windowOpenerAide()
{
  testLotetRubrique();
  if ((lotUrlAide != "") && (rubriqueUrlAide != ""))
  {
    var url = ("/html/aide/index.html?lot=" + lotUrlAide + "&rubrique=" + rubriqueUrlAide);
    OpenWindowFocus(url,"w_aide","toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=670,height=450");
  }
}

function windowOpenerInfosBourse(url)
{
  OpenWindowFocus(url,"MessagerieBourse","toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=650,height=550");
}

function Lot2Groupe(lot)
{
  var ret = "";
  switch (lot)
  {
    case "consultation" : case "impressionrib" : case"telechargement" :
      ret = "groupe_lescomptes";
    break;
    case "passerordrevir" : case "suiviordresvir" : case "listecomptes" :
      ret = "groupe_virements";
    break;
    case "infobourse" : case "comptestitres" : case "coursmarche" : case "passerordrebou" : case "suiviordresbou" : case "aidedecision" : case "fiscalite" :
      ret = "groupe_passbourse";
    break;
    case "fidcommande" :
      ret = "groupe_fidelite";
    break;
    case "vadcomment" : case "vadproduits" :
      ret = "groupe_souscrire";
    break;
    case  "personnalisation" : case "contrat" :
      ret = "groupe_personnalisation";
    break;
  }
  return ret;
}
var grp_Timeout=1384652;

function SelectGroupe(grp,boolSelect)
{
  clearTimeout(grp_Timeout);
  grp_Timeout = setTimeout("SelectGroupe_('"+grp+"','"+boolSelect+"')", 300);
}
function SelectGroupe_(grp,boolSelect)
{
  var groupe = new Array("groupe_lescomptes","groupe_virements","groupe_passbourse","groupe_fidelite","groupe_souscrire","groupe_personnalisation");
  var lot =  GetCookie("objClickSave");
  var grp_select = Lot2Groupe(lot);
  if(boolSelect=="true")boolSelect=false;else boolSelect=true;
  for(var i=0; i<groupe.length; i++)
  {
    if(grp == groupe[i] || (boolSelect && grp_select == groupe[i]))
    { window.frames[0].document.getElementById(groupe[i]).style.display = "";
	window.frames[0].document.getElementById("img_"+groupe[i]).style.display = "";
    }else{
      window.frames[0].document.getElementById(groupe[i]).style.display = "none";
	window.frames[0].document.getElementById("img_"+groupe[i]).style.display = "none";
    }
  }
}
