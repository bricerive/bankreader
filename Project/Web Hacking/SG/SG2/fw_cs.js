
function estFormatString( maString, monFormat, op)
{
  var expREG = new RegExp(monFormat, op?op:"");
  if(expREG.test(maString)) return maString;
  else return false;
}

function estNumString( numString, min, max)
{
  return estFormatString(numString, "^[0-9]"+((min)?((max)?("{"+min+","+max+"}"):("{"+min+"}")):"+")+"$");
}

function estCompteString( numString, op)
{
  var t = "00000000000"+numString;
  t = t.substring(t.length-11, t.length);
  return estFormatString(t, "^"+(op?"[A-Za-z0-9]":"[0-9]")+"{11}$");
}

function estGuichetString( numString, op)
{
  var t = "00000"+numString;
  t = t.substring(t.length-5, t.length);
  return estFormatString(t, "^"+(op?"[A-Za-z0-9]":"[0-9]")+"{5}$");
}

function estAlphaString( alphaString, min, max)
{
  return estFormatString(alphaString, "^[A-Za-z_]"+((min)?((max)?("{"+min+","+max+"}"):("{"+min+"}")):"+")+"$");
}

function estAlphanumString( alphanumString, min, max)
{
  return estFormatString(alphanumString, "^[0-9A-Za-z_]"+((min)?((max)?("{"+min+","+max+"}"):("{"+min+"}")):"+")+"$");
}
function estTelephoneString( alphanumString, min, max)
{
  return estFormatString(alphanumString, "^[0-9+.,-]"+((min)?((max)?("{"+min+","+max+"}"):("{"+min+"}")):"+")+"$");
}

function estTexteString( txtString, min, max)
{
  return estFormatString(txtString, "^[^%&<=>|\"]"+((min)?((max)?("{"+min+","+max+"}"):("{"+min+"}")):"+")+"$");
}
function estTexteStringEmail( txtString, min, max)
{
  return estFormatString(txtString, "^[^%&<=>~|\"'(){}+*#�$�%!/:;,? ���]"+((min)?((max)?("{"+min+","+max+"}"):("{"+min+"}")):"+")+"$");
}





function toEntier( entierString)
{
  var val = parseInt(entierString, 10);
  if( isNaN(val)) return 0;
  else return val;
}

function estEntierString( entierString, min, max)
{
  var expREG = /^[+-]?[0]*[0-9]{1,15}$/;
  var ok = expREG.test(entierString);
  if(ok)
  { var val = toEntier(entierString);
    if(min) ok = (val>=min);
    if(ok && max) ok = (val<=max);
    if(ok) return val.toString();
  }
  return false;
}

function toMontant( montantString)
{
  var val = parseFloat( montantString.replace(",", "."));
  if( isNaN(val)) return 0;
  else return val;
}

function estMontantString( montantString, min, max, decim, sep)
{
  var dec = (decim && decim>0 && decim <15) ? decim : 0;
  var expREG = new RegExp( (dec!=0)?"^[+-]?[0]*[0-9]{1,"+(15-dec)+"}([,.][0-9]{0,"+dec+"}[0]*)?$" : "^[+-]?[0]*[0-9]{1,15}$");
  var ok = expREG.test((dec!=0)? montantString : montantString.replace(/[.,]/, ""));
  if(ok)
  { var val = toMontant(montantString);
    if(min) ok = (val>=min);
    if(ok && max) ok = (val<=max);
    if(ok)
    { val = val.toString();
      if(dec!=0)
      { val = val+(val.indexOf(".")<0?".":"")+"000000000000000";
        val = val.substring(0,val.indexOf(".")+dec+1);
      }
      return val.replace(/[.,]/,(sep?sep:"."));
    }
  }
  return false;
}

function stringToDate( dateString, formatDateSrc)
{
  var frmSrc, j,m,a, rg0,rg, t, d;
  dateString = dateString.replace(new RegExp("([/-][0-9])$"),"$1-");
  rg0 = new RegExp("[/-]","g");
  frmSrc = "^"+((formatDateSrc)?formatDateSrc.toUpperCase():"J/M/A")+"$";
  frmSrc = frmSrc.replace(rg0,"");
  j = frmSrc.indexOf("J");
  m = frmSrc.indexOf("M");
  a = frmSrc.indexOf("A");
  if(j<1 || j>3 || m<1 || m>3 || a<1 || a>3) return false;

  frmSrc = frmSrc.replace("J","([0-9][/-]|[0-9]{2}[/-]?)");
  frmSrc = frmSrc.replace("M","([0-9][/-]|[0-9]{2}[/-]?)");
  frmSrc = frmSrc.replace("A","([0-9][/-]|[0-9]{2,4}[/-]?)");
  rg = new RegExp(frmSrc,"g");
  if( ! rg.test(dateString) ) return false;

  t = dateString.replace(rg, "Array($"+j+",$"+m+",$"+a+")");
  t = eval(t.replace(rg0,""));
  if(t[2] < 1000) t[2] += 2000;
  d = new Date(t[2], t[1]-1, t[0]);
  if( isNaN(d) || d.getDate() != t[0] || (d.getMonth()+1) != t[1]) return false;
  return d;
}

function dateToString( dateObj, formatDateCib)
{
  var frmCib, j,m,a;
  frmCib = ((formatDateCib)?formatDateCib.toUpperCase():"J/M/A");
  if(!dateObj) return "";
  j = dateObj.getDate();
  m = dateObj.getMonth()+1;
  a = dateObj.getFullYear();

  frmCib = frmCib.replace("J",((j<10)?"0":"")+j);
  frmCib = frmCib.replace("M",((m<10)?"0":"")+m);
  frmCib = frmCib.replace("A",""+a);
  return frmCib;
}

function estDateString( dateString, formatDateSrc, minDateObj, maxDateObj, formatDateCib)
{
  var d = stringToDate( dateString, formatDateSrc);
  if( !d || (minDateObj && d<minDateObj) || (maxDateObj && d>maxDateObj)) return false;
  return dateToString( d, formatDateCib?formatDateCib:formatDateSrc);
}

function estEmail(emailString)
{
  return (emailString.substr(emailString.indexOf("@"),emailString.length).indexOf(".") > 0) && (emailString.indexOf("@") > 0);
}

function ctrlSaisie(champ, msgSiVide, msgSiFormatErr, valSiErr, majSiOk, funCtrl, a1,a2,a3,a4,a5,a6,a7,a8,a9)
{
  var ret = true;
  if( champ.value.length > 0)
  {
    if(funCtrl)
    {
      ret = funCtrl(champ.value,a1,a2,a3,a4,a5,a6,a7,a8,a9);
      if(!ret && msgSiFormatErr) alert(msgSiFormatErr);
      if(ret && majSiOk) champ.value = ret;
    }
  }
  else
  {
    if(msgSiVide) {ret=false; alert(msgSiVide);}
  }
  if(!ret && valSiErr) champ.value = valSiErr;
  if(!ret && (msgSiVide || msgSiFormatErr)) champ.focus();
  return ret;
}

function ctrlComboSelect(champCombo,indiceInterdit,msgSiIndiceInterdit)
{
  var ret = champCombo.selectedIndex != indiceInterdit;
  if (!ret)
  {	
    alert(msgSiIndiceInterdit);
    champCombo.focus();
  }
  return ret;
}

// retourne maString, dans laquelle les �ventuels � sont remplac�s par E
// NB : la methode .replace ne fonctionne pas sous Mac + IE avec euro en argument
function remplaceSaisieEuro(maString)
{
  var carEuro = String.fromCharCode(8364);
  var retString = maString;
  var i;

  while((i = retString.indexOf(carEuro)) >= 0)
  {
    retString = retString.substring(0,i) + "E" + retString.substring(i+1, retString.length);
  }
  return retString ;
}


// Vrai si la chaine n'est compose que de characteres "blanc"
// faux sinon

function estBlancString(val)
{
	if(val==null)
		{return true;}
	for(var i=0;i<val.length;i++)
	{
		if((val.charAt(i)!=' ') &&(val.charAt(i)!="\t")&&(val.charAt(i)!="\n")&&(val.charAt(i)!="\r"))
		{
			return false;
		}
	}
	return true;
}

function is_radio_changed()
{
  for(var i=0; i<document.forms.length; i++ )
  { for(var j=0;j<document.forms[i].elements.length; j++ )
    { if(document.forms[i].elements[j].type=="radio")
    if ( (document.forms[i].elements[j].checked && !document.forms[i].elements[j].defaultChecked) 
    || (!document.forms[i].elements[j].checked && document.forms[i].elements[j].defaultChecked) )
     return true;
 } }
 return false;
}

function is_ComboRole()
{
  for(var i=0; i<document.forms.length; i++ )
  {
    for(var j=0;j<document.forms[i].elements.length; j++ )
    {
      if(document.forms[i].elements[j].type == "select-one")
      {
        for(var k=0;k<document.forms[i].elements[j].length;k++)
        {
          if (document.forms[i].elements[j].options[k].defaultSelected)
          {
            document.forms[i].elements[j].selectedIndex=k;
            return;
          }
        }
      }
    }
  }
}

function ctrlComboValue(champCombo,valueInterdite,msgSiValueInterdite)
{
  var ret = champCombo.options[champCombo.selectedIndex].value != valueInterdite;
  if (!ret)
  {	
    alert(msgSiValueInterdite);
    champCombo.focus();
  }
  return ret;
}
