/*
R�cup�re la position r�elle d'un objet dans la page (en tenant compte de tous ses parents)
IN 	: Obj => Javascript Object ; Prop => Offset voulu (offsetTop,offsetLeft,offsetBottom,offsetRight)
OUT	: Num�rique => position r�elle d'un objet sur la page.
*/
function GetDomOffset( Obj, Prop ) {
	var iVal = 0;
	while (Obj && Obj.tagName != 'body') {
		eval('iVal += Obj.' + Prop + ';');
		Obj = Obj.offsetParent;
	}
	return iVal;
}
/*
Affiche ou masque un objet (identifi� par son ID) et fait dispara�tre les select g�nant (en cas d'affichage)
Va d�terminer si un (ou plus) des selectbox de la page est en "collision" avec l'objet dont l'id est fourni
IN	: sId => Id de l'objet � afficher par-dessus les selects ; sVisibility => Etat � mettre � l'objet (hidden ou visible)  ; exclude => Id des selects � ne pas masquer, m�me en cas de collision
*/
function SwapSelect(sId,sVisibility,exclude) {
	//constitue la chaine pour g�rer les exclusions
	exclude=';'+((exclude=='')?'undefined':exclude)+';';
	//R�cup�re l'objet et lui applique la visibilit� choisie
	oObj = document.getElementById('axaMasterMainNavigationContent');
	oObj.style.visibility=sVisibility;
	//R�cup�re les coordonn�es exacte de l'objet sur la page
	Top_Element  = GetDomOffset(oObj, 'offsetTop');
	Left_Element  = GetDomOffset(oObj, 'offsetLeft');
	Largeur_Element  = oObj.offsetWidth;
	Hauteur_Element  = oObj.offsetHeight;
	//R�cup�re la liste des select de la page
	oSelects = document.getElementsByTagName('select');
	if (oSelects.length > 0) {
		//Parcours la liste des selects
		for (i = 0; i < oSelects.length; i++) {
			oSlt = oSelects[i];
			//Compare l'ID du select en cours de lecture avec la liste des ID � exclure
			if (exclude.indexOf(';'+oSlt.id+';')!=-1) continue;
			//Si il n'est pas exclue on se retourve ici
			//Recupere les coordonn�es exacte du select sur la page
			Top_Select = GetDomOffset(oSlt, 'offsetTop');
			Left_Select = GetDomOffset(oSlt, 'offsetLeft');
			Largeur_Select = oSlt.offsetWidth;
			Hauteur_Select = oSlt.offsetHeight;
			//Compare les positions de l'objet et du select pour savoir s'ils ont des pixels en collision (qui se chevauche)
			isLeft = false;
			if ((Left_Element > (Left_Select - Largeur_Element)) && (Left_Element < (Left_Select + Largeur_Select)))
				isLeft = true;
			isTop = false;
			if ((Top_Element > (Top_Select - Hauteur_Element)) && (Top_Element < (Top_Select + Hauteur_Select)))
				isTop = true;
			if (isLeft && isTop) {
				//S'il y a collision, on fait disparaitre le select
				sVis = (oObj.style.visibility == 'hidden') ? 'visible' : 'hidden';
				if (oSlt.style.visibility != sVis) 
					oSlt.style.visibility = sVis;
			} else
				//Sinon, on le remet visible
				if (oSlt.style.visibility != 'visible') 
					oSlt.style.visibility = 'visible';
		}
	}
}