#########################################
https://www.axa.fr/index.asp

HTTP/1.1 200 OK
Server: Microsoft-IIS/4.0
Date: Wed, 28 Dec 2005 09:06:29 GMT
Content-Type: text/html
Set-Cookie: ASPSESSIONIDTRDSTBDS=JJOPPNABGDJBLBLGJHIGAEEA; path=/
Cache-control: private
Transfer-Encoding: chunked
Set-Cookie: WEBTRENDS_ID=81.57.1.20-4253817904.29756301::EC6BA88ED5FE77252F3A83E47807CE54; path=/; expires=Sat, 26-Dec-2015 09:06:29 GMT


<html>
<head>
<link rel="SHORTCUT ICON" href="/page/img/favicon.ico">



<title>Les assurances Axa: assurance auto, habitation, vie, sant�, �pargne</title>
<meta name="description" content="Informations, simulations et demande de devis assurance auto, assurance habitation, assurance vie et sur les produits et services AXA en France en matiere de protection financiere : assurance auto, habitation, sante, assurance vie, epargne et gestion de patrimoine. Acces personnalise offrant des services reserves aux clients.">
<meta name="keywords" content="AXA, auto, automobile, moto, assistance, habitation, logement, protection juridique, prevoyance, accidents de la vie, autonomie, retraite, epargne, placements financiers, assurance vie, sante, AXA France, Axa banque">

<!-- Teasing noscript -->

<noscript> 
<div style="margin:25px 50px 0 50px; color:#000;font-family: arial, helvetica, sans-serif;line-height: 1.2em; border:red double 5px;padding:0.1em;text-align:center; font-size:2.2em;">
<p>Votre navigateur ne supporte pas les scripts inclus dans cette page.</p>
<p>Nous vous proposons un espace accessible am&eacute;nag&eacute &agrave; l'adresse suivante</p>
<p> <a href="http://accessible.axa.fr"  title="Espace accessible du site www.axa.fr">accessible.axa.fr  </a></p>
<p style=" font-size:1.2em; font-family: arial, helvetica, sans-serif; line-height: 1.2em;margin:40px 0px 20px 0px;">
<strong>AXA France</strong> vous propose de consulter sur cet espace am&eacute;nag&eacute une quinzaine de solutions AXA et de vous informer sur AXA en France.
</p> 
</div>
</noscript>

<!-- Fin Teasing noscript -->
<SCRIPT LANGUAGE="JavaScript">
<!--

		function req(P)
		{
		

			var dcs_imgarray = new Array; 
			dcs_img = new Image;
			dcs_img.src = P;
			
			
			return true;		

		}
		
		function dcs_addpara(P,name,value)
		{	
			P += "&"+name+"="+escape(value);
			return P;
		}
	
		
		
	
	
		function dcs_para(P)
		{

			P="http"+(document.URL.indexOf('https:')==0?'s':'')+"://dcs.axa.fr/dcs.gif?";
		
		      
			P += "dcsuri="+escape(window.location.pathname);
			P=dcs_addpara(P,"dcsqry",window.location.search);
		
		      
			P=dcs_addpara(P,"dcssip",window.location.host);
		
		      
			if ((window.document.referrer != "") && (window.document.referrer != "-"))
				P=dcs_addpara(P,"dcsref",window.document.referrer);
	
		      
			P=dcs_addpara(P,"dcsdat",new Date());
		
			
		
			return P;
			

		}
	
		
		
	
		
		function dcs_aff(prof,type)
		{	
	        	
	        	

	        	
	        	var P="";
	        	P=dcs_para(P);
	        	P=dcs_addpara(P,"idt",type);
	        	P=dcs_addpara(P,"titre",window.document.title);
	        
	        
	          	if(document.frames!=null)
	          	{
		          	if(document.frames.name!="haut" && parent.frames["haut"])
		       	  	{     		
	           			P=dcs_addpara(P,"idu",parent.frames["haut"].tagidu);
		           		P=dcs_addpara(P,"idb",parent.frames["haut"].tagidb);
		            		P=dcs_addpara(P,"ids","ban");
		            		P=dcs_addpara(P,"idh",window.location.host);
		          	}
	          	}
	          
	 
	        	P=dcs_addpara(P,"prof",prof);
	
	        	  
			req(P);
				
			return true;

		}
	
	
	
	
	
		
	
		function dcs_click(idt,titre,nom)
		{	
	  

	        	var P="";
	        	P=dcs_para(P);
	        	P=dcs_addpara(P,"idt",idt);
	        	P=dcs_addpara(P,"titre",titre);
	        	P=dcs_addpara(P,"nom",nom);
	        
			var dcs_imgarray = new Array; 
			var dcs_ptr = 0;
			dcs_imgarray[dcs_ptr] = new Image;
			dcs_imgarray[dcs_ptr].src = P;
			dcs_ptr++;
			return true;
	
		}
	
dcs_aff('0','p');
//-->
</SCRIPT>


<script LANGUAGE="JavaScript">
<!--
	
		var passed     = location.search ? unescape(location.search.substring(1)) + '&' : '';
		if (passed.substring(0,passed.substring(passed.indexOf('&')+1).indexOf('&')) != ""){
			var myNavgauche = passed ? passed.substring(0,passed.indexOf('&')) : '/page/nav/nav_gauch.asp' // frame de navigation gauche
			passed         = passed.substring(passed.indexOf('&')+1);
			var myNavhaut  = passed ? passed.substring(0,passed.indexOf('&')) : '/page/nav/nav_haut.asp?idxUnivers=2';// frame de navigation haute
			passed         = passed.substring(passed.indexOf('&')+1);
			var myMainFrame = passed ? passed.substring(0,passed.indexOf('&')) : '/transact/transversal/home/home_gal.asp';// frame de navigation centrale

		}
		else
		{
			var myNavgauche = '/page/nav/nav_gauch.asp';
			var myNavhaut   = '/page/nav/nav_haut.asp?idxUnivers=2'; 
			if (passed.substring(0,passed.indexOf('&')) != "")
			{
				var myMainFrame = passed.substring(0,passed.indexOf('&'))
			}
			else
			{
				var myMainFrame = '/transact/transversal/home/home_gal.asp';
			}
		}
	
	
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

if (top != self)
{
	    top.location.href = location.href;
}
else 
{
		document.write('<FRAMESET ROWS="110,*" FRAMEBORDER="NO" BORDER="0" FRAMESPACING="0" FRAMEPADDING="0" MARGINHEIGHT="0" MARGINWIDTH="0">');
		document.write('<FRAME SRC=" ' + myNavhaut + ' " name="haut" BORDER="0" FRAMEBORDER="NO" FRAMESPACING="0" FRAMEPADDING="0" MARGINHEIGHT="1" MARGINWIDTH="0" scrolling="no" noresize>');
		document.write('<FRAMESET COLS="130,*" FRAMEBORDER="NO" BORDER="0" FRAMESPACING="0" FRAMEPADDING="0" MARGINHEIGHT="0" MARGINWIDTH="0">'); 
		document.write('<FRAME SRC=" ' + myNavgauche + ' " NAME="FrameNav" BORDER="0" FRAMEBORDER="NO" FRAMESPACING="0" SCROLLING="no" FRAMEPADDING="0" MARGINHEIGHT="0" MARGINWIDTH="0" noresize>');
		document.write('<FRAME SRC=" ' + myMainFrame + ' " NAME="contenu" BORDER="0" FRAMEBORDER="NO" FRAMESPACING="0" FRAMEPADDING="0" MARGINHEIGHT="0" MARGINWIDTH="0" SCROLLING="auto" noresize>'); 
		document.write('</FRAMESET>');
		document.write('</FRAMESET>');
}
// -->
</script>

</head>

<frameset>
  <noframes>
<body bgcolor="#FFFFFF" text="#000000">


<!-- Teasing noframes -->
<div style="margin:25px 50px 100px 50px; color:#000; border:red double 5px; padding:0.1em; text-align:center;" >

<p style=" font-size:1em; font-family: arial, helvetica, sans-serif; line-height: 1.2em;">
Votre navigateur ne supporte pas les cadres inclus dans cette page.</p>
<p style=" font-size:1em; font-family: arial, helvetica, sans-serif; line-height: 1.2em;">Nous vous proposons un espace accessible am&eacute;nag&eacute &agrave; l'adresse suivante</p>
<p> <a href="http://accessible.axa.fr"  title="Espace accessible du site www.axa.fr">accessible.axa.fr  </a></p>
<p style=" font-size:1.2em; font-family: arial, helvetica, sans-serif; line-height: 1.2em;margin:40px 0px 20px 0px;">
<strong>AXA France</strong> vous propose de consulter sur cet espace am&eacute;nag&eacute une quinzaine de solutions AXA et de vous informer sur AXA en France.
</p> 

  </div>
<!-- Fin Teasing noframes -->

<h1><b><font color="#FF0000" size="7">assurance auto</font></b>, <b>assurance 
  vie</b></h1>
<p><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Site d'informations 
  sur les produits et services d'AXA en France en mati&egrave;re de <b>protection 
  financi&egrave;re</b> : <b><a href="content/08_refer/assurance-auto.htm">assurance 
  auto</a></b>, <a href="content/08_refer/assurance-habitation.htm"><b>assurance 
  habitation</b></a>, <b><a href="content/08_refer/assurance-habitation.htm">assurance 
  sant&eacute;</a></b><a href="content/05_sante/05_home.asp">,</a> <b><a href="content/08_refer/assurance-vie.htm">assurance 
  vie</a></b>, <a href="content/08_refer/assurance-voiture.htm"><b><i>assurance 
  voiture</i></b></a> et <b>gestion de patrimoine</b>.</font></p>
<p><font face="Verdana, Arial, Helvetica, sans-serif" size="2">assurance sant� 
  - <a href="content/04_patrimoine/04_home.asp">gestion de patrimoine</a> - <a href="content/08_refer/assurance-automobile.htm">assurance 
  automobile</a></font></p>
<p><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Leader de la protection 
  financi�re en France, la <b>compagnie d'assurance</b> AXA vous accompagne tout 
  au long de votre vie en mettant � votre disposition l'expertise reconnue de 
  ses conseillers et de ses collaborateurs afin de r�pondre � toutes vos questions 
  relatives � la <b>gestion de patrimoine</b> et � la protection de vos proches 
  et de vos biens : <b>mutuelle sant&eacute;</b>, <b>assurance habitation.</b></font></p>
<h1><b><font color="#FF0000" size="+7">compagnies d'assurances</font></b><font> 
  </font><b><font>Axa en France</font></b></h1>
<p align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">R�guli�rement 
  sur ce site, vous trouverez des <a href="content/00_accueil/00_solution.asp">informations 
  sur l'<b>assurance</b></a> et des dossiers actualis�s sur les th�mes de l'<a href="content/02_se_deplacer/solution/Kit_auto.asp">assurance 
  auto</a>, la protection financi&egrave;re, l'<b>assurance scolaire</b>. Vous 
  �tes client AXA, vous pouvez d�s � pr�sent consulter vos contrats. Vous n'�tes 
  pas encore client, nous vous invitons � d�couvrir nos produits et services et 
  � contacter un Conseiller AXA ou les <b><a href="content/08_refer/agents-axa-meaux.htm">agents 
  Axa</a></b>, conseillers axa pour choisir les <b>solutions et services AXA</b> 
  qui vous correspondent.</font><b> assurance compl&eacute;mentaire sant&eacute;<br>
  <a href="http://www.axa.fr/axafrance/">compagnie d'assurance</a> Axa en France - <a href="http://recrute.axa.fr/">Recrutement 
  Axa</a> - <a href="http://entreprise.axa.fr/">Assurance Entreprise</a></b></p> 

<script language="JavaScript">
<!--

		function req(P)
		{
return true;
		}
		
		function dcs_addpara(P,name,value)
		{	
			P += "&"+name+"="+escape(value);
			return P;
		}
	
		
		
	
	
		function dcs_para(P)
		{
 return true; 
		}
	
		
		
	
		
		function dcs_aff(prof,type)
		{	
	        	
	        	
 return true; 
		}
	
	
	
	
	
		
	
		function dcs_click(idt,titre,nom)
		{	
	  

		return true;

		}
	
dcs_aff('0','p');
//-->
</script>
<noscript> <img border="0" name="DCSIMG" width="1" height="1" src="https://dcs.axa.fr/njs.gif?dcsuri=/nojavascript"> 
</noscript> 
<script language="JavaScript">
<!--

		function req(P)
		{

			var dcs_imgarray = new Array; 
			var dcs_ptr = 0;
			dcs_imgarray[dcs_ptr] = new Image;
			dcs_imgarray[dcs_ptr].src = P;
			dcs_ptr++;
			return true;		

		}
		
		function dcs_addpara(P,name,value)
		{	
			P += "&"+name+"="+escape(value);
			return P;
		}
	
		
		
	
	
		function dcs_para(P)
		{

			P="http"+(document.URL.indexOf('https:')==0?'s':'')+"://dcs.axa.fr/dcs.gif?";
		
		      
			P += "dcsuri="+escape(window.location.pathname);
			P=dcs_addpara(P,"dcsqry",window.location.search);
		
		      
			P=dcs_addpara(P,"dcssip",window.location.host);
		
		      
			if ((window.document.referrer != "") && (window.document.referrer != "-"))
				P=dcs_addpara(P,"dcsref",window.document.referrer);
	
		      
			P=dcs_addpara(P,"dcsdat",new Date());
		
			
			return P;
			

		}
	
		
		
	
		
		function dcs_aff(prof,type)
		{	
	        	
	        	

	        	
	        	var P="";
	        	P=dcs_para(P);
	        	P=dcs_addpara(P,"idt",type);
	        	P=dcs_addpara(P,"titre",window.document.title);
	        
	        
	          	if(document.frames!=null)
	          	{
		          	if(document.frames.name!="haut" && parent.frames["haut"])
		       	  	{     		
	           			P=dcs_addpara(P,"idu",parent.frames["haut"].tagidu);
		           		P=dcs_addpara(P,"idb",parent.frames["haut"].tagidb);
		            		P=dcs_addpara(P,"ids","ban");
		            		P=dcs_addpara(P,"idh",window.location.host);
		          	}
	          	}
	          
	 
	        	P=dcs_addpara(P,"prof",prof);
	
	        	  
		if(window.onload!="")
		{
			if(window.onload!=null)
				{
				var bodyonload=window.onload.toString();
				bodyonload=bodyonload.slice(bodyonload.indexOf('{',0)+1,bodyonload.lastIndexOf('}',0)-1);
				window.onload=function() {req(P); eval(bodyonload)};
				}
			else
				req(P);
		}	
		else
			req(P);
				
			return true;

		}
	
	
	
	
	
		
	
		function dcs_click(idt,titre,nom)
		{	
	  

	        	var P="";
	        	P=dcs_para(P);
	        	P=dcs_addpara(P,"idt",idt);
	        	P=dcs_addpara(P,"titre",titre);
	        	P=dcs_addpara(P,"nom",nom);
	        
			var dcs_imgarray = new Array; 
			var dcs_ptr = 0;
			dcs_imgarray[dcs_ptr] = new Image;
			dcs_imgarray[dcs_ptr].src = P;
			dcs_ptr++;
			return true;
	
		}
	
dcs_aff('0','p');
//-->
</script>
<noscript> <img border="0" name="DCSIMG" width="1" height="1" src="https://dcs.axa.fr/njs.gif?dcsuri=/nojavascript"> 
</noscript> 
<script language="JavaScript">
<!--

		function req(P)
		{

			var dcs_imgarray = new Array; 
			var dcs_ptr = 0;
			dcs_imgarray[dcs_ptr] = new Image;
			dcs_imgarray[dcs_ptr].src = P;
			dcs_ptr++;
			return true;		

		}
		
		function dcs_addpara(P,name,value)
		{	
			P += "&"+name+"="+escape(value);
			return P;
		}
	
		
		
	
	
		function dcs_para(P)
		{

			P="http"+(document.URL.indexOf('https:')==0?'s':'')+"://dcs.axa.fr/dcs.gif?";
		
		      
			P += "dcsuri="+escape(window.location.pathname);
			P=dcs_addpara(P,"dcsqry",window.location.search);
		
		      
			P=dcs_addpara(P,"dcssip",window.location.host);
		
		      
			if ((window.document.referrer != "") && (window.document.referrer != "-"))
				P=dcs_addpara(P,"dcsref",window.document.referrer);
	
		      
			P=dcs_addpara(P,"dcsdat",new Date());
		
			
		
			return P;
			

		}
	
		
		
	
		
		function dcs_aff(prof,type)
		{	
	        	
	        	

	        	
	        	var P="";
	        	P=dcs_para(P);
	        	P=dcs_addpara(P,"idt",type);
	        	P=dcs_addpara(P,"titre",window.document.title);
	        
	        
	          	if(document.frames!=null)
	          	{
		          	if(document.frames.name!="haut" && parent.frames["haut"])
		       	  	{     		
	           			P=dcs_addpara(P,"idu",parent.frames["haut"].tagidu);
		           		P=dcs_addpara(P,"idb",parent.frames["haut"].tagidb);
		            		P=dcs_addpara(P,"ids","ban");
		            		P=dcs_addpara(P,"idh",window.location.host);
		          	}
	          	}
	          
	 
	        	P=dcs_addpara(P,"prof",prof);
	
	        	  
		if(window.onload!="")
		{
			if(window.onload!=null)
				{
				var bodyonload=window.onload.toString();
				bodyonload=bodyonload.slice(bodyonload.indexOf('{',0)+1,bodyonload.lastIndexOf('}',0)-1);
				window.onload=function() {req(P); eval(bodyonload)};
				}
			else
				req(P);
		}	
		else
			req(P);
				
			return true;

		}
	
	
	
	
	
		
	
		function dcs_click(idt,titre,nom)
		{	
	  

	        	var P="";
	        	P=dcs_para(P);
	        	P=dcs_addpara(P,"idt",idt);
	        	P=dcs_addpara(P,"titre",titre);
	        	P=dcs_addpara(P,"nom",nom);
	        
			var dcs_imgarray = new Array; 
			var dcs_ptr = 0;
			dcs_imgarray[dcs_ptr] = new Image;
			dcs_imgarray[dcs_ptr].src = P;
			dcs_ptr++;
			return true;
	
		}
	
dcs_aff('0','p');
//-->
</script>
<noscript> <img border="0" name="DCSIMG" width="1" height="1" src="https://dcs.axa.fr/njs.gif?dcsuri=/nojavascript"> 
</noscript> 
</body>

  </noframes>
</frameset>
</html>

#########################################
https://www.axa.fr/transact/transversal/identification/0a_navmember.asp
ASPSESSIONIDTRDSTBDS=JJOPPNABGDJBLBLGJHIGAEEA; WEBTRENDS_ID=81.57.1.20-4253817904.29756301::EC6BA88ED5FE77252F3A83E47807CE54
HTTP/1.1 100 Continue
Server: Microsoft-IIS/4.0
Date: Wed, 28 Dec 2005 09:06:30 GMT

HTTP/1.1 200 OK
Server: Microsoft-IIS/4.0
Date: Wed, 28 Dec 2005 09:06:30 GMT
Pragma: no-cache
Content-Length: 6715
Content-Type: text/html
Expires: Thu, 10 Oct 1996 11:30:14 GMT
Cache-control: no-cache


<html>
<head>
<title>Identification AXA.fr</title>
<script LANGUAGE="javascript" src="/page/lib/js/popup.js"></script>
<script language="JavaScript">

<!--Recomposition du frameset -->
// VFA 23 avril 2004 - SER49002 : MAJ de l'url � afficher
if ((top == self))
	top.location.href = '';
//	top.location.href = 'signatureConvUtl.asp';
// VFA 23 avril 2004 - FIN SER49002

// V�rifie que le champ ne contient pas que des espaces
function openFen(url)
{
	var largeur = 540;
	var hauteur = screen.height -(screen.height*20/100);
	var posx	= (screen.width-largeur)/2;
	var posy	= (screen.height-(screen.height*90/100))/2;

	var param = 'toolbar=no,directories=no,location=no,width='+largeur+',height='+hauteur+',status=no,scrollbars=yes,resizable=yes,menubar=no,left='+posx+',top='+posy+',screenX='+posx+',screenY='+posy;
	w = window.open(url,"",param);
}

function verifBlancs(obj, msg)
{	
	var valChamp = obj.value;
	for(var i=0 ; i<valChamp.length ; i++)
	{
		var carac=valChamp.substring(i,i+1);
		if(carac!=' '){
		return true;
		}
	}
	if (msg == "")
		alert("Un de vos codes d\'identification ne contient que des espaces.");
	else 
		alert (msg);
	return false;
}

// V�rifie que les champs sont corrects
function verif_form() {
	var id, mdp, n1, n2;
	id = document.loginform.id;
	mdp = document.loginform.mdp;
	if (verifBlancs(id,"Un de vos codes d\'identification ne contient que des espaces.")) {
		if (verifBlancs(mdp,"Un de vos codes d\'identification ne contient que des espaces.")) {
			return true;					
		}else
		return false;				
	}else
	return false;
}

function demandeide() {

	top.contenu.location.href="/page/client/accueil_services.asp";
}

function action() {
	
		document.loginform.id.focus();
	
		alert('Vos codes d\'identification sont incorrects.\n Merci de les v�rifier et de vous reporter � l\'aide pour l\'explication \nde la saisie des diff�rents identifiants que vous poss�dez');
	
}

function focus_mdp()
{
	document.loginform.mdp.focus();
	return true;
}
</script>
<link href="/page/lib/css/default.css" rel="stylesheet" />
<style type="text/css">
/**/	
	.titleCorner { width : 5px;}
	.logTable{ border-left : 3px solid #C30038;}
	.logCell1 {display:inline; padding-left : 5px;}
	.logCell2 {display:inline; padding-left : 12px}
	.logField{ margin-left : 7px; margin-right : 9px;}
/**/
</style>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" onLoad="action();">



<form name="loginform" id="loginform" method="post" action="http://www.axa.fr/transact/transversal/identification/0a_navmember.asp" onsubmit="return verif_form();">
	<input type="hidden" name="action" value="controlermdp"/>

  <table width="130" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>
      		<table width="122" border="0" cellspacing="0" cellpadding="0">
					<tr>
 						<td><img src="/page/img/bracket/frame-g_1.gif" width="123" height="12"></td>
					</tr>
				</table>
      </td>
    </tr>
    <tr>
      <td> 
        <table border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td height="10"> 
             <table width="122" border="0" cellspacing="0" cellpadding="0" style="margin-left : 3px;">
					<tr>
					 <td style="padding-top : 5px"><img src="/page/img/bracket/LeftCustom_bracket_top.gif" alt="" width="15" height="9"/></td>
							<td style="padding-left:5px"><h1 id="c3">Acc&egrave;s&nbsp;Clients</h1></td>
					</tr>
					<tr>
							<td colspan="2">
							<table  border="0" cellspacing="0" cellpadding="0" class="logTable">
							 <tr> 
							  <td colspan="2" class="logCell2" id="c3">Identifiant</td>
							 </tr>
							 <tr> 
								<td class="logCell1">
					<script language="javascript"> 
					if (navigator.appName == "Netscape")
						{
					 	if (navigator.userAgent.indexOf('Mac') != -1)
							{
								document.write('<input type="text" name="id" id="id" size="10" maxlength="10" class="logField" onblur="focus_mdp();">');
							}
						else
							{
							if (navigator.appVersion.substring(0,1) == "4")
								{
								document.write('<input type="text" name="id" id="id" size="10" maxlength="10" class="logField" onblur="focus_mdp();">');
								}
							else
								{		
								document.write('<input type="text" name="id" id="id" size="10" maxlength="10" class="logField" onblur="focus_mdp();">');
								}
							}
						}
					else 
						{
						if (navigator.userAgent.indexOf('Mac') != -1)
							{
							document.write('<input type="text" name="id" id="id" size="10" maxlength="10" class="logField" onblur="focus_mdp();">');
							}
						else
							{
							document.write('<input type="text" name="id" id="id" size="10" maxlength="10" class="logField" onblur="focus_mdp();">');
							}
						}
					</script>
					</td>
                  <td width="20"><a href="#" onClick="popup('/page/nav/aide/aide.htm','aide', '490', '310', 'no', 'yes');"><h1 id="c3">?</h1></a></td>
                </tr>
                <tr> 
						<td colspan="2" class="logCell2" id="c3">Code&nbsp;confidentiel</td>
					</tr>
					<tr>
						<td class="logCell1">
					<script language="javascript"> 
					if (navigator.appName == "Netscape")
						{
					 	if (navigator.userAgent.indexOf('Mac') != -1)
							{
							document.write('<input type="password" name="mdp" id="mdp" size="10" maxlength="12" class="logField">');
							}
						else
							{
							if (navigator.appVersion.substring(0,1) == "4")
								{
								document.write('<input type="password" name="mdp" id="mdp" size="10" maxlength="12" class="logField">');
								}
							else
								{		
								document.write('<input type="password" name="mdp" id="mdp" size="10" maxlength="12" class="logField">');
								}
							}
						}
					else 
						{
						if (navigator.userAgent.indexOf('Mac') != -1)
							{
							document.write('<input type="password" name="mdp" id="mdp" size="10" maxlength="12" class="logField">');
							}
						else
							{
							document.write('<input type="password" name="mdp" id="mdp" size="10" maxlength="12" class="logField">');
							}
						}
					</script>
                  </td>
                   <td><input type="image" src="/global/page/img/elements/ok_mdp.gif"  border="0" id=image1 name=image1></td>
                </tr>
                </table>
					</td> 
				</tr>
				<tr>
					<td colspan="2" class="bgc4"><img src="/page/img/bracket/LeftCustom_bracket_bottom.gif" alt="" width="15" height="6" /></td>
				 </tr>
				</table>
      </td>
    </tr>
  </table>
</form>
</body>

</html>

#########################################
https://www.axa.fr//transact/transversal/contrats/syntheseILIS.asp?numabo=19240191
ASPSESSIONIDTRDSTBDS=JJOPPNABGDJBLBLGJHIGAEEA; WEBTRENDS_ID=81.57.1.20-4253817904.29756301::EC6BA88ED5FE77252F3A83E47807CE54
HTTP/1.1 200 OK
Server: Microsoft-IIS/4.0
Date: Wed, 28 Dec 2005 09:06:30 GMT
Content-Length: 304
Content-Type: text/html
Expires: Fri, 31 May 1996 11:30:14 GMT
Cache-control: private

 <font face="Arial" size=2>
<p>Erreur d'ex&#233;cution Microsoft VBScript</font> <font face="Arial" size=2>error '800a01a8'</font>
<p>
<font face="Arial" size=2>Objet requis: 'Session(...)'</font>
<p>
<font face="Arial" size=2>/transact/lib/estclient.asp</font><font face="Arial" size=2>, line 26</font> 
