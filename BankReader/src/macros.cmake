# recurse down all subdirectories
MACRO( RecurseDown )
	file( GLOB subDirs * )
	foreach( subDir ${subDirs} )
		file(RELATIVE_PATH subDirRelative ${CMAKE_CURRENT_SOURCE_DIR} ${subDir})
		string(SUBSTRING ${subDirRelative} 0 1 firstChar)
		if (NOT ${firstChar} STREQUAL ".")
			if (IS_DIRECTORY ${subDir} )
				if (EXISTS ${subDir}/CMakeLists.txt) 
					message(STATUS "Going into: ${subDir}")
					add_subdirectory( ${subDir} )
				endif()
			endif()
		endif()
	endforeach()
ENDMACRO( RecurseDown )

# Set the folder  property to the parent folder name
MACRO( SetFolderName Target )
	# Group with other targets using the parent folder name
	get_filename_component(TargetFolder ${CMAKE_CURRENT_SOURCE_DIR} PATH)
	get_filename_component(TargetFolder ${TargetFolder} NAME)
	SET_PROPERTY(TARGET ${Target} PROPERTY FOLDER ${TargetFolder})
ENDMACRO()