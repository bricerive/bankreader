//
//  ConditionsArrayController.m
//  BankReader
//
//  Created by Brice Rivé on 3/3/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "ConditionsArrayController.h"
#import "Account.h"
#import "Condition.h"


@implementation ConditionsArrayController

- (Account *)currentAccount
{
	NSArray *selectedAccounts = [accountsController selectedObjects];
	if (selectedAccounts==nil || [selectedAccounts count]!=1) return nil;
	Account *crtAccount = [selectedAccounts objectAtIndex: 0];
	return crtAccount;
}

- (void)add:(id)sender
{
	[[self currentAccount] willAddCondition];
	[super add:sender];
	[[self currentAccount] didAddCondition];
}

- (void)remove:(id)sender
{
	[[self currentAccount] willRemoveCondition];
	[super remove:sender];
	[[self currentAccount] didRemoveCondition];
}

@end
