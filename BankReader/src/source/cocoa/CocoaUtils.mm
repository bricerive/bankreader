/*
 *  StrConv.cpp
 *  BankReader
 *
 *  Created by Brice Rivé on 10/14/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#include "CocoaUtils.h"
using std::string;

NSString *StrConv(const string &str) // convert a STL string into a Cocoa string
{ return [NSString stringWithUTF8String :str.c_str()]; }

string StrConv(NSString *str) // Convert a Cocoa string into a STL string
{ return str? [str UTF8String]: ""; }
