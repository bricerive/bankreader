//
//  Condition.mm
//  BankReader
//
//  Created by Brice Rivé on 10/18/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "Condition.h"
#include "CocoaUtils.h"
#include <mylib/idioms.h>
using namespace mylib;

@implementation Condition

+ (void)initialize
{
}

// Declare non-trivial Binding Keys dependencies
+ (NSSet *)keyPathsForValuesAffectingExpressionChanges
{
	return [NSSet setWithObjects:@"op", @"value", @"active", nil];
}

-(id) init
{
	if ((self = [super init])) {
		op = @"<";
		value=[[NSNumber numberWithFloat: 0] retain];
		active=true;
	}
	return self;
}

- (void)dealloc
{
	[op release];
	[value release];
	[super dealloc];
}


-(NSString *)getExpression
{
	return [NSString stringWithFormat: @"%@ %@", op, value];
}

#pragma mark NSCoding methods

- (void)encodeWithCoder:(NSCoder *)coder
{
	[coder encodeObject: op forKey: @"op"];
	[coder encodeFloat: [value floatValue] forKey: @"value"];
	[coder encodeBool: active forKey: @"active"];
}

- (id)initWithCoder:(NSCoder *)coder
{
	op = [[coder decodeObjectForKey:@"op"] retain];
	value = [[NSNumber numberWithFloat:[coder decodeFloatForKey:@"value"]] retain];
	active = [coder decodeBoolForKey:@"active"];
    return self;
}

#pragma mark KVM
- (BOOL)getActive
{
	return active;
}

- (void)setActive: (BOOL)val
{
	active=val;
}

- (NSString *)getOp
{
	return op;
}

- (void)setOp:(NSString *)val
{
	if (op!=val) {
		if (op) [op release];
		op = [val copy];
	}
}

- (NSNumber *)getValue
{
	return value;
}

- (void)setValue:(NSNumber *)val
{
	if (value!=val) {
		if (value) [value release];
		value = [val copy];
	}
}

-(BOOL)getExpressionChanged
{ return TRUE; }


#pragma mark Utilities

// for gdb's po command
-(NSString *) description
{
	std::string desc;
	desc += std::string("Op: ") + StrConv(op) + "\n";
	desc += std::string("Value: ") + to_string([value floatValue]) + "\n";
	desc += std::string("active: ") + to_string(active) + "\n";
	return StrConv(desc);
}

@end
