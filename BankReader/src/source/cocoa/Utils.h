//
//  Utils.h
//  BankReader
//
//  Created by Brice Rivé on 1/16/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#include <string>

std::string HostOfUrl(const std::string &url);
std::string RelativeToAbsoluteUrl(const std::string &relative, const std::string &current);
bool IsAccessibleHost(const std::string &hostName);
