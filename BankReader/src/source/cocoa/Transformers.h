//
//  Transformers.h
//  BankReader
//
//  Created by Brice Rivé on 10/13/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface bankToIconTransformer : NSValueTransformer {
}
@end

@interface valueToPeriodTransformer : NSValueTransformer {
}
@end

@interface alertedToIconTransformer : NSValueTransformer {
}
@end

@interface autoLockPeriodTransformer : NSValueTransformer {
}
@end

@interface currencyTransformer : NSValueTransformer {
}
@end
