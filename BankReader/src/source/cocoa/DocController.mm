//
//  DocController.mm
//  BankReader
//
//  Created by Brice Rivé on 3/15/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "DocController.h"
#import "BankReader.h"


@implementation DocController

// How to save the list of opened documents on application quit (to re-open them on next launch): cf. Controller.mm
- (void)closeAllDocumentsWithDelegate:(id)delegate didCloseAllSelector:(SEL)didCloseAllSelector contextInfo:(void *)contextInfo
{
	[(BankReader*)NSApp rememberOpenDocuments];
	[super closeAllDocumentsWithDelegate:delegate didCloseAllSelector:didCloseAllSelector contextInfo:contextInfo];
}

@end
