//
//  Controller.mm
//  BankReader
//
//  Created by Brice Riv� on 12/14/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

/* How to save the list of opened documents on application quit (to re-open them on next launch):

 i've since solved this issue, but it still seems far more complex than it should be. i've had to subclass NSApplication
 (to override replyToApplicationShouldTerminate) and i had to provide the delegate methods applicationShouldTerminate and applicationWillTerminate in my 
 app delegate. and i had to provide a subclass of NSQuitCommand and specify my subclass in my sdef file. and lastly, i had to provide a 
 subclass of NSDocumentController (to override closeAllDocumentsWithDelegate)
 
 basically it works this way:
 
 -in applicationShouldTerminate (in my app delegate), closeAllDocumentsWithDelegate (in my subclass of NSDocumentController),
 and in executeCommand (in my subclass of NSQuitCommand), i build an array of NSDocuments if the array doesn't already exist.
 -in applicationShouldTerminate i return NSTerminateNow.
 -in close (in MyDocument class) i either delete the document object from the array if the document doesn't have a url,
 or i replace the document object in the array with the fileURL of the document.
 (this covers the situation when the user saves an unsaved document as part of the quit process).
 -in applicationWillTerminate i close any documents that haven't yet been closed. i then iterate over the array, saving in
 prefs any URLs that are in the array.
 -in my override of replyToApplicationShouldTerminate. i release any document array to cover the situation in which the user cancels the quit.
 
 this works.
 */

#import "Controller.h"
#import "NDAlias/NDAlias.h"
#import "BankReader.h"
#import "DocController.h"

#include "View.h"
#include <AvailabilityMacros.h>

@implementation Controller

- (BOOL)applicationShouldOpenUntitledFile:(NSApplication *)sender
{
	emptyLaunched=TRUE;
	if ([[NSUserDefaults standardUserDefaults] boolForKey: @"reOpenLastDocumentOnLaunch"])
		if ([[NSUserDefaults standardUserDefaults] objectForKey: @"lastDocumentPath"])
			return FALSE;
	return TRUE;
}

- (void)applicationWillFinishLaunching:(NSNotification *)notification
{
	// How to save the list of opened documents on application quit (to re-open them on next launch): cf. Controller.mm
	// Create our subclass of NSDocunmentController here
	/*
	 The application does not ask for its shared document controller until after the applicationWillFinishLaunching: message is sent to its delegate.
	 Therefore, you can create an instance of your subclass of NSDocumentController in your application delegate�s applicationWillFinishLaunching:
	 method and that instance will be set as the shared document controller.
	 */
	[[DocController alloc] init];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	if (!emptyLaunched) return;
	if (![[NSUserDefaults standardUserDefaults] boolForKey: @"reOpenLastDocumentOnLaunch"]) return;
	NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey: @"lastDocumentPath"];
	if (!data) return;
	NDAlias *alias = [NDAlias aliasWithData: data];
	if (!alias) return;
	NSURL *fileURL = [alias url];
	if (!fileURL) return;
	NSDocumentController *docController = [NSDocumentController sharedDocumentController];
#if MAC_OS_X_VERSION_MAX_ALLOWED<MAC_OS_X_VERSION_10_4
	if ([docController openDocumentWithContentsOfURL:fileURL display:YES]==0)
		[docController openUntitledDocumentOfType:nil display:TRUE];
#else
	NSError *outError;
	if ([docController openDocumentWithContentsOfURL:fileURL display:YES error:&outError]==0)
		[docController openUntitledDocumentAndDisplay:TRUE error:&outError];
#endif
}

- (BOOL)applicationShouldTerminate:(id)sender
{
	// Restore application's dock icon
	[NSApp setApplicationIconImage: [NSImage imageNamed: @"BankReader"]];
	// How to save the list of opened documents on application quit (to re-open them on next launch): cf. Controller.mm
	[(BankReader*)NSApp rememberOpenDocuments];
	return NSTerminateNow;
}

- (void)applicationWillTerminate:(NSNotification *)aNotification
{
	// How to save the list of opened documents on application quit (to re-open them on next launch): cf. Controller.mm
	NSArray *documents = [NSApp orderedDocuments];
	if (documents!=0) {
		NSDocument *document;
		for (NSEnumerator *i=[documents objectEnumerator]; (document = [i nextObject]); )
			[document close];
	}
	bool hasLastDocument=false;
	NSArray *openDocumentURLs = [(BankReader*)NSApp getOpenDocumentURLs];
	if ([openDocumentURLs count]!=0) {
		NSURL *url = [openDocumentURLs  objectAtIndex:0];
		NDAlias *alias = [NDAlias aliasWithURL: url];
		[[NSUserDefaults standardUserDefaults] setObject:[alias data] forKey:@"lastDocumentPath"];
		hasLastDocument=true;
	}
	if (!hasLastDocument)
		[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"lastDocumentPath"];
}

@end
