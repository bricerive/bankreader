//
//  Account.h
//  BankReader
//
//  Created by Brice Riv� on 10/14/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface DatedAmount : NSObject <NSCoding>
{
	float amount;
	NSDate *date;
}
- (id)initWithData:(float)amount_ :(NSDate *)date_;
- (float)amount;
- (NSDate *)date;
@end

@interface Account : NSObject <NSCoding>
{
@private
	NSString *bankName;
	NSString *label;
	NSString *name;
	NSString *number;
	NSString *currency;
	NSString *type;
	bool active;
	bool alerted;
	NSMutableArray *conditions;
	NSMutableArray *amounts;
	id client;
}

+ (NSSet *)keyPathsForValuesAffectingAlerted;

#pragma mark -initialize
- (id) initWithData: (NSString *)bankName : (NSString *)label : (NSString *)number : (NSString *)currency : (NSString *)type;
- (void)setClient:(id) client;
- (id)getClient;

- (void) TakeHistory: (Account *)rhs;
- (void)updateAlerted;

#pragma mark -notifications from ConditionsArrayController
- (void)willAddCondition;
- (void)didAddCondition;
- (void)willRemoveCondition;
- (void)didRemoveCondition;

#pragma mark -accessors

-(NSString *)getBankName;
-(void) setBankName:(NSString *)val;
-(NSString *)getLabel;
-(void) setLabel:(NSString *)val;
-(NSString *)getName;
-(void) setName:(NSString *)val;
-(NSString *)getNumber;
-(void) setNumber:(NSString *)val;
-(NSString *)getCurrency;
-(void) setCurrency:(NSString *)val;
-(NSString *)getType;
-(void) setType:(NSString *)val;
-(NSString *)getIdentificator;
-(float)getAmount;
-(void)setAmount:(float)amount :(NSDate *)date;
-(BOOL)getActive;
-(void)setActive:(BOOL)val;
-(BOOL)getAlerted;
-(void)setAlerted:(BOOL)val;

-(NSMutableArray *)conditions;
-(NSMutableArray *)amounts;

#pragma mark -utilities

- (BOOL) isEqual: (Account *)rhs; // Used implicitly by "indexOfObject". Compares the labels.
-(NSString *) description; // for gdb's po command

@end
