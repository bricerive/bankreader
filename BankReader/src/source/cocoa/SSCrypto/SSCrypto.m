/*
 Copyright (c) 2003, Septicus Software All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution. 
 * Neither the name of Septicus Software nor the names of its contributors
 may be used to endorse or promote products derived from this software
 without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
//
//  SSCrypto.m
//  SimpleWebCam
//
//  Created by Ed Silva on Sat May 31 2003.
//  Copyright (c) 2003-2005 Septicus Software. All rights reserved.
//

#import "SSCrypto.h"

@implementation NSData (HexDump)

- (NSString *)hexdump
{
    NSMutableString *ret=[NSMutableString stringWithCapacity:[self length]*2];
    /* dumps size bytes of *data to string. Looks like:
    * [0000] 75 6E 6B 6E 6F 77 6E 20
    *                  30 FF 00 00 00 00 39 00 unknown 0.....9.
    * (in a single line of course)
    */
    unsigned int size= [self length];
    const unsigned char *p = [self bytes];
    unsigned char c;
    int n;
    char bytestr[4] = {0};
    char addrstr[10] = {0};
    char hexstr[ 16*3 + 5] = {0};
    char charstr[16*1 + 5] = {0};
    for(n=1;n<=size;n++) {
        if (n%16 == 1) {
            /* store address for this line */
            snprintf(addrstr, sizeof(addrstr), "%.4x",
                     ((unsigned int)p-(unsigned int)self) );
        }
        
        c = *p;
        if (isalnum(c) == 0) {
            c = '.';
        }
        
        /* store hex str (for left side) */
        snprintf(bytestr, sizeof(bytestr), "%02X ", *p);
        strncat(hexstr, bytestr, sizeof(hexstr)-strlen(hexstr)-1);
        
        /* store char str (for right side) */
        snprintf(bytestr, sizeof(bytestr), "%c", c);
        strncat(charstr, bytestr, sizeof(charstr)-strlen(charstr)-1);
        
        if(n%16 == 0) {
            /* line completed */
            //printf("[%4.4s]   %-50.50s  %s\n", addrstr, hexstr, charstr);
            [ret appendString:[NSString stringWithFormat:@"[%4.4s]   %-50.50s  %s\n",
                addrstr, hexstr, charstr]];
            hexstr[0] = 0;
            charstr[0] = 0;
        } else if(n%8 == 0) {
            /* half line: add whitespaces */
            strncat(hexstr, "  ", sizeof(hexstr)-strlen(hexstr)-1);
            strncat(charstr, " ", sizeof(charstr)-strlen(charstr)-1);
        }
        p++; /* next byte */
    }
    
    if (strlen(hexstr) > 0) {
        /* print rest of buffer if not empty */
        //printf("[%4.4s]   %-50.50s  %s\n", addrstr, hexstr, charstr);
        [ret appendString:[NSString stringWithFormat:@"[%4.4s]   %-50.50s  %s\n",
            addrstr, hexstr, charstr]];
    }
    return ret;
}

@end


// SSCrypto object
@implementation SSCrypto

- (id)init
{
    if ((self = [super init])){
        OpenSSL_add_all_digests();
        OpenSSL_add_all_algorithms();
        OpenSSL_add_all_ciphers();
        ERR_load_crypto_strings();
        SSL_load_error_strings();
        return self;
    }
    return nil;
}

- (id)initWithSymmetricKey:(NSData *)k
{
    if ((self = [super init])){
        OpenSSL_add_all_digests();
        OpenSSL_add_all_algorithms();
        OpenSSL_add_all_ciphers();
        ERR_load_crypto_strings();
        SSL_load_error_strings();
        [self setSymmetricKey:k];
        [self setSymmetric:YES];
        return self;
    }
    return nil;
}

- (id)initWithRSAKeys:(NSData *)pub private:(NSData *)priv
{
    if ((self = [super init])){
        OpenSSL_add_all_digests();
        OpenSSL_add_all_algorithms();
        OpenSSL_add_all_ciphers();
        ERR_load_crypto_strings();
        SSL_load_error_strings();
        [self setPublicKey:pub];
        [self setPrivateKey:priv];
        [self setSymmetric:NO];
        return self;
    }
    return nil;
}

- (id)initWithPublicKey:(NSData *)pub
{
    if ((self = [super init])){
        OpenSSL_add_all_digests();
        OpenSSL_add_all_algorithms();
        OpenSSL_add_all_ciphers();
        ERR_load_crypto_strings();
        SSL_load_error_strings();
        [self setPublicKey:pub];
        [self setSymmetric:NO];
        return self;
    }
    return nil;
}

- (id)initWithPrivateKey:(NSData *)priv
{
    if ((self = [super init])){
        OpenSSL_add_all_digests();
        OpenSSL_add_all_algorithms();
        OpenSSL_add_all_ciphers();
        ERR_load_crypto_strings();
        SSL_load_error_strings();
        [self setPrivateKey:priv];
        [self setSymmetric:NO];
        return self;
    }
    return nil;
}

- (void)dealloc
{
    EVP_cleanup();
    ERR_free_strings();
    [super dealloc];
}

- (NSData *)decrypt
{
    return [self decrypt:nil];
}

- (NSData *)decrypt:(NSString *)cipherName
{
    if (cipherText == nil){
        return nil;
    }
    
    unsigned char *outbuf;
    int outlen, templen, inlen;
    inlen = [cipherText length];
    unsigned char *input = (unsigned char *)[cipherText bytes];
    
    if ([self symmetric]) {
        unsigned char *evp_key = (unsigned char *)[[self symmetricKey] bytes];
        EVP_CIPHER_CTX cCtx;
        const EVP_CIPHER *cipher;
        
        if (cipherName){
            cipher = EVP_get_cipherbyname((const char *)[cipherName UTF8String]);
            if (!cipher){
                NSLog(@"cannot get cipher with name %@", cipherName);
                return nil;
            }
        } else {
            cipher = EVP_bf_cbc();
            if (!cipher){
                NSLog(@"cannot get cipher with name %@", @"EVP_bf_cbc");
                return nil;
            }
        }
        
        EVP_CIPHER_CTX_init(&cCtx);
        
        if (!EVP_DecryptInit(&cCtx, cipher, evp_key, NULL)) {
            NSLog(@"EVP_DecryptInit() failed for cipher: %@!", cipherName);
            EVP_CIPHER_CTX_cleanup(&cCtx);
            return nil;
        }
        EVP_CIPHER_CTX_set_key_length(&cCtx, [[self symmetricKey] length]);

        // sanity check...
        if (inlen == 0) {
            EVP_CIPHER_CTX_cleanup(&cCtx);
            return nil;
        }
        // add a couple extra blocks to the outbuf to be safe
        outbuf = (unsigned char *)calloc(inlen+32, sizeof(unsigned char));
        NSAssert(outbuf, @"Cannot allocate memory for buffer!");
        
        if (!EVP_DecryptUpdate(&cCtx, outbuf, &outlen, input, inlen)){
            NSLog(@"EVP_DecryptUpdate() failed for cipher: %@, line: %d!", cipherName, __LINE__);
            EVP_CIPHER_CTX_cleanup(&cCtx);
            return nil;
        }
        
        if (!EVP_DecryptFinal(&cCtx, outbuf + outlen, &templen)){
            NSLog(@"EVP_DecryptFinal() failed for cipher: %@, line: %d!", cipherName, __LINE__);
            EVP_CIPHER_CTX_cleanup(&cCtx);
            return nil;
        }
        
        outlen += templen;
        EVP_CIPHER_CTX_cleanup(&cCtx);
        
    } else {
        if ([self publicKey] == nil) {
            NSLog(@"cannot decrypt without the public key, which is currently nil");
            return nil;
        }
        
        BIO *publicBIO = NULL;
        RSA *publicRSA = NULL;
        
        if (!(publicBIO = BIO_new_mem_buf((unsigned char *)[[self publicKey] bytes], [[self publicKey] length]))) {
            NSLog(@"BIO_new_mem_buf() failed!");
            return nil;
        }
        
        if (!PEM_read_bio_RSA_PUBKEY(publicBIO, &publicRSA, NULL, NULL)) {
            NSLog(@"PEM_read_bio_RSA_PUBKEY() failed!");
            return nil;
        }			
        
        outbuf = (unsigned char *)malloc(RSA_size(publicRSA));
        
        if (!(outlen = RSA_public_decrypt(inlen,
                                          input,
                                          outbuf,
                                          publicRSA,
                                          RSA_PKCS1_PADDING))) {
            NSLog(@"RSA_public_decrypt() failed!");
            return nil;
        }
        
        if (outlen == -1) {
            NSLog(@"Decrypt error: %s (%s)",
                  ERR_error_string(ERR_get_error(), NULL),
                  ERR_reason_error_string(ERR_get_error()));
            return nil;
        }
        
        if (publicBIO) BIO_free(publicBIO);
        if (publicRSA) RSA_free(publicRSA);
    }
    [self setClearTextWithData:[NSData dataWithBytes:outbuf length:outlen]];
    
    if (outbuf) {
        free(outbuf);
    }
    
    return [self clearTextAsData];
}

- (NSData *)encrypt
{
    if ([self symmetric] && [self symmetricKey]) {
        return [self encrypt:@"aes128"];
    } else if ([self privateKey]) {
        return [self encrypt:nil];
    } else {
        NSLog(@"no key set!");
        return nil;
    }
}

- (NSData *)encrypt:(NSString *)cipherName
{
    if (clearText == nil) {
        return nil;
    }
    
    unsigned char *input = (unsigned char *)[clearText bytes];
    unsigned char *outbuf;
    int outlen, templen, inlen;
    inlen = [clearText length];
    
    if ([self symmetric]) {
        unsigned char *evp_key = (unsigned char *)[[self symmetricKey] bytes];
        EVP_CIPHER_CTX cCtx;
        const EVP_CIPHER *cipher;
        
        if (cipherName){
            cipher = EVP_get_cipherbyname((const char *)[cipherName UTF8String]);
            if (!cipher){
                NSLog(@"cannot get cipher with name %@", cipherName);
                return nil;
            }
        } else {
            cipher = EVP_bf_cbc();
            if (!cipher){
                NSLog(@"cannot get cipher with name %@", @"EVP_bf_cbc");
                return nil;
            }
        }
        
        EVP_CIPHER_CTX_init(&cCtx);
        
        if (!EVP_EncryptInit(&cCtx, cipher, evp_key, NULL)) {
            NSLog(@"EVP_EncryptInit() failed!");
            EVP_CIPHER_CTX_cleanup(&cCtx);
            return nil;
        }
        EVP_CIPHER_CTX_set_key_length(&cCtx, [[self symmetricKey] length]);
        
        // sanity check...
        if (inlen == 0) {
            EVP_CIPHER_CTX_cleanup(&cCtx);
            return nil;
        }
        // add a couple extra blocks to the outbuf to be safe
        outbuf = (unsigned char *)calloc(inlen+32, sizeof(unsigned char));
        NSAssert(outbuf, @"Cannot allocate memory for buffer!");
        
        if (!EVP_EncryptUpdate(&cCtx, outbuf, &outlen, input, inlen)){
            NSLog(@"EVP_EncryptUpdate() failed!");
            EVP_CIPHER_CTX_cleanup(&cCtx);
            return nil;
        }
        if (!EVP_EncryptFinal(&cCtx, outbuf + outlen, &templen)){
            NSLog(@"EVP_EncryptFinal() failed!");
            EVP_CIPHER_CTX_cleanup(&cCtx);
            return nil;
        }
        outlen += templen;
        EVP_CIPHER_CTX_cleanup(&cCtx);
        
    } else {
        if ([self privateKey] == nil) {
            NSLog(@"cannot encrypt without the key, which is currently nil");
            return nil;
        }
        
        BIO *privateBIO = NULL;
        RSA *privateRSA = NULL;
        
        if(!(privateBIO = BIO_new_mem_buf((unsigned char*)[[self privateKey] bytes], [[self privateKey] length]))) {
            NSLog(@"BIO_new_mem_buf() failed!");
            return nil;
        }
        
        if (!PEM_read_bio_RSAPrivateKey(privateBIO, &privateRSA, NULL, NULL)) {
            NSLog(@"PEM_read_bio_RSAPrivateKey() failed!");
            return nil;
        }
        
        unsigned long check = RSA_check_key(privateRSA);
        if (check != 1) {
            NSLog(@"RSA_check_key() failed with result %ld!", check);
            return nil;
        }			
        
        outbuf = (unsigned char *)malloc(RSA_size(privateRSA));
        
        if (!(outlen = RSA_private_encrypt(inlen,
                                           input,
                                           (unsigned char*)outbuf,
                                           privateRSA,
                                           RSA_PKCS1_PADDING))) {
            NSLog(@"RSA_private_encrypt failed!");
            return nil;
        }
        
        if (outlen == -1) {
            NSLog(@"Encrypt error: %s (%s)",
                  ERR_error_string(ERR_get_error(), NULL),
                  ERR_reason_error_string(ERR_get_error()));
            return nil;
        }
    }
    [self setCipherText:[NSData dataWithBytes:outbuf length:outlen]];
    
    if (outbuf) {
        free(outbuf);
    }
    
    return [self cipherTextAsData];
}

- (NSData *)digest:(NSString *)digestName
{
    if(clearText == nil) {
        return nil;
    }
    
    unsigned char outbuf[EVP_MAX_MD_SIZE];
    unsigned int templen, inlen;
    unsigned char *input=(unsigned char*)[clearText bytes];
    EVP_MD_CTX ctx;
    const EVP_MD *digest = NULL;
    
    inlen = [clearText length];
    
    if(inlen==0)
        return nil;
    if(digestName) {
        digest = EVP_get_digestbyname((const char*)[digestName UTF8String]);        
        if (!digest) {
            NSLog(@"cannot get digest with name %@",digestName);
            return nil;
        }
    } else {
        digest=EVP_md5();
        if(!digest) {
            NSLog(@"cannot get digest with name %@",@"MD5");
            return nil;
        }
    }
    
    EVP_MD_CTX_init(&ctx);
    EVP_DigestInit(&ctx,digest);
    if(!EVP_DigestUpdate(&ctx,input,inlen)) {
        NSLog(@"EVP_DigestUpdate() failed!");
        EVP_MD_CTX_cleanup(&ctx);
        return nil;			
    }
    if (!EVP_DigestFinal(&ctx, outbuf, &templen)) {
        NSLog(@"EVP_DigesttFinal() failed!");
        EVP_MD_CTX_cleanup(&ctx);
        return nil;
    }
    EVP_MD_CTX_cleanup(&ctx);
    
    return [NSData dataWithBytes:outbuf length:templen];
}

- (NSData *)symmetricKey
{
    return symmetricKey;
}

- (void)setSymmetricKey:(NSData *)k
{
    [k retain];
    [symmetricKey release];
    symmetricKey = k;
}

- (void)setPrivateKey:(NSData *)k
{
    [k retain];
    [privateKey release];
    privateKey = k;
    
    BIO *privateBIO = NULL;
    RSA *privateRSA = NULL;
    
    if ((privateBIO = BIO_new_mem_buf((unsigned char *)[privateKey bytes], -1))) {
        if (!PEM_read_bio_RSAPrivateKey(privateBIO, &privateRSA, NULL, NULL)) {
            NSLog(@"PEM_read_bio_RSAPrivateKey returned NULL for key: %@", k);
        }
    } else {
        NSLog(@"BIO_new_mem_buf returned NULL for private key: %@", k);
    }
    
    if (privateBIO) BIO_free(privateBIO);
    if (privateRSA) RSA_free(privateRSA);
}

- (void)setSymmetric:(BOOL)s
{
    symmetric = s;
}

- (BOOL)symmetric
{
    return symmetric;
}

- (NSData *)publicKey
{
    return publicKey;
}

- (void)setPublicKey:(NSData *)k
{
    [k retain];
    [publicKey release];
    publicKey = k;
}

- (NSData *)cipherTextAsData
{
    return cipherText;
}

- (NSString *)cipherTextAsString
{
    return [[[NSString alloc] initWithFormat:@"%s", [[self cipherTextAsData] bytes]] autorelease];
}

- (void)setCipherText:(NSData *)c
{
    [c retain];
    [cipherText release];
    cipherText = c;
}

- (NSData *)clearTextAsData
{
    return clearText;
}

- (NSString *)clearTextAsString
{
    return [[[NSString alloc] initWithFormat:@"%s", [[self clearTextAsData] bytes]] autorelease];
}

- (void)setClearTextWithString:(NSString *)c
{
    [c retain];
    [clearText release];
    clearText = [[NSData alloc] initWithBytes:[c UTF8String] length:([c length] + 1)];
    [c release];
}

- (void)setClearTextWithData:(NSData *)c
{
    [c retain];
    [clearText release];
    clearText = c;
}

- (NSString *)description
{
    NSString *format = @"clearText: %@, cipherText: %@, symmetricKey: %@, publicKey: %@, privateKey: %@";
    NSString *desc = [NSString stringWithFormat:format, [clearText hexdump], [cipherText hexdump],
        [symmetricKey hexdump], [publicKey hexdump], [privateKey hexdump]];
    return desc;
}

- (NSData *)privateKey
{
    return privateKey;
}

+ (NSData *)decodeBase64Data:(NSData *)base64
{
    const char *buffer = (const char *)[base64 bytes];
    char *dest = (char *)calloc([base64 length], sizeof(char));
    NSData *ret;
    
    NSAssert(dest, @"dest string memory allocation");
    
    size_t new_size = Curl_base64_decode(buffer, dest);
    
    ret = [NSData dataWithBytesNoCopy:dest length:new_size freeWhenDone:YES];
    return ret;
}

+ (NSData *)encodeBase64Data:(NSData *)base64
{
    const char *buffer = (const char *)[base64 bytes];
    size_t size = [base64 length];
    char *dest = (char *)calloc(size + (int)(size * 0.33), sizeof(char)); // add 33% more
    NSData *ret;
    
    NSAssert(dest, @"dest string memory allocation");
    
    size_t new_size = Curl_base64_encode(buffer, size, &dest);
    ret = [NSData dataWithBytesNoCopy:dest length:new_size freeWhenDone:YES];
    return ret;
}

+ (NSData *)getKeyDataWithLength:(int)length
{
    NSData *randData = nil;
    unsigned char *buffer;

    buffer = (unsigned char *)calloc(length*4, sizeof(unsigned char));
    NSAssert((buffer != NULL), @"Cannot calloc memory for buffer.");
    
    if (!RAND_pseudo_bytes(buffer, length)){
        free(buffer);
    } else {
        int slice = 0;
        int count = 0;
        unsigned char slice_buf[length];
        srandomdev();
        
        while (count < length){
            slice = random() % length;
            slice_buf[count] = buffer[slice];
            count++;
        }            
        randData = [NSData dataWithBytes:buffer length:length];
        free(buffer);
    }
    
    return randData;
}

+ (NSData *)getSHA1ForData:(NSData *)d
{
    unsigned length = [d length];
    const void *buffer = [d bytes];
    uint8_t md[SHA_DIGEST_LENGTH];
    
    (void)SHA1(buffer, length, md);
    
    return [NSData dataWithBytes:md length:SHA_DIGEST_LENGTH];
}


@end
