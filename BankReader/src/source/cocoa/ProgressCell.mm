//
//  ProgressCell.m
//  BankReader
//
//  Created by Brice Rivé on 3/24/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "ProgressCell.h"


@implementation ProgressCell

- (id)initWithProgressIndicator:(NSProgressIndicator *)progressIndicator_
{
	if ( (self = [super init]) ) {
		progressIndicator = progressIndicator_;
	}
	return self;
}

- copyWithZone : (NSZone *)zone
{
    ProgressCell *cell = (ProgressCell *)[super copyWithZone:zone];
    cell->progressIndicator = progressIndicator;
    return cell;
}

- (void)drawInteriorWithFrame : (NSRect)cellFrame inView : (NSView *)controlView
{
	if (![progressIndicator superview])
		[controlView addSubview:progressIndicator];
    [progressIndicator setFrame:cellFrame];
	if ([self objectValue])
		[progressIndicator setDoubleValue: [[self objectValue] doubleValue]];
	else
		[progressIndicator setDoubleValue: 0.0];
}

@end
