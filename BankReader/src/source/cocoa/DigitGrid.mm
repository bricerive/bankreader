//
//  DigitGrid.mm
//  BankReader
//
//  Created by Brice Riv� on 10/13/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "DigitGrid.h"
#include "CocoaUtils.h"
#include <mylib/error.h>
#include <mylib/log.h>
using namespace mylib;


#define WANTS_DUMP 1
#define DUMP WANTS_DUMP && _DEBUG

struct Pixel {
	unsigned char r,g,b;
	int Average() const {return (static_cast<int>(r)+g+b)/3;}
	int operator-(const Pixel &rhs)const {
		return abs(Average()-rhs.Average());
	}
};

static void CopyPixelsToBuffer(NSBitmapImageRep *image, Pixel *buffer)
{
#if MAC_OS_X_VERSION_MAX_ALLOWED>=MAC_OS_X_VERSION_10_4
	LogF(LOG_STATUS, "CopyPixelsToBuffer is using getPixel:");
	for (int row=0; row<[image pixelsHigh]; row++) {
		for (int col=0; col<[image pixelsWide]; col++) {
			NSUInteger pixel[4];
			[image getPixel:pixel atX:col y:row];
			buffer->r = pixel[0];
			buffer->g = pixel[1];
			buffer->b = pixel[2];
			buffer++;
		}
	}
#else
	LogF(LOG_STATUS, "CopyPixelsToBuffer is using bitmapData:");
	unsigned char *data=[image bitmapData];
	BOOL isPlanar = [image isPlanar];
	if (isPlanar) throw Err(_WHERE, "Planar image data!!!");
	int samplesPerPixel = [image samplesPerPixel];
	int bytesPerRow = [image bytesPerRow];
	
	if (samplesPerPixel<3 || samplesPerPixel>4) throw Err(_WHERE, "Bad samplesPerPixel: %d", samplesPerPixel);
	for (int row=0; row<[image pixelsHigh]; row++) {
		unsigned char *dataP = data+row*bytesPerRow;
		for (int col=0; col<[image pixelsWide]; col++) {
			buffer->r = *dataP++;
			buffer->g = *dataP++;
			buffer->b = *dataP++;
			if (samplesPerPixel==4) dataP++;
			buffer++;
		}
	}	
#endif
}

static unsigned int FindBestPosition(const Pixel *gridPixels, unsigned int gridWidth, unsigned int gridHeight
									 , const Pixel *digitPixels, unsigned int digitWidth, unsigned int digitHeight
									 , int &minRow, int &minCol)
{
	unsigned int minDiff(static_cast<unsigned int>(-1));
	for (int gridX=0; gridX<gridWidth-digitWidth+2; gridX++) {
		for (int gridY=0; gridY<gridHeight-digitHeight+1; gridY++) {
			unsigned int dist = 0;
			for (int digitX=0; digitX<digitWidth; digitX++) {
				for (int digitY=0; digitY<digitHeight; digitY++) {
					Pixel gridPixel = gridPixels[gridX+digitX + gridWidth*(gridY+digitY)];
					Pixel digitPixel = digitPixels[digitX + digitWidth*digitY];
					unsigned int pixDiff = gridPixel-digitPixel;
					dist += pixDiff;
				}
			}
			if (dist<minDiff) {
				minDiff = dist;
				minRow = gridY;
				minCol = gridX;
			}
		}
	}
	return minDiff;
}

#import <Cocoa/Cocoa.h>

static Pixel *LoadGridImageIntoNewBuffer(const std::string &gridData, unsigned int &gridHeight, unsigned int &gridWidth)
{
	 // make an autorelease pool
	ObjCAutoRelease killer([[NSAutoreleasePool alloc] init]);
	// Find beginning of image in grid data
	unsigned long imgDataOffset = gridData.find("\r\n\r\n")+4;
	if (imgDataOffset==std::string::npos) throw Err(_WHERE, "Unexpected data in image buffer");
	// Make an NSData out of it
	NSData *grilleData = [NSData dataWithBytes:gridData.c_str()+imgDataOffset length:gridData.size()-imgDataOffset];
	if (!grilleData) throw Err(_WHERE, "Cannot find grid image data");
#if DUMP
	[grilleData writeToFile:@"Grid.gif" atomically:false];
#endif
	// Make an image
	NSBitmapImageRep *gridImage = [NSBitmapImageRep imageRepWithData: grilleData];
	if (!gridImage) throw Err(_WHERE, "Cannot create grid image");
	gridHeight = [gridImage pixelsHigh];
	gridWidth = [gridImage pixelsWide];
	// Copy grid image pixel to local buffer
	Pixel *gridPixels = new Pixel[gridHeight*gridWidth];
	CopyPixelsToBuffer(gridImage, gridPixels);
	return gridPixels;
}

unsigned int DigitGrid::Process(const std::string &grid, const char *digitBuffer, int digitSize, std::vector<int> &layout)
{
	unsigned int delta=0; // cummulative registration error over the 10 digits

	// Load the grid image
	unsigned int gridHeight;
	unsigned int gridWidth;
	Pixel *gridPixels=LoadGridImageIntoNewBuffer(grid, gridHeight, gridWidth);
	
	// Look for the ten digits in the grid
	const char *digitBufferPt = digitBuffer;
	for (int digit=0; digit<10; digit++) {
		const unsigned int *headerPtr = reinterpret_cast<const unsigned int *>(digitBufferPt);
		int digitWidth = *headerPtr++;
		int digitHeight = *headerPtr++;
		const Pixel *digitPixels = reinterpret_cast<const Pixel *>(headerPtr);
		digitBufferPt = reinterpret_cast<const char *>(digitPixels + digitWidth*digitHeight);
		if (digitBufferPt-digitBuffer > digitSize) throw Err(_WHERE, "Digit buffer size error"); // internal error! don't bother about memleak
		
		int minRow;
		int minCol;

		// Brute force
		delta += FindBestPosition(gridPixels, gridWidth, gridHeight, digitPixels, digitWidth, digitHeight, minRow, minCol);

		// Store the digits coordinates
		layout.push_back(minCol);
		layout.push_back(minRow);
		layout.push_back(minCol+digitWidth);
		layout.push_back(minRow+digitHeight);
		
	}
	delete[] gridPixels;
	LogF(LOG_STATUS, "Cummulated grid error: %d", delta);
	return delta;
}
