//
//  ProgressCell.h
//  BankReader
//
//  Created by Brice Rivé on 3/24/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <AppKit/NSProgressIndicator.h>

@interface ProgressCell : NSCell {
    NSProgressIndicator *progressIndicator;
	float updateProgress;
}

- (id)initWithProgressIndicator:(NSProgressIndicator *)progressIndicator;

@end
