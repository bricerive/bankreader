//
//  ClientArrayController.h
//  BankReader
//
//  Created by brice rive on 07/03/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

// Override the client's NSArrayController to observe the selection of accounts
// and update the selected client accordingly
@interface ClientArrayController : NSArrayController {
	IBOutlet NSArrayController *accountsController;
}

@end 
