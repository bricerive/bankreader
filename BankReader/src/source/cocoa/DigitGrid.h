//
//  DigitGrid.h
//  BankReader
//
//  Created by Brice Riv� on 10/13/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#include <string>
#include <vector>

class DigitGrid {
public:
	static unsigned int Process(const std::string &grille, const char *digitBuffer, int digitSize, std::vector<int> &layout);
};

