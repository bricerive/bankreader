//
//  Client.h
//  BankReader
//
//  Created by Brice Rivé on 10/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "passwordProvider.h"
#import "notificationProvider.h"
#include <string>
#include "Server.h" // C++ core

class Bank;

@interface Client : NSObject <NSCoding>
{
@private
	NSString *name;
	NSString *bank;
	NSString *login;
	NSNumber *amount; // For fixed amount Virtual Bank
	NSString *currency; // For fixed amount Virtual Bank
	NSDate *birthDate;
	int updatePeriodIndex;
	NSDate *lastUpdate;
	NSString *lastUpdateStatus;
	double updateProgress;

    NSMutableArray *accounts;
	
	BOOL updating;
	BOOL forceUpdate;
	Server *myServer;
}

- (BOOL) checkUpdate: (id <PasswordProvider>)passwordProvider : (id <NotificationProvider>)notificationProvider;
- (void) forceUpdate;
- (void) visitWebPage;

// KVC
+ (NSSet *)keyPathsForValuesAffectingHideCurrency;
+ (NSSet *)keyPathsForValuesAffectingHideAmount;
+ (NSSet *)keyPathsForValuesAffectingHideBirthDate;
+ (NSSet *)keyPathsForValuesAffectingHideClientId;
+ (NSSet *)keyPathsForValuesAffectingHasNoAccount;
+ (NSSet *)keyPathsForValuesAffectingHasLoginInfo;
+ (NSSet *)keyPathsForValuesAffectingName;
+ (NSSet *)keyPathsForValuesAffectingHasWebSite;

- (void) setName: (NSString *)value;
- (void) setBank: (NSString *)value;
- (void) setLogin: (NSString *)value;
- (void) setBirthDate: (NSDate *)value;
- (void) setUpdatePeriodIndex: (int)value;
- (void) setLastUpdate: (NSDate *)value;
- (void) setLastUpdateStatus: (NSString *)value;
- (void) setUpdateProgress: (float)value;

- (NSString *) getName;
- (NSString *) getBank;
- (NSString *) getLogin;
- (NSDate *) getBirthDate;
- (int) getUpdatePeriodIndex;
- (NSDate *) getLastUpdate;
- (NSString *) getLastUpdateStatus;

- (NSArray *) accounts;

- (NSNumber *)getHasNoAccount;
- (NSNumber *)getHideBirthDate;
- (NSNumber *)getHideClientId;
- (NSNumber *)getHideAmount;
- (NSNumber *)getHideCurrency;
- (NSNumber *)getHasLoginInfo;
- (NSNumber *)getHasWebSite;

- (NSString *)StatusString;

@end
