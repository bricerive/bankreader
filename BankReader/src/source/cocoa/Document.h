//
//  Document.h
//  BankReader
//
//  Created by Brice Rivé on 10/12/05.
//  Copyright __MyCompanyName__ 2005 . All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "preview.h"
#include <string>
#import "passwordProvider.h"
#import "notificationProvider.h"

class ExchangeRate;

@interface Document : NSDocument <PasswordProvider, NotificationProvider>
{
	NSMutableArray *clients;
    NSMutableArray *accountLabels;
    NSMutableArray *accounts;

	IBOutlet NSTableView *clientTableView;
	IBOutlet NSPopUpButton *bankCtl;
	IBOutlet NSTextField *loginCtl;
	IBOutlet NSTextField *amountCtl;
	IBOutlet NSTextField *birthDateCtl;
	IBOutlet NSPopUpButton *currencyCtl;
	IBOutlet NSButton *updateNowCtl;
	IBOutlet preview *viewPreview;

	IBOutlet NSWindow *notificationPanel;
	IBOutlet NSTextField *notificationText;

	IBOutlet NSWindow *passwordSheet;
	IBOutlet NSTextField *passwordPrompt;
	IBOutlet NSSecureTextField *passwordValue;
	IBOutlet NSButton *passwordRemember;

	IBOutlet NSTextView *alertExpression;

	IBOutlet NSArrayController *accountsController;
	IBOutlet NSArrayController *clientsController;
	
	
	ExchangeRate *exchangeRate;
	BOOL showAllAccounts;
	BOOL showAllAlerts;
	
	// Ticker parameters
	NSString *viewer;
	NSString *currency;
	BOOL showTotal;
	BOOL showDetails;
	float tickerPeriod;
	
	NSRect windowFrame;
	NSData *lastSavedState;
}

// Declare non-trivial Binding Keys dependencies
+ (NSSet *)keyPathsForValuesAffectingHasNoClient;
+ (NSSet *)keyPathsForValuesAffectingAccounts;
+ (NSSet *)keyPathsForValuesAffectingAccountLabels;

- (NSArray *)accountLabels;
- (NSArray *)accounts;
- (void)Update:(NSTimer *)timer;
- (IBAction)passwordDialogClick:(id)sender;
- (IBAction)NotifyDialogClick:(id)sender;
- (IBAction)ConditionAccountSelected:(id)sender;
- (IBAction)UpdateAll:(id)sender;
- (IBAction)printDocument:(id)senderAddr;

- (IBAction)ClientCreated:(id)sender;
- (IBAction)BankSelected:(id)sender;
- (IBAction)CurrencySelected:(id)sender;

- (NSWindow *)documentWindow;

// KVC
- (NSNumber *)getHasNoClient;
- (BOOL)getShowAllAccounts;
- (void)setShowAllAccounts: (BOOL)val;
- (BOOL)getShowAllAlerts;
- (void)setShowAllAlerts: (BOOL)val;

- (NSString *)getPassword: (NSString *)bank: (NSString *)client: (BOOL)discardStored;
- (int) NotifyUserWithOption: (int)option :(NSString *)message;

@end
