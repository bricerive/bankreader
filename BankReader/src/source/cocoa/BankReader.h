// BankReader
// Our application object. Needed to provide static lists to GUI

#import <Cocoa/Cocoa.h>
#include "view.h"

class View;
class ExchangeRate;

@interface BankReader : NSApplication
{
	NSMutableArray *viewers;
	NSMutableArray *currencies;
	NSMutableArray *banks;
	ExchangeRate *exchange;
	View *view;
	NSArray *alertOperators;
	NSMutableArray *openDocuments;
}

- (NSArray *) viewers;
- (NSArray *) currencies;
- (NSArray *) banks;
- (NSArray *) alertOperators;

- (IBAction)ForgetPasswords:(id)sender;
- (IBAction)ViewInDock:(id)sender;

- (void)rememberOpenDocuments;
- (void)closeOpenDocument:(NSDocument *)document;
- (NSArray *)getOpenDocumentURLs;

- (void)UpdateDockIcon :(const std::string&)viewType :(bool)showDetails :(bool)showTotal :(const std::string&)currency :(const View::Accounts &)accounts :(bool)grabbing :(bool)alerting;

@end
