
@protocol PasswordProvider

- (NSString *)getPassword: (NSString *)bank : (NSString *)client : (BOOL)discardStored;

@end
