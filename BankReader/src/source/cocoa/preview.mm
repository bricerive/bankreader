//
//  preview.m
//  BankReader
//
//  Created by Brice Rive on 1/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "preview.h"
#include "view.h"

@implementation preview

- (void)dealloc {
	delete view;
	delete crtAccounts;
	[super dealloc];  
}

- (void)Update :(const std::string&)viewType :(bool)showDetails :(bool)showTotal :(const std::string&)currency :(const View::Accounts &)accounts :(bool)grabbing :(bool)alerting
{
	View *oldView=view;
	view = View::New(viewType, oldView);
	delete oldView;
	delete crtAccounts;
	crtAccounts = new View::Accounts(accounts);
	crtGrabbing = grabbing;
	crtAlerting = alerting;
	view->ShowDetails(showDetails);
	view->ShowTotal(showTotal);
	view->Currency(currency);
	view->Tick();
}

- (void)drawRect:(NSRect)rect 
{
	if (view==0) return;
	
	// clear to white
	[[NSColor whiteColor] set];  
	NSRectFill(rect);

	// Draw the view
	view->Draw(*crtAccounts, crtGrabbing, crtAlerting);
}

@end
