//
//  ConditionsArrayController.h
//  BankReader
//
//  Created by Brice Rivé on 3/3/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

// Override the conditions ArrayController to be able to notify the currently selected account
// of any addition/removal of conditions
@interface ConditionsArrayController : NSArrayController {
	IBOutlet NSArrayController *accountsController;
}

@end
