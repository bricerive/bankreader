//
//  Account.mm
//  BankReader
//
//  Created by Brice Rivé on 10/14/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#include "CocoaUtils.h"
#import "Account.h"
#include "Bank.h"
#include "Evaluator.h"
#import "Condition.h"
#include <mylib/error.h>
#include <mylib/idioms.h>
using namespace mylib;

@implementation DatedAmount

- (void)encodeWithCoder:(NSCoder *)coder
{
	[coder encodeFloat:amount forKey:@"amount"];
	[coder encodeObject:date forKey:@"date"];
}

- (id)initWithCoder:(NSCoder *)coder
{
	amount = [coder decodeFloatForKey:@"amount"];
	date = [[coder decodeObjectForKey:@"date"] retain];
	return self;
}

- (id)initWithData:(float)amount_ :(NSDate *)date_
{
	amount = amount_;
	date = [date_ copy];
	return self;
}

- (void) dealloc
{
	[date release];
	[super dealloc];
}

- (float)amount { return amount; }
- (NSDate *)date { return date; }

@end



@implementation Account

+ (void)initialize
{
}


// Declare non-trivial Binding Keys dependencies
+ (NSSet *)keyPathsForValuesAffectingAlerted
{
	return [NSSet setWithObjects:@"conditions", @"amount", nil];
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	// We're only observing our conditions, and when they change, we need to update our alerted
	[self updateAlerted];
}

- (void)observeConditions
{
	Condition *condition;
	for (NSEnumerator *conditionIdx=[conditions objectEnumerator]; (condition = [conditionIdx nextObject]); )
		[condition addObserver:self forKeyPath:@"expressionChanged" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)unobserveConditions
{
	Condition *condition;
	for (NSEnumerator *conditionIdx=[conditions objectEnumerator]; (condition = [conditionIdx nextObject]); )
		[condition removeObserver:self forKeyPath:@"expressionChanged"];
}

- (void)willAddCondition
{
	[self unobserveConditions];
}

- (void)didAddCondition
{
	[self observeConditions];
	[self updateAlerted];
}

- (void)willRemoveCondition
{
	[self unobserveConditions];
}

- (void)didRemoveCondition
{
	[self observeConditions];
	[self updateAlerted];
}

- (id)init
{
	if ( (self = [super init]) ) {
		amounts = [[NSMutableArray alloc] init];
	}
	return self;
}

- (id) initWithData: (NSString *)bankName_ : (NSString *)label_ : (NSString *)number_ : (NSString *)currency_ : (NSString *)type_
{
	if ( (self = [super init]) ) {
		amounts = [[NSMutableArray alloc] init];
		[self setBankName:bankName_];
		[self setLabel:label_];
		[self setNumber:number_];
		[self setCurrency:currency_];
		[self setType: type_];
		active = TRUE;
	}
	return self;
}

- (void)setClient:(id) client_
{
	client=client_;
}

- (id)getClient
{
	return client;
}

- (void)dealloc
{
	if (conditions) [conditions release];
	if (bankName) [bankName release];
	if (label) [label release];
	if (name) [name release];
	if (number) [number release];
	if (currency) [currency release];
	if (type) [type release];
	[amounts release];
	[super dealloc];
}

- (BOOL) isEqual: (Account *)rhs
{
	return [[self getIdentificator] isEqualToString:[rhs getIdentificator]];
}

- (void) TakeHistory: (Account *)rhs
{
	[rhs unobserveConditions];
	[self unobserveConditions];
	[conditions release];
	conditions = [[rhs conditions] retain];
	[self observeConditions];

	[amounts release];
	amounts = [[rhs amounts] retain];

	active = [rhs getActive];

	[self setName: [rhs getName]];
}

#pragma mark Accessors for Cocoa Bindings' KVP

- (NSString *)getBankName
{
	return bankName;
}

-(void) setBankName:(NSString *)val
{
	if (bankName) [bankName release];
	bankName = [val copy];
}

- (NSString *)getLabel
{
	return label;
}

-(void) setLabel:(NSString *)val
{
	if (label) [label release];
	label = [val copy];
}

- (NSString *)getName
{
	if (name) return name;
	if (label) return label;
	return NSLocalizedString(@"No label", 0);
}

-(void) setName: (NSString *)value
{
	if (name) [name autorelease];
	name=[value copy];
}

- (NSString *)getNumber
{
	return number;
}

-(void) setNumber:(NSString *)val
{
	if (number) [number release];
	number = [val copy];
}

- (NSString *)getCurrency
{
	return currency;
}

-(void) setCurrency:(NSString *)val
{
	if (currency) [currency release];
	currency = [val copy];
}

- (NSString *)getType
{
	return type;
}

-(void) setType:(NSString *)val
{
	if (type) [type release];
	type = [val copy];
}

- (BOOL)getActive
{
	return active;
}

- (float)getAmount
{
	return ([amounts count]==0)? 0.0: [[amounts objectAtIndex: [amounts count]-1] amount];
}

- (void)setAmount:(float)amount :(NSDate *)date
{
	[amounts addObject: [[DatedAmount alloc] initWithData:amount :date]];
	[self updateAlerted];
}

- (NSString *)getIdentificator
{
	return [NSString stringWithFormat: @"%@::%@::%@", bankName, label, number];
}

-(void)setActive:(BOOL)val
{
	active=val;
}

-(NSMutableArray *)conditions
{
	return conditions;
}

-(NSMutableArray *)amounts
{
	return amounts;
}

-(BOOL)getAlerted
{
	return alerted;
}

-(void)setAlerted:(BOOL)val
{
	alerted=val;
}

- (void)updateAlerted
{
	Evaluator evaluator;
	// make a map of all the account names and their values for the alerts computations
	Evaluator::VarMap varMap;
	varMap[StrConv([self getIdentificator])] = [self getAmount];
	
	BOOL newAlerted = FALSE;
	Condition *cfCondition;
	for (NSEnumerator *conditionIdx=[conditions objectEnumerator]; (cfCondition = [conditionIdx nextObject]); ) {
		if ([cfCondition getActive]) {
			std::string expression(std::string("[")+StrConv([self getIdentificator])+"] " + StrConv([cfCondition getExpression]));
			try {
				if (evaluator.Evaluate(varMap, expression))
					newAlerted = TRUE;
			} catch (Err &err) {
			}
		}
	}
	[self setAlerted: newAlerted];
}

#pragma mark NSCoding methods

- (void)encodeWithCoder:(NSCoder *)coder
{
	[coder encodeObject:[self getBankName] forKey:@"bank"];
	[coder encodeObject:[self getLabel] forKey:@"label"];
	[coder encodeObject:[self getName] forKey:@"name"];
	[coder encodeObject:[self getNumber] forKey:@"number"];
	[coder encodeObject:[self getCurrency] forKey:@"currency"];
	[coder encodeObject:[self getType] forKey:@"type"];
	[coder encodeBool:[self getActive] forKey:@"active"];
	[coder encodeObject:conditions forKey:@"conditions"];
	[coder encodeObject:amounts forKey:@"amounts"];
}

- (id)initWithCoder:(NSCoder *)coder
{
	bankName = [[coder decodeObjectForKey:@"bank"] retain];
	label = [[coder decodeObjectForKey:@"label"] retain];
	name = [[coder decodeObjectForKey:@"name"] retain];
	number = [[coder decodeObjectForKey:@"number"] retain];
	currency = [[coder decodeObjectForKey:@"currency"] retain];
	type = [[coder decodeObjectForKey:@"type"] retain];
	active = [coder decodeBoolForKey:@"active"];
	conditions = [[coder decodeObjectForKey:@"conditions"] retain];
	amounts = [[coder decodeObjectForKey:@"amounts"] retain];
	if (amounts==0)
		amounts = [[NSMutableArray alloc] init];
	if (conditions!=0) {
		[self observeConditions];
		[self updateAlerted];
	}
	return self;
}

// for gdb's po command
-(NSString *) description
{
	std::string desc;
	desc += std::string("Bank: ") + StrConv(bankName) + "\n";
	desc += std::string("Label: ") + StrConv(label) + "\n";
	desc += std::string("Description: ") + StrConv(name) + "\n";
	desc += std::string("currency: ") + StrConv(currency) + "\n";
	desc += std::string("number: ") + StrConv(number) + "\n";
	desc += std::string("active: ") + to_string(active) + "\n";
	desc += std::string("type: ") + StrConv(type) + "\n";
	return StrConv(desc);
}

@end
