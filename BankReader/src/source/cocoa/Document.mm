//
//  Document.m
//  BankReader
//
//  Created by Brice Rivé on 10/12/05.
//  Copyright __MyCompanyName__ 2005 . All rights reserved.
//

#import "Document.h"
#import "Client.h"
#import "Account.h"
#import "Transformers.h"
#import "BankReader.h"
#include "CocoaUtils.h"
#include <string>
#include "SecureStorage.h"
#include "ExchangeRate.h"
#include <mylib/log.h>
using namespace mylib;

#define ENCRYPT 1
#if ENCRYPT
#define ENCRYPT_IN 1
#define ENCRYPT_OUT 1
#import <openssl/evp.h>
#import <openssl/rand.h>
#import <openssl/rsa.h>
#import <openssl/engine.h>
#import <openssl/sha.h>
#import <openssl/pem.h>
#import <openssl/bio.h>
#import <openssl/err.h>
#import <openssl/ssl.h>
static const char *passPhrase="encrypt is safer than trust";
// RAII/RRID on openSSL
struct OpenSslResource
{
	OpenSslResource() {
		//EVP_add_cipher(EVP_bf_cbc());
        //OpenSSL_add_all_digests();
        //OpenSSL_add_all_algorithms();
        OpenSSL_add_all_ciphers();
        ERR_load_crypto_strings();
        SSL_load_error_strings();
	}
	~OpenSslResource() {
		EVP_cleanup();
		ERR_free_strings();
	}
};
static OpenSslResource openSslResource;
static const char *chosenCypher="blowfish";

#endif

#import "ProgressCell.h"

@implementation Document

#pragma mark initializations

- (NSWindow *)documentWindow
{
	NSArray *windowControllers = [self windowControllers];
	if ([windowControllers count] != 1) return nil;
	return [[windowControllers objectAtIndex: 0] window];
}

- (void)cppCtor
{
	exchangeRate = new ExchangeRate;
}

- (void)cppDtor
{
	delete exchangeRate;
}

+ (void)initialize
{
	// Declare Bindings transformers
    [NSValueTransformer setValueTransformer:[[bankToIconTransformer alloc] init] forName:@"bankToIconTransformer"];
    [NSValueTransformer setValueTransformer:[[valueToPeriodTransformer alloc] init] forName:@"valueToPeriodTransformer"];
    [NSValueTransformer setValueTransformer:[[alertedToIconTransformer alloc] init] forName:@"alertedToIconTransformer"];
    [NSValueTransformer setValueTransformer:[[currencyTransformer alloc] init] forName:@"currencyTransformer"];
}

// Declare non-trivial Binding Keys dependencies
+ (NSSet *)keyPathsForValuesAffectingHasNoClient { return [NSSet setWithObjects:@"conditions", @"clients", nil]; }
+ (NSSet *)keyPathsForValuesAffectingAccounts { return [NSSet setWithObjects:@"conditions", @"clients", nil]; }
+ (NSSet *)keyPathsForValuesAffectingAccountLabels { return [NSSet setWithObjects:@"conditions", @"clients", nil]; }


- (id)init
{
	[self cppCtor];
    self = [super init];
    if (self) {
		[self setHasUndoManager: NO];
		accountLabels = [[NSMutableArray alloc] init];
		accounts = [[NSMutableArray alloc] init];
		showAllAccounts=FALSE;
		showAllAlerts=FALSE;
		showTotal=TRUE;
		showDetails=TRUE;
		viewer=[[[NSUserDefaults standardUserDefaults] stringForKey: @"preferedViewer"] retain];
		currency=[[[NSUserDefaults standardUserDefaults] stringForKey: @"preferedCurrency"] retain];
		tickerPeriod=0.5;
    }
    return self;
}

- (void) dealloc
{
	[self cppDtor];
	[accountLabels release];
	[accounts release];
	[viewer release];
	[currency release];
	[super dealloc];
}

- (void)windowControllerDidLoadNib:(NSWindowController *) aController
{
    [super windowControllerDidLoadNib:aController];
	// Install our ticker timer
	[NSTimer scheduledTimerWithTimeInterval:tickerPeriod target:self selector:@selector(Update:) userInfo:nil repeats:NO];
	// Do this once the NIB loaded as it redirects a binding. Otherwise, the binding does not reflect the saved value for showAllAccounts
	[self setShowAllAccounts: showAllAccounts];
	[self setShowAllAlerts: showAllAlerts];
	[aController setShouldCascadeWindows: NO];
	//if ([self fileName]!=0) [aController setWindowFrameAutosaveName: [self fileName]];
	if (windowFrame.size.width!=0)
		[[self documentWindow] setFrame:windowFrame display:FALSE];

	NSProgressIndicator *progressIndicator = [[NSProgressIndicator alloc] init];
	[progressIndicator setIndeterminate:FALSE];
	//ProgressCell *progressCell = [[ProgressCell alloc] initWithProgressIndicator:progressIndicator];	
	//NSTableColumn *progColumn = [clientTableView tableColumnWithIdentifier:@"Progress"];
	//[progColumn setDataCell:progressCell];
}

- (void)close
{
	// If we are the last doc, we should clean up the dock icon
	NSArray *documents = [NSApp orderedDocuments];
	if ([documents count]==1)
		[NSApp setApplicationIconImage: [NSImage imageNamed: @"BankReader"]];

	// How to save the list of opened documents on application quit (to re-open them on next launch): cf. Controller.mm
	// Remove ourselve from the openDocuments array if we have no URL
	[NSApp closeOpenDocument:self];

	// Make sure we kill our pointer so that timer ticks don't crash
	viewPreview=0;
	[super close];
}

- (NSString *)windowNibName
{
    return @"Document";
}

#pragma mark Account lists

// Private
- (void)populateAccountList
{
	[accountLabels removeAllObjects];
	[accounts removeAllObjects];
	Client *cfClient;
	for (NSEnumerator *clientIdx=[clients objectEnumerator]; (cfClient = [clientIdx nextObject]); ) {
		NSArray *clientAccounts = [cfClient accounts];
		Account *cfAccount;
		for (NSEnumerator *accountIdx=[clientAccounts objectEnumerator]; (cfAccount = [accountIdx nextObject]); ) {
			[accountLabels addObject: [cfAccount getIdentificator]];
			[accounts addObject: cfAccount];
		}
	}
}

#pragma mark KVC Accessors

- (NSArray *) accountLabels {
	[self populateAccountList];
	return accountLabels;
}

- (NSArray *) accounts {
	[self populateAccountList];
	return accounts;
}

- (NSNumber *)getHasNoClient {
	return [NSNumber numberWithBool: clients? ([clients count]==0): TRUE];
}

- (BOOL)getShowAllAccounts
{
	return showAllAccounts;
}

- (void)setShowAllAccounts: (BOOL)val
{
	showAllAccounts=val;
	if (showAllAccounts) { // bind to our list of all accounts
		[accountsController bind: @"contentArray" toObject: self withKeyPath:@"accounts" options:nil];
	} else { // bind to the account list of the selected client
		[accountsController bind: @"contentArray" toObject: clientsController withKeyPath:@"selection.accounts" options:nil];
	}
}

- (BOOL)getShowAllAlerts
{
	return showAllAlerts;
}

- (void)setShowAllAlerts: (BOOL)val
{
	showAllAlerts=val;
}

#pragma mark Tick

- (void)Update:(NSTimer *)timer
{
	if (viewPreview==0) return; // Filter race-conditions on shutdown
	
	// Update the clients
	Client *cfClient;
	bool accountsChanged = false;
	bool updating=false;
	for (NSEnumerator *clientIdx=[clients objectEnumerator]; (cfClient = [clientIdx nextObject]); ) {
		if ([cfClient checkUpdate :self :self]) accountsChanged=true;
		if ( [[cfClient getLastUpdateStatus] localizedCompare:@"Updating"] == NSOrderedSame )
			updating=true;
	}

	// Update the accounts list
	if (accountsChanged) {
		[self willChangeValueForKey: @"accounts"];
		[self willChangeValueForKey: @"accountLabels"];
		[self didChangeValueForKey: @"accounts"];
		[self didChangeValueForKey: @"accountLabels"];	
	}
	
	// Check each active alert
	bool alerting=false;
	Account *cfAccount;
	for (NSEnumerator *accountIdx=[accounts objectEnumerator]; (cfAccount = [accountIdx nextObject]); ) {
		if ([cfAccount getAlerted])
			alerting = true;
	}
			
	// Make a C++ list of the accounts for viewers
	View::Accounts accountsCPP;
	for (NSEnumerator *accountIdx=[[self accounts] objectEnumerator]; (cfAccount = [accountIdx nextObject]); ) {
		if ([cfAccount getActive]) {
			View::ViewAccount::DatedValues datedValues;
			DatedAmount *datedAmount;
			float ratio;
			try {
				ratio = exchangeRate->GetRate(StrConv([cfAccount getCurrency]),StrConv(currency));
			} catch (Err &err) {
				// LogF(LOG_ERROR, err.what());
				updating=true;
				continue;
			}
			for (NSEnumerator *datedAmountIdx=[[cfAccount amounts] objectEnumerator]; (datedAmount = [datedAmountIdx nextObject]); ) {
				View::ViewAccount::DatedValue datedValue([datedAmount amount]*ratio, static_cast<unsigned long>(-[[datedAmount date] timeIntervalSinceNow]));
				datedValues.push_back(datedValue);
			}
			View::ViewAccount account(StrConv([cfAccount getBankName]), StrConv([cfAccount getName]),
								  StrConv([cfAccount getNumber]), StrConv([cfAccount getCurrency]),
								  StrConv([cfAccount getType]), datedValues);
			accountsCPP.push_back(account);
		}
	}
	
	// Refresh the preview
	std::string viewerC(StrConv(viewer));
	[viewPreview Update :viewerC :showDetails :showTotal :StrConv(currency) :accountsCPP :updating :alerting];
	[viewPreview setNeedsDisplay:YES]; // Trigger the refresh

	// If we are front document let the app update the dock icon
	NSArray *documents = [NSApp orderedDocuments];
	id current = [documents objectAtIndex:0];
	if (current == self) {
		if (!accountsCPP.empty()) {
			BankReader *theApp = NSApp;
			[theApp UpdateDockIcon :viewerC :showDetails :showTotal :StrConv(currency) :accountsCPP: updating: alerting];
		} else {
			[NSApp setApplicationIconImage: [NSImage imageNamed: @"BankReader"]];
		}
	}
	
	// Restart the timer
	[NSTimer scheduledTimerWithTimeInterval:tickerPeriod target:self selector:@selector(Update:) userInfo:nil repeats:NO];
}

#pragma mark IBActions

- (IBAction)ConditionAccountSelected:(id)sender;
{
	NSRange range = [alertExpression selectedRange];
	NSString *str = [alertExpression string];
	NSString *newStr = [NSString stringWithFormat: @"%@ [%@] %@",
		[str substringToIndex :range.location],
		[sender titleOfSelectedItem],
		[str substringFromIndex :range.location+range.length]
		];
	[alertExpression setString: newStr];
}

- (IBAction)UpdateAll:(id)sender
{
	// Force update the clients
	Client *cfClient;
	for (NSEnumerator *clientIdx=[clients objectEnumerator]; (cfClient = [clientIdx nextObject]); )
		[cfClient forceUpdate];
}

- (IBAction)printDocument:(id)sender
{
	[[self documentWindow] print: self];
}

// A new client was created, activate the bank selection control
- (IBAction)ClientCreated:(id)sender
{
	[clientsController add: self]; // Pass the message to the client array controller
    NSWindow *window  = [self documentWindow];
	[window makeFirstResponder: bankCtl];
}

// The client's bank has been selected, activate the next control
- (IBAction)BankSelected:(id)sender
{
	NSString *bankName = [sender titleOfSelectedItem];
	Bank::LoginInfo loginInfo;
	try {
		Bank::GetLoginInfo(StrConv(bankName), loginInfo);
	} catch (Err &err) {
		LogF(LOG_ERROR, err.what());
		return;
	}
	id nextCtl = updateNowCtl;
	for (Bank::LoginInfo::iterator i=loginInfo.begin(); i!=loginInfo.end(); ++i) {
		if (*i == Bank::LOGIN) {
			nextCtl = loginCtl;
			break;
		}
		if (*i == Bank::AMOUNT) {
			nextCtl = amountCtl;
			break;
		}
	}
    NSWindow *window  = [self documentWindow];
	[window makeFirstResponder: nextCtl];
}

// The virtual bank's currency is selected, activate update now
- (IBAction)CurrencySelected:(id)sender
{
    NSWindow *window  = [self documentWindow];
	[window makeFirstResponder: updateNowCtl];
}

#pragma mark PasswordProvider protocol

- (NSString *)getPassword: (NSString *)bank : (NSString *)client : (BOOL)discardStored
{
	std::string secureKey = StrConv(bank) + "::" + StrConv(client);
	std::string password;
	bool requestPassword=false;

	if (discardStored) {
		requestPassword=true;
	} else {
		try {
			password = SecureStorage::RetreivePassword(secureKey);
		} catch (Err &err) {
			requestPassword=true;
		}
	}
	if (requestPassword) {
		[passwordValue setStringValue: @""];
		NSString *promptFormat = NSLocalizedString(@"Please enter password for account %@", 0);
		[passwordPrompt setStringValue: [NSString stringWithFormat : promptFormat, StrConv(secureKey)]];
		[NSApp beginSheet:passwordSheet modalForWindow:[self windowForSheet] modalDelegate:self didEndSelector:0 contextInfo:0];
		[passwordSheet makeFirstResponder: passwordValue];
		int status = [NSApp runModalForWindow:passwordSheet];
		[NSApp endSheet:passwordSheet];
		[passwordSheet orderOut:self];
		if (!status) return 0;
		password = StrConv([passwordValue stringValue]);
		if ([passwordRemember intValue]!=0) {
			try {
			SecureStorage::StorePassword(secureKey, password);
			} catch (Err &err) {
				[self NotifyUserWithOption:0 :[NSString stringWithFormat : NSLocalizedString(@"%@", 0), StrConv(err.what())]];
		}
	}
	}
	return [[NSString alloc] initWithUTF8String: password.c_str()];
}

- (IBAction)passwordDialogClick:(id)sender
{
    [ NSApp stopModalWithCode: [sender tag] ];
}

#pragma mark NotificationProvider protocol
- (int) NotifyUserWithOption: (int)option :(NSString *)message;
{	
	// set the message
	[notificationText setStringValue: message];
	// bring up the modal sheet
	[NSApp beginSheet:notificationPanel modalForWindow:[self windowForSheet] modalDelegate:self didEndSelector:0 contextInfo:0];
	int status = [NSApp runModalForWindow:notificationPanel];
	[NSApp endSheet:notificationPanel];
	[notificationPanel orderOut:self];

	return status;
}

- (IBAction)NotifyDialogClick:(id)sender
{
    [ NSApp stopModalWithCode: [sender tag] ];
}

#pragma mark Document saving

#if ENCRYPT
- (NSData *)encryptData:(NSData *)input withKey:(const char *)passPhrase direction:(bool)encrypt
{
	EVP_CIPHER_CTX ctx; // Cypher context
	NSData *key = [NSData dataWithBytes:passPhrase length:strlen(passPhrase)]; // key data
	std::vector<unsigned char> outbuf([input length]+EVP_MAX_BLOCK_LENGTH); // add a couple extra blocks to the outbuf to be safe
    int updateLength, finalLength;
	
	if (encrypt) {
		if (!EVP_EncryptInit(&ctx, EVP_get_cipherbyname(chosenCypher), (unsigned char *)[key bytes], NULL)) throw Err(_WHERE, "EVP_EncryptInit failed");
		EVP_CIPHER_CTX_set_key_length(&ctx, [key length]);
		if (!EVP_EncryptUpdate(&ctx, &outbuf[0], &updateLength, (unsigned char *)[input bytes], [input length])) throw Err(_WHERE, "EVP_EncryptUpdate failed");
		if (!EVP_EncryptFinal(&ctx, &outbuf[updateLength] , &finalLength)) throw Err(_WHERE, "EVP_EncryptFinal failed");
	} else {
		if (!EVP_DecryptInit(&ctx, EVP_get_cipherbyname(chosenCypher), (unsigned char *)[key bytes], NULL)) throw Err(_WHERE, "EVP_EncryptInit failed");
		EVP_CIPHER_CTX_set_key_length(&ctx, [key length]);
		if (!EVP_DecryptUpdate(&ctx, &outbuf[0], &updateLength, (unsigned char *)[input bytes], [input length])) throw Err(_WHERE, "EVP_DecryptUpdate failed");
		if (!EVP_DecryptFinal(&ctx, &outbuf[updateLength], &finalLength)) throw Err(_WHERE, "EVP_DecryptFinal failed");
	}
	
    return [NSData dataWithBytes:&outbuf[0] length:updateLength+finalLength];
}
#endif

- (NSData *)archiveToNewData
{
	NSMutableData *data = [[NSMutableData data] retain];
	NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData: data];
	[archiver setOutputFormat:NSPropertyListBinaryFormat_v1_0];
	
	[archiver encodeObject:clients forKey:@"clients"];
	[archiver encodeBool:showAllAccounts forKey:@"showAllAccounts"];
	[archiver encodeBool:showAllAlerts forKey:@"showAllAlerts"];
	[archiver encodeBool:showTotal forKey:@"showTotal"];
	[archiver encodeBool:showDetails forKey:@"showDetails"];
	[archiver encodeObject:viewer forKey:@"viewer"];
	[archiver encodeObject:currency forKey:@"currency"];
	[archiver encodeFloat:tickerPeriod forKey:@"tickerPeriod"];
	[archiver encodeObject:NSStringFromRect([[self documentWindow] frame]) forKey:@"windowFrame"];
	
	[archiver finishEncoding];
	[archiver release];
	return data;
}

- (BOOL)needsSave
{
	NSData *currentState=[[self archiveToNewData] autorelease];
	if (lastSavedState) {
		if ([currentState isEqualToData:lastSavedState])
			return FALSE; // don't need to save if nothing's changed
	} else {
		Document *newDoc=[[[Document alloc] init] autorelease];
		NSData *newDocState=[[newDoc archiveToNewData] autorelease];
		if ([currentState isEqualToData:newDocState])
			return FALSE; // don't need to save if we're a brand new document
	}
	return TRUE;
}

- (BOOL)isDocumentEdited
{
	return [self needsSave];
}

- (void)canCloseDocumentWithDelegate:(id)delegate shouldCloseSelector:(SEL)shouldCloseSelector contextInfo:(void *)contextInfo
{
	BOOL autosave = [[NSUserDefaults standardUserDefaults] boolForKey: @"autoSaveOnClose"];
	if (autosave && [self needsSave] && [self fileURL]!=0)
		[self saveDocument: nil];
	[super canCloseDocumentWithDelegate:delegate shouldCloseSelector:shouldCloseSelector contextInfo:contextInfo];
}

-  (void)setLastSavedState:(NSData *)data
{
	if (lastSavedState) {
		[lastSavedState release];
		lastSavedState = 0;
	}
	if (data)
		lastSavedState = [data copy];
}

#if MAC_OS_X_VERSION_MAX_ALLOWED<MAC_OS_X_VERSION_10_4
- (NSData *)dataRepresentationOfType:(NSString *)typeName
#else
- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError
#endif
{
	NSData *data = [[self archiveToNewData] autorelease];
	[self setLastSavedState:data];
#if ENCRYPT_OUT
	return [self encryptData:data withKey:passPhrase direction:true];
#endif
	return data;
}

#if MAC_OS_X_VERSION_MAX_ALLOWED<MAC_OS_X_VERSION_10_4
- (BOOL)loadDataRepresentation:(NSData *)data ofType:(NSString *)typeName
#else
- (BOOL)readFromData:(NSData *)data ofType:(NSString *)typeName error:(NSError **)outError
#endif
{
#if ENCRYPT_IN
	try {
		data = [self encryptData:data withKey:passPhrase direction:false];
	} catch (...) {
		LogF(LOG_STATUS, "Data not encrypted");
	}
#endif
	
	[self setLastSavedState:data];
	[self willChangeValueForKey: @"clients"];
	[self willChangeValueForKey: @"showAllAccounts"];
	NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData: data];
	
	clients = [unarchiver decodeObjectForKey:@"clients"];
	if ([unarchiver containsValueForKey:@"showAllAlerts"])
		[self setShowAllAlerts: [unarchiver decodeBoolForKey:@"showAllAlerts"]];
	if ([unarchiver containsValueForKey:@"showAllAccounts"])
		[self setShowAllAccounts: [unarchiver decodeBoolForKey:@"showAllAccounts"]];
	if ([unarchiver containsValueForKey:@"showTotal"])
		showTotal = [unarchiver decodeBoolForKey:@"showTotal"];
	if ([unarchiver containsValueForKey:@"showDetails"])
		showDetails = [unarchiver decodeBoolForKey:@"showDetails"];
	if ([unarchiver containsValueForKey:@"viewer"])
		viewer = [[unarchiver decodeObjectForKey:@"viewer"] retain];
	if ([unarchiver containsValueForKey:@"currency"])
		currency = [[unarchiver decodeObjectForKey:@"currency"] retain];
	if ([unarchiver containsValueForKey:@"tickerPeriod"])
		tickerPeriod = [unarchiver decodeFloatForKey:@"tickerPeriod"];
	if ([unarchiver containsValueForKey:@"windowFrame"])
		windowFrame = NSRectFromString([unarchiver decodeObjectForKey:@"windowFrame"]);
	
	[unarchiver finishDecoding];
	[unarchiver release];
	[self didChangeValueForKey: @"clients"];	
	[self didChangeValueForKey: @"showAllAccounts"];	
    return YES;
}

@end
