//
//  Condition.h
//  BankReader
//
//  Created by Brice Rivé on 10/18/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface Condition : NSObject  <NSCoding>
{
	NSString *op;
	NSNumber *value;
	BOOL active;
}

- (id) init;

- (NSString *)getExpression;

// KVM
- (BOOL)getExpressionChanged;
- (BOOL)getActive;
- (void)setActive:(BOOL)val;
- (NSString *)getOp;
- (void)setOp:(NSString *)val;
- (NSNumber *)getValue;
- (void)setValue:(NSNumber *)val;

- (NSString *)description; // for gdb's po command

+ (NSSet *)keyPathsForValuesAffectingExpressionChanges;

@end
