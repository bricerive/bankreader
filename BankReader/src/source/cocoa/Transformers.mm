//
//  Transformers.mm
//  BankReader
//
//  Created by Brice Rivé on 10/13/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#include "CocoaUtils.h"

#include "Bank.h"

#import "Transformers.h"
#include "ExchangeRate.h"

@implementation bankToIconTransformer

+ (Class)transformedValueClass
{
    return [NSImage class];
}

+ (BOOL)allowsReverseTransformation
{
    return NO;
}

- (id)transformedValue:(NSString *)aBank
{
#if 0
	if (aBank==0) return 0;
	//return aBank? [NSImage imageNamed: StrConv(Bank::ShortName(StrConv(aBank)))]: 0;
	unsigned char *planes[1];
	planes[0] = (unsigned char *)Bank::IconData(StrConv(aBank));
	NSBitmapImageRep *iconImageRep = [[[NSBitmapImageRep alloc]
		initWithBitmapDataPlanes: planes
		pixelsWide: 32 pixelsHigh: 32 bitsPerSample: 8 samplesPerPixel:3 hasAlpha:NO isPlanar:NO 
		colorSpaceName:NSCalibratedRGBColorSpace bytesPerRow:0 bitsPerPixel:0] autorelease];
	NSImage *iconImage = [[[NSImage alloc] init] autorelease];
	[iconImage addRepresentation: iconImageRep];
	return iconImage;
    
#endif
    if (aBank==0) return 0;
	//return aBank? [NSImage imageNamed: StrConv(Bank::ShortName(StrConv(aBank)))]: 0;

    unsigned char *planes[1];
	planes[0] = (unsigned char *)Bank::IconData(StrConv(aBank));
    unsigned char *remapped = new unsigned char [32*32*4];
	unsigned char *p = planes[0];
    for (int i=0; i<32*32;  i++)
    {
        remapped[4*i+0] = *p++;
        remapped[4*i+1] = *p++;
        remapped[4*i+2] = *p++;
        remapped[4*i+3] = 0xFF;
    }
    planes[0] = remapped;
	NSBitmapImageRep *iconImageRep = [[[NSBitmapImageRep alloc]
                                       initWithBitmapDataPlanes: planes
                                       pixelsWide: 32 pixelsHigh: 32 bitsPerSample: 8 samplesPerPixel:4 hasAlpha:YES isPlanar:NO 
                                       colorSpaceName:NSCalibratedRGBColorSpace bytesPerRow:0 bitsPerPixel:0] autorelease];
    delete[] remapped;
	NSImage *iconImage = [[[NSImage alloc] init] autorelease];
	[iconImage addRepresentation: iconImageRep];
	return iconImage;    
    
}

@end

@implementation alertedToIconTransformer

+ (Class)transformedValueClass
{
    return [NSImage class];
}

+ (BOOL)allowsReverseTransformation
{
    return NO;
}

- (id)transformedValue:(NSNumber *)alerted
{
	if (![alerted boolValue]) return nil;
	return [NSImage imageNamed: @"alert"];
}

@end

static const char *periodStrings[] = {"Every 6h","Every 12h","Every day","Every 48h","Every week","Never"};
static const int nbPeriodStrings = sizeof(periodStrings)/sizeof(periodStrings[0]);


@implementation valueToPeriodTransformer

+ (Class)transformedValueClass
{
    return [NSString class];
}

+ (BOOL)allowsReverseTransformation
{
    return NO;
}

- (NSString *)transformedValue:(NSNumber *)value
{
	unsigned int val = [value unsignedIntValue];
	return val>=nbPeriodStrings? @"": NSLocalizedString(StrConv(periodStrings[val]), 0);
}

@end


@implementation autoLockPeriodTransformer

static const char *autoLockPeriodStrings[] = {"1mn", "2mn", "3mn", "4mn", "5mn", "10mn", "15mn", "20mn", "30mn", "1h", "never"};
static const int nbAutoLockPeriodStrings = sizeof(autoLockPeriodStrings)/sizeof(autoLockPeriodStrings[0]);

+ (Class)transformedValueClass
{
    return [NSString class];
}

+ (BOOL)allowsReverseTransformation
{
    return NO;
}

- (NSString *)transformedValue:(NSNumber *)value
{
	unsigned int val = [value unsignedIntValue];
	return val>=nbAutoLockPeriodStrings? @"": NSLocalizedString(StrConv(autoLockPeriodStrings[val]), 0);
}

@end


@implementation currencyTransformer

+ (Class)transformedValueClass
{
    return [NSString class];
}

+ (BOOL)allowsReverseTransformation
{
    return NO;
}

- (NSString *)transformedValue:(NSString *)value
{
	return StrConv(ExchangeRate::CurrencySymbol(StrConv(value)));
}

@end
