/*!
	@header NSURL+NDCarbonUtilities
	@abstract Provides method for interacting with Carbon APIs.
	@discussion The methods in <tt>NSURL(NDCarbonUtilities)</tt> are simply wrappers for functions that can bew found within the carbon API.
 */

#import <Foundation/Foundation.h>
#import <Carbon/Carbon.h>

/*!
	@category NSURL(NDCarbonUtilities)
	@abstract Provides method for interacting with Carbon APIs.
	@discussion Methods for dealing with <tt>FSRef</tt>&rsquo;s, <tt>FSSpec</tt> and other useful carbon stuff.
 */
@interface NSURL (NDCarbonUtilities)

/*!
	@method URLWithFSRef:
	@abstract Alloc and intialize a <tt>NSURL</tt>.
	@discussion Returns a file url for the file refered to by a <tt>FSRef</tt>.
	@param fsRef A pointer to a <tt>FSRef</tt>.
	@result A <tt>NSURL</tt> containing a file url.
 */
+ (NSURL *)URLWithFSRef:(const FSRef *)fsRef;

	/*!
@method URLWithFileSystemPathHFSStyle:
	 @abstract Alloc and intialize a <tt>NSURL</tt>.
	 @discussion Returns a file url for the file refered to by a HFS style path.
	 @param hfsString A <tt>NSString</tt> containing a HFS style path.
	 @result A <tt>NSURL</tt> containing a file url.
	 */
+ (NSURL *)URLWithFileSystemPathHFSStyle:(NSString *)hfsString;
	/*!
	@method getFSRef:
	@abstract Get a <tt>FSRef</tt>.
	@discussion Obtain a <tt>FSRef</tt> for a file url.
	@param fsRef A pointer to a <tt>FSRef</tt> struct, to be filled by the method.
	@result Returns <tt>YES</tt> if successful, if the method returns <tt>NO</tt> then <tt>fsRef</tt> contains garbage.
 */
- (BOOL)getFSRef:(FSRef *)fsRef;

/*!
	@method getFSSpec:
	@abstract Get a <tt>FSSpec</tt>.
	@discussion Obtain a <tt>FSSpec</tt> for a file url.
	@param fsSpec A pointer to a <tt>FSSpec</tt> struct, to be filled by the method.
	@result Returns <tt>YES</tt> if successful, if the method returns <tt>NO</tt> then <tt>fsSpec</tt> contains garbage.
 */
- (BOOL)getFSSpec:(FSSpec *)fsSpec;

/*!
	@method URLByDeletingLastPathComponent
	@abstract Delete last component of a url.
	@discussion Returns a new <tt>NSURL</tt> equivelent to the receiver with the last component removed.
	@result A new <tt>NSURL</tt>
 */
- (NSURL *)URLByDeletingLastPathComponent;

/*!
	@method fileSystemPathHFSStyle
	@abstract Returns a HFS style path.
	@discussion Returns a <tt>NSString</tt> containg a HFS style path (e.g. <tt>Macitosh HD:Users:</tt>) useful for display purposes.
	@result A new <tt>NSString</tt> containing a HFS style path for the same file or directory as the receiver.
 */
- (NSString *)fileSystemPathHFSStyle;


@end

/*!
	@category NSURL(NDCarbonUtilitiesInfoFlags)
	@abstract Adds methods to <tt>NSURL</tt> 
	@discussion Adds methods to simplify testing of the flags returned from <tt>finderInfoFlags:type:creator:</tt>
 */
@interface NSURL (NDCarbonUtilitiesInfoFlags)
@end
