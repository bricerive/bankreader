//
//  Utils.mm
//  BankReader
//
//  Created by Brice Rivé on 1/16/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#include "Utils.h"
#include "CocoaUtils.h"
#import <Cocoa/Cocoa.h>
#include <SystemConfiguration/SCNetwork.h>
using std::string;

string RelativeToAbsoluteUrl(const string &relative, const string &current)
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	ObjCAutoRelease killer(pool);
	NSURL *mergedURL = [NSURL URLWithString: StrConv(relative) relativeToURL: [NSURL URLWithString:StrConv(current)]];
	return StrConv([mergedURL absoluteString]);
}

string HostOfUrl(const string &url)
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	ObjCAutoRelease killer(pool);
	NSURL *nsUrl = [NSURL URLWithString: StrConv(url)];
	return StrConv([nsUrl host]);
}

// replacement for deprecated SCNetworkCheckReachabilityByName
#include <SystemConfiguration/SCNetworkReachability.h>
static Boolean NetworkCheckReachabilityByName(const char *nodename,SCNetworkConnectionFlags *flags)
{
    SCNetworkReachabilityRef target = SCNetworkReachabilityCreateWithName(NULL, nodename);
    Boolean ok = SCNetworkReachabilityGetFlags(target, flags);
    CFRelease(target);
    return ok;
}

bool IsAccessibleHost(const string &hostName)
{
	SCNetworkConnectionFlags flags;
	if (!NetworkCheckReachabilityByName(hostName.c_str(), &flags)) return false;
	if ((flags & kSCNetworkFlagsReachable) && !(flags & kSCNetworkFlagsConnectionRequired)) return true;
	return false;
}
