#import "BankReader.h"
#include "view.h"
#include "CocoaUtils.h"
#include "SecureStorage.h"
#include "View.h"
#include "Bank.h"
#include "ExchangeRate.h"
#include "Transformers.h"
#import "Document.h"
#include <mylib/idioms.h>
#include <mylib/log.h>
using namespace mylib;

#pragma mark ---currencies

@implementation BankReader

- (void)cppCtor
{
	exchange = new ExchangeRate;
}

- (void)cppDtor
{
	delete exchange;
}


+ (void)setupDefaults
{
    // load the default values for the user defaults
    NSString *userDefaultsValuesPath=[[NSBundle mainBundle] pathForResource:@"UserDefaults" ofType:@"plist"];
    NSDictionary *userDefaultsValuesDict=[NSDictionary dictionaryWithContentsOfFile:userDefaultsValuesPath];
    
    // set them in the standard user defaults
    [[NSUserDefaults standardUserDefaults] registerDefaults:userDefaultsValuesDict];
    
#if 0
    // if your application supports resetting a subset of the defaults to 
    // factory values, you should set those values 
    // in the shared user defaults controller
    NSArray *resettableUserDefaultsKeys=[NSArray arrayWithObjects:@"Value1",@"Value2",@"Value3",nil];
    NSDictionary *initialValuesDict=[userDefaultsValuesDict dictionaryWithValuesForKeys:resettableUserDefaultsKeys];
    
    // Set the initial values in the shared user defaults controller 
    [[NSUserDefaultsController sharedUserDefaultsController] setInitialValues:initialValuesDict];
#endif
}

+ (void)initialize
{
	[BankReader setupDefaults];
	// Declare Bindings transformers
    [NSValueTransformer setValueTransformer:[[autoLockPeriodTransformer alloc] init] forName:@"autoLockPeriodTransformer"];
}

- (void)discoverPlugins
{		      
	NSString *appSupport = @"Library/Application Support/BankReader/Plugins/";
	NSString *appPath = [[NSBundle mainBundle] builtInPlugInsPath];
	NSString *userPath = [NSHomeDirectory() stringByAppendingPathComponent:appSupport];
	NSString *sysPath = [@"/" stringByAppendingPathComponent:appSupport];
	NSArray *paths = [NSArray arrayWithObjects:appPath,	userPath, sysPath, nil];
	NSEnumerator *pathEnum = [paths objectEnumerator];
		
	NSString *path;
	while ( (path = [pathEnum nextObject]) ) {
		NSEnumerator *e = [[[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:NULL] objectEnumerator];
		NSString *name;		
		while ( (name = [e nextObject]) )
			if ( [[name pathExtension] isEqualToString:@"plugin"] ) {
				NSString *bundlePath = [path stringByAppendingPathComponent:name];
				NSBundle *plugin = [NSBundle bundleWithPath: bundlePath];
				if (plugin)
					[plugin principalClass]; // Triggers plugin load and objects registration
			}
				
	}
}

- (void)initBankList
{
	Bank::BankNames bankNames = Bank::GetBankNames();
	banks = [[NSMutableArray alloc] init];
	for (Bank::BankNames::iterator i=bankNames.begin(); i!=bankNames.end(); ++i)
		[banks addObject: StrConv(*i)];
}

- (void)initCurrencyList
{
	ExchangeRate::CurrencyList currenciesCPP = exchange->GetCurrencies();
	currencies = [[NSMutableArray alloc] init];
	for (ExchangeRate::CurrencyList::iterator i=currenciesCPP.begin(); i!=currenciesCPP.end(); ++i)
		[currencies addObject: StrConv(*i)];
}

- (void)initViewerList
{
	View::ViewNames viewNames = View::GetViewNames();
	viewers = [[NSMutableArray alloc] init];
	for (View::ViewNames::iterator i=viewNames.begin(); i!=viewNames.end(); ++i)
		[viewers addObject: StrConv(*i)];
}

static const float tickerPeriod=.5; // seconds
static const float animatePeriod=0.05; // seconds
static float alpha;
static const float alphaOpaque=1.0;
static const float alphaTransparent=0.0;
static const float alphaStepOut=0.02;
static const float alphaStepIn=0.1;
static float alphaStep=0.05;
static int noEventCount=0;
static NSMutableArray *windows;
static enum {VISIBLE=0,HIDING,HIDEN,UNHIDING} lockStatus;

- (void)AnimateIn:(NSTimer *)timer
{
	if (alpha<=alphaOpaque) {
		NSWindow *window;
		for (NSEnumerator *e=[windows objectEnumerator]; (window=[e nextObject]); ) {
			[window setAlphaValue: alpha];
		}
		alpha+=alphaStep;
	} else {
		NSWindow *window;
		for (NSEnumerator *e=[windows objectEnumerator]; (window=[e nextObject]); )
			[window setAlphaValue: alphaOpaque];
		[windows release];
		[timer invalidate];
		lockStatus=VISIBLE;
	}
}

- (void)unlock
{
	[self unhide: self];
	NSArray *documents = [self orderedDocuments];
	windows = [[NSMutableArray alloc] init];
	Document *document;
	for (NSEnumerator *e=[documents objectEnumerator]; (document=[e nextObject]); ) {
		NSWindow *window  = [document documentWindow];
		[windows addObject: window];
		[window makeKeyAndOrderFront:self];
	}
	lockStatus=UNHIDING;
	alphaStep=alphaStepIn;
	[NSTimer scheduledTimerWithTimeInterval:animatePeriod target:self selector:@selector(AnimateIn:) userInfo:nil repeats:YES];
}

- (void)AnimateOut:(NSTimer *)timer
{
	if (lockStatus==UNHIDING) { // cancelling hide!!!
		[timer invalidate];
		[self unlock];
	}
	if (alpha>=alphaTransparent) {
		NSWindow *window;
		for (NSEnumerator *e=[windows objectEnumerator]; (window=[e nextObject]); )
			[window setAlphaValue: alpha];
		alpha-=alphaStep;
	} else {
		NSWindow *window;
		for (NSEnumerator *e=[windows objectEnumerator]; (window=[e nextObject]); ) {
			[window setAlphaValue: alphaTransparent];
			[window orderOut:self];
		}
		[self hide: self];
		[windows release];
		[timer invalidate];
		lockStatus=HIDEN;
	}
}

- (void)lock
{
	NSArray *documents = [self orderedDocuments];
	windows = [[NSMutableArray alloc] init];
	Document *document;
	for (NSEnumerator *e=[documents objectEnumerator]; (document=[e nextObject]); ) {
		NSWindow *window  = [document documentWindow];
		[windows addObject: window];
	}
	lockStatus=HIDING;
	alpha=alphaOpaque;
	alphaStep=alphaStepOut;
	[NSTimer scheduledTimerWithTimeInterval:animatePeriod target:self selector:@selector(AnimateOut:) userInfo:nil repeats:YES];
}

- (void)IdleTick:(NSTimer *)timer
{
	if ((lockStatus==HIDEN||lockStatus==HIDING) && noEventCount==0) {
		LogF(LOG_STATUS, "Unlock time!!!!");
		if (lockStatus==HIDEN) {
			alpha=alphaTransparent;
			[self unlock];
		}
		lockStatus=UNHIDING;
	}

	int noEventCountSecondsValues[] = {60,120,180,240,300,600,900,1200,1800,3600};
	const int noEventCountSecondsValuesNb = SIZE_OF_ARRAY(noEventCountSecondsValues);
	int noEventCountSecondsIndex = [[NSUserDefaults standardUserDefaults] integerForKey: @"autoLock"];
	if (noEventCountSecondsIndex>=0 && noEventCountSecondsIndex<noEventCountSecondsValuesNb) {
#if _DEBUG
		int noEventCountSeconds = noEventCountSecondsValues[noEventCountSecondsIndex];
#else
		int noEventCountSeconds = noEventCountSecondsValues[noEventCountSecondsIndex];
#endif
		float noEventCountLimit = noEventCountSeconds/tickerPeriod;
		if (lockStatus==VISIBLE && noEventCount>=noEventCountLimit) {
			LogF(LOG_STATUS, "Lock time!!!!");
			[self lock];
			lockStatus=HIDING;
		}
	}
	noEventCount++;
	[NSTimer scheduledTimerWithTimeInterval:tickerPeriod target:self selector:@selector(IdleTick:) userInfo:nil repeats:NO];
}

- (void)sendEvent:(NSEvent *)anEvent
{
	// Discard application deactivated events
	// Also discard undocumented event type 21 which is sent when I click on the dock
	if (!([anEvent type]==NSAppKitDefined && [anEvent subtype]==NSApplicationDeactivatedEventType) && !([anEvent type]==21))
		noEventCount=0;
	//LogF(LOG_ERROR, "Event: %d %d", [anEvent type], [anEvent subtype]);
	[super sendEvent:anEvent];
}

// "It initializes all other instance variables to zero (or to the equivalent type for zero, such as nil, NULL, and 0.0)."
- (id) init
{
	[self cppCtor];
	if ( (self = [super init]) ) {
		[self discoverPlugins];
		[self initViewerList];
		[self initCurrencyList];
		[self initBankList];
		alertOperators = [[NSArray arrayWithObjects: @"<", @"=", @">", (char*)0] retain];
		[NSTimer scheduledTimerWithTimeInterval:tickerPeriod target:self selector:@selector(IdleTick:) userInfo:nil repeats:NO];
	}
	return self;
}

- (void) dealloc
{
	[self cppDtor];
	[viewers release];
	[currencies release];
	[banks release];
	[alertOperators release];
	[openDocuments release];
	openDocuments=0;
	delete view;
	[super dealloc];
}

- (NSArray *) viewers { return viewers; }
- (NSArray *) currencies { return currencies; }
- (NSArray *) banks { return banks; }
- (NSArray *) alertOperators { return alertOperators; }

- (IBAction)ForgetPasswords:(id)sender
{
	SecureStorage::ForgetPasswords();
}

- (IBAction)ViewInDock:(id)sender
{
	if ([sender intValue]==0) {
		delete view;
		view = 0;
		[NSApp setApplicationIconImage: [NSImage imageNamed: @"BankReader"]];
	}
}

- (void)UpdateDockIcon :(const std::string&)viewType :(bool)showDetails :(bool)showTotal :(const std::string&)currency :(const View::Accounts &)accounts :(bool)grabbing :(bool)alerting
{
	const int dockIconSize=128;
	
	if (![[NSUserDefaults standardUserDefaults] boolForKey: @"showViewerInDock"])
		return;

	NSImage *dock_image = [[NSImage alloc] initWithSize:NSMakeSize(dockIconSize,dockIconSize)];
	[dock_image setCacheMode:NSImageCacheNever];
	[dock_image lockFocus];
	
	// Clear to transparent
	[[NSColor clearColor] set];  
	NSRectFill(NSMakeRect(0,0,dockIconSize-1,dockIconSize-1));

	// Draw the view
	View *lastView=view;
	view = View::New(viewType, lastView); // To keep the current indices
	delete lastView;
	view->ShowDetails(showDetails);
	view->ShowTotal(showTotal);
	view->Currency(currency);
	view->Tick();
	view->Draw(accounts, grabbing, alerting);
	
	[dock_image unlockFocus];
	[NSApp setApplicationIconImage:dock_image];
	[dock_image release];
}

// How to save the list of opened documents on application quit (to re-open them on next launch): cf. Controller.mm
- (void)rememberOpenDocuments
{
	if (openDocuments!=0) return;
	openDocuments = [[NSMutableArray arrayWithArray:[self orderedDocuments]] retain];
}

// How to save the list of opened documents on application quit (to re-open them on next launch): cf. Controller.mm
- (void)closeOpenDocument:(NSDocument *)document
{
	if (openDocuments==0) return;
	if (![openDocuments containsObject:document]) return;
	[openDocuments removeObject:document];
#if MAC_OS_X_VERSION_MAX_ALLOWED<MAC_OS_X_VERSION_10_4
	NSString *fileName = [document fileName];
	if (fileName==0) return;
	NSURL *fileURL = [NSURL fileURLWithPath:fileName];
#else
	NSURL *fileURL = [document fileURL];
#endif
	if (fileURL==0) return;
	[openDocuments addObject:fileURL];
}

// How to save the list of opened documents on application quit (to re-open them on next launch): cf. Controller.mm
- (NSArray *)getOpenDocumentURLs
{
	return openDocuments;
}

// How to save the list of opened documents on application quit (to re-open them on next launch): cf. Controller.mm
- (void)replyToApplicationShouldTerminate:(BOOL)shouldTerminate;
{
	if (shouldTerminate==FALSE) {
		if (openDocuments!=0) {
			[openDocuments release];
			openDocuments=0;
		}
	}
	[super replyToApplicationShouldTerminate:shouldTerminate];
}

@end
