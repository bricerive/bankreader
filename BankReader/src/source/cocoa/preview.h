//
//  preview.h
//  MoneyFusion
//
//  Created by Brice Rive on 1/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#include <string>
#include "view.h"

@interface preview : NSView {
	View::Accounts *crtAccounts;
	bool crtGrabbing;
	bool crtAlerting;
	View *view;
}
- (void)dealloc;
- (void)Update :(const std::string&)viewType :(bool)showDetails :(bool)showTotal :(const std::string&)currency :(const View::Accounts &)accounts :(bool)grabbing :(bool)alerting;
@end
