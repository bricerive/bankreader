/*
 *  CocoaUtils.h
 *  BankReader
 *
 *  Created by Brice Rivé on 10/14/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#import <Cocoa/Cocoa.h>
#include <string>

NSString *StrConv(const std::string &str); // convert a STL string into a Cocoa string
std::string StrConv(NSString *str); // Convert a Cocoa string into a STL string

struct ObjCAutoRelease {
	ObjCAutoRelease(id objC_): objC(objC_) {}
	~ObjCAutoRelease() {[objC release];}
	id objC;
};


