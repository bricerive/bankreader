/*
 *  ViewElts.mm
 *  BankReader
 *
 *  Created by Brice Rivé on 3/1/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#import <Cocoa/Cocoa.h>
#include "ViewElts.h"
#include "Bank.h"
using namespace std;

void DrawBlinker(bool alerting, bool grabbing, bool blinkOn)
{
	NSString *blinkerImage = (alerting&&blinkOn)? @"RedLight14": (grabbing && !blinkOn)? @"YellowLight14": @"GreenLight14";
	NSImage *image = [NSImage imageNamed: blinkerImage];
	[image drawInRect:NSMakeRect(112, 112, 14, 14) fromRect:NSMakeRect(0, 0, 14, 14) operation: NSCompositeSourceOver fraction:0.7];
}

void DrawLogo(const NSRect &rect, const std::string &name)
{
	unsigned char *planes[1];
	planes[0] = (unsigned char *)Bank::IconData(name);
    unsigned char *remapped = new unsigned char [32*32*4];
	unsigned char *p = planes[0];
    for (int i=0; i<32*32;  i++)
    {
        remapped[4*i+0] = *p++;
        remapped[4*i+1] = *p++;
        remapped[4*i+2] = *p++;
        remapped[4*i+3] = 0xFF;
    }
    planes[0] = remapped;
	NSBitmapImageRep *iconImageRep = [[[NSBitmapImageRep alloc]
		initWithBitmapDataPlanes: planes
					  pixelsWide: 32 pixelsHigh: 32 bitsPerSample: 8 samplesPerPixel:4 hasAlpha:YES isPlanar:NO 
				  colorSpaceName:NSCalibratedRGBColorSpace bytesPerRow:0 bitsPerPixel:0] autorelease];
	[iconImageRep drawInRect:rect];
    delete[] remapped;
}

void DrawHistogram(const std::vector<std::pair<float, float> > &histo, int barYPos)
{
	const int barWidth=128;
	const int barHeight=28;
	const float amplitude=(barHeight-1)/2;
	NSColor *lightBlue = [NSColor colorWithCalibratedRed:.95 green:.95 blue:0.95 alpha:1.0];
	
	[lightBlue set];
	NSRectFill(NSMakeRect(0, barYPos, barWidth, barHeight));
	[[NSColor blackColor] set];
	NSRectFill(NSMakeRect(0, barYPos+barHeight/2, barWidth, 1));
	
	float maxVal=0;
	for (int i=0; i<barWidth; i++) {
		float val = histo[i].first + histo[i].second;
		if (val > maxVal)
			maxVal=val;
		if (-val > maxVal)
			maxVal=-val;
	}
	
	float lastTotal=0;
	[[NSColor greenColor] set];
	for (int i=0; i<barWidth; i++) {
		float v0 = histo[i].first/maxVal*amplitude;
		float v1 = histo[i].second/maxVal*amplitude;
		float total = v0+v1;
		
		if (total>lastTotal)
			[[NSColor greenColor] set];
		else if (total<lastTotal)
			[[NSColor redColor] set];
		lastTotal=total;
		NSRectFill(NSMakeRect(i, barYPos+barHeight/2+total, 1, 1));
	}
}

void DrawBar(float v0, float v1, int barYPos)
{
	const int barWidth=128;
	const int barHeight=32;
	const float alpha=0.9;
	
	int p0 = (int)(v0*(float)barWidth);
	int p1 = (int)((v0+v1)*(float)barWidth);
		
	// Make a dark blue rectangle
	[[NSColor colorWithCalibratedRed:.2 green:.2 blue:.8 alpha:alpha] set];
	NSRectFill(NSMakeRect(0, barYPos, p0, barHeight));
	
	// Make a medium blue rectangle
	[[NSColor colorWithCalibratedRed:.4 green:.4 blue:.95 alpha:alpha] set];
	NSRectFill(NSMakeRect(p0, barYPos, p1, barHeight));
	
	// Make a light blue rectangle
	[[NSColor colorWithCalibratedRed:.7 green:.7 blue:1 alpha:alpha] set];
	NSRectFill(NSMakeRect(p1, barYPos, barWidth-p1, barHeight));
}

void DrawSmiley(int x, int y, int radius, int happy)
{
	NSBezierPath *path = [NSBezierPath bezierPath];
	
	// Draw the face in the proper color
	[path appendBezierPathWithArcWithCenter:NSMakePoint(x,y) radius:radius startAngle:0 endAngle:360];
	NSColor *faceColor;
	switch (happy) {
		case 1:
			faceColor = [NSColor colorWithCalibratedRed:.2 green:1 blue:.2 alpha:.4]; // Green
			break;
		case -1:
			faceColor = [NSColor colorWithCalibratedRed:1 green:.2 blue:.2 alpha:.4]; // Red
			break;
		default:
			faceColor = [NSColor colorWithCalibratedRed:1 green:1 blue:.2 alpha:.4]; // Yellow
			break;
	}
	[faceColor set];
	[path fill];
	[[NSColor blackColor] set];
	[path setLineWidth: 3];
	[path stroke];
	
	// Draw the eyes and mouth
	[path removeAllPoints];
	[path appendBezierPathWithArcWithCenter:NSMakePoint(x-radius/2,y+radius/4) radius:radius/4 startAngle:0 endAngle:360];
	[path fill];
	
	[path removeAllPoints];
	[path appendBezierPathWithArcWithCenter:NSMakePoint(x+radius/2,y+radius/4) radius:radius/4 startAngle:0 endAngle:360];
	[path fill];
	
	[path removeAllPoints];
	switch (happy) {
		case 1:
			[path appendBezierPathWithArcWithCenter:NSMakePoint(x,y) radius:radius*2/3 startAngle:345 endAngle:195 clockwise:true];
			break;
		case -1:
			[path appendBezierPathWithArcWithCenter:NSMakePoint(x,y-radius) radius:radius*2/3 startAngle:15 endAngle:165 clockwise:false];
			break;
		default:
			[path moveToPoint:NSMakePoint(x-radius/2, y-radius/2)];
			[path lineToPoint:NSMakePoint(x+radius/2, y-radius/2)];
			break;
	}
	[path stroke];
}

