//
//  ClientArrayController.mm
//  BankReader
//
//  Created by brice rive on 07/03/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "ClientArrayController.h"
#import "Account.h"
#include "Bank.h"

@implementation ClientArrayController

- (void)awakeFromNib
{
	[accountsController addObserver:self forKeyPath:@"selection" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if (object==accountsController && [keyPath isEqualToString:@"selection"]) {
		NSArray *selectedAccounts = [accountsController selectedObjects];
		if ([selectedAccounts count]!=1) return;
		Account *selectedAccount = [selectedAccounts objectAtIndex:0];
		[self setSelectedObjects:[NSArray arrayWithObject:[selectedAccount getClient]]];
	}
	[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
}

@end
