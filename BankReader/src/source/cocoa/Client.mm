//
//  Client.m
//  BankReader
//
//  Created by Brice Rivé on 10/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "Client.h"
#import "Account.h"
#include "Bank.h"
#include "CocoaUtils.h"
#include <mylib/error.h>
#include <mylib/idioms.h>
using namespace mylib;

static const int updatePeriodNeverIndex=5;
static const int updatePeriodHours[] = {3600*6, 3600*12, 3600*24, 3600*48, 3600*24*7, 0};


@implementation Client

- (void)cppCtor
{
	myServer = new Server;
}

- (void)cppDtor
{
	delete myServer;
}

#pragma mark NSCoding protocol

- (void)encodeWithCoder:(NSCoder *)coder
{
	[coder encodeObject:name forKey:@"name"];
	[coder encodeObject:bank forKey:@"bank"];
	[coder encodeObject:login forKey:@"login"];
	[coder encodeObject:birthDate forKey:@"birthDate"];
	[coder encodeObject:amount forKey:@"amount"];
	[coder encodeObject:currency forKey:@"currency"];
	[coder encodeInt:updatePeriodIndex forKey:@"updatePeriodIndex"];
	[coder encodeObject:lastUpdate forKey:@"lastUpdate"];
	[coder encodeInt:myServer->LastUpdateStatus() forKey:@"status"];
	[coder encodeObject:accounts forKey:@"accounts"];
}

- (id)initWithCoder:(NSCoder *)coder
{
	[self cppCtor];
	name = [[coder decodeObjectForKey:@"name"] retain];
	bank = [[coder decodeObjectForKey:@"bank"] retain];
	login = [[coder decodeObjectForKey:@"login"] retain];
	birthDate = [[coder decodeObjectForKey:@"birthDate"] retain];
	amount = [[coder decodeObjectForKey:@"amount"] retain];
	currency = [[coder decodeObjectForKey:@"currency"] retain];
	updatePeriodIndex = [coder decodeIntForKey:@"updatePeriodIndex"];
	lastUpdate = [[coder decodeObjectForKey:@"lastUpdate"] retain];
	myServer->LastUpdateStatus((Server::Status)[coder decodeIntForKey:@"status"]);
	lastUpdateStatus=0;
	[self setLastUpdateStatus: [self StatusString]];
	accounts = [[coder decodeObjectForKey:@"accounts"] retain];
	Account *account;
	for (NSEnumerator *accountEnum=[accounts objectEnumerator]; (account = [accountEnum nextObject]); )
		[account setClient:self];
    
    return self;
}

+ (void)initialize
{
}

// Declare non-trivial Binding Keys dependencies
+ (NSSet *)keyPathsForValuesAffectingHideCurrency {	return [NSSet setWithObjects:@"bank", nil]; }
+ (NSSet *)keyPathsForValuesAffectingHideAmount {	return [NSSet setWithObjects:@"bank", nil]; }
+ (NSSet *)keyPathsForValuesAffectingHideBirthDate {	return [NSSet setWithObjects:@"bank", nil]; }
+ (NSSet *)keyPathsForValuesAffectingHideClientId {	return [NSSet setWithObjects:@"bank", nil]; }
+ (NSSet *)keyPathsForValuesAffectingHasNoAccount {	return [NSSet setWithObjects:@"accounts", nil]; }
+ (NSSet *)keyPathsForValuesAffectingHasLoginInfo {	return [NSSet setWithObjects:@"bank", @"login", @"birthDate", @"amount", @"currency", nil]; }
+ (NSSet *)keyPathsForValuesAffectingName {	return [NSSet setWithObjects:@"bank", nil]; }
+ (NSSet *)keyPathsForValuesAffectingHasWebSite {	return [NSSet setWithObjects:@"bank", nil]; }


- (id) init
{
	[self cppCtor];
	if ( (self=[super init]) ) {
		updatePeriodIndex = updatePeriodNeverIndex; // Never by default
		accounts = [[NSMutableArray alloc] init];
		[self setLastUpdateStatus: [self StatusString]];
	}
	return self;
}

- (void) dealloc
{
	[self cppDtor];
	if (name) [name release];
	if (bank) [bank release];
	if (login) [login release];
	if (birthDate) [birthDate release];
	if (lastUpdate) [lastUpdate release];
	if (lastUpdateStatus) [lastUpdateStatus release];
	[accounts release];
	[super dealloc];
}

// private
- (BOOL) hasNeededLoginInfo
{
	if (!bank) return FALSE;
	Bank::LoginInfo loginInfo;
	Bank::GetLoginInfo(StrConv(bank), loginInfo);
	for (Bank::LoginInfo::iterator i=loginInfo.begin(); i!=loginInfo.end(); ++i) {
		switch (*i) {
			case Bank::LOGIN:
				if (!login) return FALSE;
				break;
			case Bank::DATEOFBIRTH:
				if (!birthDate) return FALSE;
				break;
			case Bank::PASSWORD:
				break;
			case Bank::AMOUNT:
				if (!amount) return FALSE;
				break;
			case Bank::CURRENCY:
				if (!currency) return FALSE;
				break;
			default:
				return FALSE;
		}
	}
	return TRUE;
}

#pragma mark KVC Accessors

- (NSString *)getName {
	if (name) return name;
	if (bank) return bank;
	return NSLocalizedString(@"New Client", 0);
}

- (NSString *)getBank {
	return bank;
}

- (NSString *)getLogin {
	return login;
}

- (NSDate *)getBirthDate {
	return birthDate;
}

- (int) getUpdatePeriodIndex {
	return updatePeriodIndex;
}

- (NSString *)getLastUpdateStatus {
	return NSLocalizedString(lastUpdateStatus, 0);
}

- (NSDate *)getLastUpdate {
	return lastUpdate;
}

- (NSArray *)accounts {
	return accounts;
}

- (NSNumber *)getHideClientId {
	return [NSNumber numberWithBool: (bank? !Bank::NeedsClientId(StrConv(bank)): TRUE)];
}

- (NSNumber *)getHideBirthDate {
	return [NSNumber numberWithBool: (bank? !Bank::NeedsBirthDate(StrConv(bank)): TRUE)];
}

- (NSNumber *)getHideAmount {
	return [NSNumber numberWithBool: (bank? !Bank::NeedsLoginInfoItem(StrConv(bank), Bank::AMOUNT): TRUE)];
}

- (NSNumber *)getHideCurrency {
	return [NSNumber numberWithBool: (bank? !Bank::NeedsLoginInfoItem(StrConv(bank), Bank::CURRENCY): TRUE)];
}

- (NSNumber *)getHasNoAccount {
	return [NSNumber numberWithBool: ([accounts count]==0)];
}

- (NSNumber *)getHasLoginInfo {
	return [NSNumber numberWithBool: [self hasNeededLoginInfo]];
}

- (NSNumber *)getHasWebSite {
	return [NSNumber numberWithBool: (bank != 0? strlen(Bank::GetUrl(StrConv(bank)))!=0: FALSE)];
}

- (void) setName: (NSString *)value {
	if (name) [name autorelease];
	name=[value copy];
}

- (void) setBank: (NSString *)value {
	if (bank) [bank autorelease];
	bank=[value copy];
	[self setLastUpdate: 0];
	[self setLogin: 0];
	[self setUpdatePeriodIndex: updatePeriodNeverIndex];
}

- (void) setLogin: (NSString *)value {
	if (login) [login autorelease];
	login=[value copy];
}

- (void) setBirthDate:(NSDate *)value {
	if (birthDate) [birthDate autorelease];
	birthDate=[value copy];
}

- (void) setUpdatePeriodIndex:(int)value {
	updatePeriodIndex = value;
}

- (void) setLastUpdateStatus: (NSString *)value
{
	if (lastUpdateStatus) {
		if ([lastUpdateStatus isEqualToString:value])
			return;
		[lastUpdateStatus release];
	}
	lastUpdateStatus = [value copy];
}

- (void) setLastUpdate: (NSDate *)value
{
	if (lastUpdate) [lastUpdate release];
	lastUpdate=[value copy];
}

- (void) setUpdateProgress: (float)value
{
	updateProgress=value;
}

#pragma mark Updating

// private
- (BOOL)shouldUpdate
{
	if (updating) return FALSE;
	if (![self hasNeededLoginInfo]) return FALSE;
	if (forceUpdate) return TRUE;
	if (updatePeriodIndex==updatePeriodNeverIndex) return FALSE;
	if (!lastUpdate) return TRUE;
	NSTimeInterval elapsed = [[NSDate date] timeIntervalSinceDate: lastUpdate];
	if (elapsed>=updatePeriodHours[updatePeriodIndex]) return TRUE;
	return FALSE;
}

static void ProgressCallbackGlue(Client *client, float value)
{
	ObjCAutoRelease killer([[NSAutoreleasePool alloc] init]);
	[client setUpdateProgress:value];
}

// private
- (void) update: (id <PasswordProvider>)passwordProvider : (id <NotificationProvider>)notificationProvider
{
	forceUpdate=FALSE;
	
	// Handle password if required by this bank
	Bank::LoginInfo loginInfo;
	Bank::GetLoginInfo(StrConv(bank), loginInfo);
	std::string password;
	NSString *passwordStr;
	for (Bank::LoginInfo::iterator i=loginInfo.begin(); i!=loginInfo.end(); ++i) {
		if (*i == Bank::PASSWORD) { // If this bank requires a password try to get it
			Server::Status lastStatus = myServer->LastUpdateStatus();
			bool discardLastPassword = lastStatus==Server::InvalidPasswordError || lastStatus==Server::RejectedPasswordError;
			passwordStr = [passwordProvider getPassword: bank: login: discardLastPassword];
			if (!passwordStr) { // Give up if we could not get a password
				[notificationProvider NotifyUserWithOption:0 :[NSString stringWithFormat : NSLocalizedString(@"No Password Given for client %@", 0), [self getName]]];
				[self setLastUpdateStatus: @"No password"];
				[self setLastUpdate: [NSDate date]];
				return;
			}
			password = StrConv(passwordStr);
			break;
		}
	}
	
	// Build the login data map
	Bank::LoginData loginData;
	if (login) loginData[Bank::LOGIN] = StrConv(login);
	if (!password.empty()) loginData[Bank::PASSWORD] = password;
	if (birthDate) loginData[Bank::DATEOFBIRTH] = StrConv([birthDate description]);
	if (amount) loginData[Bank::AMOUNT] = to_string([amount floatValue]);
	if (currency) loginData[Bank::CURRENCY] = StrConv(currency);
	
	// Trigger asynchronous update
	updating = TRUE;
	try {
		Bank::ProgressCallback callback = Loki::BindFirst(Loki::Functor<void, TYPELIST_2(Client *, float)>(&ProgressCallbackGlue), self);
		myServer->TriggerUpdate(StrConv(bank), loginData, callback);
	} catch (Err &err) {
		[notificationProvider NotifyUserWithOption:0 :[NSString stringWithFormat : NSLocalizedString(@"DefaultErrorForClient", 0), [self getName]]];
		updating = FALSE;
		[self setLastUpdate: [NSDate date]];
	}
	[self setLastUpdateStatus: [self StatusString]];
}

- (BOOL) handleUpdated: (id <NotificationProvider>)notificationProvider
{
	Bank::AccountList bankAccounts;
	if (!myServer->CheckUpdated(bankAccounts)) return FALSE; // Not done updating yet
	
	updating = FALSE;
	NSDate *now = [NSDate date];
	[self setLastUpdate: now];
	
	Server::Status updateStatus = myServer->LastUpdateStatus();
	[self setLastUpdateStatus: [self StatusString]];
    
	// Handle error according to the server's update status
	//	Initial, Updated, InternalError, ServerError, ProtocolError, InvalidPasswordError, RejectedPasswordError
	if (updateStatus!=Server::Updated) {
		NSDictionary *errorMessages = [NSDictionary dictionaryWithObjectsAndKeys: @"InternalErrorForClient", [NSNumber numberWithInt:Server::InternalError], @"ServerErrorForClient", [NSNumber numberWithInt:Server::ServerError], @"ProtocolErrorForClient", [NSNumber numberWithInt:Server::ProtocolError], @"InvalidPasswordErrorForClient", [NSNumber numberWithInt:Server::InvalidPasswordError], @"RejectedPasswordErrorForClient", [NSNumber numberWithInt:Server::RejectedPasswordError]
                                       , (char *)0];
		NSString *errorMessage = [errorMessages objectForKey: [NSNumber numberWithInt:myServer->LastUpdateStatus()]];
		[notificationProvider NotifyUserWithOption:0 :[NSString stringWithFormat : NSLocalizedString(errorMessage, 0), [self getName]]];
		return TRUE;
	}
	
	// Do a first pass through the accounts to identify duplicate labels
	for (Bank::AccountList::iterator i=bankAccounts.begin(); i!=bankAccounts.end(); ++i) {
		//NSDictionary *dictionary;
	}
    
	// Update our accounts list
    [self willChangeValueForKey: @"accounts"];
    NSMutableArray *prevAccounts = accounts; // remember previous accounts for persistent attributes
    accounts = [[NSMutableArray alloc] init]; // Start with a new list
    for (Bank::AccountList::iterator i=bankAccounts.begin(); i!=bankAccounts.end(); ++i) {
        // Hack for Virtual bank: if there is no explicit name, use the bank name.
        std::string accountName = i->Description();
        if (accountName.empty()) accountName = StrConv([self getName]);
        Account *account = [[[Account alloc] initWithData: bank: StrConv(accountName): StrConv(i->Number()):
                             StrConv(i->Currency()): NSLocalizedString(@"Current", @"Account type")] autorelease];
        [account setClient:self];
        NSUInteger prevAccountIdx;
        if ((prevAccountIdx=[prevAccounts indexOfObject:account])!=NSNotFound) {
            Account *prevAccount = [prevAccounts objectAtIndex:prevAccountIdx];
            [account TakeHistory: prevAccount];
        }
        [account setAmount: i->Amount(): now];
        [accounts addObject: account];
    }
    [prevAccounts release];
    [self didChangeValueForKey: @"accounts"];
	return TRUE;
}

- (BOOL) checkUpdate: (id <PasswordProvider>)passwordProvider : (id <NotificationProvider>)notificationProvider
{
	if ([self handleUpdated: notificationProvider])
		return TRUE;
	if ([self shouldUpdate])
		[self update: passwordProvider :notificationProvider];
	return FALSE;
}

- (void) forceUpdate
{
	forceUpdate=TRUE;
}

- (void) visitWebPage
{
	if (!bank) return;
	if (login) {
		[[NSPasteboard generalPasteboard] declareTypes:[NSArray arrayWithObject:NSStringPboardType] owner:nil];
		[[NSPasteboard generalPasteboard] setString: login forType:NSStringPboardType];
	}
    
	const char *bankUrl = Bank::GetUrl(StrConv(bank));
	NSURL *url = [NSURL URLWithString: StrConv(bankUrl)];
	NSWorkspace *sharedWorkspace = [NSWorkspace sharedWorkspace];
	[sharedWorkspace openURL: url];
}

- (NSString *)StatusString
{
	if (updating)
		return @"Updating";
	
	switch (myServer->LastUpdateStatus()) {
		case Server::Initial:
			return @"Needs Update";
		case Server::Updated:
			return @"Updated";
		case Server::InternalError:
			return @"InternalError";
		case Server::ServerError:
			return @"ServerError";
		case Server::ProtocolError:
			return @"ProtocolError";
		case Server::InvalidPasswordError:
			return @"InvalidPasswordError";
		case Server::RejectedPasswordError:
			return @"RejectedPasswordError";
		default:
			return @"UnknownError";
	}
}

@end
