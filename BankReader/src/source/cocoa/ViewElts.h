/*
 *  ViewElts.h
 *  BankReader
 *
 *  Created by Brice Rivé on 3/1/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */
#include <string>
#include <vector>
#include <utility>

void DrawBlinker(bool alerting, bool grabbing, bool blinkOn);
void DrawLogo(const NSRect &rect, const std::string &name);
void DrawHistogram(const std::vector<std::pair<float, float> > &histo, int barYPos=96);
void DrawBar(float v0, float v1, int barYPos);
void DrawSmiley(int x, int y, int radius, int happy);
