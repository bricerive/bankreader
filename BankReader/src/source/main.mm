//
//  main.m
//  BankReader
//
//  Created by Brice Riv� on 10/12/05.
//  Copyright __MyCompanyName__ 2005 . All rights reserved.
//

#import <Cocoa/Cocoa.h>
#include <boost/filesystem.hpp>
#include <string>
#include <fstream>
#include <mylib/log.h>
using namespace mylib;
using namespace std;
using namespace boost::filesystem;

int main(int argc, char *argv[])
{
    // Get the location for config and log files
    // We use the location of the executable because that is the only thing 
    // we can rely on. For example when inside of a DropScript app, the 
    // current dir will be /
    // this means that config files must be copied to where the executable
    // is (in the build dir AND in the script dir).
    // Also means that debug runs should start in the build dir
    path mypath = system_complete( path( argv[0] ) ).branch_path();
    string exeName = strrchr(argv[0], '/')+1;
    
    //	// Setup config
    //    path configFilePath = mypath / (exeName + ".config");
    //	config::ConfigDocument::SetFileName(configFilePath.string());
    
    // Setup logging
    path logFilePath = mypath / (exeName + "_log.txt");
    std::ofstream logFile(logFilePath.string().c_str());
	Log::AddStreamBuf(logFile.rdbuf());
    Log::SetLevel(Log::status);

 	try {
        return NSApplicationMain(argc, (const char **) argv);
	} catch (Err &err) {
		LogF(LOG_ERROR, err.what());
	}
}
