/*
 *  Bank.cpp
 *  BankReader
 *
 *  Created by Brice Rive on 1/13/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#include "Bank.h"
#include "StrUtils.h"
#include <boost/regex.hpp>
#include "StoreAccounts.h"
#include "CurlPP.h"
#include "Utils.h"
#include <mylib/error.h>
#include <mylib/log.h>

using std::string;
using namespace mylib;

#pragma mark Base class

const string Bank::serverErrorWhy("ServerError");
const string Bank::protocolErrorWhy("ProtocolError");
const string Bank::invalidPasswordErrorWhy("InvalidPasswordError");
const string Bank::rejectedPasswordErrorWhy("RejectedPasswordError");


Bank::Bank(CurlPP &curl_, const LoginData &loginData_)
: loginData(loginData_), curl(curl_){
	curl.ResetCookies();
}

void Bank::CheckNumeric(const string &str, unsigned int size)
{
	if (str.size() != size) throw Err(_WHERE, invalidPasswordErrorWhy);
	for (int i=0; i<size; i++)	if (str[i]<'0' || str[i]>'9') throw Err(_WHERE, invalidPasswordErrorWhy);
}

void Bank::GetUrlIntoString(const string &url, string &str)
{
	try {
		curl.GetUrl(url, str);
	} catch (Err &err) {
		if (err.Why() == CurlPP::inaccessibleServerErrorWhy) throw Err(_WHERE, serverErrorWhy);
		throw Err(_WHERE, protocolErrorWhy);
	}
}

void Bank::GetUrlIntoContent(const string &url)
{
	GetUrlIntoString(url, content);
}

void Bank::PostIntoContent(const std::string &url, const std::string &post)
{
	try {
		curl.PostUrl(url, post, content);
	} catch (Err &err) {
		if (err.Why() == CurlPP::inaccessibleServerErrorWhy) throw Err(_WHERE, serverErrorWhy);
		throw Err(_WHERE, protocolErrorWhy);
	}
}

string Bank::GrabInContent(const string &regexpr)
{
	try {
		return GrabInString(regexpr, content);
	} catch (Err &err) {
		throw Err(_WHERE, protocolErrorWhy);
	}
}

string Bank::AbsoluteUrlRelativeToCurrent(const string &relative)
{
	return RelativeToAbsoluteUrl(relative, curl.CurrentUrl());
}

bool Bank::SearchInContent(const string &regexpr)
{
	return SearchInString(regexpr, content);
}

void Bank::GrabAccountsInContent(const char *expression_, const char *currency, bool swap)
{
    GrabAccountsInString(expression_, currency, swap, content);
}

void Bank::GrabAccountsInString(const char *expression_, const char *currency, bool swap, const string &str)
{
	boost::regex expression(expression_);
	boost::sregex_iterator m1(str.begin(), str.end(), expression);
	boost::sregex_iterator m2;
    std::for_each(m1, m2, StoreAccounts(accounts, ShortName(), currency, swap));
}

void Bank::Grab(ProgressCallback &progressCallback)
{
	GrabAccounts(progressCallback);
}

const Bank::AccountList &Bank::GetAccounts()
{
	return accounts;
}

void Bank::GetHtmlInfo(string &url_, string &content_)
{
	url_=url;
	content_=content;
}

#pragma mark Static methods

string Bank::GrabInString(const string &regexpr, const string &str)
{
	boost::regex expression(regexpr);
	boost::sregex_iterator m1(str.begin(), str.end(), expression);
	if (m1==boost::sregex_iterator()) throw Err(_WHERE, "No match in string for regex");
	return (*m1)[1];
}

bool Bank::SearchInString(const string &regexpr, const string &str)
{
	return regex_search(str.begin(), str.end(), boost::regex(regexpr)); 
}

#pragma mark Static virtual methods

Bank *Bank::NewBank(const string &name, CurlPP &curl, const LoginData &loginData)
{
	return (*MapEntry(name)->second.factory)(curl, loginData);
}

string Bank::ShortName(const string &name)
{
	return (*MapEntry(name)->second.shortName)();
}

const unsigned char *Bank::IconData(const string &name)
{
	return (*MapEntry(name)->second.getIconData)();
}

const char *Bank::GetUrl(const string &name)
{
	return (*MapEntry(name)->second.url)();
}

void Bank::GetLoginInfo(const string &name, LoginInfo &loginInfo)
{
	return (*MapEntry(name)->second.loginInfo)(loginInfo);
}

bool Bank::NeedsLoginInfoItem(const std::string &name, LoginDataType loginItem)
{
	LoginInfo loginInfo;
	GetLoginInfo(name, loginInfo);
	for (LoginInfo::iterator i=loginInfo.begin(); i!=loginInfo.end(); ++i) {
		if ((*i) == loginItem)
			return true;
	}
	return false;
}

bool Bank::NeedsClientId(const string &name)
{
	return NeedsLoginInfoItem(name, LOGIN);
}

bool Bank::NeedsBirthDate(const string &name)
{
	return NeedsLoginInfoItem(name, DATEOFBIRTH);
}

Bank::BankNames Bank::GetBankNames()
{
	BankNames bankNames;
	for (FactoryMap::iterator i=TheMap().begin(); i!=TheMap().end(); ++i)
		bankNames.push_back(i->first);
	return bankNames;
}

#pragma mark Factory

void Bank::Register(const string &name, const BankStaticMethods &methods)
{
	// Check Bank API version compatibility
	if (ApiVersion((*methods.pluginVersion)()) != apiVersion)
		throw Err(_WHERE, "Incompatible Bank API version in plugin: %s", name.c_str());
	
	std::pair<const string, BankStaticMethods> insertVal(name, methods);
	// Simply try to insert the plugin in the map
	std::pair<FactoryMap::iterator,bool> insertResult;
	if ((insertResult=TheMap().insert(insertVal)).second) {
		LogF(LOG_STATUS, "Bank register: %s", name.c_str());
		return;
	}
	// If the plugin is already there, we need to compare the versions
	unsigned long newVersion = MajorMinorVersion((*methods.pluginVersion)());
	unsigned long oldVersion = MajorMinorVersion((*(insertResult.first->second.pluginVersion))());
	if (newVersion>oldVersion) { // discard old one
		insertResult.first->second = methods;
		//LogF(LOG_STATUS, "Bank plugin %s updated to version %08X (was %08X)", name.c_str(), newVersion, oldVersion);
	} else if (newVersion<oldVersion) {
		// silently discard
	} else {
		throw Err(_WHERE, "Duplicate Bank ID: %s", name.c_str());
	}
}

Bank::FactoryMap::iterator Bank::MapEntry(const string &name)
{
	FactoryMap::iterator i = TheMap().find(name);
	if (i==TheMap().end())
		throw Err(_WHERE, "Unknown Bank ID: %s", name.c_str());
	return i;
}

Bank::FactoryMap &Bank::TheMap()
{
	static FactoryMap *theMap=0; // never gets deleted
	if (theMap==0) theMap = new FactoryMap;
	return *theMap;
}

