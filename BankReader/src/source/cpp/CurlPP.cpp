/*
 *  CurlPP.cpp
 *  DockTicker
 *
 *  Created by Brice Rive on Mon Dec 15 2003.
 *  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
 *
 */

#include "CurlPP.h"
#include "StrUtils.h"
#include "Utils.h"
#include <iostream> // for cout
#include <curl/curl.h>
#include <boost/regex.hpp>
#include <mylib/error.h>
#include <mylib/log.h>
#include <mylib/from_string.h>

#define WANTS_DUMP 1
#define VERBOSE 0
#define DUMP WANTS_DUMP && _DEBUG

using namespace mylib;
using std::cout;
using std::string;

// Error strings
const string CurlPP::curlErrorWhy("Curl error");
const string CurlPP::inaccessibleServerErrorWhy("Cannot access server");
const string CurlPP::internalServerErrorWhy("HTTP status = 500 (Internal Server Error)");
const string CurlPP::unhandledHttpStatusErrorWhy("unexpected HTTP status");

// Safari's signature
//static const string defaultUserAgent = "Mozilla/5.0 (Macintosh; U; PPC Mac OS X; en-us) AppleWebKit/103u (KHTML, like Gecko) Safari/100.1";
// IE Mac static const string defaultUserAgent = "Mozilla/4.0 (compatible; MSIE 5.22; Mac_PowerPC)";
// IE PC
//static const string defaultUserAgent("Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 1.0.3705)");
static const string defaultUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:8.0.1) Gecko/20100101 Firefox/8.0.1");

extern "C" {
	// Function pointer that should match the following prototype:
	// size_t function( void *ptr, size_t size, size_t nmemb, void *stream);
	// This function gets called by libcurl as soon as there is data reveiced
	// that needs to be saved. The size of the data pointed to by ptr is size
	// multiplied with nmemb, it will not be zero terminated. Return the number
	// of bytes actually taken care of. If that amount differs from the amount
	// passed to your function, it'll signal an error to the library and it will
	// abort the transfer and return CURLE_WRITE_ERROR.
	//
	// Set the stream argument with the CURLOPT_WRITEDATA option.
	//
	// NOTE: you will be passed as much data as possible in all invokes, but
	// you cannot possibly make any assumptions. It may be one byte, it may be
	// thousands. The maximum amount of data that can be passed to the write
	// callback is defined in the curl.h header file: CURL_MAX_WRITE_SIZE.
	static size_t writeFunction(void *buffer, size_t size, size_t nmemb, void *userp)
    {
		string *str = reinterpret_cast<string *>(userp);
		str->append(reinterpret_cast<char *>(buffer), size*nmemb);
		return size*nmemb;
    }

	// Function pointer that should match the following prototype:
	//  size_t function( void *ptr, size_t size, size_t nmemb, void *stream);.
	// This function gets called by libcurl as soon as there is received header
	// data that needs to be written down. The headers are guaranteed to be written
	// one-by-one and only complete lines are written. Parsing headers should be
	// easy enough using this. The size of the data pointed to by ptr is size
	// multiplied with nmemb. The pointer named stream will be the one you passed
	// to libcurl with theCURLOPT_WRITEHEADER option. Return the number of bytes
	// actually written or return -1 to signal error to the library (it will cause
	// it to abort the transfer with a CURLE_WRITE_ERROR return code).
	size_t headerFunction( void *ptr, size_t size, size_t nmemb, void *stream)
    {
		string headerLine;
		for (unsigned int i=0; i<size*nmemb; i++)
			headerLine += reinterpret_cast<char *>(ptr)[i];
		reinterpret_cast<CurlPP *>(stream)->ParseHeaderLine(headerLine);
		return size*nmemb;
	}
}

CurlPP::CurlPP()
{
    easyhandle = curl_easy_init();

#if VERBOSE    
    curl_easy_setopt(easyhandle, CURLOPT_VERBOSE, 1);
#endif
	
    // set the error message buffer
    curl_easy_setopt(easyhandle, CURLOPT_ERRORBUFFER, curlErrorBuffer);
    
    // set the data receive handler
    curl_easy_setopt(easyhandle, CURLOPT_WRITEFUNCTION, writeFunction);
    
    // We need the headers
    curl_easy_setopt(easyhandle, CURLOPT_HEADER, true);
    curl_easy_setopt(easyhandle, CURLOPT_HEADERFUNCTION, headerFunction);
    curl_easy_setopt(easyhandle, CURLOPT_WRITEHEADER, this);
    
    // Set a decent agent as it is needed pretty much everywhere on the Web
    curl_easy_setopt(easyhandle, CURLOPT_USERAGENT, defaultUserAgent.c_str());
	
	// Ignore signal for multithreaded apps
    curl_easy_setopt(easyhandle, CURLOPT_NOSIGNAL, true);
}

CurlPP::~CurlPP()
{
	curl_easy_cleanup(easyhandle);
}

string CurlPP::HostPart(const string &url)
{
    unsigned int hostBeg = url.find("//");
	unsigned int hostEnd = url.find("/", hostBeg+2);
    return url.substr(0, hostEnd);
}

unsigned long CurlPP::Perform(const string &url, string &content)
{
    CURLcode success;
	content="";
	crtHeaders.clear();
    curl_easy_setopt(easyhandle, CURLOPT_FILE, &content);
	curl_easy_setopt(easyhandle, CURLOPT_URL, url.c_str());
	curl_easy_setopt(easyhandle, CURLOPT_REFERER, currentUrl.c_str());
    
    // disable CA check directly here
    // - because it's baqsed on using a file in the Curl stage directoyry that isonly accessible on build machine
    // - because we don;t care for it
	curl_easy_setopt(easyhandle, CURLOPT_SSL_VERIFYPEER, false);
    
	if ((success = curl_easy_perform(easyhandle)) != CURLE_OK) {
		LogF(LOG_ERROR, "CURL error: %s", curlErrorBuffer);
		throw Err(_WHERE, curlErrorWhy);
	}
	curl_easy_setopt(easyhandle, CURLOPT_HTTPGET, true); // default
	unsigned long httpStatus;
	if ((success =curl_easy_getinfo(easyhandle, CURLINFO_RESPONSE_CODE, &httpStatus)) != CURLE_OK) {
		LogF(LOG_ERROR, "CURL error: %s", curlErrorBuffer);
		throw Err(_WHERE, curlErrorWhy);
	}
	return httpStatus;
}

void CurlPP::GetUrl(const string &url, string &page, string post)
{
    bool received=false;
    string content, headerVal;
	currentUrl = RelativeToAbsoluteUrl(url, currentUrl);
	string hostName = HostOfUrl(currentUrl);
	if (!IsAccessibleHost(hostName)) throw Err(_WHERE, inaccessibleServerErrorWhy);
#if DUMP
	unsigned long startOfServerName = currentUrl.find("//")+2;
	std::string serverNameAndRestOfUrl = currentUrl.substr(startOfServerName);
	std::string serverName = serverNameAndRestOfUrl.substr(0, serverNameAndRestOfUrl.find("/")) +".txt";
	std::ofstream file(serverName.c_str(), std::ios::out|std::ios::app);
#endif

    if (!post.empty())
    {
        curl_easy_setopt(easyhandle, CURLOPT_POSTFIELDS, post.c_str());
    }

    while (!received) {
		unsigned long httpStatus = Perform(currentUrl, content);
		AddMetaHttpHeaders(content);
#if DUMP
		file << "#########################################" << std::endl;
		file << "### URL: " << currentUrl << std::endl;
		file << "### Cookies: " << cookies << std::endl;
        if (!post.empty())
        {
            file << "### Post: " << post << std::endl;
            post="";
        }
		file << "### Content: " << content << std::endl;
#endif
        switch (httpStatus) {
            case 100: // Continue -> do it again
                break;
            case 200: // OK -> done
				if (HasHeader("refresh", headerVal)) {
					string seconds = StrUtils::GrabString(headerVal, "", ";");
					string refresh;
					try {
						refresh = StrUtils::GrabString(headerVal, "URL=", "");
					} catch (Err &err) {
						refresh = StrUtils::GrabString(headerVal, "url=", "");
					}
					sleep(from_string<int>(seconds)); 
					currentUrl = RelativeToAbsoluteUrl(refresh, currentUrl);
				} else
					received=true;
				break;
			case 302: // Redirect
			case 301: // Moved permanently
				if (HasHeader("location", headerVal)) {
					currentUrl = RelativeToAbsoluteUrl(headerVal, currentUrl);
				}
				break;
            case 304: // Refresh -> do it again
                break;
			case 500: // 500 Internal Server Error: The server encountered an unexpected condition which prevented it from fulfilling the request.
                throw Err(_WHERE, internalServerErrorWhy);
            default:
				LogF(LOG_ERROR, "Unhandled HTTP status = %d", httpStatus);
                throw Err(_WHERE, unhandledHttpStatusErrorWhy);
        }
        MergeCookies(StrUtils::GrabCookies(content)); // The headers are inside of content too!
    }
    page = content;
}

void CurlPP::PostUrl(const string &url, const string &post, string &page)
{
	//LogF(LOG_STATUS, url + " <- " + post);
    GetUrl(url, page, post);
}

// HTTP header syntax:
// <name>: <value>
void CurlPP::ParseHeaderLine(const string &headerLine)
{
	unsigned int i;
	string name, value;
	for (i=0; i<headerLine.size();) {
		char c = headerLine[i++];
		if (c==':') break;
		name += tolower(c);
	}
	for (; i<headerLine.size(); ++i) {
		char c = headerLine[i];
		if (c!=' ' && c!='\t') break;
	}
	for (; i<headerLine.size(); ++i) {
		char c = headerLine[i];
		if (c=='\r' && headerLine[i+1]=='\n') break;
		value += c;
	}
	crtHeaders.push_back(make_pair(name, value));
}

class AddAHeader {
public:
	AddAHeader(CurlPP::Headers & curlHdrs_):curlHdrs(curlHdrs_) {}
	bool operator()(const boost::smatch &what);
private:
	CurlPP::Headers &curlHdrs;
};

bool AddAHeader::operator()(const boost::smatch &what) 
{
	curlHdrs.push_back(make_pair(what[1].str(), what[2].str()));
	return true;
}

// Http header equivalents located in the html itself!!!
// <head>
// <meta http-equiv="CacheControl" content = "no-cache" />
// <meta http-equiv="Pragma" content = "no-cache" />
// <meta http-equiv="Expires" content = "-1" />
// <meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../../../g1_e/ssl/identification/3-acc_ide1.ide"/>
// <title>Cr&eacute;dit Agricole - Services de banque en ligne</title>
// </head>
void CurlPP::AddMetaHttpHeaders(const std::string &content)
{
	const std::string expr("<[[:space:]]*[mM][eE][tT][aA][[:space:]]*[hH][tT][tT][pP]-[eE][qQ][uU][iI][vV][[:space:]]*=[[:space:]]*\"(.*?)\"[[:space:]]*[cC][oO][nN][tT][eE][nN][tT][[:space:]]*=[[:space:]]*\"(.*?)\"");
	boost::regex expression(expr);
	boost::sregex_iterator m1(content.begin(), content.end(), expression);
	boost::sregex_iterator m2;
    std::for_each(m1, m2, AddAHeader(crtHeaders));
}

// case-independent (ci) equality binary function
// for use by ciStringEqual
template <class T>
struct ci_equal_to : public std::binary_function<T,T,bool> 
{
	bool operator() (const T& c1, const T& c2) const 
    { return tolower (c1) == tolower (c2); }
};

// case-independent (ci) string compare
// returns true if strings are EQUAL
bool ciStringEqual (string s1, string s2)
{
	
	std::pair <string::const_iterator, 
	string::const_iterator> result =
    mismatch (s1.begin (), s1.end (),   // source range
              s2.begin (),              // comparison start
              ci_equal_to<unsigned char> ());  // comparison
	
	// match if both at end
	return result.first == s1.end () &&
		result.second == s2.end ();
	
} // end of ciStringEqual 

// Needs to be case-independent
bool CurlPP::HasHeader(const string &name, string &value)
{
	for (Headers::iterator i=crtHeaders.begin(); i!=crtHeaders.end(); i++) {
		if (ciStringEqual(i->first,name)) {
			value = i->second;
			return true;
		}
	}
	return false;
}

void CurlPP::ResetCookies()
{
	cookies.clear();
	curl_easy_setopt(easyhandle, CURLOPT_COOKIE, cookies.c_str());
}

void CurlPP::MergeCookies(const std::string &newCookies)
{
    StrUtils::MergeCookies(cookies, newCookies);
    curl_easy_setopt(easyhandle, CURLOPT_COOKIE, cookies.c_str());
}

void CurlPP::DisableCertificates()
{
	curl_easy_setopt(easyhandle, CURLOPT_SSL_VERIFYPEER, false);
}

void CurlPP::SetSslVersion(int version)
{
	curl_easy_setopt(easyhandle, CURLOPT_SSLVERSION, version);
}
