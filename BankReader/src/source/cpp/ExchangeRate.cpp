/*
 *  ExchangeRate.cpp
 *  BankReader
 *
 *  Created by Brice Rive on 1/11/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#include "ExchangeRate.h"
#include "mylib/time.h"
#include "curlPP.h"
#include "StrUtils.h"
#include <boost/regex.hpp>
#include <mylib/error.h>
#include <mylib/log.h>
#include <mylib/idioms.h>
using std::string;
using namespace mylib;

struct Currency {
	const char * const name;
	const char * const label;
	const char * const symbol;
};
static const Currency currencyDefinitions[] = {
	{"Euro","EUR","€"} , {"Dollar","USD","$"}, {"Pound","GBP","£"}, {"Yen","JPY","¥"},
	{"Franc","6.55957", "₣"}, {"Mark","1.95583", "DEM"}, {"Schilling", "13.7603", "ATS"},
	{"Franc Belge","40.3399", "BEF"}, {"Peseta","166.386","₧"}, {"Lira","1936.27","₤"},
	{"Irish Pound","0.787564","IEP"}, {"Guilder","2.20371","NLG"}, {"Escudos","200.482","PTE"},
	{"Finnish Markka","5.94573","FIM"}, {"Drachme","340.750","₯"}
};
static const int nbCurrencyDefinitions = sizeof(currencyDefinitions)/sizeof(currencyDefinitions[0]);


const long ExchangeRate::validSeconds=600;

bool ExchangeRate::ThreadDone(const ExchangeRate::ThreadRecordSp &threadRecord)
{
	return threadRecord->thread.Done();
}

float ExchangeRate::GetRate(const string &src, const string &dst)
{
	// Cleanup our threads
	threads.erase(remove_if(threads.begin(), threads.end(), ThreadDone), threads.end());
	
	// Start with identity rate
	float rate = 1.0;
	// Convert source to Euro for old European currencies
	string srcLabel = CurrencyLabel(src);
	if (srcLabel[0]>='0' && srcLabel[0]<='9') {
		rate /= from_string<float>(srcLabel);
		srcLabel = "EUR";
	}
	// Convert destination to Euro for old European currencies
	string dstLabel = CurrencyLabel(dst);
	if (dstLabel[0]>='0' && dstLabel[0]<='9') {
		rate *= from_string<float>(dstLabel);
		dstLabel = "EUR";
	}
	// Check if we're done
	if (srcLabel==dstLabel) return rate;
	// Otherwise we need to ask for actual exchange rate
	// Do we already have the rate and is it still valid
	string key = srcLabel+dstLabel;
	ratesCriticalSection.Lock();
	Rates::iterator pos = rates.find(key);
	bool noRate = pos==rates.end();
	ratesCriticalSection.Unlock();
	long s,ms;
	Time::CurrentTime(s,ms);
	if (noRate || pos->second.expires<=s) {
		// Can we start a new thread
		if (threads.size() >= maxNbThreads)
			throw Err(_WHERE, "Too many threads");
		// Don't we already have a thread fetching this rate?
		if (find_if(threads.begin(), threads.end(), ThreadForKey(key))==threads.end()) {
			// Make a functor for grabing the exchange rate
			mylib::Thread::ExecuteFun execute = Loki::BindFirst(Loki::Functor<void, TYPELIST_1(string)>(this, &ExchangeRate::GrabValue), key);
			// Start a thread
			threads.push_back(ThreadRecordSp(new ThreadRecord(execute, key)));
		}
		// if we have no rate whatsoever, throw
		if (noRate) throw mylib::Err(_WHERE, "Rate not available yet");
		// otherwise, use the old one \/
	}
	return rate * pos->second.val;
}

string ExchangeRate::CurrencyLabel(const string &currency)
{
	for (int i=0; i<nbCurrencyDefinitions; i++)
		if (currency == currencyDefinitions[i].name)
			return currencyDefinitions[i].label;
	return "Error";
}

string ExchangeRate::CurrencySymbol(const string &currency)
{
	for (int i=0; i<nbCurrencyDefinitions; i++)
		if (currency == currencyDefinitions[i].name)
			return currencyDefinitions[i].symbol;
	return "Error";
}


ExchangeRate::CurrencyList ExchangeRate::GetCurrencies()
{
	CurrencyList list;
	for (int i=0; i<nbCurrencyDefinitions; i++)
		list.push_back(currencyDefinitions[i].name);
	return list;
}

static string GrabInString(const string &regexpr, const string &str)
{
	boost::regex expression(regexpr);
	boost::sregex_iterator m1(str.begin(), str.end(), expression);
	if (m1==boost::sregex_iterator()) throw Err(_WHERE, "No match in string for regex");
	return (*m1)[1];
}

void ExchangeRate::GrabValue(string key)
{
	try {
		// Open main page
		string page;
		string url = string("http://finance.yahoo.com/q?s=") + key + "%3dX&d=1b";
		CurlPP curl;
		curl.GetUrl(url, page);
		long s,ms;
		Time::CurrentTime(s,ms);
		// return the value
		Rate rate;
		// was: rate.val = from_string<float>(GrabInString("Last Trade:.*?([[:digit:]]+\\.[[:digit:]]+)", page));
        // new HTML: <span class="time_rtq_ticker"><span id="yfs_l10_usdeur=x">0.7671</span></span>
		rate.val = from_string<float>(GrabInString("<span class=\"time_rtq_ticker\"><span id=\".*?=x\">([[:digit:]]+\\.[[:digit:]]+)</span></span>", page));
		rate.expires = s + validSeconds;
		// Update the rates
		ratesCriticalSection.Lock();
		rates[key] = rate;
		ratesCriticalSection.Unlock();
	} catch (Err &err) {
		LogF(LOG_ERROR, err.what());
		return;
	}
}

