/*
 *  view.h
 *  BankReader
 *
 *  Created by Brice Rive on 1/12/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once
#include <string>
#include <vector>
#include <map>

class View {
	
public:
	
	class ViewAccount {		
	public:
		struct DatedValue {
			DatedValue(float value_, unsigned long secondsBeforeNow_): value(value_), secondsBeforeNow(secondsBeforeNow_) {}
			float value;
			unsigned long secondsBeforeNow;
		};
		typedef std::vector<DatedValue> DatedValues;
		ViewAccount(std::string bankName_, std::string name_, std::string number_, std::string currency_, std::string type_, const DatedValues &values_)
		: bankName(bankName_),name(name_),number(number_),currency(currency_),type(type_),datedValues(values_){}
		std::string BankName()const {return bankName;}
		std::string Name()const {return name;}
		std::string Number()const {return number;}
		std::string Currency()const {return currency;}
		std::string Type()const {return type;}
		const DatedValues &GetDatedValues()const {return datedValues;}
		float Value()const {return datedValues[datedValues.size()-1].value;}
		
	private:
		std::string bankName;
		std::string name;
		std::string number;
		std::string currency;
		std::string type;
		DatedValues datedValues;
	};
	typedef std::vector<ViewAccount> Accounts;
	
	View();
		
	typedef std::vector<std::string> ViewNames;
	static ViewNames GetViewNames();
	
	virtual ~View() {}
	void Tick() {++tickerIdx;}
		
	virtual void Draw(const Accounts &accounts, bool grabbing, bool alerting)=0;
	
	void ShowDetails(bool val) {showDetails=val;}
	void ShowTotal(bool val) {showTotal=val;}
	void Currency(const std::string &val) {currency=val;}
	void TickerIdx(int val) {tickerIdx=val;}
	void BlinkOn(bool val) {blinkOn=val;}
	bool ShowDetails()const {return showDetails;}
	bool ShowTotal()const {return showTotal;}
	std::string Currency()const {return currency;}
	int TickerIdx()const {return tickerIdx;}
	bool BlinkOn()const {return blinkOn;}
	
protected:
		
	bool showDetails;
	bool showTotal;
	int tickerIdx;
	bool blinkOn;
	std::string currency;
	float TotalValue(const Accounts &accounts);
	void ComputeHistogram(const Accounts &accounts, std::vector<std::pair<float, float> > &histo, bool (*test)(const ViewAccount &account)=0);
	
	//! @name View Registry
	//! Manage the view registry and provide static-virtuals for accessing sub-classes info using the name.
	//! Entries in the registry are keyed by the view name and provide a set of static methods to access
	//! view specific info.
	//@{
public:
	static const unsigned char apiVersion=1;
	//! Make a version number from its sub parts (API, major, minor)
	static unsigned long Version(unsigned char major, unsigned char minor) {return apiVersion<<24 | major<<8 | minor;}
	//! Extract the API version from a version number
	static unsigned char ApiVersion(unsigned long version) {return version>>24;}
	//! Extract the major/minor version from a version number
	static unsigned long MajorMinorVersion(unsigned long version) {return version&0xFFFF;}
	//! Factory method. Creates a view by it's name. Optionaly takes a second parameter from which
	//! base-class attributes will be copied.
	static View *New(const std::string &name, View *rhs=0);
protected:
	// view static methods we want to access polymorphically using the view name for selector
	typedef View *(*_Factory)();
	typedef unsigned long (*_Version)();
	struct ViewStaticMethods {
		_Factory factory;
		_Version version;
		ViewStaticMethods(_Factory factory_, _Version version_) :factory(factory_), version(version_) {}
	};
	static void Register(const std::string &name, const ViewStaticMethods &methods);
private:
	typedef std::map<std::string, ViewStaticMethods> FactoryMap;
	static FactoryMap &TheMap();
	//@}
};

#define REGISTER_VIEW(name, major, minor, className) \
static View *New() {return new className;} \
static unsigned long Version() { return View::Version(major, minor); } \
struct Registrar { Registrar() {View::Register(#name, ViewStaticMethods(&className::New, &className::Version));} }; \
static Registrar registrar
