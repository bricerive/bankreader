/*
 *  StoreAccounts.h - Tool to help with bank data parsing.
	Could be part of Bank.h/cpp but there is a conflict between Obj-C's "id" and code in boost/regex.
 *  BankReader
 *
 *  Created by Brice Rive on 4/14/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once
#include <boost/regex.hpp>
#include "Bank.h"

class StoreAccounts {
public:
	StoreAccounts(Bank::AccountList &accounts_, std::string bankId_, std::string currency_, bool swap_=false);
	bool operator()(const boost::smatch &what);
private:
	Bank::AccountList &accounts;
	std::string bankId;
	std::string currency;
	bool swap;
};
