/*
 *  Bank.h - Abstract class for Bank plugins
 *  BankReader
 *
 *  Created by Brice Rive on 1/13/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once
#include <string>
#include <map>
#include <vector>
#include "loki/Functor.h"

class CurlPP;

//! Bank registry and abstract bank.
//! This class has two functions: Manage the registry of bank types and provide the abstract interface for the banks
class Bank {

public:
	
	//! The account information extracted from a bank's web server.
	class Account {
	public:
		Account(std::string name_, std::string number_, std::string currency_, std::string type_, float amount_)
			: name(name_),number(number_),currency(currency_),type(type_),amount(amount_){}
		std::string Description()const {return name;}
		std::string Number()const {return number;}
		std::string Currency()const {return currency;}
		std::string Type()const {return type;}
		float Amount()const {return amount;}
	private:
		std::string name;
		std::string number;
		std::string currency;
		std::string type;
		float amount;
	};
	
	//! A List of Bank::Account
	typedef std::vector<Account> AccountList;

	//! @name Login Info
	//@{
	typedef enum {LOGIN, PASSWORD, DATEOFBIRTH, AMOUNT, CURRENCY, TYPE} LoginDataType;
	typedef std::vector<LoginDataType> LoginInfo;
	typedef std::map< LoginDataType, std::string> LoginData;
	//@}
	
	//! Go and grabs accounts
	typedef Loki::Functor<void, TYPELIST_1(float)> ProgressCallback;
	void Grab(ProgressCallback &progressCallback);
	//! Return the final url of the grab as well as the HTML content
	void GetHtmlInfo(std::string &url_, std::string &content_);
	//! return the account list found by the grab
	const AccountList &GetAccounts();

	
	//! @name Error identifiers
	//@{
	static const std::string serverErrorWhy;
	static const std::string protocolErrorWhy;
	static const std::string invalidPasswordErrorWhy;
	static const std::string rejectedPasswordErrorWhy;
	//@}

protected:
	LoginData loginData;
	CurlPP &curl;
	std::string content;
	std::string url;
	AccountList accounts;
	
	
	//! @name Utilities
	//! Utilities provided to all the sub-classes.
	//@{
protected:
	void GetUrlIntoString(const std::string &url, std::string &str);
	void GetUrlIntoContent(const std::string &url);
	void PostIntoContent(const std::string &url, const std::string &post);
	std::string GrabInContent(const std::string &regexpr);
	bool SearchInContent(const std::string &regexpr);
	static std::string GrabInString(const std::string &regexpr, const std::string &str);
	static bool SearchInString(const std::string &regexpr, const std::string &str);
	void GrabAccountsInContent(const char *expression_, const char *currency, bool swap=false);
    void GrabAccountsInString(const char *expression_, const char *currency, bool swap, const std::string &str);
	static void CheckNumeric(const std::string &str, unsigned int size);
	std::string AbsoluteUrlRelativeToCurrent(const std::string &relative);
	//@}

		
	//! @name Virtual interface
	//@{
public:
	virtual ~Bank() {}	
protected:
	Bank(CurlPP &curl_, const LoginData &loginData_);
private:
	//! Get the accounts from the web server
	virtual void GrabAccounts(ProgressCallback &progressCallback)=0;
	//! Return the bank short name
	virtual std::string ShortName()const=0;
	//@}
	
	//! @name Bank Registry
	//! Manage the bank registry and provide static-virtuals for accessing sub-classes info using the name
	//! (static-virtuals are methods that are static to a sub-class - no pointer available to an object - and
	//! that we want to access polymorphicaly using the name).
	//! Entries in the registry are keyed by the bank name and provide a set of static methods to access
	//! bank specific info.
	//@{
public:
	static const unsigned char apiVersion=1;
	static unsigned long Version(unsigned char major, unsigned char minor) {return apiVersion<<24 | major<<8 | minor;}
	static unsigned char ApiVersion(unsigned long version) {return version>>24;}
	static unsigned long MajorMinorVersion(unsigned long version) {return version&0xFFFF;}
	//! Bank factory: create an instance of a bank of the given name
	static Bank *NewBank(const std::string &name, CurlPP &curl, const LoginData &loginData);
	//! Return the short name for that bank
	static std::string ShortName(const std::string &name);
	//! Return the icon data for that bank. The icon data is a 32x32 block of RGBA bytes
	static const unsigned char *IconData(const std::string &name);
	//! Return the URL for the Web site of that bank
	static const char *GetUrl(const std::string &name);
	//! Get the description of what info is needed to login into that bank's site
	static void GetLoginInfo(const std::string &name, LoginInfo &loginInfo);
	//! Tells whether that bank requires a specific loginInfo item
	static bool NeedsLoginInfoItem(const std::string &name, LoginDataType loginItem);
	//! Tells whether that bank requires a birthdate
	static bool NeedsBirthDate(const std::string &name);
	//! Tells whether that bank requires a clientID
	static bool NeedsClientId(const std::string &name);
	//! number of registered banks
	static int NbBankIds();
	//! return the ID of a registered bank
	static std::string BankId(unsigned int idx);
	//! return a list of the registered banks
	typedef std::vector<std::string> BankNames;
	static BankNames GetBankNames();
protected:
	// bank static methods we want to access virtually
	typedef Bank *(*BankFactory)(CurlPP &curl, const LoginData &loginData);
	typedef const std::string (*BankShortName)();
	typedef void (*BankLoginInfo)(LoginInfo &loginInfo);
	typedef const unsigned char *(*BankIconData)();
	typedef const char *(*BankUrl)();
	typedef unsigned long (*PluginVersion)();
	struct BankStaticMethods {
		BankFactory factory;
		BankShortName shortName;
		BankLoginInfo loginInfo;
		BankIconData getIconData;
		BankUrl url;
		PluginVersion pluginVersion;
		BankStaticMethods(BankFactory factory_, BankShortName shortName_, BankLoginInfo loginInfo_, BankIconData iconData_, BankUrl url_, PluginVersion pluginVersion_)
			:factory(factory_), shortName(shortName_), loginInfo(loginInfo_), getIconData(iconData_), url(url_), pluginVersion(pluginVersion_) {}
	};
	
	static void Register(const std::string &name, const BankStaticMethods &methods);
	//@}
	
private:
	typedef std::map<std::string, BankStaticMethods> FactoryMap;
	static FactoryMap &TheMap();
	static FactoryMap::iterator MapEntry(const std::string &name);
};

// Bank factory registration
#define BANK_FACTORY_REGISTRATION(className, bankName, pluginMajor, pluginMinor, url) \
static Bank *New(CurlPP &curl, const LoginData &loginData) { return new className(curl, loginData); } \
struct Registrar { Registrar() {Bank::Register(bankName, BankStaticMethods(&className::New, &className::StaticShortName,\
																		   &className::LoginInfo, &className::IconData, &className::Url, &className::Version));} }; \
static Registrar registrar; \
static const std::string StaticShortName() { return #className; } \
static const char *Url() { return url; } \
static const unsigned char *IconData() { return className##Icon; } \
static unsigned long Version() { return Bank::Version(pluginMajor, pluginMinor); }

