/*
 *  Server.cpp
 *  BankReader
 *
 *  Created by Brice Rivé on 12/7/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#include "Server.h"
#include <mylib/thread.h>
#include <mylib/error.h>
#include <mylib/log.h>

using namespace mylib;
using namespace Loki;

bool Server::ctorCalled=false;

Server::Server()
    : status(Initial)
    , asyncDoneFlag(false)
    , myBank(0)
    , thread(Functor<void>(this, &Server::UpdateFunc),false)
{
	ctorCalled=true;
}

Server::~Server()
{
	thread.Stop();
	delete myBank;
}

#pragma mark Updating

// Thread func.
void Server::UpdateFunc()
{
	try {
		myBank->Grab(progressCallBack);
		status = Updated;
	} catch ( std::exception &err) {// Diagnose errors
		// /\ catch on std::exception instead of mylib::Err. That's because on Panther, exceptions
		// seem to loose their typeid passing when across bundles. That is, the plugin and the main app
		// don't seem to have the same typeid for the same class!!! It seems to be that Apple optimized typeid
		// comparison by using the typeid's name adress instead of doing a strcmp (see <typeid>).
		// That fails in plugin architecture if the appl and its plugin have different copies of the typeid struct.
		if (Bank::rejectedPasswordErrorWhy==err.what()) {
			LogF(LOG_ERROR, "Server login error: %s", err.what());
			status = RejectedPasswordError;
		} else if (Bank::invalidPasswordErrorWhy==err.what()) {
			LogF(LOG_ERROR, "Invalid password: %s", err.what());
			status = InvalidPasswordError;
		} else if (Bank::protocolErrorWhy==err.what()) {
			LogF(LOG_ERROR, "Cannot access update info: %s", err.what());
			status = ProtocolError;
		} else if (Bank::serverErrorWhy==err.what()) {
			LogF(LOG_ERROR, "Cannot access server: %s", err.what());
			status = ServerError;
		} else {
			LogF(LOG_ERROR, "Unexpected exception[%08X,%s]: %s", &typeid(err), typeid(err).name(), err.what());
			status = InternalError;
		}
	}
	asyncDoneFlag = true; // Atomic
}

void Server::TriggerUpdate(const std::string &bankName, const Bank::LoginData &loginData, Bank::ProgressCallback callback)
{
	if (!ctorCalled) LogF(LOG_ERROR, "Server::TriggerUpdate: ctor not called!!!");
	try {
		curl.ResetCookies();
		// Create the bank object
		myBank = Bank::NewBank(bankName, curl, loginData);		
		// Trigger asynchronous update
		progressCallBack = callback;
		thread.Start();
	} catch (Err &err) {
		status = InternalError;
		asyncDoneFlag = true; // Atomic
		throw;
	}
}

bool Server::CheckUpdated(Bank::AccountList &bankAccounts)
{
	if (!asyncDoneFlag) // Atomic
		return false;
	asyncDoneFlag = false;
	if (status==Updated)
		bankAccounts = myBank->GetAccounts();
	delete myBank;
	myBank = 0;
	return true;
}
