/*
 *  SecureStorage.cpp
 *  MoneyFusionLight
 *
 *  Created by Brice Rivé on 10/5/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#include "SecureStorage.h"

#include <CoreFoundation/CoreFoundation.h>
#include <Security/Security.h>
#include <CoreServices/CoreServices.h>
#include <mylib/error.h>

using namespace mylib;
using std::string;

static const char *serviceName("BankReader");
static const int serviceNameLength(strlen(serviceName));
static const int defaultKeyChain(0);
static SecKeychainItemRef * const notInterestedInItemRef(0);
static const SecKeychainAttributeList *noAttributes(0);

void SecureStorage::StorePassword(const string &accountName, const string &pass)
{
	// If the item exists in the keychain, simply modify it
	UInt32 passwordLength;
	void *passwordData;
	SecKeychainItemRef itemRef;
	if (SecKeychainFindGenericPassword(defaultKeyChain, serviceNameLength, serviceName, accountName.size(), accountName.c_str(),
										&passwordLength, &passwordData, &itemRef) == noErr) {
		const char *passwordText = reinterpret_cast<char *>(passwordData);
		std::string oldPassword(passwordText, passwordText+passwordLength);
		if (pass == oldPassword) return;
		if (SecKeychainItemModifyAttributesAndData (itemRef, noAttributes, pass.size(), pass.c_str()) != noErr)
			throw Err(_WHERE, "Cannot modify password in keychain (%s)", accountName.c_str());
		return;
	}
	// Otherwise, create a new item in the keychain
	static const SecAccessRef singleApplicationAccess = 0;
	string label = string(serviceName)+":"+accountName;
    SecKeychainAttribute attrs[] = {
		{ kSecLabelItemAttr, label.size(), (void *)label.c_str() },
		{ kSecServiceItemAttr, serviceNameLength, (void *)serviceName},
		{ kSecAccountItemAttr, accountName.size(), (void *)accountName.c_str() }
    };
    SecKeychainAttributeList attributes = { sizeof(attrs) / sizeof(attrs[0]), attrs };
	OSStatus result;
    if ((result=SecKeychainItemCreateFromContent(kSecGenericPasswordItemClass, &attributes, pass.size(), pass.c_str(),
										   defaultKeyChain, singleApplicationAccess, 0))!=noErr)
		throw Err(_WHERE, "Cannot store password in keychain (%s): %d", accountName.c_str(), result);
}

const std::string SecureStorage::RetreivePassword(const string &accountName)
{
	UInt32 passwordLength;
	void *passwordData;
	OSStatus status = SecKeychainFindGenericPassword(defaultKeyChain, serviceNameLength, serviceName, accountName.size(), accountName.c_str(),
													  &passwordLength, &passwordData, notInterestedInItemRef);
	if (status!=noErr) throw Err(_WHERE, "Cannot retreive password from keychain (%s)", accountName.c_str());
	const char *passwordText = reinterpret_cast<char *>(passwordData);
	std::string password(passwordText, passwordText+passwordLength);
	SecKeychainAttributeList * const noAttributeDataToRelease(0);
	status = SecKeychainItemFreeContent (noAttributeDataToRelease, passwordData);
	return password;
}

void SecureStorage::ForgetPasswords()
{
	// Create a search object
    SecKeychainAttribute attrs[] = {{ kSecServiceItemAttr, serviceNameLength, (void *)serviceName}};
    SecKeychainAttributeList attributes = { sizeof(attrs) / sizeof(attrs[0]), attrs };
	
	OSStatus status;
	do {
		SecKeychainSearchRef searchRef;
		status = SecKeychainSearchCreateFromAttributes (0, kSecGenericPasswordItemClass, &attributes, &searchRef);
		if (status==noErr) {
			SecKeychainItemRef itemRef;
			status=SecKeychainSearchCopyNext(searchRef, &itemRef);
			if (status==noErr) {
				status = SecKeychainItemDelete(itemRef);
				CFRelease(itemRef);
			}
			CFRelease(searchRef);
		}
	} while (status==noErr);
}

