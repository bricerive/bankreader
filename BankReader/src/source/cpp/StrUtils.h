/*
 *  StrUtils.h
 *  DockTicker
 *
 *  Created by Brice Rive on Mon Dec 15 2003.
 *  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
 *
 */
#pragma once
#include <string>

class StrUtils {
public:
    static void MergeCookies(std::string &cookies, const std::string &newCookies);
    static std::string GrabString(const std::string &content, const std::string &tagB, const std::string &tagE);
    static std::string GrabCookies(const std::string &header);
    static float GetUsAmount(const std::string &str);
    static float GetFrAmount(const std::string &str);
	static std::string RemoveHtmlSpaces(const std::string &s);
    static void RemoveString(const std::string &search, std::string &str);
    static void ReplaceString(const std::string &search, const std::string &replace, std::string &str);
};
