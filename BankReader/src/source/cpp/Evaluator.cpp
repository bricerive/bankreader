/*
 *  Evaluator.cpp
 *  BankReader
 *
 *  Created by Brice Rive on 1/15/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#include "Evaluator.h"

#if 1

// Spirit v2.5 allows you to suppress automatic generation
// of predefined terminals to speed up complation. With
// BOOST_SPIRIT_NO_PREDEFINED_TERMINALS defined, you are
// responsible in creating instances of the terminals that
// you need (e.g. see qi::uint_type uint_ below).
#define BOOST_SPIRIT_NO_PREDEFINED_TERMINALS

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <mylib/error.h>

using namespace mylib;
namespace qi = boost::spirit::qi;
namespace ascii = boost::spirit::ascii;



///////////////////////////////////////////////////////////////////////////
//  Our calculator grammar
///////////////////////////////////////////////////////////////////////////
template <typename Iterator>
struct EvaluateGammar : qi::grammar<Iterator, double(), ascii::space_type>
{
    EvaluateGammar() : EvaluateGammar::base_type(expression)
    {
        qi::_val_type _val;
        qi::_1_type _1;
        qi::uint_type uint_;
        
        
        inequality =
        expression [_val=_1]
        >> (
            (">=" >> expression[_val = _val>=_1])
            | ("<=" >> expression[_val= _val<=_1])
            | ("=" >> expression[_val= _val==_1])
            | ("<" >> expression[_val= _val<_1])
            | (">" >> expression[_val= _val>_1])
            | ("!=" >> expression[_val= _val!=_1])
            );

        expression =
        term                            [_val = _1]
        >> *(   ('+' >> term            [_val += _1])
             |   ('-' >> term            [_val -= _1])
             )
        ;
        
        term =
        factor                          [_val = _1]
        >> *(   ('*' >> factor          [_val *= _1])
             |   ('/' >> factor          [_val /= _1])
             )
        ;
        
        factor =
        uint_                           [_val = _1]
        |   '(' >> expression           [_val = _1] >> ')'
        |   ('-' >> factor              [_val = -_1])
        |   ('+' >> factor              [_val = _1])
        ;
    }
    
    qi::rule<Iterator, double(), ascii::space_type> inequality, expression, term, factor;
};

bool Evaluator::Evaluate(const VarMap &varMap, const std::string &expression)
{
    try {
        boost::spirit::ascii::space_type space; // Our skipper
        typedef std::string::const_iterator iterator_type;
        EvaluateGammar<iterator_type> evaluate;
        double result;
        std::string::const_iterator iter = expression.begin();
        std::string::const_iterator end = expression.end();
        if (phrase_parse(iter, end, evaluate, space, result) && iter==end)
            return result!=0;
    } catch (Err &err) {
        throw Err(_WHERE, "Expression evaluation error [%s]", err.Why().c_str());
    }
    throw Err(_WHERE, "Expression parsing error");
}


#else
#define BOOST_SPIRIT_USE_OLD_NAMESPACE
#include <boost/spirit/include/classic_core.hpp>
#include <boost/spirit/include/classic_attribute.hpp>
#include <boost/spirit/include/phoenix1_functions.hpp>
#include <boost/spirit/include/classic_loops.hpp>
#include <boost/spirit/include/classic_chset.hpp>
#include <boost/spirit/include/classic_confix.hpp>
#include <boost/spirit/include/classic_escape_char.hpp>

#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/phoenix/function.hpp>
#include <boost/spirit/include/classic_closure.hpp>
#include <boost/spirit/include/classic_grammar.hpp>
#include <iostream>
#include <mylib/error.h>
using namespace mylib;

using namespace std;
using namespace boost::spirit;
using namespace boost::phoenix;

// Boost-Spirit/Phoenix parser/evaluator
//--------------------------------------

// Make the varaible map visible
static const Evaluator::VarMap *gVarMap=0;

// Define a closure
struct calc_closure : closure<calc_closure, double>
{
  member1 val;
};

// define a lazy-function
struct varEval_impl
{
  template <typename Item1, typename Item2>
  struct result
  {
    typedef float type;
  };
  
  template <typename Item1, typename Item2>
    float operator()(Item1 first, Item2 last) const
  {
    std::string varStr(first, last);
    Evaluator::VarMap::const_iterator var = gVarMap->find(varStr);
    if (var==gVarMap->end())
		throw Err(_WHERE, "Unknown account: %s", varStr.c_str());
    return var->second;
  }
};
boost::phoenix::function<varEval_impl> const varEval = varEval_impl();


// define the parser/evaluator
struct calculator : public grammar<calculator, calc_closure::context_t>
{
  template <typename ScannerT>
  struct definition
  {
    definition(calculator const& self)
  {
      top = inequality[self.val = _1];
      
      inequality
        =   expression[inequality.val=_1]
        >> ( 
            (">=" >> expression[inequality.val=double(inequality.val>=_1)])
             | ("<=" >> expression[inequality.val=(inequality.val<=_1?1:0)])
             | ("=" >> expression[inequality.val=(inequality.val==_1?1:0)])
             | ("<" >> expression[inequality.val=(inequality.val<_1?1:0)])
             | (">" >> expression[inequality.val=(inequality.val>_1?1:0)])
             | ("!=" >> expression[inequality.val=(inequality.val!=_1?1:0)])
             );
      
      expression
        =   term[expression.val = _1]
        >> *(   ('+' >> term[expression.val += _1])
                |   ('-' >> term[expression.val -= _1])
                )
        ;
      
      term
        =   factor[term.val = _1]
        >> *(   ('*' >> factor[term.val *= _1])
                |   ('/' >> factor[term.val /= _1])
                )
        ;
      
      factor
        =   ureal_p[factor.val = _1]
        |   '(' >> expression[factor.val = _1] >> ')'
        |   ('-' >> factor[factor.val = -_1])
        |   ('+' >> factor[factor.val = _1])
		|   ('[' >> (+(~ch_p(']')))[factor.val = varEval(_1, _2)] >> ']')
        ;
  }
    
    typedef rule<ScannerT, calc_closure::context_t> rule_t;
    rule_t inequality, expression, term, factor;
    rule<ScannerT> top;
    
    rule<ScannerT> const&
      start() const { return top; }
  };
};

bool Evaluator::Evaluate(const VarMap &varMap, const std::string &expression)
{
  calculator calc;    //  Our parser  
  parse_info<> info; // the parsing status
  double n = 0; // the evaluation result
  gVarMap = &varMap;
  try {
      info = parse(expression.c_str(), calc[boost::phoenix::ref(n) = _1], space_p);
  } catch (Err &err) {
    throw Err(_WHERE, "Expression evaluation error [%s]", err.Why().c_str());
  }
  if (info.full) return n!=0;
  throw Err(_WHERE, "Expression parsing error");
}
#endif