/*
 *  StrUtils.cpp
 *  DockTicker
 *
 *  Created by Brice Rive on Mon Dec 15 2003.
 *  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
 *
 */

#include "StrUtils.h"
#include <mylib/error.h>
#include <mylib/idioms.h>
using std::string;
using namespace mylib;

void StrUtils::MergeCookies(string &cookies, const string &newCookies)
{
    // parse new cookies
    size_t nameB, nameE, valB, valE, crt=0;
    while ((nameE=newCookies.find("=", crt))!=string::npos) {
        nameB=crt;
        nameE=nameE-1;
        valB=nameE+2;
        valE = newCookies.find(";", valB);
        if (valE==string::npos)
            valE = newCookies.size()-1;
        else
            valE=valE-1;
        string name = newCookies.substr(nameB, nameE-nameB+1);
        string val = newCookies.substr(valB, valE-valB+1);
        if ((nameB=cookies.find(name)) != string::npos) {
            size_t oldValB=cookies.find("=",nameB)+1;
            size_t oldValE = cookies.find(";", oldValB);
            if (oldValE==string::npos)
                oldValE = cookies.size()-1;
            else
                oldValE=oldValE-1;
            cookies.erase(oldValB,oldValE-oldValB+1);
            cookies.insert(oldValB, val);
        } else {
            if (cookies.size()==0)
                cookies = name + "=" + val;
            else
                cookies = cookies + "; " + name + "=" + val;
        }
        crt = valE+3;
    }
}

string StrUtils::GrabString(const string &content, const string &tagB, const string &tagE)
{
    size_t tagBpos=0, tagEpos=content.size();
	if (tagB.size()>0) {
		tagBpos = content.find(tagB);
		if (tagBpos==string::npos)
			throw Err(_WHERE, "Beginning tag not found");
	}
    unsigned int valPos = tagBpos + tagB.size();
	if (tagE.size()>0) {
		tagEpos = content.find(tagE, valPos);
		if (tagEpos==string::npos)
			throw Err(_WHERE, "End tag not found");
	}
    return content.substr(valPos, tagEpos-valPos);
}

// look for cookie declarations and concatenate them in result string
string StrUtils::GrabCookies(const string &header)
{
    string result;
    const string cookieTag = "Set-Cookie: ";
    const string cookieTag2 = "Set-cookie: ";
    size_t cookiePos=0, cookieStart=0, cookieEnd, cookieEol, cookieEqual;
    
    // the headers below are valid, so we need to parse more precisely
    //  Set-Cookie: HttpOnly
    //  Set-Cookie: HttpOnly=;path=;domain=;
    while ((cookiePos = header.find(cookieTag, cookieStart)) != string::npos
           ||(cookiePos = header.find(cookieTag2, cookieStart)) != string::npos) {
        cookieStart = cookiePos + cookieTag.size();
        cookieEol = header.find('\n', cookieStart); // to eol
        cookieEqual = header.find('=', cookieStart);
        cookieEnd = header.find(";", cookiePos);
        if (cookieStart)
            result += "; ";
        if (cookieEqual>=cookieEol)
        {
            result += header.substr(cookieStart, cookieEol-1-cookieStart)+"=";
        }
        else if (cookieEnd>=cookieEol)
        {
            result += header.substr(cookieStart, cookieEol-1-cookieStart);            
        }
        else
        {
            result += header.substr(cookieStart, cookieEnd-cookieStart);            
        }
        cookiePos = cookieEol;
    }
    
    return result;
}

float StrUtils::GetUsAmount(const string &str)
{
    string s = str;
    RemoveString(",", s);
    return from_string<float>(s);
}

void StrUtils::RemoveString(const string &search, string &str)
{
    size_t br;
    while ((br = str.find(search)) != string::npos)
      str.erase(br, search.size());
}

float StrUtils::GetFrAmount(const string &str)
{
    string s = str;
    RemoveString("&nbsp;", s); // remove spaces
    RemoveString(" ", s); // remove whites
    RemoveString("\xa0", s);
    RemoveString("+", s); // remove +
    RemoveString(".", s); // remove .
    RemoveString("&#160;", s); // remove .
    ReplaceString(",", ".", s); // replace ,
    // Fortuneo: 1¬†373,4
    RemoveString("\\n", s);
    RemoveString("\\t", s);
    RemoveString("\xc2", s);
    return from_string<float>(s);
}

void StrUtils::ReplaceString(const string &search, const string &replace, string &str)
{
    size_t br;
    while ((br = str.find(search)) != string::npos)
		str.replace(br, search.size(), replace);
}

string StrUtils::RemoveHtmlSpaces(const string &str)
{
    string s(str);
    ReplaceString("&nbsp;", " ", s);
    ReplaceString("\t", "", s);
    ReplaceString("\n", "", s);
    ReplaceString("\r", "", s);
    ReplaceString("&euro;", "€", s);
    ReplaceString("&Eacute;", "É", s);
    ReplaceString("&eacute;", "é", s);
    ReplaceString("&egrave;", "è", s);
    ReplaceString("\350", "è", s);
    ReplaceString("\351", "é", s);
	ReplaceString("\364", "ô", s);
    ReplaceString("&#039;", "'", s);
    ReplaceString("&#233;", "é", s);
	return s;
}

