/*
 *  ExchangeRate.h
 *  BankReader
 *
 *  Created by Brice Rive on 1/11/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once
#include <map>
#include <vector>
#include <string>
#include "mylib/thread.h"
#include "boost/smart_ptr.hpp"

class ExchangeRate {
	
public:
	float GetRate(const std::string &src, const std::string &dst);
	
	typedef std::vector<std::string> CurrencyList;
	static CurrencyList GetCurrencies();
	
	static std::string CurrencyLabel(const std::string &currency);
	static std::string CurrencySymbol(const std::string &currency);
	
private:
		
	struct Rate {
		float val;
		float expires;
	};
	typedef std::map<std::string, Rate> Rates;
	Rates rates;
	mylib::CriticalSection ratesCriticalSection; // Thread-safe access to rates map

	static const long validSeconds;
	
	void GrabValue(std::string key);
	
	static const int maxNbThreads=2;
	typedef boost::shared_ptr<mylib::Thread> SpThread;
	struct ThreadRecord {
		ThreadRecord(mylib::Thread::ExecuteFun execute, const std::string &key_):thread(execute),key(key_) {}
		mylib::Thread thread;
		std::string key;
	};
	typedef boost::shared_ptr<ThreadRecord> ThreadRecordSp;
	typedef std::vector<ThreadRecordSp> Threads;
	Threads threads;
	static bool ThreadDone(const ExchangeRate::ThreadRecordSp &threadRecord);
	struct ThreadForKey {
		ThreadForKey(const std::string &key_): key(key_) {}
		bool operator()(const ExchangeRate::ThreadRecordSp &threadRecord) {return threadRecord->key==key;}
		const std::string &key;
	};
		
};
