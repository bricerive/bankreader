/*
 *  Server.h - This class is to handle the asynchronous part of grabbing client accounts
 *  BankReader
 *
 *  Created by Brice Rivé on 12/7/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */
#include <string>
#include <vector>
#include <mylib/thread.h>
#include "Bank.h"
#include "CurlPP.h"

class Server {
public:

	Server();
	~Server();

	void TriggerUpdate(const std::string &bankName, const Bank::LoginData &loginData, Bank::ProgressCallback callback);
	bool CheckUpdated(Bank::AccountList &bankAccounts);

	enum Status {Initial, Updated, InternalError, ServerError, ProtocolError, InvalidPasswordError, RejectedPasswordError};
	Status LastUpdateStatus()const {return status;}
	void LastUpdateStatus(Status val) {status=val;}
	
private:

	void UpdateFunc();
	
	Status status;
	bool asyncDoneFlag;

	Bank *myBank;
    mylib::Thread thread;
	CurlPP curl;
	Bank::ProgressCallback progressCallBack;
	
	static bool ctorCalled;
};

