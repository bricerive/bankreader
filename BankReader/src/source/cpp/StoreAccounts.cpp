/*
 *  StoreAccounts.cpp
 *  BankReader
 *
 *  Created by Brice Rive on 4/14/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#include "StoreAccounts.h"
#include "StrUtils.h"
#include <mylib/log.h>
using namespace mylib;

StoreAccounts::StoreAccounts(Bank::AccountList &accounts_, std::string bankId_, std::string currency_, bool swap_)
:accounts(accounts_), bankId(bankId_), currency(currency_), swap(swap_)
{
}

bool StoreAccounts::operator()(const boost::smatch &what) 
{
	std::string accountName(StrUtils::RemoveHtmlSpaces(what[swap?2:1].str()));
	std::string accountNumber(StrUtils::RemoveHtmlSpaces(what[swap?1:2].str()));
	float value = StrUtils::GetFrAmount(what[3].str());
	if (currency=="Dollar")
		value = StrUtils::GetUsAmount(what[3].str());
	LogF(LOG_STATUS, bankId + " " + accountName + " [" + accountNumber + "] = " + std::to_string(value) + "(" + what[3].str() + ")");
	accounts.push_back(Bank::Account(accountName,accountNumber,currency,"Current",value));
	return true;
}
