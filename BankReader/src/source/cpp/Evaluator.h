/*
 *  Evaluator.h
 *  BankReader
 *
 *  Created by Brice Rive on 1/15/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once
#include <string>
#include <map>

class Evaluator {
  
public:
  typedef std::map<std::string, float> VarMap;
  bool Evaluate(const VarMap &varMap, const std::string &expression);
  
private:
    
};

