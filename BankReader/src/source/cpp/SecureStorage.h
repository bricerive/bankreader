#pragma once
/*
 *  SecureStorage.h
 *  MoneyFusionLight
 *
 *  Created by Brice Rivé on 10/5/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#include <string>

class SecureStorage {
public:
	static void StorePassword(const std::string &key, const std::string &pass);
	static const std::string RetreivePassword(const std::string &key);
	static void ForgetPasswords();
};
