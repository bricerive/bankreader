/*!\file CurlPP.h
*  DockTicker
*
*  Created by Brice Rive on Mon Dec 15 2003.
*  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
*
*/
#pragma once
#include "curl/curl.h"
#include <string>
#include <vector>
#include <utility>

extern "C" size_t headerFunction( void *ptr, size_t size, size_t nmemb, void *stream);

class CurlPP {
  
public:
	CurlPP();
	virtual ~CurlPP();
	void ResetCookies();
	void MergeCookies(const std::string &newCookies);
	void GetUrl(const std::string &url, std::string &page, std::string post="");
	void PostUrl(const std::string &url, const std::string &post, std::string &page);
	std::string CurrentUrl()const {return currentUrl;}
	void DisableCertificates();
	void SetSslVersion(int version);

	static std::string HostPart(const std::string &url);

	typedef std::vector< std::pair<std::string, std::string> > Headers;

	//! @name Error identifiers
	//@{
	static const std::string internalServerErrorWhy;
	static const std::string curlErrorWhy;
	static const std::string inaccessibleServerErrorWhy;
	static const std::string unhandledHttpStatusErrorWhy;
	//@}
	
private:
	CurlPP(CurlPP&);
	CURL *easyhandle;
	char curlErrorBuffer[1024];
	std::string cookies;
	std::string currentUrl;
	std::string userAgent;

	Headers crtHeaders;

	unsigned long Perform(const std::string &url, std::string &content);

	bool HasHeader(const std::string &name, std::string &value);
	void ParseHeaderLine(const std::string &headerLine);
	
	void AddMetaHttpHeaders(const std::string &content);
	
	friend size_t headerFunction( void *ptr, size_t size, size_t nmemb, void *stream);
};

