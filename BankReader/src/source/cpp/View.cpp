/*
 *  view.cpp
 *  BankReader
 *
 *  Created by Brice Rive on 1/12/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#include "view.h"
#include <mylib/error.h>
#include <mylib/log.h>
#include <utility>
using namespace mylib;

View::View():showDetails(false), showTotal(false),tickerIdx(0), blinkOn(false), currency("Euro")
{
}

void View::Register(const std::string &name, const ViewStaticMethods &methods)
{
	// Check API version compatibility
	if (ApiVersion((*methods.version)()) != apiVersion)
		throw Err(_WHERE, "Incompatible API version in plugin: %s", name.c_str());
	
	std::pair<const std::string, ViewStaticMethods> insertVal(name, methods);
	// Simply try to insert the plugin in the map
	std::pair<FactoryMap::iterator,bool> insertResult;
	if ((insertResult=TheMap().insert(insertVal)).second) {
		LogF(LOG_STATUS, "View register: %s", name.c_str());
		return;
	}
	// If the plugin is already there, we need to compare the versions
	unsigned long newVersion = MajorMinorVersion((*methods.version)());
	unsigned long oldVersion = MajorMinorVersion((*(insertResult.first->second.version))());
	if (newVersion>oldVersion) { // discard old one
		insertResult.first->second = methods;
		// LogF(LOG_STATUS, "View plugin %s updated to version %08X (was %08X)", name.c_str(), newVersion, oldVersion);
	} else if (newVersion<oldVersion) {
		// silently discard
	} else {
		throw Err(_WHERE, "Duplicate View ID: %s", name.c_str());
	}	
}

View *View::New(const std::string &name, View *oldView)
{
	FactoryMap::iterator i = TheMap().find(name);
	if (i==TheMap().end())
		throw Err(_WHERE, "Unknown View ID: %s", name.c_str());
	View *newView = (*(i->second.factory))();
	if (oldView) {
		newView->ShowDetails(oldView->ShowDetails());
		newView->ShowTotal(oldView->ShowTotal());
		newView->Currency(oldView->Currency());
		newView->BlinkOn(oldView->BlinkOn());
		newView->TickerIdx(oldView->TickerIdx());
	}
	return newView;
}

View::ViewNames View::GetViewNames()
{
	ViewNames viewNames;
	for (FactoryMap::iterator i=TheMap().begin(); i!=TheMap().end(); ++i)
		viewNames.push_back(i->first);
	return viewNames;
}

View::FactoryMap &View::TheMap()
{
	static FactoryMap *theMap=0; // never gets deleted
	if (theMap==0) theMap = new FactoryMap;
	return *theMap;
}

float View::TotalValue(const Accounts &accounts)
{
	float totalValue=0;
	for (Accounts::const_iterator i=accounts.begin(); i!=accounts.end(); ++i)
		totalValue += i->Value();
	return totalValue;
}

static const int nBuckets=128;
static const int bucketSeconds=60*60*6;

void View::ComputeHistogram(const Accounts &accounts, std::vector<std::pair<float, float> > &histo, bool (*test)(const ViewAccount &account))
{
	// zero output histogram
	histo.clear();
	for (int i=0; i<nBuckets; i++)
		histo.push_back(std::make_pair<float, float>(0,0));
	
	// Loop on the accounts
	for (View::Accounts::const_iterator i=accounts.begin(); i!=accounts.end(); ++i) {
		View::ViewAccount account = *i;
		bool accountTest = test? (*test)(account): false;
		
		 // start with a zero at oldest (leftmost) bucket
		std::pair<float, float> accountHistogram[nBuckets]; // histogram for this account
		int lastKnownValueBucket=nBuckets-1;
		float lastKnownValue=0;
		if (accountTest)
			accountHistogram[nBuckets-1-lastKnownValueBucket].second = lastKnownValue;
		else
			accountHistogram[nBuckets-1-lastKnownValueBucket].first = lastKnownValue;
		
		// Loop on the dated values for this account
		const ViewAccount::DatedValues &datedValues = account.GetDatedValues();
		for (ViewAccount::DatedValues::const_iterator i=datedValues.begin(); i!=datedValues.end(); ++i) {
			ViewAccount::DatedValue currentValue = *i;
			unsigned int currentBucket = currentValue.secondsBeforeNow / bucketSeconds; // on what bucket does this value fall
			if (lastKnownValueBucket >= currentBucket) { // if this value is newer than the last we've seen
				for (int i=lastKnownValueBucket; i>currentBucket; i--) { // fill up to this bucket with the previous value
					if (accountTest)
						accountHistogram[nBuckets-1-i].second = lastKnownValue;
					else
						accountHistogram[nBuckets-1-i].first = lastKnownValue;
				}
				// update last know with current
				lastKnownValueBucket = currentBucket;
				lastKnownValue = currentValue.value;
			}
		}
		for (int i=lastKnownValueBucket; i>=0; i--) {
			if (accountTest)
				accountHistogram[nBuckets-1-i].second = lastKnownValue;
			else
				accountHistogram[nBuckets-1-i].first = lastKnownValue;
		}
		for (int i=0; i<nBuckets; i++) {
			histo[i].first += accountHistogram[i].first;
			histo[i].second += accountHistogram[i].second;
		}
	}
}
