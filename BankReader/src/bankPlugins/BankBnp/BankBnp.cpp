/*
 *  BankBnp.cpp
 *  BankReader
 *
 *  Created by Brice Rive on 1/13/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#include "BankBnp.h"
#include "DigitGrid.h"
using std::string;
#include "CurlPP.h"
#include <boost/regex.hpp>

BankBnp::Registrar BankBnp::registrar;

static string GridClicks(const string &grid, const string &password)
{
	// Access our digits images
	extern char BankBnpDigits3[];
	extern int BankBnpDigits3NBytes;

	// Find the location of the numbers in the grid
	std::vector<int> layout;
    try {
		DigitGrid::Process(grid, BankBnpDigits3, BankBnpDigits3NBytes, layout);
	} catch (Err &err) {
		throw Err(_WHERE, Bank::protocolErrorWhy);
	}
    
	// Find the clicks for the password
	int javaScriptCoords[] = {
        5,5,27,26,     32,5,54,26,     59,5,81,26,     86,5,108,26,     113,5,135,26,
        5,32,27,53,     32,32,54,53,     59,32,81,53,     86,32,108,53,     113,32,135,53,
        1,59,27,80,     32,59,54,80,     59,59,81,80,     86,59,105,80,     113,59,135,80,   
        5,86,27,107,     32,86,54,107,     59,86,81,107,     86,86,108,107,     113,86,135,107,
        5,113,27,134,     32,113,54,134,     59,113,81,134,     86,113,108,134,     113,113,135,134,
	};
    
	string gridClicks;
	for (int i=0; i<6; i++) {
		int digit = password[i]-'0';
		int x = layout[digit*4];
		int y = layout[digit*4+1];
		for (int j=0; j<sizeof(javaScriptCoords)/sizeof(javaScriptCoords[0]); j+=4) {
			if (x>=javaScriptCoords[j] && x<=javaScriptCoords[j+2] && y>=javaScriptCoords[j+1] && y<=javaScriptCoords[j+3]) {
				int index = j/4 + 1;
				gridClicks += to_string((index<10)?"0":"")+to_string(index);
                break;
			}
		}
	}
	return gridClicks;
}

void BankBnp::GrabAccounts(ProgressCallback &progressCallback)
{
	progressCallback(0);
	// Check the password for conformance
	std::string password = loginData[PASSWORD];
	CheckNumeric(password, 6);
	
	// Get entry page
	GetUrlIntoContent("https://www.secure.bnpparibas.net/banque/portail/particulier/HomeConnexion?type=homeconnex");
	progressCallback(10);
	
	// Load the numeric grid image
	string grid;
	GetUrlIntoString("https://www.secure.bnpparibas.net/NSImgGrille", grid);
	progressCallback(40);

	// Get the digit clicks string
	string gridClicks = GridClicks(grid, password);
	progressCallback(60);
	
	// Get the timeStamp
	string time = GrabInContent("<input type=\"hidden\" name=\"time\" value=\"(.*?)\"");

	// Post login
	string postUrl("https://www.secure.bnpparibas.net");
	postUrl += GrabInContent("<form.*name=\"logincanalnet\".*action=\"(.*?)\"");
	PostIntoContent(postUrl,
				 "time="+time+
				 "&act=actionParValidationEntree"
				 "&outil=IdentificationGraphique"
				 "&etape=1"
				 "&bouton=espece"
				 "&ch5="+gridClicks+
				 "&ch1="+loginData[LOGIN]+"&x=25&y=5");
	progressCallback(70);
		
	// Check for password error
	if (SearchInContent("NSError")) throw Err(_WHERE, rejectedPasswordErrorWhy);
	if (SearchInContent("Veuillez renouveler votre saisie")) throw Err(_WHERE, rejectedPasswordErrorWhy);

	// Get javascript forward
	GetUrlIntoContent(GrabInContent("document.location.replace\\(\"(.*?)\"\\);"));
	progressCallback(80);

	// Check for password error
	if (SearchInContent("NSError")) throw Err(_WHERE, rejectedPasswordErrorWhy);
	if (SearchInContent("Veuillez renouveler votre saisie")) throw Err(_WHERE, rejectedPasswordErrorWhy);

	// Grab accounts    
//  	const char *expression = "headers=\"Libelle_compte\">.*?>(.*?)<.*?"
//    "headers=\"Numero_compte\">(.*?)<.*?"
//    "headers=\"Solde\">.*?>(.*?)<";
//	GrabAccountsInContent(expression, "Euro");

    // grab the table with all the accounts
    string tableComptes = GrabInContent("class=\"tableCompte(.*?)/table>");

    // Loop over the table rows
    boost::sregex_iterator tr1(tableComptes.begin(), tableComptes.end(), boost::regex("(<tr.*?</tr>)"));
    boost::sregex_iterator tr2;
    for (boost::sregex_iterator trI = tr1; trI!=tr2; ++trI)
    {
        string tr = (*trI)[0];
//        int nbAccounts = accounts.size();
        GrabAccountsInString("<a.*?>(.*?)<.*?class=\"libelleCompte\">(.*?)<.*?<a.*?>(.*?)<", "Euro", false, tr);
//        if (nbAccounts == accounts.size())
//            GrabAccountsInString("<td.*?>(.*?)</td>.*?<td.*?>(.*?)</td>.*?<strong>(.*?)</strong>", "Euro", false, tr);
    }
    if (accounts.empty()) throw Err(_WHERE, protocolErrorWhy);	
	progressCallback(100);

}
