/*
 *  BankBnp.h
 *  MoneyFusionLight
 *
 *  Created by Brice Rive on 4/27/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once
#include "Bank.h"
extern const unsigned char BankBnpIcon[];

class BankBnp: public Bank {
	
	BankBnp(CurlPP &curl_, const LoginData &loginData_):Bank(curl_,loginData_){}
	
	// Bank virtual interface
	virtual void GrabAccounts(ProgressCallback &progressCallback);
	virtual std::string ShortName() const { return StaticShortName(); }

	// Bank factory registration
	static void LoginInfo(LoginInfo &loginInfo) {loginInfo.push_back(LOGIN); loginInfo.push_back(PASSWORD);}
	BANK_FACTORY_REGISTRATION(BankBnp, "BNP Parisbas", 1,0, "https://www.secure.bnpparibas.net/banque/portail/particulier/HomeConnexion?type=homeconnex")
};
