/*
 *  BankBrd.cpp
 *  BankReader
 *
 *  Created by Brice Rivé on 12/29/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#include "BankBrd.h"
#include "CurlPP.h"
using std::string;

BankBrd::Registrar BankBrd::registrar;

void BankBrd::GrabAccounts(ProgressCallback &progressCallback)
{
	progressCallback(0);
	// Post the login
	PostIntoContent("https://www.bred.fr/authentification/servlet/GestionAuthentificationServlet",
				 "typeDemande=ID&siteestB2P=false&id=" +loginData[LOGIN]+ "&pass=" +loginData[PASSWORD]);
	progressCallback(25);
	
	// Check for password error
	if (SearchInContent("ressaisir votre identifiant"))	throw Err(_WHERE, rejectedPasswordErrorWhy);
	
	// Post the consultation selection
	PostIntoContent("https://www.bred.fr/authentification/servlet/GestionAuthentificationServlet",
				 "typeDemande=RAH&site=bred&nom_application=compte_consultation&CaseACocher=&MessageEnCours=MESSPREP&NbPageLu=1");
	progressCallback(50);

	// Get consultation frame
	GetUrlIntoContent("https://www.bred.fr/homebanking/servlet/ApplicationWrapperServlet?nom_application=compte_consultation");
	progressCallback(75);
	
	// Cleanup characters
	size_t br=0;
	while ((br = content.find("\340", br)) != string::npos)
		content.replace(br, 1, "à");
	br=0;
	while ((br = content.find("\351", br)) != string::npos)
		content.replace(br, 1, "é");

	// Grab accounts
	const char *expression = "reloadApplication.'nom_application'.*?><.*?>(.*?)<.*?'numero_compte', '(.*?)'.*?CLASS=\"txt.*?\">.*?CLASS=\"txt.*?\">(.*?) ";
	GrabAccountsInContent(expression, "Euro");
	if (accounts.empty()) throw Err(_WHERE, protocolErrorWhy);	
	progressCallback(100);
}
