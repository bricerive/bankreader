/*
 *  BankCco.cpp
 *  BankReader
 *
 *  Created by Brice Rivé on 12/26/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#include "BankCco.h"
#include "CurlPP.h"
using std::string;

BankCco::Registrar BankCco::registrar;

void BankCco::GrabAccounts(ProgressCallback &progressCallback)
{
	progressCallback(0);
	std::string login = loginData[LOGIN];
	std::string password = loginData[PASSWORD];
	
	// Post the login
	PostIntoContent("https://www.cortalconsors.fr/euroWebFr/-",
			 "userId=" +login+ "&nip=" +password+ "&$$event_login=" "&realm=" "&$part=Home.Desks.Welcome.login");
	progressCallback(25);
	
	// Check for password error
	if (SearchInContent("<strong>Erreur</strong>"))	throw Err(_WHERE, rejectedPasswordErrorWhy);
	
	// Get "Tableau de Bord"
	GetUrlIntoContent("https://www.cortalconsors.fr/euroWebFr/-?$part=MonalisaFR.Desks.consultingAndTrading.Desks.CustAccEval");
	progressCallback(50);
	// Get "Gesstion de comptes"
	GetUrlIntoContent("https://www.cortalconsors.fr/euroWebFr/-?$part=MonalisaFR.Desks.consultingAndTrading.Desks.CustAccEval.menubar.menubarNavTwo&select=accountmanagement");
	progressCallback(75);

	const char *expression = "<tr class=\".*?-row\".*?&nbsp;&nbsp;[[:space:]]*(.*?)&nbsp;&nbsp;(.*?)&nbsp;&nbsp;.*?<td class=\"numeric\">[[:space:]]*(.*?)[[:space:]]*</td>";
	GrabAccountsInContent(expression, "Euro");
	if (accounts.empty()) throw Err(_WHERE, protocolErrorWhy);	
	progressCallback(100);
}

