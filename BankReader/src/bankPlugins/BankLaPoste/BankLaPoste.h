/*
 *  BankLaPoste.h
 *  BankReader
 *
 *  Created by Brice Rive on 1/13/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once
#include "Bank.h"
extern const unsigned char BankLaPosteIcon[];

class BankLaPoste: public Bank {
	
	BankLaPoste(CurlPP &curl, const LoginData &loginData):Bank(curl,loginData){}
	
	// Bank virtual interface
	virtual void GrabAccounts(ProgressCallback &progressCallback);
	virtual std::string ShortName() const { return StaticShortName(); }

	// Bank factory registration
	static void LoginInfo(LoginInfo &loginInfo) {loginInfo.push_back(LOGIN); loginInfo.push_back(PASSWORD);}
	BANK_FACTORY_REGISTRATION(BankLaPoste, "La Poste", 1,0, "https://www.labanquepostale.fr/index.html")
};
