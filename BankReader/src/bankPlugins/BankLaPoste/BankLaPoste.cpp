/*
 *  BankLaPoste.cpp
 *  BankReader
 *
 *  Created by Brice Rive on 1/13/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#include "BankLaPoste.h"
using std::string;

BankLaPoste::Registrar BankLaPoste::registrar;

void BankLaPoste::GrabAccounts(ProgressCallback &progressCallback)
{
	progressCallback(0);
	// Get entry page
	GetUrlIntoContent("https://www.videoposte.com/statique/index.html");
	progressCallback(20);
	
	// Post the login info
	PostIntoContent("https://www.videoposte.com/videoposte/frame/f_contenu/index.html",
					"ID="+loginData[LOGIN]+
					"&PWD="+loginData[PASSWORD]+
					"&ORIGIN=portail");
	progressCallback(40);

	// Check for password error
	if (SearchInContent("Identifiant ou mot de passe incorrect"))	throw Err(_WHERE, rejectedPasswordErrorWhy);

	// Get the summary frame
	GetUrlIntoContent("https://www.videoposte.com/videoposte/frame/f_contenu/f_releve.html?");
	progressCallback(60);
	GetUrlIntoContent("https://www.videoposte.com/videoposte/releve/liste_comptes.html");
	progressCallback(80);
		
	GrabAccountsInContent("lienlibelle.*?\">(.*?)</a>.*?\"donnes\">(.*?)</span>.*?\"donnes.*?\">(.*?)</span>", "Euro");
	if (accounts.empty()) throw Err(_WHERE, protocolErrorWhy);	
	progressCallback(100);
}
