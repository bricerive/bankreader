/*
 *  BankRandom.h
 *  BankReader
 *
 *  Created by Brice Rive on 1/18/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once
#include "Bank.h"
extern const unsigned char BankRandomIcon[];

#ifdef _DEBUG

class BankRandom: public Bank {
	
	BankRandom(CurlPP &curl_, const LoginData &loginData_):Bank(curl_,loginData_){}
	
	// Bank virtual interface
	virtual void GrabAccounts(ProgressCallback &progressCallback);
	virtual std::string ShortName() const { return StaticShortName(); }

	// Bank factory registration
	static void LoginInfo(LoginInfo &) {}
	BANK_FACTORY_REGISTRATION(BankRandom, "_RandomBank", 1,0, "http://localhost");
};

#endif
