/*
 *  BankRandom.cpp
 *  BankReader
 *
 *  Created by Brice Rive on 1/18/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#ifdef _DEBUG

#include "BankRandom.h"
using std::vector;
using std::string;

BankRandom::Registrar BankRandom::registrar;

void BankRandom::GrabAccounts(ProgressCallback &progressCallback)
{
	progressCallback(0);
	static const char * const fakeAccounts[] = {"RandomAccount0","RandomAccount1","RandomAccount2","RandomAccount3"};
	static const unsigned int nbFakeAccounts = sizeof(fakeAccounts)/sizeof(fakeAccounts[0]);
	
	accounts.clear();
	for (int i=0; i<nbFakeAccounts; ++i) {
		Account account(fakeAccounts[i], "00000", "Euro", (std::rand()%2)?"Savings":"Current", std::rand()%10000);
		accounts.push_back(account);						
	}
	progressCallback(100);
}

#endif
