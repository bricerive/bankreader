/*
 *  BankVirtual.cpp
 *  BankReader
 *
 *  Created by Brice Rivé on 1/30/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include "BankVirtual.h"
using std::string;

BankVirtual::Registrar BankVirtual::registrar;

void BankVirtual::GrabAccounts(ProgressCallback &progressCallback)
{
	progressCallback(0);
	std::string amount = loginData[AMOUNT];
	accounts.push_back(Bank::Account("","",loginData[CURRENCY],"Savings",from_string<float>(amount)));
	progressCallback(100);
}
