/*
 *  BankVirtual.h
 *  BankReader
 *
 *  Created by Brice Rivé on 1/30/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once
#include "Bank.h"
extern const unsigned char BankVirtualIcon[];

class BankVirtual: public Bank {
	
	BankVirtual(CurlPP &curl_, const LoginData &loginData_):Bank(curl_,loginData_){}
	
	// Bank virtual interface
	virtual void GrabAccounts(ProgressCallback &progressCallback);
	virtual std::string ShortName() const { return StaticShortName(); }
	
	// Bank factory registration
	static void LoginInfo(LoginInfo &loginInfo) {loginInfo.push_back(AMOUNT); loginInfo.push_back(CURRENCY);}
	BANK_FACTORY_REGISTRATION(BankVirtual, "Virtual", 1,0, "")
};
