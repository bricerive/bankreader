/*
 *  BankSog.h
 *  BankReader
 *
 *  Created by Brice Rive on 1/13/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

// Note: As the name contains accentuated chars, this source file's character encoding is important.
// It should be set as UTF8

#pragma once
#include "Bank.h"
extern const unsigned char BankSogIcon[];

class BankSog: public Bank {
	
	BankSog(CurlPP &curl_, const LoginData &loginData_):Bank(curl_,loginData_){}
	
	// Bank virtual interface
	virtual void GrabAccounts(ProgressCallback &progressCallback);
	virtual std::string ShortName() const { return StaticShortName(); }
	
	// Bank factory registration
	static void LoginInfo(LoginInfo &loginInfo) {loginInfo.push_back(LOGIN); loginInfo.push_back(PASSWORD);}
	BANK_FACTORY_REGISTRATION(BankSog, "Société Générale", 1,0, "https://particuliers.societegenerale.fr/")
};
