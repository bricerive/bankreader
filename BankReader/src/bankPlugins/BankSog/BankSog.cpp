/*
 *  BankSog.cpp
 *  BankReader
 *
 *  Created by Brice Rive on 1/13/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#include "BankSog.h"
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include "DigitGrid.h"
#include "CurlPP.h"
using std::string;

BankSog::Registrar BankSog::registrar;
extern char BankSogDigits1[];
extern int BankSogDigits1NBytes;
extern char BankSogDigits2[];
extern int BankSogDigits2NBytes;

void BankSog::GrabAccounts(ProgressCallback &progressCallback)
{
    progressCallback(0);

    // Access login info
    std::string login = loginData[LOGIN];
	std::string password = loginData[PASSWORD];    
	CheckNumeric(password, 6); // Check the password for conformance
    

    // Open main page
	GetUrlIntoContent("https://particuliers.societegenerale.fr/");
	progressCallback(10);

    // Add-in the cookies set by JavaScript
    curl.MergeCookies(to_string("acces=NN; type=NN"));

    // Call server to get cryptogramme and size of the virtual keyboard.
    // This is needed later on to construct an URL to the server-generated image, and to send the secret code in encoded form.
    // It fetches a JASON variable like this:
    //    _vkCallback({ 'code': 0
    //                , 'crypto': 'aYaizQ8V0Dbg9oDpN_ro1uHPUykWrLP_UVNaXh9_a5Th8WZlkqUh2WeTGdc8j9RbnORBCM6CXRkGZaVmyeKGe9CojohQc5PcHPxN2oiPpNGEhJUvrkyQE9Nks2OR2iGAPp0rqyWgM8yBG7DKXAAFvtHp2S-pcrKB1kwzl1hwregsI0VKAAAAgKgsiV1ZsxJDwB1zqOEow-WnUoXVGIkgZceCbM4peHAzce8Z0yQ43y-x2yRQ_IPimeGF2J6tbj-YwcGerF-LpZvRzZ1noizBiCYv8Xe-1Xp_q1_JUcI3ohRJrbzwrOEGzmVwYCPyYm_CefchFLZSJ77d99CuefVxVQGWWFs3qd7xAAAAEMwVR6Hx9o3cLecKiWzp1qkJ1SVCzrJAKixcSrYebGB-80DACI-rBGNQtdbVETDukm7rJhhd7Mw7Ku9o5-lo5CxXnJoD6kLnPM_F8CwzWPwU52tS3_U5WR6rwm6aiBrCMJEW9E9-R7R-lFRAfVZon3PJg9LZx-SjPWDCUcpM8fytsRxd9uhxU_rX3d4nOQxw8v9M5X-yKKhBRALfsCt47_PhP536Qys-e4i8AA6jEefVcGFpbQc7WUsSqAMC8s4ek6kGvMoGqONnPCMPhHT4Kz8='
    //                , 'nbrows': 4
    //                , 'nbcols': 4
    //                , 'grid': [0,12,71,0,0,0,27,9,10,0,133,249,103,98,145,0,0,22,41,0,0,0,55,16,237,0,131,190,107,123,141,0,0,225,167,0,0,0,110,183,16,0,31,230,73,104,18,0,0,242,140,0,0,0,51,11,2,0,197,184,100,14,236,0,0,188,126,0,0,0,212,185,223,0,85,144,95,232,127,0,0,53,56,0,0,0,35,113,129,0,168,161,191,245,244,0]
    //              });
    string jason;
    //	GetUrlIntoString("https://particuliers.secure.societegenerale.fr/cvcsgenclavier?mode=json&estSession=0", jason);
	GetUrlIntoString("https://particuliers.secure.societegenerale.fr//sec/vk/gen_crypto?estSession=0", jason);
    
    
    
    // Extract the cryptogram from the server's JASON
	string cryptogramme_js;
	try {
		cryptogramme_js = GrabInString("\'crypto\': \'(.*?)\'", jason);
	} catch (Err &err) {
		throw Err(_WHERE, protocolErrorWhy);
	}
	progressCallback(20);
    
    // Load the numeric grid image
	string grid;
    //string gridUrl = "https://particuliers.secure.societegenerale.fr/cvcsgenimage?modeClavier=0&cryptogramme="+ cryptogramme_js;
    string gridUrl = "https://particuliers.secure.societegenerale.fr//sec/vk/gen_ui?modeClavier=0&cryptogramme="+ cryptogramme_js;
    
	GetUrlIntoString(gridUrl, grid);
	progressCallback(30);

    // Find the location of the numbers in the grid
	std::vector<int> layout;
	try {
		unsigned int delta = DigitGrid::Process(grid, BankSogDigits1, BankSogDigits1NBytes, layout);
		progressCallback(40);
		std::vector<int> secondLayout;
		if (delta > DigitGrid::Process(grid, BankSogDigits2, BankSogDigits2NBytes, secondLayout))
			layout = secondLayout;
		progressCallback(50);
	} catch (Err &err) {
		throw Err(_WHERE, protocolErrorWhy);
	}
    
    // Find the clicks for the password
	struct Box {
		int left, top, width, height;
		int Right() {return left+width-1;}
		int Bottom() {return top+height-1;}
	};
	Box boxes[] = {
        {0, 0, 24, 23},{24, 0, 24, 23},{48, 0, 24, 23},{72, 0, 24, 23},
		{0, 23, 24, 23},{24, 23, 24, 23},{48, 23, 24, 23},{72, 23, 24, 23},
		{0, 46, 24, 23},{24, 46, 24, 23},{48, 46, 24, 23},{72, 46, 24, 23},
		{0, 69, 24, 23},{24, 69, 24, 23},{48, 69, 24, 23},{72, 69, 24, 23}
	};
	std::vector<int> gridClicks;
	for (int i=0; i<6; i++) {
		int digit = password[i]-'0';
		int x = layout[digit*4];
		int y = layout[digit*4+1];
		for (int j=0; j<sizeof(boxes)/sizeof(boxes[0]); j++) {
			if (x>boxes[j].left && x<boxes[j].Right() && y>boxes[j].top && y<boxes[j].Bottom()) {
				gridClicks.push_back(j);
			}
		}
	}
    
    // Extract the grid code from the server's JASON
 	string grilles_js;
	try {
        grilles_js = GrabInString("\'grid\': \\[(.*?)\\]", jason);
	} catch (Err &err) {
		throw Err(_WHERE, protocolErrorWhy);
	}
	std::vector<string> encodes;
	boost::algorithm::split(encodes, grilles_js, boost::algorithm::is_any_of(","));
	int ndata = encodes.size()/password.size();
    
    //
	// Convert the click positions into special code
	//  	cvcs_codsec += codecase[((nbchar * ndata) + index)] ;
	// for example, codecase is something like that:
	//	0	0	56	216	62	250	0	133	247	197	28	0	100	42	0	0
	//	0	0	124	208	171	49	0	99	149	191	90	0	210	172	0	0
	//	0	0	206	110	73	201	0	108	68	112	148	0	219	134	0	0
	//	0	0	113	192	151	188	0	162	103	33	25	0	157	153	0	0
	//	0	0	51	89	82	226	0	153	27	109	131	0	93	100	0	0
	//	0	0	27	85	227	160	0	135	133	153	209	0	95	155	0	0
    // It contains 6 times 16 codes.
    // 6 is the password length
    // 16 is the grid size
    // 0 are for empty grid cells
	string passcode;
	for (int i=0; i<gridClicks.size(); ++i) {
		if (i>0) passcode+="%2C";
		passcode += encodes[i*ndata+gridClicks[i]];
	}
    
    // POST the login
    //<form id="n2g_authentification" name="authentification" method="post" action="https://particuliers.secure.societegenerale.fr//acces/authlgn.html" onsubmit="return false;">
    //    <input type="text" id="codcli" name="codcli" value="" size="20" maxlength="8" class="form_connexion_home_zone" title="saisissez votre code client" autocomplete="off" />
    //    <input type="button" name="button" id="button" value="OK" title="Activation du clavier virtuel graphique" class="form_connexion_home_btn_ok"
    //        onclick='if (
    //        n2g_ctrlSaisie(document.forms["n2g_authentification"].codcli, "Vous devez saisir votre code client.","La saisie de votre code client est incorrecte.\n Pour en savoir plus, cliquez sur le ? à côté du code client.","",false,n2g_estNumString,8))
    //        {vk.show();}' />
    //    <input type="text" id="tc_visu_saisie" value="" readonly size="6">
    //    <input id="versionCla" type="hidden" name="versionCla" value="">
    //    <input id="cryptocvcs" type="hidden" name="cryptocvcs" value="">
    //    <input id="cvcs_visu_cache" type="hidden" value="">
    //    <input type="hidden" name="codsec" id="codsec" value="">
    //    <input type="hidden" name="categNav" value="W3C">
    //    <script type="text/javascript">
    //        function VKvalidate() { 
    //            document.getElementById('versionCla').value = vk.getKeyboardMode();
    //            document.forms["authentification"].submit();
    //        } 
    //        vk = new VirtualKeyboard2(false, 'codsec', { validatefn: VKvalidate, urlBase: 'https://particuliers.secure.societegenerale.fr/', zones: [ {x:0, y:0, height:500} ] });    
    //    </script>
    //</form>

    /*
     POST //acces/authlgn.html HTTP/1.1
     Referer: https://particuliers.societegenerale.fr/
     Cookie: xtvrn=$412253$; GDASESSID=xXpryFS9UBpmrA+9LJEZsrEc2bY=; SERVERID=server141; vk_mode=0
     Content-Type: application/x-www-form-urlencoded
     Content-Length: 757
     codcli=38534322
         &user_id=38534322
         &versionCla=0
         &categNav=W3C
         &codsec=68%2C175%2C216%2C64%2C207%2C10
         &cryptocvcs=WAEAAOvzZlAAAAAAAAAAgIgqlNngerDRHyKDxgQ2vtvd9ngf4oLeog6BclKvwJxbK7WIfzCx5OHS2YFU6GDxHKYPzlN7W6kpbpTtVwy3L27WJFkV-JAyvB7d2DwerTI0kDVE8iFxD9aQLGjZpzWvfwRFI_UMKkvz_YqE5UQkM70F9ftS9dQBhexyjLvPb7tfAAAACAFzpYjaqRE6i3sM2_bntJNirAgl7Bf6J-RBwDQ3oHrxVYm0dZvGn8x5NcjXy4O51hcfoBExai5z-00YNQis2dJyiza-1Zl8JlihaYSszuu8B83ZnsbRt0x8NawLu_hn0NoCQRLZ2-CHLrr6s-niENW9FmxxG2Z0AKC-xYBJ6FrhkSoXMS4Tl-0Ix5gXd8sAO6GeTX6R9nZMT5KyoJUUBC2cA05HGJXPRcMgWFfAO7qQiKXCe9uejkXIo1OdnR12t8qhWOGaXW7uGPpg6wofRZ6jq8Tz_e8tjp0atDp96rVk5YQW-chz6aYYCOsgFRdbDS-Bsp4sqbUWvc1j9hFluBldY6BURzlVvv6f1WZFS9W5cYEXB2XVDO2pugkUKhpI4UeyaTYflghlsouyhyZBRyvR5mGWmDQhmL0IojqIXW3fwvQd6vhfQdI%3D
         &vk_op=auth
     */
    PostIntoContent("https://particuliers.secure.societegenerale.fr//acces/authlgn.html",
                    "codcli="+login+
                    "&user_id=" +login+
                    "&versionCla=0"
                    "&cryptocvcs=" +cryptogramme_js+
                    "&codsec=" +passcode+
                    "&categNav=W3C"
                    "&vk_op=auth"
                    );

	// Check for password error
	if (SearchInContent("saisies sont incorrectes")) throw Err(_WHERE, rejectedPasswordErrorWhy);
	progressCallback(60);
    
	// Cleanup accents
	size_t br=0;
	while ((br = content.find("\351", br)) != string::npos)
		content.replace(br, 1, "é");
	br=0;
	while ((br = content.find("\350", br)) != string::npos)
		content.replace(br, 1, "è");
    
    // Now grab the accounts
	const char *expression = "LGNTableRow.*?class=\"TypeCompte\" >(.*?)<.*?NumeroCompte.*?>(.*?)<.*?class=\"Solde\".*?>(.*?)<";    
	GrabAccountsInContent(expression, "Euro");
	if (accounts.empty()) throw Err(_WHERE, protocolErrorWhy);
	progressCallback(100);
}
