/*
 *  BankSim.h
 *  BankReader
 *
 *  Created by Brice Rive on 1/16/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once
#include "Bank.h"
extern const unsigned char BankSimIcon[];

#ifdef _DEBUG

class BankSim: public Bank {

	BankSim(CurlPP &curl_, const LoginData &loginData_):Bank(curl_,loginData_){}

	// Bank virtual interface
	virtual void GrabAccounts(ProgressCallback &progressCallback);
	virtual std::string ShortName() const { return StaticShortName(); }

	// Bank factory registration
	static void LoginInfo(LoginInfo &) {}
	BANK_FACTORY_REGISTRATION(BankSim, "_SimBank", 1,0, "http://localhost");
};

#endif
