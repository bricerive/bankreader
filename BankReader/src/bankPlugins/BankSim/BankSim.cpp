/*
 *  BankSim.cpp
 *  BankReader
 *
 *  Created by Brice Rive on 1/16/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#ifdef _DEBUG

#include "BankSim.h"
#include "StrUtils.h"
#include "CurlPP.h"

using std::string;

BankSim::Registrar BankSim::registrar;

void BankSim::GrabAccounts(ProgressCallback &progressCallback)
{
	progressCallback(0);
	// Open main page
	GetUrlIntoContent("http://localhost/~brice/bankSim.html");
	GrabAccountsInContent("account: ([^[:blank:]]*?) ([^[:blank:]]*?) ([^[:blank:]]*?) Euro end", "Euro");
	GrabAccountsInContent("account: ([^[:blank:]]*?) ([^[:blank:]]*?) ([^[:blank:]]*?) Dollar end", "Dollar");
	if (accounts.empty()) throw Err(_WHERE, protocolErrorWhy);	
	progressCallback(100);
}

#endif
