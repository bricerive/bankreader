/*
 *  ING.cpp
 *  BankReader
 *
 *  Created by Brice Rive on 1/13/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#include "BankIngDirect.h"
#include "CurlPP.h"
using std::string;

BankIngDirect::Registrar BankIngDirect::registrar;

void BankIngDirect::GrabAccounts(ProgressCallback &progressCallback)
{
	progressCallback(0);
	std::string login = loginData[LOGIN];
	std::string password = loginData[PASSWORD];
	std::string dateOfBirth = loginData[DATEOFBIRTH];
	
	// Get login page
	GetUrlIntoContent("https://www.ingdirect.fr/secure/general?command=displayLogin");
	progressCallback(20);
	
	string jour = dateOfBirth.substr(0,2);
	string mois = dateOfBirth.substr(3,2);
	string annee = dateOfBirth.substr(6,4);
	// Post the login info
	PostIntoContent("https://www.ingdirect.fr/secure/general",
				 "command=login"
				 "&locale=fr_FR"
				 "&device=web"
				 "&logdatelogin=1"
				 "&ACN="+login+
				 "&JOUR="+jour+
				 "&MOIS="+mois+
				 "&ANNEE="+annee);
	progressCallback(40);
		
	// Post the login info
	PostIntoContent("https://www.ingdirect.fr/secure/general",
					"command=login"
					"&locale=fr_FR"
					"&device=web"
					"&digitvalide=1;2;3;4"
					"&PIN1=" );
	progressCallback(40);

	// Check for password error
	if (SearchInContent("Merci de bien vouloir les saisir à nouveau"))	throw Err(_WHERE, rejectedPasswordErrorWhy);
	
	// Get the account summary page
	GetUrlIntoContent("https://www.ingdirect.fr/secure/general?command=alertMessage");
	progressCallback(60);
	GetUrlIntoContent("https://www.ingdirect.fr/secure/general?command=displayTRAccountSummary");
	progressCallback(80);

	const char *expression = "BgdTabGri.*?Bleu11b\">(.*?)</td>.*?Bleu11\">(.*?)</a>.*?Bleu11\">(.*?)</a>";
	GrabAccountsInContent(expression, "Euro");
	if (accounts.empty()) throw Err(_WHERE, protocolErrorWhy);	
	progressCallback(100);
}
