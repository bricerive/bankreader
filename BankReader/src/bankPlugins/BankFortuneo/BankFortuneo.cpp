/*
 *  BankFortuneo.cpp
 *  BankReader
 *
 *  Created by Brice Rivé on 3/17/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include "BankFortuneo.h"
#include "CurlPP.h"
using std::string;

BankFortuneo::Registrar BankFortuneo::registrar;

void BankFortuneo::GrabAccounts(ProgressCallback &progressCallback)
{
	string login = loginData[LOGIN];
	string password = loginData[PASSWORD];
	
    curl.SetSslVersion(3);

    progressCallback(0);
	
	// Login page (to get the cookies)
	GetUrlIntoContent("https://www.fortuneo.fr/fr/identification.jsp");
	progressCallback(10);
        
	// Post the login
    PostIntoContent("https://www.fortuneo.fr/checkacces"
                    , "locale=fr"
                    "&login=" +login+
                    "&passwd=" +password+
                    "&idDyn=false");
	progressCallback(20);    
    
	// Check for password error
	if (SearchInContent("Votre mot de passe et/ou votre identifiant"))	throw Err(_WHERE, rejectedPasswordErrorWhy);
	progressCallback(30);
    
    // Get synthese main page
    GetUrlIntoContent("https://www.fortuneo.fr/fr/prive/mes-comptes/synthese-globale/synthese-mes-comptes.jsp");
	progressCallback(40);
    
    // Grab AJAX request info
    /*
     <div id="as_syntheseGlobalComptes.do_">
     
     <br>
     <span class="loading"><img src="/datas/images/loading.gif" alt="loading" /></span>
     <input type="hidden" id="asynchhttp:__fortuneobankone.GICM.NET:8443_fortuneobankone_syntheseGlobalComptes.do" value="as_syntheseGlobalComptes.do_" name="6fde8c3331997782bd1ce1eb90468404">
     </div>
     */
    string ajaxDiv = GrabInContent("<div id=\"as_syntheseGlobalComptes.do_\">(.*?)</div>");
    string value = GrabInString("value=\"(.*?)\"", ajaxDiv);
    string key = GrabInString("name=\"(.*?)\"", ajaxDiv);
    string ajaxUrl=string("https://www.fortuneo.fr/AsynchAjax?key0=") + key + "&div0=" + value + "&time=0";
	progressCallback(50);

    // Get AJAX div content
    do 
    {
        GetUrlIntoContent(ajaxUrl);
        
    } while (!SearchInContent("COMPTE_ACTIF"));
	progressCallback(60);
    
    /*
     <td nowrap=\"nowrap\">
     <a href=\"/fr/prive/mes-comptes/compte-titres-pea/situation/portefeuille-temps-reel.jsp?COMPTE_ACTIF=0B4KU9ZJL8\">014804539101<\/a>
     <\/td>
     <td>
     <a href=\"/fr/prive/mes-comptes/compte-titres-pea/situation/portefeuille-temps-reel.jsp?COMPTE_ACTIF=0B4KU9ZJL8\">Compte titres MR RIVE BRICE<\/a>
     <\/td>
     <td class=\"numb\" nowrap=\"nowrap\">
     1¬†373,4
     EUR
     <\/td>
     */

    const char *expression = "COMPTE_ACTIF=.*?\\\">(.*?)<.*?COMPTE_ACTIF=.*?\\\">(.*?)<.*?<td.*?>(.*?)<";
	GrabAccountsInContent(expression, "Euro");
	if (accounts.empty()) throw Err(_WHERE, protocolErrorWhy);
    progressCallback(100);
}

