/*
 *  BankFortuneo.h
 *  BankReader
 *
 *  Created by Brice Rivé on 3/17/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once
#include "Bank.h"
extern const unsigned char BankFortuneoIcon[];

class BankFortuneo: public Bank {
	
	BankFortuneo(CurlPP &curl_, const LoginData &loginData_) :Bank(curl_,loginData_) {}
	
	// Bank virtual interface
	virtual void GrabAccounts(ProgressCallback &progressCallback);
	virtual std::string ShortName() const { return StaticShortName(); }
	
	// Bank factory registration
	static void LoginInfo(LoginInfo &loginInfo) {loginInfo.push_back(LOGIN); loginInfo.push_back(PASSWORD);}
	BANK_FACTORY_REGISTRATION(BankFortuneo, "Fortuneo", 1,0, "https://www.fortuneo.fr/fr/identification.jsp")
};
