/*
 *  BankCaIdF.cpp
 *  BankReader
 *
 *  Created by Brice Rivé on 12/26/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#include "BankCaIdf.h"
#include "CurlPP.h"
using std::string;
#include "Utils.h"

BankCaIdF::Registrar BankCaIdF::registrar;

void BankCaIdF::GrabAccounts(ProgressCallback &progressCallback)
{
	progressCallback(0);
	string login = loginData[LOGIN];
	string password = loginData[PASSWORD];
	
	// Sanity check
	CheckNumeric(login, 11);
	CheckNumeric(password, 6);
	
	// Load entry page
	GetUrlIntoContent("http://www.ca-idf.fr");
	progressCallback(25);
	
	// Get form action url
	string formUrl = GrabInContent("<form.*?action=\"?(.*?)[\"[:space:]>]");
		
	// Post the login
	PostIntoContent(AbsoluteUrlRelativeToCurrent(formUrl),
		"typeAuthentification=souris"
		"&code=" +password+
		"&numero=" +login+
		"&cookie=off"
		"&code_souris=" +password);
	progressCallback(50);

	// Check for password error
	if (SearchInContent("Votre identification est incorrecte"))	throw Err(_WHERE, rejectedPasswordErrorWhy);
	
	// Grab the frame URL
	string frameUrl = GrabInContent("<frame name=\"contenu\" src=\"?(.*?)[\"[:space:]>]");
	progressCallback(75);
	
	// Open the summary frame
	GetUrlIntoContent(AbsoluteUrlRelativeToCurrent(frameUrl));

	const char *expression = "\\.\\./releve/0-list.*?\">(.*?) n.(.*?)</a>.*?([0-9 ,]+)EUR";
	GrabAccountsInContent(expression, "Euro");
	expression = "\\.\\./releve/releveCarte.*?\">(.*?)</a> - (.*?2006).*?([0-9 ,]+)EUR";
	GrabAccountsInContent(expression, "Euro");
	if (accounts.empty()) throw Err(_WHERE, protocolErrorWhy);	
	progressCallback(100);
}

