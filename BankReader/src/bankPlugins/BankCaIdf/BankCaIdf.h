/*
 *  BankCaIdF.h
 *  BankReader
 *
 *  Created by Brice Riv√© on 12/26/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once
#include "Bank.h"
extern const unsigned char BankCaIdFIcon[];

class BankCaIdF: public Bank {
	
	BankCaIdF(CurlPP &curl_, const LoginData &loginData_) :Bank(curl_,loginData_) {}
	
	// Bank virtual interface
	virtual void GrabAccounts(ProgressCallback &progressCallback);
	virtual std::string ShortName() const { return StaticShortName(); }
	
	// Bank factory registration
	static void LoginInfo(LoginInfo &loginInfo) {loginInfo.push_back(LOGIN); loginInfo.push_back(PASSWORD);}
	BANK_FACTORY_REGISTRATION(BankCaIdF, "Crédit Agricole IdF", 1,0, "http://www.ca-idf.fr")
};
