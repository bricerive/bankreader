/*
 *  BankWfo.cpp
 *  BankReader
 *
 *  Created by Brice Rive on 1/13/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#include "BankWfo.h"
#include <boost/regex.hpp>
#include "CurlPP.h"

using std::string;
using std::vector;

BankWfo::Registrar BankWfo::registrar;

void BankWfo::GrabAccounts(ProgressCallback &progressCallback)
{
	progressCallback(0);
	
	// Get signon page
	GetUrlIntoContent("https://online.wellsfargo.com");
	progressCallback(20);
	
	// Grab the Signon form action
	// <form method="post" name="Signon" AutoComplete="off" action="https://online.wellsfargo.com:443/ca1_xa1_on/cgi-bin/session.cgi?screenid=SIGNON">
	//string actionUrl = GrabInContent("<form .*?\"Signon\".*?action=\"(.*?)\"");
	string actionUrl = GrabInContent("Enter your username.*?<form action=\"(.*?)\"");
	progressCallback(40);

	// Post Login
    PostIntoContent(actionUrl,
					"LOB=CONS"
					"&userid=" +loginData[LOGIN]
					+ "&password=" +loginData[PASSWORD]
					+ "&destination=AccountSummary"
					+ "&inboxItemId="
					+ "&origination=Wib");
	progressCallback(60);
	
	// Check for password error
	if (SearchInContent("We do not recognize")) throw Err(_WHERE, rejectedPasswordErrorWhy);
	if (SearchInContent("We cannot verify your entry")) throw Err(_WHERE, rejectedPasswordErrorWhy);
	progressCallback(70);

    
	// Add a cookie to skip the Alerts advertisement
    if (content.find("SplashPageForm")!=string::npos) {
        curl.MergeCookies(to_string("WF_SPLASH=1002|1|1145872847426|UNKNOWN"));
		string continueAction = GrabInContent("<form name=\"SplashPageForm\" method=\"post\" action=\"(.*?)\"");
		PostIntoContent(continueAction, "submitValue=Continue to online session");
		progressCallback(80);
    }
	
    // Skip this ad
    // <form method="post" action="https://online.wellsfargo.com/das/cgi-bin/session.cgi?sessargs=ISuI1LjQNFPMD-qoE_OXxI325P9UEBXr">
    // <input name="Decline" value="Not at this time" type="submit" class="secondary"/>
    // </form>
    if (content.find("value=\"Not at this time\"")!=string::npos) {
		string continueAction = GrabInContent("action=\"(.*?)\"><input name=\"Decline\"");
		PostIntoContent(continueAction, "submitValue=Continue to online session");
		progressCallback(80);
    }
	
    //const char *expression = "class=\"productName\">[[:space:]]*(.*?)[[:space:]]*</a>.*?\"cashAccountNum\">[[:space:]]*(.*?)[[:space:]]*</TD>.*?View Account Activity for.*?\\$(.*?)[[:space:]]*</a>";
    // const char *expression = "<a class=\"amount\".*?View Account Activity for&nbsp;[[:space:]]*(.*?)[[:space:]]*(.*?)[[:space:]]*.*?\\$(.*?)[[:space:]]*</a>";

//	<a class="amount" href="https://online.wellsfargo.com/das/cgi-bin/session.cgi?sessargs=7sQasbXE67ODug3oqPkrpQtm1vXIB2dj"
//    title="View Account Activity for&nbsp;
//    SAVINGS XXX-XXX9962 ">
//    $153,816.98
//    </a>
//
//    <a class="amount" href="https://online.wellsfargo.com/das/cgi-bin/session.cgi?sessargs=SbtiXbG18DI1KdHmiPkcs3SMwNRJfM0n"
//    title="View Account Activity for&nbsp;
//    CHECKING XXX-XXX6361 ">
//    $3,727.58
//    </a>
    
    //const char *expression = "<a class=\"amount\".*?View Account Activity for&nbsp;[[:space:]]*(.*?)[[:space:]]*(.*?) \">[[:space:]]*\\$(.*?)[[:space:]]*</a>";
    const char *expression = "<a class=\"amount\".*?View Account Activity for&nbsp;(.*?) X(.*?)&nbsp;\\$(.*?)\">";
	GrabAccountsInContent(expression, "Dollar");
	if (accounts.empty()) throw Err(_WHERE, protocolErrorWhy);	
	progressCallback(100);
}
