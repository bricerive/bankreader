/*
 *  BankCcf.cpp
 *  BankReader
 *
 *  Created by Brice Rive on 1/13/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#include "BankCcf.h"
#include "CurlPP.h"
#include <vector>
using std::vector;
using std::string;

BankCcf::Registrar BankCcf::registrar;

void BankCcf::GrabAccounts(ProgressCallback &progressCallback)
{	
	progressCallback(0);
	
	//https://www.hsbc.fr/1/2/hsbc-france/particuliers/login
	
	//if (UName.length==11) 
    //	form.action="https://client.hsbc.fr/cgi-bin/emcgi";
    //else if (UName.length==6) 
	//	form.action="https://secure.hsbc-direct.hsbc.fr/vitrine/enligne/main1.htm.pgi";
	
	
	// Submit Login
	PostIntoContent("https://client.hsbc.fr/cgi-bin/emcgi",
				 "Appl=WEBACC" "&secret=" "&CODE_ABONNE=" +loginData[LOGIN]+ "&Ident=" +loginData[LOGIN]);
	progressCallback(25);
	
	// Grab the session ID
	string sessionId = GrabInContent("sessionid=(.*?) ");
	
	// Submit Password
	string passUrl = "https://client.hsbc.fr/cgi-bin/emcgi?sessionId=" + sessionId;
	PostIntoContent(passUrl, "Secret=" +loginData[PASSWORD]);
	progressCallback(50);
	
	// Check for password error
	if (SearchInContent("sont incorrectes")) throw Err(_WHERE, rejectedPasswordErrorWhy);

	// Grab the frame URL
	string frameUrl = GrabInContent("<frame name=\"FrameWork\" src=\"?(.*?)[\"[:space:]>]");

	// Open the summary frame
	GetUrlIntoContent("https://client.hsbc.fr"+frameUrl);
	progressCallback(75);

	// Grab accounts
	const char *expression = "<td colspan=\"3\" class=\"txt\".*?>.*?<a.*?/a>.*?<a.*?>(.*?)</a>.*?<td.*?>(.*?)</td>[[:space:]]*<td.*?>(?:<FONT COLOR=\"#CC0000\">)?(.*?)</td>";
	GrabAccountsInContent(expression, "Euro");
	if (accounts.empty()) throw Err(_WHERE, protocolErrorWhy);	
	progressCallback(100);
}
