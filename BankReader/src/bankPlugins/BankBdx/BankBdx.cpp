/*
 *  BankBdx.cpp
 *  BankReader
 *
 *  Created by Brice Rive on 1/13/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#include "BankBdx.h"
#include "DigitGrid.h"
#include "CurlPP.h"
#include <boost/regex.hpp>
using std::string;

extern char BankBdxDigits3[];
extern int BankBdxDigits3NBytes;

BankBdx::Registrar BankBdx::registrar;

void BankBdx::GrabAccounts(ProgressCallback &progressCallback)
{
	progressCallback(0);
	// Check the password for conformance
	std::string password = loginData[PASSWORD];
	CheckNumeric(password, 5);

    curl.DisableCertificates();

    // Get main page
	GetUrlIntoContent("https://www.axabanque.fr/connexion/index.html?fmt=default");
	progressCallback(10);
    
    // Grab the grid url
	string gridUrl = GrabInContent("<img id=\"imagePave\" src=\"(.*?)\"");

	// Load the numeric grid image
    string grid;
	GetUrlIntoString(gridUrl, grid);
	progressCallback(20);
    
	// Find the location of the numbers in the grid
	std::vector<int> layout;
	try {
		DigitGrid::Process(grid, BankBdxDigits3, BankBdxDigits3NBytes, layout);
	} catch (Err &err) {
		throw Err(_WHERE, protocolErrorWhy);
	}
	progressCallback(60);
	
	// Find the clicks for the password
	struct Box {
		double left, top, right, bottom;
		int id;
	};
//	Box boxes[] = { {6,6,36,36},{36,6,66,36},{66,6,96,36},{96,6,126,36},{126,6,156,36},{156,6,186,36},{186,6,216,36},{216,6,246,36},
//		{6,36,36,66},{36,36,66,66},{66,36,96,66},{96,36,126,66},{126,36,156,66},{156,36,186,66},{186,36,216,66},{216,36,246,66},
//		{6,66,36,96},{36,66,66,96},{66,66,96,96},{96,66,126,96},{126,66,156,96},{156,66,186,96},{186,66,216,96},{216,66,246,96}
//	};    
    Box boxes[] = { {0,0,31,33},{32,0,63,33},{64,0,97,33},{98,0,129,33},{130,0,161,33},{162,0,193,33},{164,0,225,33},{226,0,257,33},
        {0,34,31,67},{32,34,63,67},{64,34,97,67},{98,34,129,67},{130,34,161,67},{162,34,193,67},{164,34,225,67},{226,34,257,67},
        {0,68,31,101},{32,68,63,101},{64,68,97,101},{98,68,129,101},{130,68,161,101},{162,68,193,101},{164,68,225,101},{226,68,257,101}
    };
    
	string gridClicks;
	for (int i=0; i<5; i++) {
		int digit = password[i]-'0';
		int x = layout[digit*4];
		int y = layout[digit*4+1];
		for (int j=0; j<sizeof(boxes)/sizeof(boxes[0]); j++) {
			if (x>boxes[j].left && x<boxes[j].right && y>boxes[j].top && y<boxes[j].bottom) {
				gridClicks += 'a'+j;
			}
		}
	}
    
	string url = GrabInContent("action=\"(.*?)\"");
    
	// POST the login
	PostIntoContent(url,
                    "login=" +loginData[LOGIN]+ 
                    "&codepasse=" +gridClicks+
                    "&motDePasse=" +gridClicks+
                    "&_idJsp0_SUBMIT=1"+
                    "&_idJsp0%3A_idcl="+
                    "&_idJsp0%3A_link_hidden_="
                    );
	progressCallback(80);
    
	// Check for password error
	if (SearchInContent("incorrects ou incomplets")) throw Err(_WHERE, rejectedPasswordErrorWhy);
    
	// Grab accounts
    
    /*
     <td class="colonne_couleur_type_2 compte">
     <a href="#" onclick="noDoubleClic(this);;return oamSubmitForm('_idJsp1','_idJsp1:tableaux-comptes-courant:0:_idJsp184',null,[['paramCodeProduit','1'],['paramNumContrat','482711'],['paramCodeFamille','1'],['paramNumCompte','13114231509'],['paramCodeSousProduit','2']]);" id="_idJsp1:tableaux-comptes-courant:0:_idJsp184">Liquidit&#233;s</a>
     </td>
     <td class="colonne_couleur_type_2 libelle">
     M. BRICE RIVE 13114231509
     </td>
     <td class="colonne_couleur_type_1 montant"><strong>
     4&#160;142,71 </strong></td>
     */
/*
 <tr class="ligne1">
 <td class="colonne_couleur_type_2 compte">
 <a href="#" onclick="noDoubleClic(this);;return oamSubmitForm('_idJsp1','_idJsp1:tableaux-comptes-courant:0:_idJsp184',null,[['paramCodeProduit','1'],['paramNumContrat','482711'],['paramCodeFamille','1'],['paramNumCompte','13114231509'],['paramCodeSousProduit','2']]);" id="_idJsp1:tableaux-comptes-courant:0:_idJsp184">Liquidit&#233;s</a>
 </td>
 <td class="colonne_couleur_type_2 libelle">
 M. BRICE RIVE 13114231509
 </td>
 <td class="colonne_couleur_type_1 montant"><strong>
 3&#160;493,71 </strong></td>
 </tr>
 */
    // La structure de la page de panorama des comptes:
    //
    // 		<div id="table-panorama">
    //          TABLES
    //      </div>
    //
    // TABLES:
    //      <table ... </table>
    
    // locate the panorama div content
    string tablePanorama = GrabInContent("<div id=\"table-panorama\">(.*?)</div>");
    // Loop over the tables in there
    boost::sregex_iterator m1(tablePanorama.begin(), tablePanorama.end(), boost::regex("(<table.*?</table>)"));

    boost::sregex_iterator m2;
    for (boost::sregex_iterator tableI = m1; tableI!=m2; ++tableI)
    {
        const boost::smatch &what(*tableI);

        // Get the table body
        if (!SearchInString("<tbody>(.*?)</tbody>", what[0])) continue;


        string tbody = GrabInString("<tbody>(.*?)</tbody>", what[0]);

        // Loop over the table rows
        boost::sregex_iterator tr1(tbody.begin(), tbody.end(), boost::regex("(<tr.*?</tr>)"));
        boost::sregex_iterator tr2;
        for (boost::sregex_iterator trI = tr1; trI!=tr2; ++trI)
        {
            string tr = (*trI)[0];
            // Loop over the row columns
            boost::sregex_iterator td1(tr.begin(), tr.end(), boost::regex("(<td.*?</td>)"));
            boost::sregex_iterator td2;
            for (boost::sregex_iterator tdI = td1; tdI!=td2; ++tdI)
            {
                string td = (*tdI)[0];
            }
            int nbAccounts = accounts.size();
            GrabAccountsInString("<td.*?>.*?<a.*?>(.*?)</a>.*?<td.*?>(.*?)</td>.*?<strong>(.*?)</strong>", "Euro", false, tr);
            if (nbAccounts == accounts.size())
                GrabAccountsInString("<td.*?>(.*?)</td>.*?<td.*?>(.*?)</td>.*?<strong>(.*?)</strong>", "Euro", false, tr);
        }
    }
	if (accounts.empty()) throw Err(_WHERE, protocolErrorWhy);	
	progressCallback(100);
}
