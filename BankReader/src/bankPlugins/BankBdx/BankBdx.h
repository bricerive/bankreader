/*
 *  BankBdx.h
 *  BankReader
 *
 *  Created by Brice Rive on 1/13/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once
#include "Bank.h"
extern const unsigned char BankBdxIcon[];

class BankBdx: public Bank {
	
	BankBdx(CurlPP &curl_, const LoginData &loginData_):Bank(curl_,loginData_){}
	
	// Bank virtual interface
	virtual void GrabAccounts(ProgressCallback &progressCallback);
	virtual std::string ShortName() const { return StaticShortName(); }
	
	// Bank factory registration
	static void LoginInfo(LoginInfo &loginInfo) {loginInfo.push_back(LOGIN); loginInfo.push_back(PASSWORD);}
	BANK_FACTORY_REGISTRATION(BankBdx, "Banque Directe", 1,0, "https://www.axabanque.fr/connexion/index.html?fmt=default")
};
