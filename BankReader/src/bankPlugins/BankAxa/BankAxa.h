/*
 *  BankAxa.h
 *  BankReader
 *
 *  Created by Brice Rive on 1/13/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once
#include "Bank.h"
extern const unsigned char BankAxaIcon[];

class BankAxa: public Bank {
	
	BankAxa(CurlPP &curl_, const LoginData &loginData_) :Bank(curl_,loginData_) {}

	// Bank virtual interface
	virtual void GrabAccounts(ProgressCallback &progressCallback);
	virtual std::string ShortName() const { return StaticShortName(); }

	// Bank factory registration
	static void LoginInfo(LoginInfo &loginInfo) {loginInfo.push_back(LOGIN); loginInfo.push_back(PASSWORD);}
	BANK_FACTORY_REGISTRATION(BankAxa, "Axa Banque", 1,0, "https://client.axa.fr/Cadre/Accueil.aspx?ReturnUrl=%2findex.aspx");
};
