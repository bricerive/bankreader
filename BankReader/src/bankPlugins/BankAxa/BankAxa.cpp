/*
 *  BankAxa.cpp
 *  BankReader
 *
 *  Created by Brice Rive on 1/13/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#include "BankAxa.h"
#include "CurlPP.h"
#include "DigitGrid.h"
using std::string;
#include <sstream>
using namespace std;
#include "Utils.h"

BankAxa::Registrar BankAxa::registrar;
extern char BankAxaDigits[];
extern int BankAxaDigitsNBytes;

void BankAxa::GrabAccounts(ProgressCallback &progressCallback)
{
	progressCallback(0);
	
    curl.DisableCertificates();

	//curl.SetSslVersion(3);

	// Login page
    string mainUrl("https://client.axa.fr/_layouts/NOSI/LoginPage.aspx?ReturnUrl=%2f_layouts%2fAuthenticate.aspx%3fSource%3d%252f&Source=%2f");
    GetUrlIntoContent(mainUrl);
	progressCallback(10);
	
    // Grab the name of the grid image
    //     <td colspan="4"><img usemap="#MapVirtualKeyboard_ctl0__vkbMotdepasse__ctl1" src="/Temp\clavier104n1kfkj1ici.jpg" alt="" border="0" /></td>
    string gridImageName = GrabInContent("usemap=\"#MapVirtualKeyboardvkMotDePasse_ctl01\" src=\"(.*?)\"");
    // Replace backslashes by slashes
    // Cleanup accents
	size_t br=0;
	while ((br = gridImageName.find("\\", br)) != string::npos)
		gridImageName.replace(br, 1, "/");    
    string gridImageUrl = string("https://client.axa.fr") + gridImageName;
	progressCallback(20);
    
    // Get the grid image
	string gridImage;
	GetUrlIntoString(gridImageUrl, gridImage);
	progressCallback(30);

    // Access login info
    std::string login = loginData[LOGIN];
	std::string password = loginData[PASSWORD];    
	CheckNumeric(password, 6); // Check the password for conformance
    
    // Find the location of the numbers in the grid
	std::vector<int> layout;
	try {
		DigitGrid::Process(gridImage, BankAxaDigits, BankAxaDigitsNBytes, layout);
	} catch (Err &err) {
		throw Err(_WHERE, protocolErrorWhy);
	}
	progressCallback(40);
    
    // Find the clicks for the password
    // grifdClicks contains the indices of the boxes to click
	struct Box {
		int left, top, right, bottom;
	};
	Box boxes[] = {
        {0,0,30,30},{30,0,60,30},{60,0,90,30},{90,0,120,30},{120,0,150,30},{150,0,180,30},{180,0,210,30},{210,0,240,30}
        ,{0,30,30,60},{30,30,60,60},{60,30,90,60},{90,30,120,60},{120,30,150,60},{150,30,180,60},{180,30,210,60},{210,30,240,60}
        ,{0,60,30,90},{30,60,60,90},{60,60,90,90},{90,60,120,90},{120,60,150,90},{150,60,180,90},{180,60,210,90},{210,60,240,90}
    };
    const unsigned int nBoxes = sizeof(boxes)/sizeof(boxes[0]);
	std::vector<int> gridClicks;
	for (int i=0; i<6; i++) {
		int digit = password[i]-'0';
		int x = layout[digit*4];
		int y = layout[digit*4+1];
		for (int j=0; j<nBoxes; j++) {
			if (x>boxes[j].left && x<boxes[j].right && y>boxes[j].top && y<boxes[j].bottom) {
				gridClicks.push_back(j);
			}
		}
	}

    // Generate the passcode
    ostringstream os;
	for (int i=0; i<gridClicks.size(); ++i) {
        os << gridClicks[i] << ",";
	}
    string passcode = os.str();
    
    
    // Submit 
    // This is what each click does:
    //    function SelecVirtualKeyboard(num,clientPassDispId,clientCountId,clientPassId)
    //    {
    //        document.getElementById(clientPassDispId).value+="*";
    //        var obj=document.getElementById(clientCountId);
    //        obj.value=(parseInt(obj.value) + 1);
    //        document.getElementById(clientPassId).value+= num + ",";
    //    }
    // called as: javascript:SelecVirtualKeyboard(0,'_ctl0__vkbMotdepasse_PassWordDisp','_ctl0__vkbMotdepasse_CounterVirtualKeyboard','_ctl0__vkbMotdepasse_CellSelecVirtualKeyboard');

    // The form is:
    //    <form name="loginForm" method="post" action="Accueil.aspx?ReturnUrl=%2findex.aspx" id="loginForm" onSubmit="return false;">
    //    <input type="hidden" name="__EVENTTARGET" value="" />
    //    <input type="hidden" name="__EVENTARGUMENT" value="" />
    //    <input type="hidden" name="__VIEWSTATE" value="dDwxNTcyOTE5MDIxO3Q8O2w8aTwxPjs+O2w8dDw7bDxpPDE+O2k8Mz47PjtsPHQ8O2w8aTwwPjs+O2w8dDw7bDxpPDE+O2k8Mj47aTw2PjtpPDc+O2k8OD47aTw5PjtpPDEwPjtpPDExPjtpPDEyPjtpPDEzPjtpPDE1PjtpPDE2PjtpPDIwPjtpPDIxPjtpPDIyPjtpPDIzPjtpPDI0PjtpPDI1PjtpPDI2PjtpPDI3Pjs+O2w8dDxwPHA8bDxOYXZpZ2F0ZVVybDs+O2w8aHR0cDovL3d3dy5heGEuZnIvUGFnZXMvbm91cy1jb250YWN0ZXIuYXNweDs+Pjs+Ozs+O3Q8cDxwPGw8TmF2aWdhdGVVcmw7PjtsPGh0dHA6Ly93d3cuYXhhLmZyL1BhZ2VzL21pZXV4LW5vdXMtY29ubmFpdHJlLmFzcHg7Pj47Pjs7Pjt0PHA8cDxsPE5hdmlnYXRlVXJsOz47bDxodHRwOi8vd3d3LmF4YS5mci9hdXRvLW1vdG8vUGFnZXMvYXNzdXJhbmNlLWF1dG8uYXNweDs+Pjs+Ozs+O3Q8cDxwPGw8TmF2aWdhdGVVcmw7PjtsPGh0dHA6Ly93d3cuYXhhLmZyL2hhYml0YXRpb24tZmFtaWxsZS1sb2lzaXJzOz4+Oz47Oz47dDxwPHA8bDxOYXZpZ2F0ZVVybDs+O2w8aHR0cDovL3d3dy5heGEuZnIvc2FudGU7Pj47Pjs7Pjt0PHA8cDxsPE5hdmlnYXRlVXJsOz47bDxodHRwOi8vd3d3LmF4YS5mci9wcmV2b3lhbmNlLXByb3RlY3Rpb24tZGVzLXByb2NoZXM7Pj47Pjs7Pjt0PHA8cDxsPE5hdmlnYXRlVXJsOz47bDxodHRwOi8vd3d3LmF4YS5mci9iYW5xdWUtY3JlZGl0Oz4+Oz47Oz47dDxwPHA8bDxOYXZpZ2F0ZVVybDs+O2w8aHR0cDovL3d3dy5heGEuZnIvZXBhcmduZS1wbGFjZW1lbnRzOz4+Oz47Oz47dDxwPHA8bDxOYXZpZ2F0ZVVybDs+O2w8aHR0cDovL3d3dy5heGEuZnIvcmV0cmFpdGUtYXNzdXJhbmNlLXZpZTs+Pjs+Ozs+O3Q8cDxwPGw8QXJyYXlDZWxsQ29udGVudDs+O2w8QFN5c3RlbS5JbnQzMiwgbXNjb3JsaWIsIFZlcnNpb249MS4wLjUwMDAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1iNzdhNWM1NjE5MzRlMDg5PGk8LTE+O2k8Nz47aTw2PjtpPC0xPjtpPDM+O2k8LTE+O2k8NT47aTw0PjtpPDI+O2k8MD47aTwtMT47aTwtMT47aTwtMT47aTwtMT47aTwtMT47aTwtMT47aTwtMT47aTw5PjtpPC0xPjtpPDE+O2k8LTE+O2k8LTE+O2k8LTE+O2k8OD47Pjs+Pjs+O2w8aTwwPjs+O2w8dDw7bDxpPDA+O2k8MT47PjtsPHQ8O2w8aTwwPjs+O2w8dDw7bDxpPDA+Oz47bDx0PHA8cDxsPEltYWdlVXJsOz47bDwvVGVtcFxcY2xhdmllcjEwNG4xa2ZrajFpY2kuanBnOz4+O3A8bDx1c2VtYXA7PjtsPCNNYXBWaXJ0dWFsS2V5Ym9hcmRfY3RsMF9fdmtiTW90ZGVwYXNzZV9fY3RsMTs+Pj47Oz47Pj47Pj47dDw7bDxpPDA+O2k8Mj47PjtsPHQ8O2w8aTwwPjs+O2w8dDxwPGw8aW5uZXJodG1sOz47bDxcPHNwYW5cPkFpZGVcPC9zcGFuXD5cPElNRyBzcmM9Ii4uL3Jlc3NvdXJjZXMvdHJhbnNwYXJlbnQuZ2lmIiBhbGlnbj0iYWJzTWlkZGxlIiBib3JkZXI9IjAiXD47Pj47Oz47Pj47dDw7bDxpPDA+O2k8MT47PjtsPHQ8cDxwPGw8TmF2aWdhdGVVcmw7PjtsPGphdmFzY3JpcHQ6RXJhc1ZpcnR1YWxLZXlib2FyZCgnX2N0bDBfX3ZrYk1vdGRlcGFzc2VfUGFzc1dvcmREaXNwJywnX2N0bDBfX3ZrYk1vdGRlcGFzc2VfQ291bnRlclZpcnR1YWxLZXlib2FyZCcsJ19jdGwwX192a2JNb3RkZXBhc3NlX0NlbGxTZWxlY1ZpcnR1YWxLZXlib2FyZCcpXDs7Pj47Pjs7Pjt0PHA8cDxsPE5hdmlnYXRlVXJsOz47bDxqYXZhc2NyaXB0OkVyYXNWaXJ0dWFsS2V5Ym9hcmQoJ19jdGwwX192a2JNb3RkZXBhc3NlX1Bhc3NXb3JkRGlzcCcsJ19jdGwwX192a2JNb3RkZXBhc3NlX0NvdW50ZXJWaXJ0dWFsS2V5Ym9hcmQnLCdfY3RsMF9fdmtiTW90ZGVwYXNzZV9DZWxsU2VsZWNWaXJ0dWFsS2V5Ym9hcmQnKVw7Oz4+Oz47Oz47Pj47Pj47Pj47Pj47dDxwPHA8bDxOYXZpZ2F0ZVVybDs+O2w8L0NhZHJlL2luZGV4LmFzcHg/cmVlbml0PWV0YXBlMTs+Pjs+Ozs+O3Q8cDxwPGw8TmF2aWdhdGVVcmw7PjtsPC9DYWRyZS9pbmRleC5hc3B4P2lkb2w9ZXRhcGUxOz4+Oz47Oz47dDxwPHA8bDxOYXZpZ2F0ZVVybDs+O2w8aHR0cDovL3d3dy5lc3BhY2UtcHJlc3NlLmF4YS5mci87Pj47Pjs7Pjt0PHA8cDxsPE5hdmlnYXRlVXJsOz47bDxodHRwOi8vd3d3LnJlY3J1dGUuYXhhLmZyOz4+Oz47Oz47dDxwPHA8bDxOYXZpZ2F0ZVVybDs+O2w8aHR0cDovL3d3dy5heGEuZnIvUGFnZXMvbm9zLWFjdHVhbGl0ZXMuYXNweDs+Pjs+Ozs+O3Q8cDxwPGw8TmF2aWdhdGVVcmw7PjtsPGh0dHA6Ly93d3cuYXhhLmZyL1BhZ2VzL2luZm9zLXJzcy5hc3B4Oz4+Oz47Oz47dDxwPHA8bDxOYXZpZ2F0ZVVybDs+O2w8aHR0cDovL3d3dy5heGEuZnIvUGFnZXMvcmVjaGVyY2hlLWRpc3RyaWJ1dGV1ci5hc3B4Oz4+Oz47Oz47dDxwPHA8bDxOYXZpZ2F0ZVVybDs+O2w8aHR0cDovL3d3dy5heGEuZnIvUGFnZXMvcGxhbi1kZS1zaXRlLmFzcHg7Pj47Pjs7Pjt0PHA8cDxsPE5hdmlnYXRlVXJsOz47bDxodHRwOi8vd3d3LmF4YS5mci9QYWdlcy9tZW50aW9ucy1sZWdhbGVzLmFzcHg7Pj47Pjs7Pjt0PHA8cDxsPE5hdmlnYXRlVXJsOz47bDxodHRwOi8vd3d3LmF4YS5mci9QYWdlcy9ub3VzLWNvbnRhY3Rlci5hc3B4Oz4+Oz47Oz47Pj47Pj47dDxwPHA8bDxUZXh0Oz47bDxIb21lIFBhZ2UgU2l0ZSBDbGllbnQtTGFuY2V1ci1FbnRyw6llIGRhbnMgbGUgbW9kdWxlOz4+Oz47Oz47Pj47Pj47PonafgVwPmjJ2i++2Cq14N3IlVr7" />
    //    <input name="_ctl0:_vkbMotdepasse:PassWordDisp" type="password" maxlength="6" size="6" id="_ctl0__vkbMotdepasse_PassWordDisp" disabled="disabled" /></td>
    //    <input name="_ctl0:_vkbMotdepasse:CellSelecVirtualKeyboard" id="_ctl0__vkbMotdepasse_CellSelecVirtualKeyboard" type="hidden" />
    //    <input name="_ctl0:_vkbMotdepasse:CounterVirtualKeyboard" id="_ctl0__vkbMotdepasse_CounterVirtualKeyboard" type="hidden" value="0" />
    //    <input name="_ctl0:_tbxIdentifiant" type="text" value="Votre identifiant" maxlength="20" id="_ctl0__tbxIdentifiant" class="id_form_champ_big" onclick="javascript:this.value='';" />
    
    // Looked at HTTP headers with LiveHTTPHewaders under Firefox:
    //    __EVENTTARGET=_ctl0%3A_btnValider
    //    &__EVENTARGUMENT=
    //    &__VIEWSTATE=dDwxNTcyOTE5MDIxO3Q8O2w8aTwxPjs%2BO2w8dDw7bDxpPDE%2BO2k8Mz47PjtsPHQ8O2w8aTwwPjs%2BO2w8dDw7bDxpPDE%2BO2k8Mj47aTw2PjtpPDc%2BO2k8OD47aTw5PjtpPDEwPjtpPDExPjtpPDEyPjtpPDEzPjtpPDE1PjtpPDE2PjtpPDIwPjtpPDIxPjtpPDIyPjtpPDIzPjtpPDI0PjtpPDI1PjtpPDI2PjtpPDI3Pjs%2BO2w8dDxwPHA8bDxOYXZpZ2F0ZVVybDs%2BO2w8aHR0cDovL3d3dy5heGEuZnIvUGFnZXMvbm91cy1jb250YWN0ZXIuYXNweDs%2BPjs%2BOzs%2BO3Q8cDxwPGw8TmF2aWdhdGVVcmw7PjtsPGh0dHA6Ly93d3cuYXhhLmZyL1BhZ2VzL21pZXV4LW5vdXMtY29ubmFpdHJlLmFzcHg7Pj47Pjs7Pjt0PHA8cDxsPE5hdmlnYXRlVXJsOz47bDxodHRwOi8vd3d3LmF4YS5mci9hdXRvLW1vdG8vUGFnZXMvYXNzdXJhbmNlLWF1dG8uYXNweDs%2BPjs%2BOzs%2BO3Q8cDxwPGw8TmF2aWdhdGVVcmw7PjtsPGh0dHA6Ly93d3cuYXhhLmZyL2hhYml0YXRpb24tZmFtaWxsZS1sb2lzaXJzOz4%2BOz47Oz47dDxwPHA8bDxOYXZpZ2F0ZVVybDs%2BO2w8aHR0cDovL3d3dy5heGEuZnIvc2FudGU7Pj47Pjs7Pjt0PHA8cDxsPE5hdmlnYXRlVXJsOz47bDxodHRwOi8vd3d3LmF4YS5mci9wcmV2b3lhbmNlLXByb3RlY3Rpb24tZGVzLXByb2NoZXM7Pj47Pjs7Pjt0PHA8cDxsPE5hdmlnYXRlVXJsOz47bDxodHRwOi8vd3d3LmF4YS5mci9iYW5xdWUtY3JlZGl0Oz4%2BOz47Oz47dDxwPHA8bDxOYXZpZ2F0ZVVybDs%2BO2w8aHR0cDovL3d3dy5heGEuZnIvZXBhcmduZS1wbGFjZW1lbnRzOz4%2BOz47Oz47dDxwPHA8bDxOYXZpZ2F0ZVVybDs%2BO2w8aHR0cDovL3d3dy5heGEuZnIvcmV0cmFpdGUtYXNzdXJhbmNlLXZpZTs%2BPjs%2BOzs%2BO3Q8cDxwPGw8QXJyYXlDZWxsQ29udGVudDs%2BO2w8QFN5c3RlbS5JbnQzMiwgbXNjb3JsaWIsIFZlcnNpb249MS4wLjUwMDAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1iNzdhNWM1NjE5MzRlMDg5PGk8LTE%2BO2k8LTE%2BO2k8LTE%2BO2k8LTE%2BO2k8LTE%2BO2k8LTE%2BO2k8MD47aTwyPjtpPC0xPjtpPDE%2BO2k8OT47aTwtMT47aTw2PjtpPDg%2BO2k8LTE%2BO2k8LTE%2BO2k8ND47aTw1PjtpPC0xPjtpPC0xPjtpPC0xPjtpPC0xPjtpPDM%2BO2k8Nz47Pjs%2BPjs%2BO2w8aTwwPjs%2BO2w8dDw7bDxpPDA%2BO2k8MT47PjtsPHQ8O2w8aTwwPjs%2BO2w8dDw7bDxpPDA%2BOz47bDx0PHA8cDxsPEltYWdlVXJsOz47bDwvVGVtcFxcY2xhdmllcjEwNGpuajZ4eWlxbjguanBnOz4%2BO3A8bDx1c2VtYXA7PjtsPCNNYXBWaXJ0dWFsS2V5Ym9hcmRfY3RsMF9fdmtiTW90ZGVwYXNzZV9fY3RsMTs%2BPj47Oz47Pj47Pj47dDw7bDxpPDA%2BO2k8Mj47PjtsPHQ8O2w8aTwwPjs%2BO2w8dDxwPGw8aW5uZXJodG1sOz47bDxcPHNwYW5cPkFpZGVcPC9zcGFuXD5cPElNRyBzcmM9Ii4uL3Jlc3NvdXJjZXMvdHJhbnNwYXJlbnQuZ2lmIiBhbGlnbj0iYWJzTWlkZGxlIiBib3JkZXI9IjAiXD47Pj47Oz47Pj47dDw7bDxpPDA%2BO2k8MT47PjtsPHQ8cDxwPGw8TmF2aWdhdGVVcmw7PjtsPGphdmFzY3JpcHQ6RXJhc1ZpcnR1YWxLZXlib2FyZCgnX2N0bDBfX3ZrYk1vdGRlcGFzc2VfUGFzc1dvcmREaXNwJywnX2N0bDBfX3ZrYk1vdGRlcGFzc2VfQ291bnRlclZpcnR1YWxLZXlib2FyZCcsJ19jdGwwX192a2JNb3RkZXBhc3NlX0NlbGxTZWxlY1ZpcnR1YWxLZXlib2FyZCcpXDs7Pj47Pjs7Pjt0PHA8cDxsPE5hdmlnYXRlVXJsOz47bDxqYXZhc2NyaXB0OkVyYXNWaXJ0dWFsS2V5Ym9hcmQoJ19jdGwwX192a2JNb3RkZXBhc3NlX1Bhc3NXb3JkRGlzcCcsJ19jdGwwX192a2JNb3RkZXBhc3NlX0NvdW50ZXJWaXJ0dWFsS2V5Ym9hcmQnLCdfY3RsMF9fdmtiTW90ZGVwYXNzZV9DZWxsU2VsZWNWaXJ0dWFsS2V5Ym9hcmQnKVw7Oz4%2BOz47Oz47Pj47Pj47Pj47Pj47dDxwPHA8bDxOYXZpZ2F0ZVVybDs%2BO2w8L0NhZHJlL2luZGV4LmFzcHg%2FcmVlbml0PWV0YXBlMTs%2BPjs%2BOzs%2BO3Q8cDxwPGw8TmF2aWdhdGVVcmw7PjtsPC9DYWRyZS9pbmRleC5hc3B4P2lkb2w9ZXRhcGUxOz4%2BOz47Oz47dDxwPHA8bDxOYXZpZ2F0ZVVybDs%2BO2w8aHR0cDovL3d3dy5lc3BhY2UtcHJlc3NlLmF4YS5mci87Pj47Pjs7Pjt0PHA8cDxsPE5hdmlnYXRlVXJsOz47bDxodHRwOi8vd3d3LnJlY3J1dGUuYXhhLmZyOz4%2BOz47Oz47dDxwPHA8bDxOYXZpZ2F0ZVVybDs%2BO2w8aHR0cDovL3d3dy5heGEuZnIvUGFnZXMvbm9zLWFjdHVhbGl0ZXMuYXNweDs%2BPjs%2BOzs%2BO3Q8cDxwPGw8TmF2aWdhdGVVcmw7PjtsPGh0dHA6Ly93d3cuYXhhLmZyL1BhZ2VzL2luZm9zLXJzcy5hc3B4Oz4%2BOz47Oz47dDxwPHA8bDxOYXZpZ2F0ZVVybDs%2BO2w8aHR0cDovL3d3dy5heGEuZnIvUGFnZXMvcmVjaGVyY2hlLWRpc3RyaWJ1dGV1ci5hc3B4Oz4%2BOz47Oz47dDxwPHA8bDxOYXZpZ2F0ZVVybDs%2BO2w8aHR0cDovL3d3dy5heGEuZnIvUGFnZXMvcGxhbi1kZS1zaXRlLmFzcHg7Pj47Pjs7Pjt0PHA8cDxsPE5hdmlnYXRlVXJsOz47bDxodHRwOi8vd3d3LmF4YS5mci9QYWdlcy9tZW50aW9ucy1sZWdhbGVzLmFzcHg7Pj47Pjs7Pjt0PHA8cDxsPE5hdmlnYXRlVXJsOz47bDxodHRwOi8vd3d3LmF4YS5mci9QYWdlcy9ub3VzLWNvbnRhY3Rlci5hc3B4Oz4%2BOz47Oz47Pj47Pj47dDxwPHA8bDxUZXh0Oz47bDxIb21lIFBhZ2UgU2l0ZSBDbGllbnQtTGFuY2V1ci1FbnRyw6llIGRhbnMgbGUgbW9kdWxlOz4%2BOz47Oz47Pj47Pj47PvPR7IKj9YGepQVydsXS2bL%2F64zD
    //    &_ctl0%3A_vkbMotdepasse%3ACellSelecVirtualKeyboard=16%2C7%2C6%2C22%2C6%2C13%2C
    //    &_ctl0%3A_vkbMotdepasse%3ACounterVirtualKeyboard=6
    //    &_ctl0%3A_tbxIdentifiant=19240191
    
    // New form
    //    <form name="Form1" method="post" action="LoginPage.aspx?ReturnUrl=%2f_layouts%2fAuthenticate.aspx%3fSource%3d%252f&amp;Source=%2f" onsubmit="javascript:return WebForm_OnSubmit();" id="Form1" autocomplete="off">
    //    <input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
    //    <input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
    //    <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJMTc5NDUzNTUxD2QWAgIBD2QWCgIBDw8WAh4QSXNDdXN0b21Qb3N0QmFja2dk.....A9gEEl9wL0=" />
    //    <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEWCALa0qP0...WvyauSb8NWp" />
    //    <input type="hidden" name="ctl03$hfClientJSEnabled" id="ctl03_hfClientJSEnabled" value="False" />
    //    <input name="tbUserName" type="text" id="tbUserName" tabindex="350" class="axa-inputidun" name="num_client" />
    //    <img id="vkMotDePasse_ctl01" usemap="#MapVirtualKeyboardvkMotDePasse_ctl01" src="/_layouts/images/nosi/ClavierVirtuel/clavier512n2sobzuqwf.jpg" style="border-width:0px;" />
    //    <input name="vkMotDePasse$CellSelecVirtualKeyboard" type="hidden" id="vkMotDePasse_CellSelecVirtualKeyboard" />
    //    <input name="vkMotDePasse$CounterVirtualKeyboard" type="hidden" id="vkMotDePasse_CounterVirtualKeyboard" value="0" />
    //    <input name="vkMotDePasse$vkMotDePasse_RequiredCheck" type="text" id="vkMotDePasse_vkMotDePasse_RequiredCheck" style="display:none;" />
    //    <map name="MapVirtualKeyboardvkMotDePasse_ctl01">...
    //    </map>
    //    <input type="submit" name="btnLogOn" value="" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;btnLogOn&quot;, &quot;&quot;, true, &quot;Logon&quot;, &quot;&quot;, false, false))" id="btnLogOn" tabindex="351" class="axa-inputorangevaliderbig" alt="Validez" value=" " />
    //	</form>
    // Grab __VIEWSTATE code
    string viewStateValue = GrabInContent("name=\"__VIEWSTATE\".*?value=\"(.*?)\"");
    string eventValidationValue = GrabInContent("name=\"__EVENTVALIDATION\".*?value=\"(.*?)\"");

    // Submit the form
    PostIntoContent("https://client.axa.fr/LoginPage.aspx?ReturnUrl=%2f_layouts%2fAuthenticate.aspx%3fSource%3d%252f&amp;Source=%2f",
                    "__EVENTTARGET=__Page"
                    "&" "__EVENTARGUMENT="
                    "&" "__VIEWSTATE=" +viewStateValue+
                    "&" "__EVENTVALIDATION=" +eventValidationValue+
                    "&" "ctl03$hfClientJSEnabled=True"
                    "&" "tbUserName=" +login+
                    "&" "vkMotDePasse$CellSelecVirtualKeyboard=" +passcode+
                    "&" "vkMotDePasse$CounterVirtualKeyboard=6"
                    "&" "vkMotDePasse$vkMotDePasse_RequiredCheck=6"
                    );
	progressCallback(50);
   
	// Check for password error
	if (SearchInContent("attention vous disposez de 3 tentatives pour vous identifier")) throw Err(_WHERE, rejectedPasswordErrorWhy);
	progressCallback(60);

    // Go to contracts page
    viewStateValue = GrabInContent("name=\"__VIEWSTATE\".*?value=\"(.*?)\"");
    PostIntoContent("Accueil.aspx?ReturnUrl=%2findex.aspx",
                    "__EVENTTARGET=_menuNavigation:MenuItemRepeater:_ctl0:_ctl3"
                    "&" "__EVENTARGUMENT="
                    "&" "__VIEWSTATE=" +viewStateValue+
                    "&" "_ctl0:_vkbMotdepasse:PassWordDisp=******"
                    "&" "_ctl0:_vkbMotdepasse:CellSelecVirtualKeyboard=" +passcode+
                    "&" "_ctl0:_vkbMotdepasse:CounterVirtualKeyboard=6"
                    "&" "_ctl0:_tbxIdentifiant=" +login
                    );
	progressCallback(75);

	// Grab accounts
	const char *expression = "Type\">(.*?)<.*?Numero\">(.*?)<.*?InvisibleForDetachement\">(.*?)<";
	GrabAccountsInContent(expression, "Euro");
	if (accounts.empty()) throw Err(_WHERE, protocolErrorWhy);	
	progressCallback(100);
}
