/*
 *  BankMonaBanq.cpp
 *  BankReader
 *
 *  Created by Brice Rivé on 3/15/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include "BankMonaBanq.h"
#include "CurlPP.h"
#include "DigitGrid.h"
using std::string;
#include "Utils.h"

// Digits definition
extern char BankMonaBanqDigits1[];
extern int BankMonaBanqDigits1NBytes;

BankMonaBanq::Registrar BankMonaBanq::registrar;

void BankMonaBanq::GrabAccounts(ProgressCallback &progressCallback)
{
	progressCallback(0);
	string login = loginData[LOGIN];
	string password = loginData[PASSWORD];
	
	// Sanity check
	CheckNumeric(login, 11);
	CheckNumeric(password, 6);
	
	curl.DisableCertificates();
	
	// Open login page
	GetUrlIntoContent("https://www.monabanq.com/client/");
	progressCallback(20);
	
	// Grab numeric grid url
	string gridUrl;
	string randomizer;
	try {
		randomizer = GrabInContent("VirtualKeyboard.jpeg\\?puzzleId=test&amp;rnd=(.*?)\"");
		gridUrl = "https://www.monabanq.com/client/VirtualKeyboard.jpeg?puzzleId=test&amp;rnd="+randomizer;
	} catch (Err &err) {
		throw Err(_WHERE, protocolErrorWhy);
	}
	progressCallback(40);
	
	// Load the numeric grid image
	string grid;
	GetUrlIntoString(gridUrl, grid);
	progressCallback(50);
	
	// Find the location of the numbers in the grid
	std::vector<int> layout;
	unsigned int delta;
	try {
		delta = DigitGrid::Process(grid, BankMonaBanqDigits1, BankMonaBanqDigits1NBytes, layout);
	} catch (Err &err) {
		throw Err(_WHERE, protocolErrorWhy);
	}
	if (delta>50000)
		throw Err(_WHERE, protocolErrorWhy);
	
	// Find the clicks for the password
	struct Box {
		int left, top, right, bottom;
	};
	Box boxes[] = {	{0,0,20,19},{22,0,42,19},{44,0,64,19},{66,0,86,19},{88,0,108,19},{0,21,20,40},{22,21,42,40},{44,21,64,40},{66,21,86,40},{88,21,108,40} };
	string gridClicks;
	for (int i=0; i<password.size(); i++) {
		int digit = password[i]-'0';
		int x = layout[digit*4];
		int y = layout[digit*4+1];
		for (int j=0; j<sizeof(boxes)/sizeof(boxes[0]); j++) {
			if (x>boxes[j].left && x<boxes[j].right && y>boxes[j].top && y<boxes[j].bottom) {
				gridClicks+=to_string(j);
			}
		}
	}
	if (gridClicks.size()!=password.size())
		throw Err(_WHERE, protocolErrorWhy);
	progressCallback(60);
	
	// Post the login
	PostIntoContent("https://www.monabanq.com/client/Valider.do",
					"numident="+login 
					+"&codeSecretRndgen="+randomizer
					+"&codeSecretDisplay=******"
					+"&codeSecret="+gridClicks
					+"&puzzleId=test"
					+"&redir=null");
	progressCallback(80);
	
	// Check for password error
	if (SearchInContent("compte ou de client est incorrect"))	throw Err(_WHERE, rejectedPasswordErrorWhy);
	
	// Grab the accounts
	const char *expression = "<td class=\"libelle\"><a.*?>(.*?)n&deg; (.*?)</a>.*?Solde(.*?)&euro;";
	GrabAccountsInContent(expression, "Euro");
	if (accounts.empty()) throw Err(_WHERE, protocolErrorWhy);	
	progressCallback(100);
}

