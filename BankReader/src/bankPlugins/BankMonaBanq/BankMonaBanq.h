/*
 *  BankMonaBanq.h
 *  BankReader
 *
 *  Created by Brice Rivé on 3/15/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once
#include "Bank.h"
extern const unsigned char BankMonaBanqIcon[];

class BankMonaBanq: public Bank {
	
	BankMonaBanq(CurlPP &curl_, const LoginData &loginData_) :Bank(curl_,loginData_) {}
	
	// Bank virtual interface
	virtual void GrabAccounts(ProgressCallback &progressCallback);
	virtual std::string ShortName() const { return StaticShortName(); }
	
	// Bank factory registration
	static void LoginInfo(LoginInfo &loginInfo) {loginInfo.push_back(LOGIN); loginInfo.push_back(PASSWORD);}
	BANK_FACTORY_REGISTRATION(BankMonaBanq, "MonaBanq", 1,0, "https://www.monabanq.com/client/")
};
