/*
 *  BankHedios.cpp
 *  BankReader
 *
 *  Created by Brice Rivé on 3/17/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include "BankHedios.h"
#include "CurlPP.h"
using std::string;

BankHedios::Registrar BankHedios::registrar;

void BankHedios::GrabAccounts(ProgressCallback &progressCallback)
{
	std::string login = loginData[LOGIN];
	std::string password = loginData[PASSWORD];
	
    
    progressCallback(0);
	
	// Login page (not really necessary)
	GetUrlIntoContent("http://www.hedios.com/");
	progressCallback(10);
        
    /*
     <form name="identification" method="POST">
     <input name="login" id="login" type="text" value="Email" class="input_text_hedios" onfocus="if ($('#login').val() == 'Email'){$('#login').val('');$('#login').css('color', '#504A41');}"  onblur="if ($('#login').val() == ''){$('#login').val('Email');$('#login').css('color', '#ACAFB2');}"/>
     
     <input id="password-clear" type="text" value="Mot de passe" class="input_text_hedios" onkeypress="if (event.keyCode == 13) verif_form(document.identification);" style="margin-bottom:0.2em; color:#ACAFB2;" autocomplete="off"/>
     <input id="password" type="password" name="password" value="" class="input_text_hedios" onkeypress="if (event.keyCode == 13) verif_form(document.identification);" style="margin-bottom:0.2em; color:#504A41;" autocomplete="off"/>
     <a href="javascript:LostPassword(document.identification);" class="password_lost">Mot de passe oubli&eacute; ?</a>
     <a href="javascript:verif_form(document.identification);" class="grand_bouton" style="margin-top:14px; width:240px;">Valider</a>
     <!--<input type="submit" name="" value="Valider" onclick="javascript:verif_form(document.identification);" class="grand_bouton" style="margin-top:17px;width:240px;padding-top:2px;"/>	-->								
     </form>
     */
    
	// Post the login
	PostIntoContent("http://www.hedios.com/", "login=" +login+ "&password=" +password);
	progressCallback(30);

    // Load investments page
	GetUrlIntoContent(AbsoluteUrlRelativeToCurrent("mon-compte/mes-souscriptions"));
	progressCallback(50);
    
	// Check for password error
	if (SearchInContent("Votre mot de passe et/ou votre identifiant"))	throw Err(_WHERE, rejectedPasswordErrorWhy);
	progressCallback(60);
	
    // nunmerical format is: 24000.00
//	const char *expression = "id=\"Div1Link\" class=\"texte-10-noir\"><strong>(.*?)</strong>"
//        ".*?class=\"texte-10-noir\">(.*?)</td>"
//        ".*?Valeur : ([0-9]*)";
//    const char *expression = "id=\"Div1Link\" class=\"texte-10-noir\"><strong>(.*?)</strong>"
//    ".*?<nobr>(.*?)</nobr>"
//    ".*?<nobr>([0-9]*)";
    const char *expression = "<div class=\"nom_produit\".*?>(.*?)<.*?(Valorisation.*?):.*?>(.*?) ";
    
	GrabAccountsInContent(expression, "Euro");
	if (accounts.empty()) throw Err(_WHERE, protocolErrorWhy);
    progressCallback(100);
}

