/*
 *  BankLyonnais.h
 *  BankReader
 *
 *  Created by Brice Rivé on 3/30/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once
#include "Bank.h"
extern const unsigned char BankLyonnaisIcon[];

class BankLyonnais: public Bank {
	
	BankLyonnais(CurlPP &curl, const LoginData &loginData):Bank(curl,loginData){}
	
	// Bank virtual interface
	virtual void GrabAccounts(ProgressCallback &progressCallback);
	virtual std::string ShortName() const { return StaticShortName(); }
	
	// Bank factory registration
	static void LoginInfo(LoginInfo &loginInfo) {loginInfo.push_back(LOGIN); loginInfo.push_back(PASSWORD);}
	BANK_FACTORY_REGISTRATION(BankLyonnais, "Lyonnais", 1,0, "https://particuliers.secure.lcl.fr/index.html")
};
