/*
 *  BankLyonnais.cpp
 *  BankReader
 *
 *  Created by Brice Rivé on 3/30/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include "BankLyonnais.h"
#include "CurlPP.h"

using std::string;

BankLyonnais::Registrar BankLyonnais::registrar;

void BankLyonnais::GrabAccounts(ProgressCallback &progressCallback)
{
	progressCallback(0);
	
	// Get entry page
	//GetUrlIntoContent("https://particuliers.secure.lcl.fr/index.html");
	//progressCallback(20);
	
	// Post the login info
	string login = loginData[LOGIN];
	PostIntoContent("https://particuliers.secure.LCL.fr/everest/UWBI/UWBIAccueil?DEST=IDENTIFICATION",
					"agenceId="+login.substr(0,4)+
					"&compteId="+login.substr(4,6)+
					"&CodeId="+loginData[PASSWORD]);
	progressCallback(40);

	// Check for password error
	if (SearchInContent("saisies sont incorrectes"))	throw Err(_WHERE, rejectedPasswordErrorWhy);
	
	// Add-in the cookies set by JavaScript
	//curl.MergeCookies("lclgen=particuliers; aklBlocage=OUI; aklGeneralisation=NON; aklVersionActiveX=1%2C2%2c35");
	
	// Get the summary frame
	//GetUrlIntoContent("https://particuliers.secure.LCL.fr/everest/UWBI/UWBIAccueil?DEST=CO");
	//progressCallback(60);
	GetUrlIntoContent("https://particuliers.secure.LCL.fr/everest/UWSP/UWSPAccueil?DEST=SYNTHESEPAR&FCTEL=SYNTHESECOMPTES");
	progressCallback(80);
	
	GrabAccountsInContent("<tr class=\"tbl.*?width=\"22%\">(.*?)</td>[[:space:]]*<td>(.*?)[[:space:]]*</td>.*?width=\"16%\"><.*?>(.*?) ", "Euro", true);
	if (accounts.empty()) throw Err(_WHERE, protocolErrorWhy);	
	progressCallback(100);
}
