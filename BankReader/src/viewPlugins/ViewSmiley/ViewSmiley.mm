//
//  ViewSmiley.mm
//  BankReader
//
//  Created by Brice Rivé on 12/15/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "ViewSmiley.h"
#include <string>
using std::string;
#include "mylib/idioms.h"
using namespace mylib;
#include "CocoaUtils.h"
#include "Bank.h"
#include "ViewElts.h"

ViewSmiley::Registrar ViewSmiley::registrar;



void ViewSmiley::Draw(const View::Accounts &accounts, bool grabbing, bool alerting)
{	
	if (accounts.empty()) return;

	// Define text attributes
	NSFont *bigFont = [NSFont fontWithName:@"Courier" size:28];
	
	NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
	[style setAlignment: NSRightTextAlignment];
	
	NSDictionary *accountValueAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
		bigFont, NSFontAttributeName, [NSColor blackColor], NSForegroundColorAttributeName, style, NSParagraphStyleAttributeName, (char *)0];
	NSDictionary *accountNegativeValueAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
		bigFont, NSFontAttributeName, [NSColor redColor], NSForegroundColorAttributeName, style, NSParagraphStyleAttributeName, (char *)0];

	if (showTotal) {
		// Draw the total amount
		float totalValue=TotalValue(accounts);
		NSString *totalAmount = StrConv(to_string((int)totalValue));
		[totalAmount drawInRect:NSMakeRect(0, 0, 128, 32) withAttributes:totalValue<0?accountNegativeValueAttributes:accountValueAttributes];
	}
	
	// Draw the smiley
	int happy = grabbing? 0: alerting? -1: 1;
	DrawSmiley(64, 84, 40, happy);
}
