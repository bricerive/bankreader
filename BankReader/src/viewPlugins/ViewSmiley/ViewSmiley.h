//
//  ViewSmiley.h
//  BankReader
//
//  Created by Brice Rivé on 12/15/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#pragma once
#include "view.h"
#include <string>

class ViewSmiley: public View {
	
public:
	virtual void Draw(const View::Accounts &accounts, bool grabbing, bool alerting);
	
private:
	REGISTER_VIEW(Smiley, 1,0, ViewSmiley);
};

