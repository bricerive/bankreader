//
//  ViewBar.h
//  BankReader
//
//  Created by Brice Rivé on 12/15/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#include "view.h"

class ViewBar: public View {
	
public:
	virtual void Draw(const View::Accounts &accounts, bool grabbing, bool alerting);
	
private:
	REGISTER_VIEW(Bar, 1,0, ViewBar);
};
