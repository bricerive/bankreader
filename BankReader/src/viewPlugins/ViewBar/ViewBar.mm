//
//  ViewBar.mm
//  BankReader
//
//  Created by Brice Rivé on 12/15/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//
#include "viewBar.h"
#include "CocoaUtils.h"
#include "mylib/idioms.h"
#include "Bank.h"
#include "ViewElts.h"
using namespace mylib;

ViewBar::Registrar ViewBar::registrar;

void ViewBar::Draw(const View::Accounts &accounts, bool grabbing, bool alerting)
{
	// Layout
	const int rowSize=32;
	const int accountNameRow=1*rowSize;
	const int barRow=3*rowSize;
	const int accountAmountRow=0*rowSize;
	const int globalInfoRow=2*rowSize;

	if (accounts.empty()) return;

	// Draw the bar
	float totalValue=TotalValue(accounts);
	float totalForeign=0;
	float totalAvailable=0;
	for (View::Accounts::const_iterator i=accounts.begin(); i!=accounts.end(); ++i) {
		if (i->Currency()!="Euro") totalForeign += i->Value();
			//if (i->available) totalAvailable += i->val;
	}
	float v0 = (totalValue-totalForeign-totalAvailable) / totalValue;
	float v1 = totalForeign / totalValue;	
	DrawBar(v0, v1, barRow);
	
	NSFont *font = [NSFont fontWithName:@"Courier" size:14];
	NSFont *bigFont = [NSFont fontWithName:@"Courier" size:28];
	NSColor *paleBlue = [NSColor colorWithCalibratedRed:.7 green:.7 blue:1 alpha:1];
	NSMutableParagraphStyle *style = [[[NSMutableParagraphStyle alloc] init] autorelease];
	[style setAlignment: NSRightTextAlignment];
	NSDictionary *accountNameAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
		font, NSFontAttributeName, paleBlue, NSForegroundColorAttributeName, (char *)0];
	NSDictionary *accountValueAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
		bigFont, NSFontAttributeName, [NSColor blackColor], NSForegroundColorAttributeName, style, NSParagraphStyleAttributeName, (char *)0];
	NSDictionary *accountNegativeValueAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
		bigFont, NSFontAttributeName, [NSColor redColor], NSForegroundColorAttributeName, style, NSParagraphStyleAttributeName, (char *)0];
		
	// Draw the details
	if (showDetails && !accounts.empty()) {
		
		// Select current account to view
		if (tickerIdx >= accounts.size()) tickerIdx=0;
		const ViewAccount &account = accounts[tickerIdx];
		
		// Draw the account logo+name
		DrawLogo(NSMakeRect(0,accountNameRow,32,32), account.BankName());
		NSString *accountId = StrConv(account.Name());
		[accountId drawInRect:NSMakeRect(32, accountNameRow, 128-32, 32) withAttributes:accountNameAttributes];
		
		// Draw the amount
		NSString *amount = StrConv(to_string((int)(account.Value())));
		bool negative = account.Value()<0;
		[amount drawInRect:NSMakeRect(0, accountAmountRow, 128, 32) withAttributes:negative?accountNegativeValueAttributes:accountValueAttributes];
		
	}
	
	if (showTotal && !accounts.empty()) {
		// Draw the total amount
		float totalValue=TotalValue(accounts);
		NSString *totalAmount = StrConv(to_string((int)totalValue));
		[totalAmount drawInRect:NSMakeRect(0, globalInfoRow, 128, 32) withAttributes:totalValue<0?accountNegativeValueAttributes:accountValueAttributes];
	}

	blinkOn = !blinkOn;
	DrawBlinker(alerting, grabbing, blinkOn);
}
