//
//  ViewType.mm
//  BankReader
//
//  Created by Brice Rivé on 12/15/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//
#include "ViewType.h"
#include "CocoaUtils.h"
#include "mylib/idioms.h"
#include "Bank.h"
#include "ViewElts.h"
using namespace mylib;

ViewType::Registrar ViewType::registrar;

void ViewType::Draw(const View::Accounts &accounts, bool grabbing, bool alerting)
{
	// Layout
	const int rowSize=32;
	const int accountNameRow=3*rowSize;
	const int accountHistoryRow=2*rowSize;
	const int accountAmountRow=1*rowSize;
	const int globalInfoRow=0*rowSize;

	NSFont *font = [NSFont fontWithName:@"Courier" size:14];
	NSFont *bigFont = [NSFont fontWithName:@"Courier" size:28];
	NSColor *paleBlue = [NSColor colorWithCalibratedRed:.7 green:.7 blue:1 alpha:1];
	NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
	[style setAlignment: NSRightTextAlignment];
	NSDictionary *accountNameAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
		font, NSFontAttributeName, paleBlue, NSForegroundColorAttributeName, (char *)0];
	NSDictionary *accountValueAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
		bigFont, NSFontAttributeName, [NSColor blackColor], NSForegroundColorAttributeName, style, NSParagraphStyleAttributeName, (char *)0];
	NSDictionary *accountNegativeValueAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
		bigFont, NSFontAttributeName, [NSColor redColor], NSForegroundColorAttributeName, style, NSParagraphStyleAttributeName, (char *)0];
	
	// Draw the details
	if (showDetails && !accounts.empty()) {
			
			// Select current account to view
			if (tickerIdx >= accounts.size()) tickerIdx=0;
			const ViewAccount &account = accounts[tickerIdx];
			
			// Draw the account logo+name
			DrawLogo(NSMakeRect(0.0,accountNameRow,32.0,32.0), account.BankName());
			NSString *accountId = StrConv(account.Name());
			[accountId drawInRect:NSMakeRect(32, accountNameRow, 128-32, 32) withAttributes:accountNameAttributes];
			
			// Draw the history
			View::Accounts singleAccount;
			singleAccount.push_back(account);
			std::vector<std::pair<float, float> > histo;
			ComputeHistogram(singleAccount, histo);
			DrawHistogram(histo, accountHistoryRow);
			
			// Draw the amount
			NSString *amount = StrConv(to_string((int)(account.Value())));
			bool negative = account.Value()<0;
			[amount drawInRect:NSMakeRect(0, accountAmountRow, 128, 32) withAttributes:negative?accountNegativeValueAttributes:accountValueAttributes];

	}
	
	if (showTotal && !accounts.empty()) {
		// Draw the total amount
		float totalValue=TotalValue(accounts);
		NSString *totalAmount = StrConv(to_string((int)totalValue));
		[totalAmount drawInRect:NSMakeRect(0, globalInfoRow, 128, 32) withAttributes:totalValue<0?accountNegativeValueAttributes:accountValueAttributes];
	}
	
	blinkOn = !blinkOn;
	DrawBlinker(alerting, grabbing, blinkOn);
}
