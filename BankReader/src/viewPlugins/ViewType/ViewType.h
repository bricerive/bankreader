//
//  ViewType.h
//  BankReader
//
//  Created by Brice Rivé on 12/15/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#include "view.h"

class ViewType: public View {
	
public:
	virtual void Draw(const View::Accounts &accounts, bool grabbing, bool alerting);
	
private:
	REGISTER_VIEW(Types, 1,0, ViewType);
};
