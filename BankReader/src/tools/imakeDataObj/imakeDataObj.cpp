//============================================================================
// Name        : imakeDataObj.cpp
// Author      : Brice Rivé from original idea by Ted Merrill
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

/*
 *
 * original by Ted Merrill modified by Brice Rivé
 *
 * Brief Description: Make .o type object files containing the data
 *              from a given binary file, suitable for linking into a program.
 *
 * Command Line Synopsis:
 *      For use to create the prototype .c file:
 *              imakeDataObj -c <binary-file-name> <rootname>
 *          (std output must be redirected to appropriate file)
 *      For use to patch an object file:
 *              imakeDataObj -p <binary-file-name> <objfile>
 *          (patches the named object file in place)
 *      Where:
 *              The contents of the file named <binary-file-name>
 *              is to be encapsulated in its entirety into the resulting
 *              object.
 *              The data is found in the global variable:
 *                      extern char rootname[];
 *              and the size of data in bytes is found in:
 *                      extern int rootnameNBytes;
 *              ... where rootname is replaced by desired name.
 *              The output from -c is a C language file to be compiled;
 *              this does NOT contain binary data but instead contains
 *              a special magic pattern that the -p option will search for.
 *              The -p option expects to find the compiled form of the .c
 *              file from the -c stage; it searches for a special magic
 *              pattern and replaces it with the contents of binary file.
 *
 * Long Description:
 *              There are several straight-forward way of encapsulating data
 *              into a program, as follows; none of them are adequate:
 *              1. Produce a .c file with an initialized array. The problem
 *              is that this takes a very long time, and the compiler is
 *              likely to choke on such a big input.
 *              Otherwise, this would be the preferred method.
 *              2. Produce an assembler file; faster and more reliable
 *              than the .c file approach, but still takes considerable time
 *              and requires non-portable knowledge about the assembler.
 *              3. Produce a .o file directly; the fastest method, but
 *              requires non-portable (and sometimes difficult to obtain)
 *              knowledge about the object format.
 *
 *              So instead of using a straight-forward method, we instead
 *              create a dummy .c file with an array big enough for the
 *              data but initialized just with some magic numbers;
 *              compile this file; and then search the object for the
 *              magic numbers and replace this with the desired data.
 *              We generate the compiler instructions (and in fact
 *              figure out what the object files are called) with imakeObj,
 *              and the only real non-portable information we need is the
 *              object extensions used for different target types.
 *              This scheme should work so long as the compiler
 *              normally produces unencoded data, stores zeroes for
 *              uninitialized elements of initialized array,
 *              and in the absence of any checksumming of the object file.
 *              The use of imakeObj is kludgey and hard for someone like
 *              you to understand perhaps, but is imakeObj is unlikely
 *              to undergo an incompatible change, and it is unlikely
 *              that this program would be available yet imakeObj be not...
 *              well, actually we've had problems since i wrote the above,
 *              particularly the lucid compiler chokes on huge arrays.
 *              As a result, we've put in special code for unix case (-tu).
 */


#include <iostream>
#include <fcntl.h>
#include <sys/stat.h>
#include "mylib/error.h"
using namespace std;
using namespace mylib;

static void Usage(char **argv)
{
	static const char *helpStr=
			"##### %s: encapsulate binary (raw) data into linkable object files.\n"
			"Usage: %s -<function> <args>\n"
			"Functions and associated args:\n"
			"    -h for this message\n"
			"    -c <binary-filename> <global-rootname>\n"
			"          This produces the c code (to stdout), to be compiled.\n"
			"    -p <binary-filename> <object-filename>\n"
			"          This patches the compiled object file with the data.\n";
	printf(helpStr, argv[0], argv[0]);
}

// Magic string -- used to identify portion of object file needing patching
static const char *magicStr = "imakeDataObj -p patches binary data here";

static unsigned long FileSize(const char *pathName)
{
	struct stat statBuf;
	ASSERT_THROW(stat(pathName, &statBuf)>=0)("imakeDataObj: no such file %s\n", pathName);
	ASSERT_THROW(statBuf.st_size>=0);
	return statBuf.st_size;
}

// MakeC -- prints to std output a c file that gives the data array
//       that we want, minus the data, which gets patched in later.
static void MakeC(char *binFileName, char *globalName)
{
	unsigned long binSize = FileSize( binFileName );

	// Make sure we declare enough memory for our magic string.
	int globalSize = max(binSize, strlen(magicStr)) + 1;

	const char *cppStr =
			"// THIS IS NOT SOURCE CODE (produced by imakeDataObj -c)\n"
			"// This file must first be compiled, for example with:\n"
			"//    gcc -arch x86_64 -c -x c++ -O0 <cppFile> -o <oFile>\n"
			"// and then patched using imakeDataObj -p with the correct binary data.\n"
			"// The string that initializes the char array is there to\n"
			"// help find the data in the object file.\n"
			"char %s[%d] = \"%s\";\n"
			"// The following external gives number of bytes stored (may be zero)\n"
			"int %sNBytes = %ld;\n";
	printf(cppStr, globalName, globalSize, magicStr, globalName, binSize);
}

// MakeO -- patches the object file
static void MakeO(char *binFileName, char *oFileName)
{
	unsigned long binSize = FileSize(binFileName);

	unsigned long oFileSize = FileSize(oFileName);

	// Read in the object file
	int fileId = open(oFileName, O_RDONLY);
	ASSERT_THROW(fileId>=0)("imakeDataObj -p : can not open object file %s for reading.\n",oFileName);
	char *buffer = (char *)malloc(oFileSize);
	ASSERT_THROW(buffer)("imakeDataObj -p : malloc failure, %d byte\n", oFileSize);
	ASSERT_THROW(read(fileId, buffer, oFileSize)==oFileSize)("imakeDataObj -p : failed to read file %s\n", oFileName);
	close(fileId);

	/* Try to find the magic string. Search through the file
	 *   until we find it. For safety sake, don't search too far
	 *   such that the magic string would fall off the end.
	 *   Pragmatically, we don't need to search the last BinSize bytes
	 *   since we should have found the magic string at the begin of
	 *   the buffer which should be at least that size.
	 */
	int globalSize = max(binSize, strlen(magicStr)) + 1;
	int magicOffset = oFileSize - globalSize;
	ASSERT_THROW(magicOffset>=0)("imakeDataObj -p : object file too small.\n");
	int offset;
	for ( offset = 0; offset < magicOffset; offset++ )
	{
		if ( ! strcmp( buffer+offset, magicStr ) ) goto Found;
	}
	fprintf(stderr, "imakeDataObj -p : object %s file doesn't contain magic string.\n", oFileName );
	exit(1);

	Found:      /* here when we find the offset to data */

	// Read in the binary data into the object file
	fileId = open(binFileName, O_RDONLY);
	ASSERT_THROW(fileId>=0)("imakeDataObj -p : can't open binary file %s for reading.\n", binFileName);
	// Overwrite the buffer part of the object file copy in memory
	ASSERT_THROW(read(fileId, buffer+offset, binSize)==binSize)("imakeDataObj -p : read error on binary file %s.\n", binFileName );
	close(fileId);

	fileId = open( oFileName, O_WRONLY );
	ASSERT_THROW(fileId>=0)("imakeDataObj -p : can't open object file %s for rewriting.\n", oFileName );
	ASSERT_THROW(write( fileId, buffer, oFileSize)==oFileSize)("imakeDataObj -p : write error on object file %s.\n", oFileName );
	close(fileId);
	free(buffer);
}

int main(int argc, char **argv)
{
	try {
		ASSERT_THROW(argv[1]);
		if (!strcmp(argv[1], "-h")) {
			Usage(argv);
			exit(0);
		}
		ASSERT_THROW(argv[1][0]=='-' && argv[1][1] && argv[2] && argv[3]);

		switch (argv[1][1]) {
		case 'c':
			MakeC(argv[2], argv[3]);
			break;
		case 'p':
			MakeO(argv[2], argv[3]);
			break;
		default:
			ERR_THROW("Unexpected command %c", argv[1][1]);
		}
	} catch (Err &err) {
		printf("##### %s exception: %s\n", argv[0], err.what());
		Usage(argv);
		exit(1);
	}
	exit(0);
}
