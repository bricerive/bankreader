/*
 *  main.cpp
 *  DigitsProcessor
 *
 *  Created by Brice Rivé on 10/14/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */
#include <iostream>
#include <string>
using namespace std;

/*
 *  DigitsProcessor.cpp
 *  MoneyFusionLight
 *
 *  Created by Brice Rivé on 10/9/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#include <iterator>
#include <fstream>
#include <algorithm>
#include "mylib/error.h"
#include "lodepng.h"

using namespace mylib;

void ProcessDigits(const std::string &format, const std::string &outputFilename)
{
	std::ofstream output(outputFilename.c_str(), std::ios::out|std::ios::binary);
	// Look for the ten digits in the grid
	for (int digit=0; digit<10; digit++) {

		string digitFileName = format + (char)('0'+digit) + ".png";
		unsigned char *out;
		unsigned w,h;
		ASSERT_THROW(!lodepng_decode_file(&out, &w, &h, digitFileName.c_str(), LCT_RGB, 8));
		output.write((char *)&w, sizeof(w));
		output.write((char *)&h, sizeof(h));
		std::copy(out, out+w*h, std::ostream_iterator<unsigned char>(output));
		free(out);
	}
}

int main(int argc, char *argv[])
{
	if (argc!=3) {
		cout << "digitsProcessor: Generate dataObj file for a set of digit images" << endl;
		cout << "Usage: digitsProcessor <digit images filename pattern> <outputFile prefix>" << endl;
		exit(1);
	}

	// Generate the bin file
	string binFile;
	binFile = string(argv[2]) + ".bin";
	cout << "Generating binary file " << binFile << " from digits in " << argv[1] << endl;
	ProcessDigits(argv[1], binFile);

	// generate the cpp file
	string cppFile;
	cppFile = string(argv[2]) + ".cpp";
	string command;
	command = string("imakeDataObj -c ") + binFile + " " + argv[2] + ">" + cppFile;
	cout << "Generating cpp file " << cppFile << " from binary file " << binFile << "(" << command << ")" <<endl;
	system(command.c_str());

	// generate the .o file
	string oFile(argv[2]);
	oFile+=string(".o");
	command = string("gcc -arch x86_64 -c -x c++ -O0 " + cppFile + " -o " + oFile);
	cout << "Generating .o file " << oFile << " from cpp file " << oFile << "(" << command << ")" <<endl;
	system(command.c_str());

	// patch the .o file with the data
	command = string("imakeDataObj -p ") + binFile + " " + oFile;
	cout << "Patching .o file " << oFile << " width binary file " << binFile << "(" << command << ")" <<endl;
	system(command.c_str());
}
